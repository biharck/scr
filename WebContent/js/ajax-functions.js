function ajaxTeste(parametro1, parametro2){
	loadAjaxLoading();

	$.getJSON('/SCR/adm/crud/Papel?ACAO=exemploAjax',{'id':parametro1,'nome':parametro2},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){	
	  			dialog('Falha de Processamento',data.msg);
	  		}else{
	  			resposta = data.nome +"  -  " + data.endereco +"  -  " + data.id;
				$('#div-ajax').html(resposta);
			}
		});
}


function ajaxBuscaPorteTransportador(parametro1){
	loadAjaxLoading();

	$.getJSON('/SCR/publico/process/Transportador?ACAO=ajaxProcuraPorteTransportador',{'idTipoTransportador':parametro1},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){	
	  			dialog('Falha de Processamento',data.msg);
	  		}else{
	  			$("#campos").fadeIn();
	  			if(data.porte){
	  				
					$("#selecTipoTransportador").html("Transportador de Grande Volume (mais de 1 m�)");
					$("label[for='nomeFantasia']").html("Nome Fantasia<br>");
					$("label[for='cpfCnpjTransient']").html("CPF/CNPJ<br>");
					$('#oculta1').show();
					$('#oculta2').show();
					$('#oculta3').show();
					$('#oculta4').show();
					$('#oculta5').hide();
				}else{
					$("#selecTipoTransportador").html("Transportador de Pequeno Volume (menos de 1 m�)");
					$("label[for='nomeFantasia']").html("Nome<br>");
					$("label[for='cpfCnpjTransient']").html("CPF<br>");
					$('#oculta1').hide();
					$('#oculta2').hide();
					$('#oculta3').hide();
					$('#oculta4').hide();
					$('#oculta5').show();
				}
	  		}
		});
}

function ajaxBuscaGrandeGerador(parametro1){
	loadAjaxLoading();

	$.getJSON('/SCR/adm/crud/GrandeGerador?ACAO=ajaxBuscaGrandeGerador',{'idGrandeGerador':parametro1},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){	
	  			dialog('Falha de Processamento  ao Buscar Grande Gerador',data.msg);
	  		}else{
		  		/*TESTE
		  			var dados = '{"nome": "Rodrigo","sobrenome": "Aramburu","idade":"25","site":"http://www.botecodigital.info"}';
					var objTeste = jQuery.parseJSON(dados);
				*/
					
				//recebe objeto grandeGerador
				var obj = jQuery.parseJSON(data.obj);
	  			//carrega campos
				$("#gg_nomeFantasia").val(obj.nomeFantasia);
				$("#gg_razaoSocial").val(obj.razaoSocial);
				$("#gg_cpfCnpjTransient").val(obj.cpfCnpjTransient);
				$("#gg_cep").val(obj.cep);
				$("#gg_endereco").val(obj.endereco);
				$("#gg_numero").val(obj.numero);
				$("#gg_complemento").val(obj.complemento);
				$("#gg_bairro").val(obj.bairro);
				if(obj.uf != null)
				    $("#gg_uf").val(obj.uf.nome);
				if(obj.municipio != null)
				    $("#gg_municipio").val(obj.municipio.nome);
				if(obj.telefone1 != null)	
					$("#gg_telefone1").val(obj.telefone1.value);
				else
					$("#gg_telefone1").val("");
				if(obj.telefone2 != null)
					$("#gg_telefone2").val(obj.telefone2.value);
				else
					$("#gg_telefone2").val("");
				if(obj.celular != null)
					$("#gg_celular").val(obj.celular.value);
				else
					$("#gg_celular").val("");
				$("#gg_email").val(obj.email);
				$("#gg_responsavel").val(obj.responsavel);
				
				$("#campos_gg").fadeIn();
			}
		});
}

function ajaxBuscaTransportador(parametro1){
	loadAjaxLoading();

	$.getJSON('/SCR/adm/crud/Transportador?ACAO=ajaxBuscaTransportador',{'idTransportador':parametro1},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){	
	  			dialog('Falha de Processamento  ao Buscar Transportador',data.msg);
	  		}else{
				$("#campos_transp").fadeIn();
				//recebe objeto Transportador
				var obj = jQuery.parseJSON(data.obj);
	  			//carrega campos
				$("#transp_nomeFantasia").val(obj.nomeFantasia);
				$("#transp_razaoSocial").val(obj.razaoSocial);
				$("#transp_cpfCnpjTransient").val(obj.cpfCnpjTransient);
				$("#transp_cep").val(obj.cep);
				$("#transp_endereco").val(obj.endereco);
				$("#transp_numero").val(obj.numero);
				$("#transp_complemento").val(obj.complemento);
				$("#transp_bairro").val(obj.bairro);
				if(obj.uf != null)
					$("#transp_uf").val(obj.uf.nome);
				if(obj.municipio != null)
					$("#transp_municipio").val(obj.municipio.nome);
				if(obj.telefone1 != null)	
					$("#transp_telefone1").val(obj.telefone1.value);
				else
					$("#transp_telefone1").val("");
				if(obj.telefone2 != null)
					$("#transp_telefone2").val(obj.telefone2.value);
				else
					$("#transp_telefone2").val("");
				if(obj.celular != null)
					$("#transp_celular").val(obj.celular.value);
				else
					$("#transp_celular").val("");
				$("#transp_email").val(obj.email);
				$("#transp_responsavel").val(obj.responsavel);
			
			}
		});
}

function ajaxBuscaDestinoFinal(parametro1){
	loadAjaxLoading();

	$.getJSON('/SCR/adm/crud/DestinoFinal?ACAO=ajaxBuscaDestinoFinal',{'idDestinoFinal':parametro1},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){	
	  			dialog('Falha de Processamento ao Buscar Destino Final',data.msg);
	  		}else{
	  			$("#campos_dest").fadeIn();
				//recebe objeto DestinoFinal
				var obj = jQuery.parseJSON(data.obj);
	  			//carrega campos
				$("#dest_nomeFantasia").val(obj.nomeFantasia);
				$("#dest_razaoSocial").val(obj.razaoSocial);
				$("#dest_cpfCnpjTransient").val(obj.cpfCnpjTransient);
				$("#dest_cep").val(obj.cep);
				$("#dest_endereco").val(obj.endereco);
				$("#dest_numero").val(obj.numero);
				$("#dest_complemento").val(obj.complemento);
				$("#dest_bairro").val(obj.bairro);
				if(obj.uf != null)
				    $("#dest_uf").val(obj.uf.nome);
				if(obj.municipio != null)
				    $("#dest_municipio").val(obj.municipio.nome);
				$("#dest_inscricaoMunicipal").val(obj.inscricaoMunicipal);
				$("#dest_incricaoEstadual").val(obj.inscricaoEstadual);
				if(obj.telefone1 != null)	
					$("#dest_telefone1").val(obj.telefone1.value);
				else
					$("#dest_telefone1").val("");
				if(obj.telefone2 != null)
					$("#dest_telefone2").val(obj.telefone2.value);
				else
					$("#dest_telefone2").val("");
				if(obj.celular != null)
					$("#dest_celular").val(obj.celular.value);
				else
					$("#dest_celular").val("");
				$("#dest_email").val(obj.email);
				$("#dest_responsavel").val(obj.responsavel);
			}
		});
}

function ajaxBuscaCredenciaisPEV(parametro1){
	loadAjaxLoading();

	$.getJSON('/SCR/adm/crud/PontoEntrega?ACAO=ajaxBuscaCredenciaisPEV',{'idCredencial':parametro1},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){	
	  			dialog('Falha de Processamento ao Buscar Credencias PEV',data.msg);
	  		}else{
	  			$("#campos_cred").fadeIn();
				//recebe objeto CredenciaisPEV
				var obj = jQuery.parseJSON(data.obj);
	  			//carrega campos
				$("#cred_endereco").val(obj.endereco);
				$("#cred_numero").val(obj.numero);
				if(obj.uf != null)
				    $("#cred_uf").val(obj.uf.nome);
				if(obj.municipio != null)
				    $("#cred_municipio").val(obj.municipio.nome);
			}
		});
}



function ajaxBuscaCep(parametro1){
	
	dialog('Aten��o','Aguarde, estamos buscando o endere�o...');
	
	loadAjaxLoading();

	$.getJSON('/SCR/publico/process/BuscaCep?ACAO=ajaxBuscaCep',{'cep':parametro1},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){	
	  			dialog('Falha de Processamento',data.msg);
	  		}else{
				//recebe objeto CredenciaisPEV
				var obj = jQuery.parseJSON(data.obj);
	  			//carrega campos
				$("#endereco").val(obj.logradouro);
				$("#bairro").val(obj.bairro);
				$("#numero").focus();
				
				if(obj.idUf != null && obj.idMunicipio != null){
					$('#uf option[value="br.com.biharckgroup.scr.bean.Uf[id='+obj.idUf+']"]').prop('selected', true);
					$('#uf option[value="br.com.biharckgroup.scr.bean.Uf[id='+obj.idUf+']"]').change();
					//ao terminar ajax que carrega municipios atraves do uf 
					//metodo function selecionaComboUfManual chamado no final de addItensToCombo, arq ajax.js, seleciona o combo com o valor do campo valorIdMunAjax
					$("#div-auxiliar").html('<input type="hidden" name="valorIdMunAjax" id="valorIdMunAjax"/>');
					valorIdMunAjax=obj.idMunicipio;
					$("#div-auxiliar").html('<input type="hidden" name="valorIdMunAjax" id="valorIdMunAjax"/>');
					$("input[name='valorIdMunAjax']").val(valorIdMunAjax);
				}
				
				closeDialog();
			}
		});
}

function selecionaComboUfManual(){
	valorIdMunAjax = $("input[name='valorIdMunAjax']").val();
	if(!(valorIdMunAjax == "" || valorIdMunAjax == null || valorIdMunAjax == '<null>')){
  		$('#municipio option[value="br.com.biharckgroup.scr.bean.Municipio[id='+valorIdMunAjax+']"]').prop('selected', true);
	}
}

function buscaTipoResiduo(grupo,index){
	loadAjaxLoading();
	$.getJSON('/SCR/adm/crud/EntradaResiduoPEV?ACAO=buscaTipoResiduo',{'grupo':grupo,'index':index},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){
	  			openDialogInformation('Ops! Ocorreu um erro ao buscar os Tipos de Res�duos. Tente novamente por favor!');
	  		}else{
	  			for(var i=0;i<data.length;i++){
	  		        var obj = data[i];
	  		        for(var key in obj){
	  		            eval(obj[key]);
	  		        }
	  		    }
			}
		});	
}

function buscaTipoResiduoRetirada(grupo,index){
	loadAjaxLoading();
	$.getJSON('/SCR/adm/crud/RetiradaResiduoPEV?ACAO=buscaTipoResiduo',{'grupo':grupo,'index':index},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){
	  			openDialogInformation('Ops! Ocorreu um erro ao buscar os Tipos de Res�duos. Tente novamente por favor!');
	  		}else{
	  			for(var i=0;i<data.length;i++){
	  		        var obj = data[i];
	  		        for(var key in obj){
	  		            eval(obj[key]);
	  		        }
	  		    }
			}
		});	
}


function buscaTipoResiduoProjeto(grupo,index){
	loadAjaxLoading();
	$.getJSON('/SCR/grandeGerador/crud/Projeto?ACAO=buscaTipoResiduo',{'grupo':grupo,'index':index},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){
	  			openDialogInformation('Ops! Ocorreu um erro ao buscar os Tipos de Res�duos. Tente novamente por favor!');
	  		}else{
	  			for(var i=0;i<data.length;i++){
	  		        var obj = data[i];
	  		        for(var key in obj){
	  		            eval(obj[key]);
	  		        }
	  		    }
			}
		});	
}

function ajaxBuscaCapacidadeDestinoFinal(parametro1){
	loadAjaxLoading();

	$.getJSON('/SCR/adm/crud/DestinoFinal?ACAO=ajaxBuscaDestinoFinal',{'idDestinoFinal':parametro1},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){	
	  			dialog('Falha de Processamento ao Buscar Destino Final',data.msg);
	  		}else{
	  			$("#campos_dest").fadeIn();
				//recebe objeto DestinoFinal
				var obj = jQuery.parseJSON(data.obj);
	  			//carrega campos
				$("#capacidadeTotal").val(obj.capacidadeTotal);
				$("#capacidadeAtual").val(obj.capacidadeAtual);
			}
		});
}

function ajaxBuscaProjeto(parametro1){
	loadAjaxLoading();

	$.getJSON('/SCR/grandeGerador/crud/Projeto?ACAO=ajaxBuscaProjeto',{'numeroProjeto':parametro1},
	  	function(data){
	  		endAjaxLoading();
	  		if(data.erro){	
	  			dialog('Falha de Processamento ao Buscar Projeto',data.msg);
	  		}else{
				//recebe objeto projeto
				var obj = jQuery.parseJSON(data.obj);
	  			//carrega campos do endere�o da obra
				$("input[name='cepObra']").val(obj.cepObra);
				$("input[name='cepObra']").attr("readonly","readonly");
				
				$("input[name='enderecoObra']").val(obj.enderecoObra);
				$("input[name='enderecoObra']").attr("readonly","readonly");
				
				$("input[name='numeroObra']").val(obj.numeroObra);
				$("input[name='numeroObra']").attr("readonly","readonly");
				
				$("input[name='complementoObra']").val(obj.complementoObra);
				$("input[name='complementoObra']").attr("readonly","readonly");
				
				$("input[name='bairroObra']").val(obj.bairroObra);
				$("input[name='bairroObra']").attr("readonly","readonly");
				
				if(obj.ufObra != null && obj.municipioObra != null){
					$('select[name="ufObra"] option[value="br.com.biharckgroup.scr.bean.Uf[id='+obj.ufObra.id+']"]').prop('selected', true);
					$('select[name="ufObra"]').attr("disabled",true);
					$('select[name="ufObra"] option[value="br.com.biharckgroup.scr.bean.Uf[id='+obj.ufObra.id+']"]').change();
					$('select[name="municipioObra"]').attr("disabled",true);
					valorIdMunAjax=obj.municipioObra.id;
					//ao terminar ajax que carrega municipios atraves do uf 
					//metodo function selecionaComboUfManual chamado no final de addItensToCombo, arq ajax.js, seleciona o combo com o valor do campo valorIdMunAjax
					$("#div-auxiliar").html('<input type="hidden" name="valorIdMunAjax" id="valorIdMunAjax"/>');
					$("input[name='valorIdMunAjax']").val(valorIdMunAjax);
				}
				//define gg
				if(obj.grandeGerador != null){
					$("#enumIdentificaGerador").val('SIM');
					$("#enumIdentificaGerador").attr("disabled",true);
					$("#enumIdentificaGerador").change();
					//preenche selectOnPath
					$('input[name="grandeGerador"]').val('br.com.biharckgroup.scr.bean.GrandeGerador[id='+obj.grandeGerador.id+', nomeFantasia='+obj.grandeGerador.razaoSocial+']"]');
					$("input[name='grandeGerador_label']").val(obj.grandeGerador.razaoSocial);
					$("input[name='grandeGerador_label']").change();
					$("button[name='grandeGerador_btn']").hide();
					$("button[name='grandeGerador_btnUnselect']").hide();
				}
			}
		});
}


