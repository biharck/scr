/*
	Fun��o que cria uma m�scara para cpf e cpnj no mesmo campo
*/
function cpfcnpj(campo){
        if(campo.value.length == 3){
                campo.value = campo.value + '.';
                return false;
        }
        if(campo.value.length == 7){
                campo.value = campo.value + '.';
                return false;
        }
        if(campo.value.length == 11){
                campo.value = campo.value + '-';
                return false;
        }
        if(campo.value.length == 15){
                p0=campo.value.charAt(0);
                p1=campo.value.charAt(1);
                p2=campo.value.charAt(2);
                p3=campo.value.charAt(4);
                p4=campo.value.charAt(5);
                p5=campo.value.charAt(6);
                p6=campo.value.charAt(8);
                p7=campo.value.charAt(9);
                p8=campo.value.charAt(10);
                p9=campo.value.charAt(12);
                p10=campo.value.charAt(13);
                p11=campo.value.charAt(14);
                campo.value = '';
                campo.value = p0 + p1 + '.' + p2 + p3 + p4 + '.' + p5 + p6 + p7 + '/' + p8 + p9 + p10 + p11 + '-';
                p0='';
                p1='';
                p2='';
                p3='';
                p4='';
                p5='';
                p6='';
                p7='';
                p8='';
                p9='';
                p10='';
                p11='';
                return false;
        }
}

/*
	M�todo que cria uma m�scara que � passada como par�metro
*/
function formatar_mascara(src, mascara) {
	var campo = src.value.length;
	var saida = mascara.substring(0,1);
	var texto = mascara.substring(campo);
	if(texto.substring(0,1) != saida) {
		src.value += texto.substring(0,1);
	}
}

function equals(p1,p2){
	if(p1 == p2) return true;
	else return false
		
}

function validaIgualdadeCampo(p1,p2,nomeCampo){
	if(!equals($(p1).val(),$(p2).val())){
		dialog('Aten��o','A redigita��o do campo '+nomeCampo+' n�o confere!');
		$(p2).focus();
		return false;
	}
	return true;
}

function validaTamMinimoCampo(p1,nomeCampo,tam){
	if($(p1).val().length < tam){
		dialog('Aten��o','O campo '+nomeCampo+' deve ter tamanho maior que '+tam+'!');
		$(p1).focus();
		return false;
	}
	return true;
}

function validaSenhaForte(senha) {
	//valida se senha possui letra, numero e caratcter especial
	val = $(senha).val();              
    if((val.search(/[a-z]/) != -1 || val.search(/[A-Z]/) != -1 ) && 
	   (val.search(/[0-9]/) != -1)							   &&
  	   (val.search(/[@!#$%&*+=?|-]/) !=- 1 || val.search(/\./) != -1)
    ) {
		return true;
	}else{
		dialog('Aten��o','O campo senha deve conter letra, n�mero e caracter especial!');
		$(senha).focus();
		return false;
	}
		
} 


//m�todo chamado quando se clica no bot�o "limpar" na fun��o selectOnePath de guia, guiadestinoFinal e guiaTransportador .jsp
function limpaCamposGuia(){
	//habilita e limpa campos
	$("input[name='cepObra']").val("");
	$("input[name='cepObra']").removeAttr('readonly');
	
	$("input[name='enderecoObra']").val("");
	$("input[name='enderecoObra']").removeAttr('readonly');
	
	$("input[name='numeroObra']").val("");
	$("input[name='numeroObra']").removeAttr('readonly');
	
	$("input[name='complementoObra']").val("");
	$("input[name='complementoObra']").removeAttr('readonly');
	
	$("input[name='bairroObra']").val("");
	$("input[name='bairroObra']").removeAttr('readonly');
	
	$('select[name="ufObra"]').val(null);
	$('select[name="ufObra"]').removeAttr('disabled');
	
	$('select[name="municipioObra"]').val(null);
	$('select[name="municipioObra"]').removeAttr('disabled');
	
	//define gg
	$('select[name="enumIdentificaGerador"]').val(null);
	$('select[name="enumIdentificaGerador"]').removeAttr('disabled');
	$('select[name="enumIdentificaGerador"]').change();

	$("#campos_gg").fadeOut();
	
	$("input[name='valorIdMunAjax']").val(null);
}		

/* Dialog Jquery ui */

/*
	Usado para substituir o alert do javascript
*/
function dialog(titulo,mensagem){
	$('#dialog').html('<p>'+mensagem+'</p>');
	$('#dialog').dialog({
		autoOpen: false,
		width: 600,
		title:titulo,
		modal:true,
		show:{ 
			effect: 'fade'
		},
		buttons: {
			"Ok": function() {
				$(this).dialog("close");
			}
		}
	});
	$('#dialog').dialog('open');
}
/*
	Fun��o para for�ar o fechamento do dialog de alert
*/
function closeDialog(){
	$('#dialog').dialog("close");
}
/*
	Fun��o que abre um dialog de aguarde
*/
function dialogAguarde(){
	$('#dialog').html('<p>Por favor, Aguarde ...</p>');
		$('#dialog').dialog({
			autoOpen: false,
			width: 600,
			title:'Aten��o',
			modal:true,
			show:{ 
				effect: 'fade'
			}
		});
	$('#dialog').dialog('open');
}

/*
	Fun��o para o dialog de voltar do CRUD
*/
function dialogVoltar(){
	$("#dialog-confirm").dialog({
		closeText: 'hide',
		closeOnEscape: false,
		resizable: false,
		width: 600,
		modal: true,
		show:{ 
			effect: 'fade'
		},		
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();}
	});
	return false;
}

function dialogDelete(param){
	
	url = window.location.href;
	
	if(url.indexOf('?')!=-1){
		url = location.href.substr(0,location.href.indexOf('?'))
	}
	url+='?ACAO=excluir&'+param;
	
	$('#dialog').html('<p>Deseja realmente excluir esse registro?</p>');
	$('#dialog').dialog({
		autoOpen: false,
		width: 600,
		title:'Aten��o',
		modal:true,
		show:{ 
			effect: 'fade'
		},
		buttons: {
			"Sim": function() {
				location.href = url;
			},
			"N�o": function() {
				$(this).dialog("close");
			}
		}
	});
	$('#dialog').dialog('open');
}
/*Fim Dialog jquery ui*/