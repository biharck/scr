<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<script type="text/javascript">
	function validaCamposLogin(){
		dialogAguarde();
		login = trim(document.getElementsByName('login')[0].value);
		senha = trim(document.getElementsByName('senha')[0].value);
		erros='';
		if(login==null||login==undefined||login.length==0)
			erros=erros+'<br/>'+'O campo Login � obrigat�rio';
		if(senha==null||senha==undefined||senha.length==0)
			erros=erros+'<br/>'+'O campo Senha � obrigat�rio';
		if(erros.length!=0){
			dialog('Aten��o',erros);
			return false;
		}
		return true;
	}
	function dialogOptCadastro(){
		$("#dialog-opt-auto-cadastro").dialog({
			closeText: 'hide',
			closeOnEscape: false,
			resizable: false,
			width: 600,
			modal: true,
			show:{ 
				effect: 'fade'
			}		
		});
		return false;
	}	
	function dialogEnviaSenha(){
		$("#dialog-envia-senha").dialog({
			closeText: 'hide',
			closeOnEscape: false,
			resizable: false,
			width: 600,
			modal: true,
			show:{ 
				effect: 'fade'
			}		
		});
		return false;
	}
	function enviaSenha(){
		email = $('#email').val();
		if(email==''){
			$('#email').focus();
			dialog('Aten��o','Por favor, informe um e-mail para podermos enviar a nova senha pra voc�!');
		}
		
		if(checkMail(email)){
			return true;
		}else{
			$('#email').focus();
			dialog('Aten��o','Por favor, digite um e-mail v�lido!');
		}
		return false;
	}
	function checkMail(mail){	
	    var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
	    if(typeof(mail) == "string"){
	    	if(mail=="") return true;
	        if(er.test(mail)){ return true; }
	    } else if(typeof(mail) == "object"){
	    	if(mail.value=="") return true;
	        if(er.test(mail.value)){ return true; }
		}	    
		return false;
	}
		
</script>
<!-- ui-dialog para mensagens -->
<div id="dialog">
	<p></p>
</div>
<div id="dialog-opt-auto-cadastro" style="display:none;" title="Ol�">
	<p>Antes de voc� se cadastrar, deixe-me ajudar!</p><br>
	
	
	<div class="p_dialog p_dialog_success" onclick="location.href='/SCR/publico/process/GrandeGerador'">
		<p>Grande Gerador</p>
		<p class="p_dialog_sub p_dialog_sub_success">Se voc� � gera mais de 1 m� de res�duo.</p>
	</div><br>
	<div class="p_dialog p_dialog_warning" onclick="location.href='/SCR/publico/process/PequenoGerador'">
		<p>Pequeno Gerador</p>
		<p class="p_dialog_sub p_dialog_sub_success">Se voc� � gera at� de 1 m� de res�duo.</p>
	</div><br>	
	<div class="p_dialog p_dialog_error" onclick="location.href='/SCR/publico/process/DestinoFinal'">
		<p>Destino Final</p>
		<p class="p_dialog_sub p_dialog_sub_error">Retorna para a tela de cria��o/edi��o de dados antes de voc� ter clicado em voltar.</p>
	</div><br>
	<div class="p_dialog p_dialog_alert" onclick="location.href='/SCR/publico/process/Transportador'">
		<p>Transportador</p>
		<p class="p_dialog_sub p_dialog_sub_alert">Se voc� transporta algum tipo de res�duo.</p>
	</div><br>
</div>
<div id="dialog-envia-senha" style="display:none;" title="Esqueceu sua senha?">
	<n:form name="enviaSenhaForm" url="/publico/process/EnviaSenha" action="pesquisarDados"  onsubmit="return  enviaSenha()">
<!-- 	onblur="enviaSenha($('#email').val())" -->
		<p>Preencha o campo abaixo para podermos enviar uma nova senha pra voc�!</p><br>
		<label for="email">Email</label>
		<input type="text" name="email" id="email" style="width:300px;" >
		<input type="submit"  value="Enviar" name="btnEnviar"/>
	</n:form>
</div>
<n:form validateFunction="validaCamposLogin">
	<n:bean name="usuario">
		<t:propertyConfig renderAs="double" mode="input">
			<n:panelGrid 
					columns="6" 
					align="center" 
					style=" padding:2px; border: none;margin-top: 300px;" 
					columnStyles="font-size: 12px;">
				<t:property name="login" id="login"/>
				<t:property name="senha" type="password" label="Senha"/>
				<n:submit type="submit" action="doLogin" panelColspan="2" panelAlign="right">Logar</n:submit>
				<n:panel colspan="6">
					<center>Esqueceu sua senha? <a href="javascript:dialogEnviaSenha();"><font style="color:#1F75CC">Clique aqui!</font></a></center>				
				</n:panel>
				<n:panel colspan="6">
					<center>Ainda n�o � usu�rio? <a href="javascript:dialogOptCadastro();"><font style="color:#1F75CC">Clique aqui e se cadastre!</font></a></center>				
				</n:panel>
			</n:panelGrid>
		</t:propertyConfig>
	</n:bean>
</n:form>
<style type="text/css">
	body{
		background: url('/SCR/img/style/bg-image.png');
	}
</style>
<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#login').focus();
	 });
</script>