<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
    <t:janelaEntrada submitConfirmationScript="valida()">
        <t:tabelaEntrada>
             <n:panelGrid columns="1">
                <c:if test="${param.ACAO != 'consultar'}">
                	<t:property name="id" colspan="1"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
                </c:if>
                
                <n:tabPanel id="" colspan="1">
                    <n:panel title="Identifica��o">
                        <n:group legend="Identifica��o " columns="1">
                        	<n:panelGrid columns="6">
	                        	<c:if test="${param.ACAO != 'criar'}">
	                            	<t:property name="numeroGuia" colspan="2" readonly="readonly" renderAs="doubleline"/>
	                        	</c:if>
	                            <t:property name="projeto" renderAs="doubleline" colspan="2" selectOnePath="/grandeGerador/crud/Projeto?showMenu=false" onchange="verifProjeto();" />
								<t:property name="enumSituacao" renderAs="doubleline" colspan="2" disabled="true"/>
								<t:property name="enumStatus" renderAs="doubleline" colspan="2" disabled="true"/>
	                        </n:panelGrid>     
                        </n:group>    
						 <n:group legend="Identifica��o Grande Gerador" columns="1">
                            <t:property name="enumIdentificaGerador" id="enumIdentificaGerador" renderAs="doubleline" onchange="verifEnumIdentificaGerador();"/>
                            <n:panelGrid columns="4">
                                <t:property name="grandeGerador" selectOnePath="/adm/crud/GrandeGerador?showMenu=false&hideNovo=true" colspan="2" renderAs="doubleline" onchange="verifGrandeGerador();"/>  
                            </n:panelGrid>
                            <jsp:include page="/WEB-INF/jsp/include/camposGrandeGerador.jsp"/> 
                        </n:group>
                    </n:panel>                 
                    <n:panel title="Transportador">
                        <n:group legend="Identifica��o Transportador" columns="1">
                            <t:property name="transportador" id="transportador" colspan="4" renderAs="doubleline" onchange="verifTransportador();" />
                            <jsp:include page="/WEB-INF/jsp/include/camposTransportador.jsp"/>
                        </n:group>
                    </n:panel>
                    <n:panel title="Planta">
                        <n:group legend="Endere�o da Planta" columns="1">
                            <n:panelGrid columns="4">
                                <n:panel colspan="1">
                                    <t:property name="cepObra" id="cep" maxlength="9" renderAs="doubleline" onkeypress="formatar_mascara(this,  \"#####-###\" );" class="tootip" title="Somente n�meros" onchange="ajaxBuscaCep($('#cep').val())"/>
                                </n:panel>
                                <t:property name="enderecoObra" id="endereco" colspan="2" style="width:400px" renderAs="doubleline" />
                                <t:property name="numeroObra" id="numero" colspan="1" renderAs="doubleline" />
                                <t:property name="complementoObra" colspan="1" renderAs="doubleline" />
                                <t:property name="bairroObra" id="bairro" colspan="1" renderAs="doubleline" />
                                <n:comboReloadGroup useAjax="true">
                                    <t:property name="ufObra" id="uf" colspan="1" renderAs="doubleline" />
                                    <t:property name="municipioObra" id="municipio" colspan="1" renderAs="doubleline" />
                                </n:comboReloadGroup>
                                <t:property name="responsavelObra" colspan="2" style="width:400px" renderAs="doubleline" />
                            </n:panelGrid>
                        </n:group>
                    </n:panel> 
                    <n:panel title="Res�duos">
                        <n:group legend="Res�duos" columns="1">
                            <n:panel colspan="4">
                                <t:detalhe name="residuosGuia" style="width:800px" labelnovalinha="Adicionar Res�duo">
                                    <n:column header="Tipo de Res�duo">
                                    	<t:property name="tipoResiduo" />
                                    </n:column>
                                    <n:column header="Quantidade">
                                    	<t:property name="quantidade" type="money" />
                                    </n:column>
                                    <n:column header="Unidade de Medida">
                                    	<t:property name="unidadeMedida" />
                                    </n:column>
                                </t:detalhe>
                            </n:panel>
                        </n:group>
                    </n:panel>
                    <n:panel title="Transporte">
                        <n:group legend="Identifica��o Transporte" columns="1">
                            <n:panel>
                                <n:panelGrid columns="4">
                                    <t:property name="tipoTransporte" renderAs="doubleline" style="width:300px"/>
                                    <t:property name="identificacaoTransporte" renderAs="doubleline" />
                                    <t:property name="placa" renderAs="doubleline" onkeypress="formatar_mascara(this,  \"###-####\");"
                                        maxlength="8" />
                                    <t:property name="dataEnvio" renderAs="doubleline" colspan="1" />
        							<t:property name="dataEntrega" renderAs="doubleline" colspan="1" class="tootip" title="A data de recebimento deve ser superior ou igual a data de envio"/>
        							<t:property name="dataRetirada" renderAs="doubleline"  colspan="3" class="tootip" title="A data de retirada deve ser superior ou igual a data de recebimento" />
                                    <t:property name="destinoFinal" id="destinoFinal" style="width:300px" colspan="4" renderAs="doubleline" onchange="verifDestinoFinal();" readonly="readonly" disabled="true" />
                                </n:panelGrid>
                            </n:panel>
                            <jsp:include page="/WEB-INF/jsp/include/camposDestinoFinal.jsp"/>        
                        </n:group>
                    </n:panel>
                </n:tabPanel>    
           </n:panelGrid>   
         </t:tabelaEntrada>
    </t:janelaEntrada>
</t:entrada>

<script type="text/javascript">

$(document).ready(function() {
    $("#destinoFinal").attr('disabled', 'disabled');
    $('#transportador').focus();
    $("#campos_transp").fadeOut(); 
    $("#campos_gg").fadeOut();
    $("#campos_dest").fadeOut();
    verifEnumIdentificaGerador();
    
    //consultar --> carrega pelo id, pois combo n�o possui valor
    acao = '${param.ACAO}';
    if(acao == 'consultar'){
        ajaxBuscaTransportador(${guiaDestinoFinal.transportador.id});  
        ajaxBuscaGrandeGerador(${guiaDestinoFinal.grandeGerador.id});
        ajaxBuscaDestinoFinal(${guiaDestinoFinal.destinoFinal.id});
    }else if(!${empty guiaDestinoFinal.id}){//edi��o 
        verifTransportador();
        verifGrandeGerador();
        verifDestinoFinal();
    }else //incluir, pois j� est� pr�-definido
        verifDestinoFinal();
    
    tipo = $("input[name='projeto_label']").val();
    if(!(tipo == "" || tipo == null || tipo == '<null>'))
    	ajaxBuscaProjeto(tipo);
   
});


function verifEnumIdentificaGerador() {
	tipoIdent = $("#enumIdentificaGerador").val();
     
     if(tipoIdent == 'SIM'){
       $("input[name='grandeGerador_label']").show();
       
       tipo = $("input[name='grandeGerador']").val();
       if(!(tipo == "" || tipo == null || tipo == '<null>'))
       		$("button[name='grandeGerador_btn']").hide();
       else
    	   $("button[name='grandeGerador_btn']").show();
     }else{
   	   $("input[name='grandeGerador']").val("<null>");
   	   $("input[name='grandeGerador_label']").val("");
   	   $("input[name='grandeGerador_label']").hide();
       $("button[name='grandeGerador_btn']").hide();
       $("button[name='grandeGerador_btnUnselect']").hide(); 
     }  
     $("#campos_gg").fadeOut();
} 
  
function verifProjeto() {
	tipo = $("input[name='projeto_label']").val();
    if(!(tipo == "" || tipo == null || tipo == '<null>'))
         ajaxBuscaProjeto(tipo);
} 

function verifTransportador() {
     tipo = $("#transportador").val();
     
     if(tipo == "" || tipo == null || tipo == '<null>'){
        $("#campos_transp").fadeOut();  
        dialog('Aten��o','Voc� deve selecionar o Transportador');
        return false
     }else
        ajaxBuscaTransportador(tipo);
}

function verifGrandeGerador() {
	 tipo = $("input[name='grandeGerador']").val();
	 tipoIdent = $("#enumIdentificaGerador").val();
	 if((tipoIdent == 'SIM') && (tipo == "" || tipo == null || tipo == '<null>')){
	 	$("#campos_gg").fadeOut();
	    dialog('Aten��o','Voc� deve selecionar o Grande Gerador');
	 	return false
	 }else if((tipoIdent == 'SIM') && !(tipo == "" || tipo == null || tipo == '<null>'))
	    ajaxBuscaGrandeGerador(tipo);	 
}

function verifDestinoFinal() {
     tipo = $("#destinoFinal").val();

     if(tipo == "" || tipo == null || tipo == '<null>'){
        $("#campos_dest").fadeOut();
        dialog('Aten��o','Voc� deve selecionar o DestinoFinal');
        return false
     }else
        ajaxBuscaDestinoFinal(tipo);     
}

function valida(){
	tipoIdent = $("#enumIdentificaGerador").val();
   	
   	tipo = $("input[name='grandeGerador']").val();

   	if((tipoIdent == 'SIM') && (tipo == "" || tipo == null || tipo == '<null>')){
   		dialog('Aten��o','Voc� deve selecionar o Grande Gerador');
		return false;
   	}else
   		return true;
}

$('button[name="projeto_btnUnselect"]').click(function(){
	limpaCamposGuia();
 	$("#campos_gg").hide();
});

$('button[name="grandeGerador_btnUnselect"]').click(function(){
	$("input[name='grandeGerador']").val("<null>");
	$("input[name='grandeGerador_label']").val("");
	$("#campos_gg").fadeOut();
});

</script>