<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:listagem novolabel="Nova Guia">
    <t:janelaFiltro>
        <t:tabelaFiltro>
            <n:panel>
            	<n:panelGrid columns="8">
                    <t:property name="nomeFantasia" colspan="4" style="width:400px;"/>
					<t:property name="identificacaoTransporte" colspan="2" />
                    <t:property name="numeroGuia" colspan="2"/>
                    <t:property name="status" colspan="2" />
                    <t:property name="situacao" colspan="2" />
                    <t:property name="numeroProjeto" colspan="2"/>
                    <t:property name="placa" colspan="2"/>
                    <t:property name="destinoFinal" style="width:200px" colspan="2" readonly="readonly" disabled="true"/>
                    <t:property name="transportador" style="width:200px" colspan="2"/>
               		<t:property name="grandeGerador" style="width:200px" />
               		<t:property name="enumCancelada" readonly="readonly" disabled="true"/>
               		<t:property name="dataCriacao"/>
                </n:panelGrid>    
            </n:panel>
        </t:tabelaFiltro>
    </t:janelaFiltro>
    <t:janelaResultados>
        <t:tabelaResultados>
            <t:property name="numeroGuia" label="N�mero" />
            <t:property name="projeto.numeroProjeto" label="Projeto"/>
            <t:property name="grandeGerador" label="Gerador"/>
            <t:property name="transportador"/>
            <t:property name="tipoTransporte"/>
            <t:property name="timeInc" label="Registro"/>
            <t:property name="dataEntrega" label="Entrega"/>
            <t:property name="dataRetirada" label="Retirada"/>
            <t:property name="placa"/>
            <t:property name="destinoFinal" label="Destino"/>
            <t:property name="enumSituacao"/>
            <t:property name="status" trueFalseNullLabels="Confirmada,Pendente,"/>
            <t:acao>
				<n:link url="/adm/relatorio/GuiaCompleta" action="gerar" parameters="idGuia=${guiaDestinoFinal.id}" class="outterTableHeaderLink">
					<span class="tootip" title="Relat�rio com todas informa��es da Guia">Imprimir </span>
				</n:link>
			</t:acao>
        </t:tabelaResultados>
    </t:janelaResultados>
</t:listagem>