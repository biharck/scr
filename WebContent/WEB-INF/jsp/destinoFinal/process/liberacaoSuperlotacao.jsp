<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="SCR <img src='/SCR/img/style/next.png'> <span class='titulo-pagina-corrente'>Libera��o de Superlota��o do Destino Final</span>">
    <n:bean name="liberacaoSuperlotacao">
        <div class="inputWindow">
        	<t:tabelaEntrada  colspan="2">
        		<n:panelGrid columns="6">
        			<t:property name="data" renderAs="doubleline"  colspan="6" mode="output"/>
      				<t:property name="destinoFinal"  renderAs="doubleline" id="destinoFinal" colspan="2" onchange="verifDestinoFinal();"/>
      				<t:property name="destinoFinal.capacidadeTotal" id="capacidadeTotal"  renderAs="doubleline" colspan="2" readonly="readonly"/>
      				<t:property name="destinoFinal.capacidadeAtual" id="capacidadeAtual"  renderAs="doubleline" colspan="2" readonly="readonly"/>
      				<t:property name="responsavel"  renderAs="doubleline" style="width:400px" colspan="4" />
      				<t:property name="capacidadeLiberada"  renderAs="doubleline" colspan="2" />
      				<t:property name="motivo" colspan="6" type="TEXT_AREA"  renderAs="doubleline" cols="70" rows="5"/>
        		</n:panelGrid>
        	</t:tabelaEntrada>
            <div class="actionBar">
                <n:submit action="salvar" validate="true" confirmationScript="${janelaEntradaTag.submitConfirmationScript}" onclick="dialogAguarde();" class="salvar-registro">salvar</n:submit>
            </div>
        </div>
        <br><br>
    </n:bean>
</t:tela>

<script type="text/javascript">
     $(document).ready(function() {
        $('#destinoFinal').focus();
     });
     
     function verifDestinoFinal() {
    	 tipo = $("#destinoFinal").val();
    	 if(tipo == "" || tipo == null || tipo == '<null>'){
            dialog('Aten��o','Voc� deve selecionar o DestinoFinal');
            return false
    	 }else
    		 ajaxBuscaCapacidadeDestinoFinal(tipo);	 
    }
     
</script>
