<%@page import="br.com.biharckgroup.scr.util.SCRUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>


<div id="dialog-dataRecebimento" style="display:none;" title="Ol� <%= SCRUtil.getLoginUsuarioLogado() %>">
<p>Para receber a Guia sem diverg�ncias, informe por favor a data de recebimento!</p><br>
	<div class="p_dialog p_dialog_success">
		<br />
		Data de Recebimento&nbsp;<input type="text" id="dataRecebimento" name="dataRecebimento" value="" maxlength="10" size="11" onkeyup="mascara_data(this, event, 'dd/MM/yyyy');" onkeypress="return valida_tecla_data(this, event, 'dd/MM/yyyy')" onchange="">
		<script>
			$(function() {
				$( "#dataRecebimento" ).datepicker({ 
					altFormat: "dd/mm/yyyy" 
				});
			});
		</script>
		<span class="requiredMark">*</span>
						
	</div><br>
</div>

<div id="dialog-recusar" style="display:none;" title="Ol� <%= SCRUtil.getLoginUsuarioLogado() %>">
<p>Para recusar esta Guia, voc� deve nos informar o motivo!</p><br>
	<div class="p_dialog p_dialog_error" style="height:12.2em;">
		
		<br />
		<br />
		Data de Recebimento&nbsp;<input type="text" id="dataRecebimento3" name="dataRecebimento3" value="" maxlength="10" size="11" onkeyup="mascara_data(this, event, 'dd/MM/yyyy');" onkeypress="return valida_tecla_data(this, event, 'dd/MM/yyyy')" onchange="" class="hasDatepicker">
		<script>
			$(function() {
				$( "#dataRecebimento3" ).datepicker({ 
					altFormat: "dd/mm/yyyy" 
				});
			});
		</script>
		<span class="requiredMark">*</span>
		<br />
		<br />
		Motivo&nbsp;<textarea id="motivo3" rows="5" cols="60"></textarea>
		
		<span class="requiredMark">*</span>
						
	</div><br>
</div>

<div id="dialog-receber-pendente" style="display:none;" title="Ol� <%= SCRUtil.getLoginUsuarioLogado() %>">
<p>Para recusar esta Guia, voc� deve nos informar o motivo!</p><br>
	<div class="p_dialog p_dialog_warning" style="height:12.2em;">
		<br />
		<br />
		Data de Recebimento&nbsp;<input type="text" id="dataRecebimento2" name="dataRecebimento2" value="" maxlength="10" size="11" onkeyup="mascara_data(this, event, 'dd/MM/yyyy');" onkeypress="return valida_tecla_data(this, event, 'dd/MM/yyyy')" onchange="">
		<script>
			$(function() {
				$( "#dataRecebimento2" ).datepicker({ 
					altFormat: "dd/mm/yyyy" 
				});
			});
		</script>
		<span class="requiredMark">*</span>
		<br />
		<br />
		Motivo&nbsp;<textarea id="motivo2" rows="5" cols="60"></textarea>
		
		<span class="requiredMark">*</span>
						
	</div><br>
</div>
            
<div class="pageTitle"> SCR <img src="/SCR/img/style/next.png"> <span class="titulo-pagina-corrente">Recebimento de Guias</span></div>            
<t:tela>
	<div>
		<n:bean name="guiaFiltro" >
	       <n:panelGrid columns="4" >		
                <n:panel colspan="2"> Destino Final </n:panel>
                <n:panel colspan="2"> <t:property name="destinoFinal" style="width:300px" readonly="readonly" disabled="true"/> </n:panel>
                <n:panel colspan="2">N�mero da Guia </n:panel>
                <n:panel colspan="2"><t:property name="numeroGuia"/></n:panel>
            </n:panelGrid>
			<n:submit action="pesquisar">Pesquisar</n:submit>
		</n:bean>
	</div>
	<div>
		<br />
		<br />
	</div>
	<n:dataGrid itens="${guias}" var="guia" itemType="br.com.biharckgroup.scr.bean.Guia" cellspacing="0" 
		rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
		rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
		id="tabelaResultados" varIndex="index">

		     
		<t:property name="numeroGuia" label="N�mero"  mode="output"/>
		<t:property name="projeto.numeroProjeto" label="Projeto" mode="output"/>
		<t:property name="grandeGerador" label="Gerador" mode="output"/>
		<t:property name="transportador" mode="output"/>
		<t:property name="tipoTransporte" mode="output" label="Tipo"/>
		<t:property name="timeInc" label="Registro" mode="output"/>
		<t:property name="dataEnvio" label ="Envio" mode="output"/>
		<t:property name="dataEntrega" label="Recebimento" mode="output"/>
		<t:property name="dataRetirada" label="Retirada" mode="output"/>
		<t:property name="dataChegada" label="Recebimento Dest. Final" mode="output"/>
		<t:property name="placa" mode="output"/>
		<t:property name="destinoFinal" label="Destino" mode="output"/>
		<t:property name="enumSituacao" mode="output"/>
		<t:property name="status" trueFalseNullLabels="Confirmada,Pendente," mode="output"/>
        <n:column header="Res�duos">
             <c:forEach items="${guia.residuosGuia}" var="bean">
                 <c:out value="${bean.tipoResiduo.descricao}"/> -
                 <c:out value="${bean.quantidade}"/>
                 <c:out value="${bean.unidadeMedida.nome}"/><br/>
             </c:forEach>
         </n:column>
        <c:if test="${empty guia.situacao }">
			<n:column header="A��o">
				<a href="javascript:dialogDataRecebimento(${guia.id});">
					<div class="tootip-warnig" title="Caso a Guia esteja correta, clique aqui para receber!"><font style="color:green;">Receber</div>
				</a>
				<n:link url="javascript:dialogReceberPendente(${guia.id});" action="receber" parameters="id=${guia.id}" class="outterTableHeaderLink">
					<div class="tootip" title="Recebimento da Guia com algumas Observa��es."><font style="color:#B07E00;">Divergente </font></div>
				</n:link>
				<n:link url="javascript:dialogRecusada(${guia.id});" action="recusar" parameters="id=${guia.id}" class="outterTableHeaderLink">
					<div class="tootip-alert" title="Se a Guia estiver divergente em alguma situa��o, clique aqui para recusar esta Guia."><font style="color:#CC0000;">Recusar</font> </div>
				</n:link>
			</n:column>
		</c:if>	
	</n:dataGrid>
	<script type="text/javascript">
		function dialogDataRecebimento(id){
			$('#dialog-dataRecebimento').dialog({
				autoOpen: false,
				width: 600,				
				modal:true,
				show:{ 
					effect: 'fade'
				},
				buttons: {
					"Receber Guia": function() {
						dialogAguarde();
						
						if($('#dataRecebimento').val()==''){
							closeDialog();
							dialog('Aten��o','A data de recebimento deve ser preenchida!');
						}else{
							location.href = '/SCR/destinoFinal/process/RecebimentoGuia?ACAO=receber&id='+id+'&dataRecebimento='+$('#dataRecebimento').val();
						}
					}
				}
			});
			$('#dialog-dataRecebimento').dialog('open');
		}
		
		function dialogRecusada(id){
			$('#dialog-recusar').dialog({
				autoOpen: false,
				width: 600,				
				modal:true,
				show:{ 
					effect: 'fade'
				},
				buttons: {
					"Recusar Guia": function() {
						dialogAguarde();
						
						if($('#dataRecebimento3').val()==''){
							closeDialog();
							dialog('Aten��o','A data de recebimento com diverg�ncia deve ser preenchida!');
						}else if($('#motivo3').val()==''){
							closeDialog();
							dialog('Aten��o','O motivo de recebimento com diverg�ncia deve ser informado!');
						}
						else{
							location.href = '/SCR/destinoFinal/process/RecebimentoGuia?ACAO=recusar&id='+id+'&dataRecebimento='+$('#dataRecebimento3').val()+'&motivo='+$('#motivo3').val();
						}
					}
				}
			});
			$('#dialog-recusar').dialog('open');
		}

		function dialogReceberPendente(id){
			$('#dialog-receber-pendente').dialog({
				autoOpen: false,
				width: 600,				
				modal:true,
				show:{ 
					effect: 'fade'
				},
				buttons: {
					"Receber Guia com Pend�ncia": function() {
						dialogAguarde();
						
						if($('#dataRecebimento2').val()==''){
							closeDialog();
							dialog('Aten��o','A data de recebimento com diverg�ncia deve ser preenchida!');
						}else if($('#motivo2').val()==''){
							closeDialog();
							dialog('Aten��o','O motivo de recebimento com diverg�ncia deve ser informado!');
						}
						else{
							location.href = '/SCR/destinoFinal/process/RecebimentoGuia?ACAO=receberDivergente&id='+id+'&dataRecebimento='+$('#dataRecebimento2').val()+'&motivo='+$('#motivo2').val();
						}
					}
				}
			});
			$('#dialog-receber-pendente').dialog('open');
		}
		
		
		
		
	</script>
</t:tela>