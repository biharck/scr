<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:listagem>
    <t:janelaFiltro>
        <t:tabelaFiltro>
            <n:panel>
                <n:panelGrid columns="4" >
                    <c:choose>
                          <c:when test="<%= br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaGrandeGerador() %>">
				              <t:property name="grandeGerador" colspan="4" readonly="readonly" disabled="true" style="width:200px"/>
                          </c:when>
                    	  <c:otherwise>
                    	  	  <t:property name="grandeGerador" colspan="4" selectOnePath="/adm/crud/GrandeGerador?showMenu=false" style="width:200px"/>
                    	  </c:otherwise>
                    </c:choose>
                     <c:choose>
                          <c:when test="<%= br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaGrandeGerador() %>">
				              <t:property name="transportador" colspan="4" readonly="readonly" disabled="true" style="width:200px"/>
                          </c:when>
                    	  <c:otherwise>
                    	  	  <t:property name="transportador" colspan="4" style="width:200px" />
                    	  </c:otherwise>
                    </c:choose>
                    <t:property name="destinoFinal" colspan="2" style="width:200px"/>
                    <t:property name="dataCriacao" colspan="2"/>
                    <t:property name="numeroProjeto" colspan="2"/>
                    <t:property name="enumStatusProjeto" colspan="2" />
                    <n:panel valign="top" style="padding-top:4px">Responsáveis</n:panel>
                    <n:dataGrid itemType="br.com.biharckgroup.scr.bean.Responsavel" itens="${listaResponsavel}" bodyStyleClasses="," styleClass=",">
                        <n:column width="20">
                            <n:input name="responsaveis" value="${row}" type="checklist" itens="${projeto.responsaveis}"/>
                        </n:column>
                        <n:column>
                            <t:property name="nome" mode="output" label=" "/>
                        </n:column>
                    </n:dataGrid>
                </n:panelGrid>
            </n:panel>
        </t:tabelaFiltro>
    </t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados showEditarLink="${empty param.hideOptions}" showExcluirLink="${empty param.hideOptions}">
			<t:property name="grandeGerador"/>
            <t:property name="numeroProjeto"/>
            <t:property name="caracteristicaObra"/>
            <t:property name="materiaisObra"/>
            <t:property name="enumStatusProjeto"/>
			<t:acao>
	             <n:link url="/adm/relatorio/ProjetoCompleto" action="gerar" parameters="idProjeto=${projeto.id}" class="outterTableHeaderLink">
	   	            <span class="tootip" title="Relatório com todas informações do Projeto">Imprimir </span>
	             </n:link>
			</t:acao>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>