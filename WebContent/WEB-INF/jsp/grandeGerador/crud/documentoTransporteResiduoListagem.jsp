<%@page import="br.com.biharckgroup.scr.util.SCRUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem novolabel="Novo RTR">
    <t:janelaFiltro >
        <t:tabelaFiltro>
            <n:panel>
                <n:panelGrid columns="4">
                    <t:property name="projeto" colspan="2" selectOnePath="/grandeGerador/crud/Projeto?showMenu=false"/>
                    <t:property name="enumSituacao" colspan="2" />
                    <t:property name="chave" colspan="4" style="width:400px;"/>
                </n:panelGrid>
            </n:panel>    
        </t:tabelaFiltro>
    </t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados showEditarLink="${empty param.hideOptions}" showExcluirLink="false" >
            <t:property name="projeto.numeroProjeto"/>
            <t:property name="chave"/>
            <t:property name="enviado" label="Enviado ao T�c." />
            <t:property name="enumSituacao"/>
            <t:property name="timeInc" label="Data Cria��o Doc."/>
        </t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>