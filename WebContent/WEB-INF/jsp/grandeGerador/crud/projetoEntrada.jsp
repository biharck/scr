<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
      <n:tabPanel id="Projeto">
          <n:panel title="Identifica��o">
          	<n:panelGrid columns="1">
				<n:group legend="Resumo" columns="1">
					<n:panelGrid columns="4">
						<c:if test="${param.ACAO != 'consultar'}">
                        	<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
                        </c:if>
                        <c:if test="${param.ACAO != 'criar'}">
                            <t:property name="numeroProjeto" colspan="2" readonly="readonly" renderAs="doubleline"/>
                        </c:if>
                        <t:property name="grandeGerador" colspan="2" style="width:300px;" renderAs="doubleline" readonly="readonly" disabled="true"/>
			        	<t:property name="dataInicio" colspan="1" renderAs="doubleline"  />
			        	<t:property name="dataTermino" colspan="1" renderAs="doubleline" />
		        	</n:panelGrid>
		        </n:group>
		        <n:group legend="Localiza��o da Planta" columns="1">
					<n:panelGrid columns="4">
					<n:panel colspan="1">
							<t:property name="cepObra" id="cep" maxlength="9" renderAs="doubleline" onkeypress="formatar_mascara(this,  \"#####-###\" );" class="tootip" title="Somente n�meros" onchange="ajaxBuscaCep($('#cep').val())"/>
						</n:panel>
						<t:property name="enderecoObra" id="endereco" colspan="2" style="width:400px" renderAs="doubleline" />
						<t:property name="numeroObra" id="numero" colspan="1" renderAs="doubleline" />
						<t:property name="complementoObra" colspan="1" renderAs="doubleline" />
						<t:property name="bairroObra" id="bairro" colspan="1" renderAs="doubleline" />
						<n:comboReloadGroup useAjax="true">
							<t:property name="ufObra" id="uf" colspan="1" renderAs="doubleline" />
							<t:property name="municipioObra" id="municipio" colspan="1" renderAs="doubleline" />
						</n:comboReloadGroup>
						<t:property name="caracteristicaObra" type="TEXT_AREA"  colspan="4" renderAs="doubleline" cols="80" rows="5"
									class="tootip-center" title="Finalidade, prazo de execu��o, �reas, pavimentos e outras descri��es." />
    					<t:property name="materiaisObra" type="TEXT_AREA" colspan="4" renderAs="doubleline" cols="80" rows="5"
    								class="tootip-center" title="Preparo de canteiro, funda��es, estrutura, veda��es, revestimentos, cobertura, etc." />
					</n:panelGrid>
				</n:group>
          	</n:panelGrid>
          </n:panel>
          <n:panel title="Res�duos">
			<n:panelGrid columns="4">
                 <t:property name="descricaoDestino" type="TEXT_AREA" colspan="4" renderAs="doubleline" cols="80" rows="5"
                                                  class="tootip-center" title="Eventuais res�duos ambulat�rios, refeit�rios etc." />
                 
                <n:panel colspan="4" title="Tipos de Res�duos">
                 	<t:detalhe name="gruposTipoResiduoProjeto" style="width:800px" labelnovalinha="Adicionar Res�duo">
	                    <n:column header="Grupo">
	                    	<t:property name="grupoTipoResiduo" onchange="buscaTipoResiduoProjeto(this.value,${index})"/>
	                    </n:column>
	                    <n:column header="Tipo de Res�duo">
	                    	<t:property name="tipoResiduo" />
	                    </n:column>
	                    <n:column header="Quantidade">
	                   		<t:property name="quantidade" type="money" />
	                   	</n:column>
	                   	<n:column header="Unidade de Medida">
	                   		<t:property name="unidadeMedida" />
	                   	</n:column>
	                   	<n:column header="Destino Final">
	                   		<t:property name="destinoFinal" />
	                   	</n:column>
	              	</t:detalhe>
                </n:panel>
			</n:panelGrid>
          </n:panel>
          <n:panel title="Iniciativas">
               <n:panelGrid columns="4">
                  <t:property name="iniciativaMinimizacao" type="TEXT_AREA" colspan="4" renderAs="doubleline" cols="80" rows="5"
                                  class="tootip-center" title="Escolha dos materiais, orienta��o da m�o de obra e respons�veis, controles a serem adotados, etc.)" />
                  <t:property name="iniciativaAbsorcao" type="TEXT_AREA" colspan="4" renderAs="doubleline" cols="80" rows="5"
                                class="tootip-center" title="Reutiliza��o dos res�duos de demoli��o, reutiliza��o nas diversas etapas,etc." />
                  <t:property name="iniciativaAcondicionamento" type="TEXT_AREA" colspan="4" renderAs="doubleline" cols="80" rows="5"
                                class="tootip-center" title="Forma de organiza��o das quatro classes, dispositivos empregados, etc." />
               </n:panelGrid>
          </n:panel>  
          <n:panel title="Transportadores">
             	
              <n:panel valign="top" style="padding-top:4px" renderAs="doubleline">
              	<br />
              		<i>Identifica��o dos agentes licenciados respons�veis pelo transporte dos res�duos:</i>
              	<br />
              </n:panel>
             <n:panel><br /></n:panel>
              <t:detalhe name="transportadoresProjeto" style="width:500px" labelnovalinha="Adicionar Transportador" >
                <n:column header="Transportador">
                	<t:property name="transportador" selectOnePath="/adm/crud/Transportador?showMenu=false"/>
                </n:column>
              </t:detalhe>
         </n:panel>   
         <n:panel title="Respons�veis">
         		<br />
              <t:detalhe name="responsaveisProjeto" style="width:500px" labelnovalinha="Adicionar Respons�vel" >
              	<n:column header="Respons�vel">
                	<t:property name="responsavel" selectOnePath="/adm/crud/Responsavel?showMenu=false"/>
                </n:column>
              </t:detalhe>
         </n:panel>             
      </n:tabPanel>
    </t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
$(document).ready(function() {
    $('#dataInicio').focus();
});
</script>			