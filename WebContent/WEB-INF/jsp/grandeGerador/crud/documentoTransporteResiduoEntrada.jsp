<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada >
	<t:janelaEntrada showSubmit="false" >
      	<n:panel>
           	<n:panel valign="top" style="padding-top:4px" renderAs="doubleline">
              	<c:if test="${param.ACAO != 'consultar'}">
	              	<br />
	              		<i>Orienta��es:</i><br/>
	              		- Indicar qual projeto para que o sistema liste as guias emitidas no mesmo;<br/>
	              		- Um email ser� enviado ao T�cnico da SEMEA para avalia��o do Relat�rio de Transporte de Res�duos - RTR;<br/>
	              		- Ap�s Aprova��o/Reprova��o do RTR pelo T�cnico da SEMEA, o Grande Gerador e T�cnico da SPU ser�o informados;
	              		<c:if test="${superUser && documentoTransporteResiduo.enviado}">
	              			<br/><br/>
	              			<b>Se h� �bice quanto ao relat�rio informe a situa��o "Aprovado" sen�o informe "Reprovado",
	              			neste caso ser� enviado uma email ao respons�vel do PGRCC informando a necessidade de explica��o sobre as guias pendentes.</b>
	              		</c:if>
	              	<br /><br/>
	              </c:if>	
            </n:panel>
           	<n:panelGrid columns="4" > 
                <t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
                <t:property name="projeto"  colspan="2" selectOnePath="/grandeGerador/crud/Projeto?showMenu=false"/>
                <c:if test="${documentoTransporteResiduo.id != null}">
	            	<t:property name="enumSituacao" id="enumSituacao"  colspan="2" renderAs="doubleline" disabled="disabled" />
                	<t:property name="chave"  style="width:400px"  colspan="4" readonly="readonly" renderAs="doubleline" />
                	<t:property name="enviado"  readonly="readonly" renderAs="doubleline" />
                </c:if>	
           	</n:panelGrid>
           	<div align="right">
           		<c:if test="${param.ACAO != 'consultar'}">
	           	 	<c:if test="${documentoTransporteResiduo.id == null}">
	                	<n:submit url="/grandeGerador/crud/DocumentoTransporteResiduo" action="filtroGuiaByProjeto">Buscar Guias</n:submit> 
					</c:if>
					<c:if test="${guias != null && !documentoTransporteResiduo.enviado}">
						<n:submit url="/grandeGerador/crud/DocumentoTransporteResiduo" action="enviarEmail"> Enviar ao T�cnico SEMEA</n:submit>
					</c:if>
					<c:if test="${superUser && documentoTransporteResiduo.enviado}">
						<n:submit url="/grandeGerador/crud/DocumentoTransporteResiduo" action="alterarSituacao"> Alterar Situa��o</n:submit>
					</c:if>
				</c:if>	
	    	</div>   
    	</n:panel>
	</t:janelaEntrada>
	<c:if test="${guias != null}">
		<n:panel title="Guias">
	    	<n:dataGrid itens="guias" itemType="br.com.biharckgroup.scr.bean.Guia" var="guia"
			cellspacing="0"
				rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
				rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
				id="tabelaResultados" varIndex="index">
			    <t:property name="numeroGuia" label="N� Guia" mode="output"/>
            	<t:property name="projeto.numeroProjeto" label="N� Projeto" mode="output"/>
				<t:property name="grandeGerador" mode="output"/>
				<t:property name="tipoTransporte" mode="output"/>
				<t:property name="dataEntrega" mode="output"/>
				<t:property name="dataRetirada" mode="output"/>
				<t:property name="placa" mode="output"/>
				<t:property name="destinoFinal" mode="output"/>
				<t:property name="enumSituacao" mode="output"/>
				<t:property name="status" trueFalseNullLabels="Confirmada,Pendente, " mode="output"/>
			</n:dataGrid>
		</n:panel>	
	</c:if>
</t:entrada>

<script type="text/javascript">
$(document).ready(function() {
    superUser = ${superUser};
    emailEnviado = ${documentoTransporteResiduo.enviado};
    if(superUser && emailEnviado)
    	$("#enumSituacao").removeAttr('disabled');
	else
		$("#enumSituacao").attr('disabled', 'disabled');  
    
});

</script>