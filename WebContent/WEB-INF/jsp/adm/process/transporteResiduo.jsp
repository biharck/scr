<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:relatorio titulo="Relat�rio <img src='/SCR/img/style/next.png'><span class='titulo-pagina-corrente'> Transporte de Res�duos - RTR </span>">
 <t:janelaFiltro>
     <t:tabelaFiltro submitAction="filtroGuia" >
     	 <n:panelGrid  columns="6">
            <t:property name="projeto" colspan="2"  /><!-- selectOnePath="/grandeGerador/crud/Projeto" -->
     	 </n:panelGrid>
     </t:tabelaFiltro>
     Gerar Relat�rio de Transporte de Res�duos - RTR e Envia e-mail para o T�cnico da SEMEA e para o Grande Gerador, informando um relat�rio pr�vio: 
     <n:submit action="gerar" >Gerar RTR/Enviar</n:submit>
 </t:janelaFiltro>
</t:relatorio>
<n:dataGrid itens="guias" itemType="br.com.biharckgroup.scr.bean.Guia" var="guia"
	cellspacing="0"
		rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
		rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
		id="tabelaResultados" varIndex="index">
    <t:property name="numeroGuia"/>
    <t:property name="projeto.numeroProjeto"/>
	<t:property name="grandeGerador"/>
	<t:property name="tipoTransporte"/>
	<t:property name="timeInc" label="Data do Registro"/>
	<t:property name="dataEntrega"/>
	<t:property name="dataRetirada"/>
	<t:property name="placa"/>
	<t:property name="destinoFinal"/>
	<t:property name="enumSituacao"/>
	<t:property name="status" trueFalseNullLabels="Confirmada,Pendente, "/>
</n:dataGrid>
