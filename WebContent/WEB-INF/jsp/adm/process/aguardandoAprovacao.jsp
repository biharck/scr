<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="SCR <img src='/SCR/img/style/next.png'> <span class='titulo-pagina-corrente'>Cadastros Aguardando Aprova��o</span>">
	<n:dataGrid itens="cadastros" itemType="br.com.biharckgroup.scr.bean.AguardandoAprovacao"
		var="cad"
		cellspacing="0"  
		rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
		rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
		id="tabelaResultados" varIndex="index">
		<t:property name="id" mode="output"/>
		<t:property name="enumTipoCadastro" mode="output"/>
		<t:property name="nome" mode="output"/>
		<t:property name="dataCadastro" mode="output" pattern="dd/MM/yyyy HH:MM:s"/>
		<n:column header="A��o">
			<c:if test="${cad.enumTipoCadastro.tipo == 1}">
				<a rel="modalFrame" href="/SCR/adm/crud/Transportador?ACAO=editar&id=${cad.id}&showMenu=false&showLinkBar=false" title-modal="Cadastro de ${cad.enumTipoCadastro.nome}">
					<button class="tootip" title='Clique aqui para ver os dados cadastrados deste Transportador' >ver cadastro</button>
				</a>			
			</c:if>
			<c:if test="${cad.enumTipoCadastro.tipo == 2}">
				<a rel="modalFrame" href="/SCR/adm/crud/DestinoFinal?ACAO=editar&id=${cad.id}&showMenu=false&showLinkBar=false" title-modal="Cadastro de ${cad.enumTipoCadastro.nome}">
					<button class="tootip" title='Clique aqui para ver os dados cadastrados deste Destino Final.' >ver cadastro</button>
				</a>			
			</c:if>
		</n:column>
	</n:dataGrid>
</t:tela>

<script type="text/javascript">
 	$(function() {
			$('a[rel=modalFrame]').click(function(e) {
				e.preventDefault();
				var $this = $(this);
				var horizontalPadding = 30;
				var verticalPadding = 30;
		        $('<iframe id="aprovacao" class="externalSite" src="' + this.href + '" />').dialog({
		            title: ($this.attr('title-modal')) ? $this.attr('title-modal') : '',
		            autoOpen: true,
		            width: 980,
		            height: 630,
		            modal: true,
		            resizable: true,
					autoResize: true,
		            overlay: {
		                opacity: 0.5,
		                background: "black"
		            },
		            open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();}
		        }).width(1000 - horizontalPadding).height(630 - verticalPadding);	        
			});
		});
		function closeIframe(){
		   var iframe = document.getElementById('aprovacao');
		   iframe.parentNode.removeChild(iframe);
		   dialog('Aten��o','Aguarde um instante por favor...')
		   location.href = location.href+"?sucesso=true";		   
		}

</script>