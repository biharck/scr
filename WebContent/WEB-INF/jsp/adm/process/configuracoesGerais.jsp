<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="SCR <img src='/SCR/img/style/next.png'> <span class='titulo-pagina-corrente'>Configura��es Gerais do Sistema</span>">
    <n:bean name="configuracoesGerais">
        <div class="inputWindow">
        	<t:property name="id" renderAs="doubleline" type="hidden" showLabel="false" write="false" label=" "/>
        	<t:tabelaEntrada  colspan="2">
        		<n:panelGrid columns="6">
        			<n:group legend="Autentca��o para envio de Email" colspan="6">
        				<t:property name="usuarioEmail"  renderAs="doubleline" id="usuarioEmail" style="width:300px;"/>
        				<t:property name="senhaEmail"  renderAs="doubleline"  style="width:300px"/>
        				<t:property name="smtp"  renderAs="doubleline"  style="width:300px"/>
        				<t:property name="porta"  renderAs="doubleline"  style="width:300px"/>
        			</n:group>
        			<n:group legend="Email T�cnico SPU" colspan="6">
        				<t:property name="emailSPU"  renderAs="doubleline"  style="width:300px"/>
        			</n:group>
        			<n:group legend="Telefone T�cnico SPU" colspan="6">
        				<t:property name="telefoneSPU"  renderAs="doubleline"  style="width:300px"/>
        			</n:group>
        			<n:group legend="Telefone SEMEA" colspan="6">
        				<t:property name="telefoneSEMEA"  renderAs="doubleline"  style="width:300px"/>
        			</n:group>
        		</n:panelGrid>
        	</t:tabelaEntrada>
            <div class="actionBar">
                <n:submit action="salvar" validate="true" confirmationScript="${janelaEntradaTag.submitConfirmationScript}" onclick="dialogAguarde();" class="salvar-registro">salvar</n:submit>
            </div>
        </div>
        <br><br>
    </n:bean>
</t:tela>

<script type="text/javascript">
     $(document).ready(function() {
        $('#usuarioEmail').focus();
     });
</script>
