<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="SCR <img src='/SCR/img/style/next.png'> <span class='titulo-pagina-corrente'>Empresa</span>">
    <n:bean name="empresa">
        <div class="inputWindow">
        	<t:property name="id" renderAs="doubleline" type="hidden" showLabel="false" write="false" label=" "/>
        	<t:tabelaEntrada  colspan="2">
        		<n:panelGrid columns="6">
        			<t:property name="cnpj" class="tootip" title="Somente n�meros" renderAs="doubleline" id="cnpj" colspan="6" style="width:150px;"/>
        			<t:property name="razaoSocial"  renderAs="doubleline" colspan="6" style="width:500px"/>
        			<t:property name="nomeFantasia" renderAs="doubleline" colspan="6" style="width:500px"/>
        			<t:property name="inscricaoMunicipal" renderAs="doubleline" colspan="3" style="width:250px"/>
        			<t:property name="inscricaoEstadual" renderAs="doubleline" colspan="3" style="width:250px"/>
        			<t:property name="email" renderAs="doubleline" colspan="6" style="width:500px" />
        			<t:property name="emailAlternativo" renderAs="doubleline" colspan="6" style="width:500px"/>
        			<t:property name="twitter" renderAs="doubleline" colspan="3" style="width:250px"/>
        			<t:property name="webSite" renderAs="doubleline" colspan="3" style="width:250px"/>
        		</n:panelGrid>
        		<n:panel id="picture" style="text-align:center;">
        			<c:if test="${param.ACAO != 'criar'}">
        				<c:if test="${showpicture}">
        					<n:link url="/DOWNLOADFILE/${empresa.logomarca.cdfile}">
        						<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${empresa.logomarca.cdfile}" id="imagem" class="foto"/>
        					</n:link>
        				</c:if>
        				<c:if test="${!showpicture}">
        					<div class="image">
        						<img src="${pageContext.request.contextPath}/img/imagenotfound.png" id="imagem" style="padding-top:13px;"/>
        					</div>
        				</c:if>
        				<c:if test="${param.ACAO == 'criar' || param.ACAO == 'editar'}">
        					<br>							
        					
        				</c:if>
        			</c:if>
        			<t:property name="logomarca"  id="foto" label="" renderAs="doubleline" onchange="verifyExt();" />
        		</n:panel>
        		<n:panel colspan="2">-<span class="titulo">Endere�o</span></n:panel>
        		<n:panelGrid columns="6" colspan="2">
        			<t:property name="cep" id="cep" renderAs="doubleline" colspan="1" style="width:100px;" onkeypress="formatar_mascara(this,  \"#####-###\");" class="tootip" title="Somente n�meros" onchange="ajaxBuscaCep($('#cep').val())"/>
        			<t:property name="endereco" id="endereco" renderAs="doubleline" colspan="3" style="width:400px;"/>
        			<t:property name="numero" id="numero" renderAs="doubleline" colspan="2"/>
        			<t:property name="complemento" renderAs="doubleline" colspan="2" style="width:100px;"/>
        			<t:property name="bairro" id="bairro" renderAs="doubleline" colspan="1" style="width:200px;"/>
        			<n:comboReloadGroup useAjax="true">
        				<t:property name="uf" id="uf" renderAs="doubleline" colspan="1" />
        				<t:property name="municipio" id="municipio"  renderAs="doubleline" colspan="1"/>
        			</n:comboReloadGroup>
        		</n:panelGrid>
        	</t:tabelaEntrada>
            <div class="actionBar">
                <n:submit action="salvar" validate="true" confirmationScript="${janelaEntradaTag.submitConfirmationScript}" onclick="dialogAguarde();" class="salvar-registro">salvar</n:submit>
            </div>
        </div>
        <br><br>
    </n:bean>
</t:tela>

<script type="text/javascript">
     $(document).ready(function() {
        $('#cnpj').focus();
     });
</script>
