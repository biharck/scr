<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<n:panelGrid columns="4">
				<t:property name="descricao" style="width:495px;" colspan="4"/>
				<t:property name="grupoTipoResiduo"/>
				<t:property name="classeTipoResiduo"/>
				<t:property name="ativo"/>
				<t:property name="reciclavel"/>
			</n:panelGrid>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<t:property name="descricao"/>
			<t:property name="grupoTipoResiduo"/>
			<t:property name="classeTipoResiduo"/>
			<t:property name="ativo" label="Status" trueFalseNullLabels="Ativo,Inativo, "/>
			<t:property name="reciclavel"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
