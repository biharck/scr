<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<n:panelGrid columns="4" >
				<t:property name="pontoEntrega" colspan="2"/>
				<t:property name="idGuia" colspan="2"/>
				<t:property name="dataIni"  colspan="2"/>
				<t:property name="dataFim"  colspan="2"/>
				<t:property name="responsavel" colspan="4" style="width:400px;"/>
			</n:panelGrid>
		</t:tabelaFiltro>
	</t:janelaFiltro>

	<t:janelaResultados>
		<t:tabelaResultados>
			<t:property name="pontoEntrega"/>
			<t:property name="guia"/>
			<t:property name="data"/>
			<t:property name="responsavel" />
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>