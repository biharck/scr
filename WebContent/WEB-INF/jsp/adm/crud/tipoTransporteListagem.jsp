<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<n:panel>
				<n:panelGrid columns="4">
					<t:property name="nome" style="width:400px" colspan="2"/>
					<t:property name="descricao" style="width:400px" colspan="2"/>
					<t:property name="ativo"/>
				</n:panelGrid>
			</n:panel>		
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados >
			<t:property name="nome"/>
			<t:property name="descricao"/>
			<t:property name="capacidade"/>
			<t:property name="unidadeMedida"/>
			<t:property name="ativo" label="Status" trueFalseNullLabels="Ativo,Inativo, "/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
