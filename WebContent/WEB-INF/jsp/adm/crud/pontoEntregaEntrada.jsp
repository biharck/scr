<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
		<t:janelaEntrada submitConfirmationScript="validaIgualdade()">
			<t:tabelaEntrada>
				<n:panel>
					<n:panelGrid columns="4" >
						<c:if test="${param.ACAO != 'consultar'}">
							<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
						</c:if>
						
						<t:property name="identificacao" id="identificacao" colspan="4" renderAs="doubleline"/>
						<t:property name="nome" style="width:400px" colspan="2" renderAs="doubleline" id="nome"/>
					
						<n:panel colspan="2">
							<t:property name="cep" id="cep" onkeypress="formatar_mascara(this,  \"#####-###\");" maxlength="9" renderAs="doubleline" class="tootip" title="Somente n�meros" onchange="ajaxBuscaCep($('#cep').val())"/>
						</n:panel>
						<t:property name="endereco" id="endereco" colspan="2" style="width:400px" renderAs="doubleline"/>
						<t:property name="numero" id="numero" colspan="1" renderAs="doubleline"/>
						
						<t:property name="complemento" renderAs="doubleline"/>
						<t:property name="bairro" id="bairro" renderAs="doubleline"/>
						<n:comboReloadGroup useAjax="true">
							<t:property name="uf" id="uf" renderAs="doubleline"/>
							<t:property name="municipio" id="municipio" renderAs="doubleline"/>
						</n:comboReloadGroup>
						
						<t:property name="telefone1" class="tootip" title="Somente n�meros" renderAs="doubleline"/>
						<t:property name="telefone2" class="tootip" title="Somente n�meros" renderAs="doubleline"/>
						<t:property name="celular" class="tootip" title="Somente n�meros" renderAs="doubleline"/>
						<t:property name="responsavel" colspan="2" renderAs="doubleline" style="width:400px"/>
						<t:property name="email" colspan="2" style="width:400px" renderAs="doubleline" id="email"/>
						<c:if test="${empty pontoEntrega.id}">
							<t:property name="reEmail" colspan="2" style="width:400px" renderAs="doubleline" id="reemail" onchange="validaIgualdadeCampo($('#email'),this,'e-mail')"/>
						</c:if>
						<t:property name="observacoes" type="TEXT_AREA" colspan="2" renderAs="doubleline" cols="50" rows="5"/>
						<c:if test="${!empty pontoEntrega.id}">
							<t:property name="ativo" renderAs="doubleline" />
						</c:if>
					</n:panelGrid>
				</n:panel>				
			</t:tabelaEntrada>
		</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#identificacao').focus();
	 });
	 
	 function validaIgualdade(){
	 	edicao = ${empty pontoEntrega.id};
	 	if(edicao)
	 		return validaIgualdadeCampo($('#email'),$('#reemail'),'e-mail');
	 	else return true;
	 }
</script>