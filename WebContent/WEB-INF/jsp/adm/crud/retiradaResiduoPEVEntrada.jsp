<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panelGrid columns="1">
				<t:property name="id" colspan="1" renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
				<n:group legend="Resumo das credenciais do PEV" columns="1">
					<t:property name="pontoEntrega" id="pontoEntrega" colspan="4"	renderAs="doubleline" onchange="verificaPEV();" />
					<jsp:include page="/WEB-INF/jsp/include/camposCredenciaisPEV.jsp"/>   
				</n:group>
				<n:group legend="Informa��es sobre os Res�duos" columns="1">
					<n:panelGrid columns="4">
					    <t:property name="guia" colspan="1" renderAs="doubleline" selectOnePath="/adm/crud/Guia?showMenu=false"/>
						<t:property name="data" colspan="1" renderAs="doubleline" />
						<t:property name="hora" class="tootip" title="Somente n�meros" colspan="2" renderAs="doubleline" style="width:150px"  type="text" maxlength="5"  onkeypress='formatar_mascara(this, "##:##");' />
						<t:property name="responsavel" colspan="2" renderAs="doubleline" style="width:400px"/>
					</n:panelGrid>
				</n:group>
                <n:group legend="Identifica��o Transportador" columns="1">
                    <t:property name="transportador" id="transportador" colspan="4" renderAs="doubleline" onchange="verifTransportador();" />
                    <jsp:include page="/WEB-INF/jsp/include/camposTransportador.jsp"/>
                </n:group>
                <n:group legend="Tipos de Res�duos" columns="1">
                 <n:panel colspan="4">
                  <t:detalhe name="grupoTipoResiduosRetiradaResiduoPEV" style="width:800px" labelnovalinha="Adicionar Res�duo">
                    <t:property name="grupoTipoResiduo" onchange="buscaTipoResiduoRetirada(this.value,${index})"/>
                    <t:property name="tipoResiduo" />
                   <t:property name="quantidade" type="money" />
                   <t:property name="unidadeMedida" />
                  </t:detalhe>
                 </n:panel>
                </n:group>
			</n:panelGrid>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>	

<script type="text/javascript">
$(document).ready(function() {
    $('#pontoEntrega').focus();
    $("#campos_cred").fadeOut(); 
    
    //consultar --> carrega pelo id, pois combo n�o possui valor
    acao = '${param.ACAO}';
    if(acao == 'consultar'){
        ajaxBuscaTransportador(${retiradaResiduoPEV.transportador.id});
        ajaxBuscaCredenciaisPEV(${retiradaResiduoPEV.pontoEntrega.id});
    }else if(!${empty retiradaResiduoPEV.id}){//edi��o 
        verificaPEV()();
        verifTransportador();
    }   

});

function verificaPEV(){
  tipo = $("#pontoEntrega").val();
  if(tipo == "" || tipo == null || tipo == '<null>'){
     $("#campos_cred").fadeOut();  
     dialog('Aten��o','Voc� deve selecionar a Credencial');
     return false
  }else
    ajaxBuscaCredenciaisPEV(tipo);
}

function verifTransportador() {
     tipo = $("#transportador").val();
     
     if(tipo == "" || tipo == null || tipo == '<null>'){
        $("#campos_transp").fadeOut();  
        dialog('Aten��o','Voc� deve selecionar o Transportador');
        return false
     }else
        ajaxBuscaTransportador(tipo);
}
</script>			