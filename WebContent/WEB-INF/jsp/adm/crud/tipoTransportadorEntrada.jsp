<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
		<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
					<t:property name="nome" colspan="2" style="width:400px" renderAs="doubleline" id="nome"/>
					<t:property name="descricao" colspan="2" style="width:400px" renderAs="doubleline"/>
					<t:property name="enumTipoGerador" colspan="2" renderAs="doubleline"/>
					<t:property name="ativo" renderAs="doubleline"/>
				</n:panelGrid>
			</n:panel>	
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#nome').focus();
	 });
</script>	 