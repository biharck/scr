<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<c:if test="${empty showOnlyMsg}">
	<t:listagem>
		<t:janelaFiltro>
			<t:tabelaFiltro>
				<n:panel>
					<n:panelGrid columns="4">
						<t:property name="razaoSocial" style="width:400px" colspan="4"/>
						<t:property name="nomeFantasia" style="width:400px" colspan="4"/>
						<c:if test="<%=br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaTransportador() %>">
							<t:property name="cpfCnpjTransient" readOnly="readOnly" mode="output"/>
						</c:if>
						<c:if test="<%=!br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaTransportador() %>">
							<t:property name="cpfCnpjTransient" class="tootip" title="Somente n�meros" onkeypress="cpfcnpj(this);" maxlength="18" />
						</c:if>
					</n:panelGrid>
				</n:panel>	
			</t:tabelaFiltro>
		</t:janelaFiltro>
		<t:janelaResultados>
			<t:tabelaResultados>
				<n:column header="CPF/CNPJ">
					<t:property name="cpf"/>
					<t:property name="cnpj"/>
				</n:column>
				<t:property name="razaoSocial"/>
				<t:property name="nomeFantasia"/>
				<n:column header="Endere�o">
					<t:property name="endereco"/>
					<t:property name="complemento"/>
				</n:column>
				<t:property name="telefone1"/>
				<t:property name="validadeCadastro"/>
			</t:tabelaResultados>
		</t:janelaResultados>
	</t:listagem>
</c:if>
<c:if test="${showOnlyMsg}">
	<div align="center">
		<br />
		<br />
		<n:link onclick="javascript:window.parent.closeIframe();">
			<font style="font-size: 16px;">Fechar </font>
		</n:link>
	</div>
</c:if>