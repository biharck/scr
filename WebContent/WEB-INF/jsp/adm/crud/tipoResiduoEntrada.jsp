<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4" >
					<c:if test="${param.ACAO != 'consultar'}">
						<t:property name="id" colspan="4" renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
					</c:if>
					
					<t:property name="descricao" id="descricao" style="width:400px;" colspan="4" renderAs="doubleline"/>
					<n:comboReloadGroup useAjax="true">
						<t:property name="grupoTipoResiduo" colspan="2" renderAs="doubleline"/>
						<t:property name="classeTipoResiduo" colspan="2" renderAs="doubleline"/>
					</n:comboReloadGroup>
					<t:property name="reciclavel"  renderAs="doubleline"/>
					<c:if test="${!empty tipoResiduo.id}">
						<t:property name="ativo"  renderAs="doubleline"/>
					</c:if>
				</n:panelGrid>
			</n:panel>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#descricao').focus();
	 });
</script>
