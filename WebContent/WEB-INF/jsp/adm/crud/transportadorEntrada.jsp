<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<br />
	 <n:panel valign="top" style="padding-top:4px">
	<br />
	 <div id="selecTipoTransportador"></div>
	<br />
	</n:panel>
	<t:janelaEntrada submitConfirmationScript="validaIgualdade()" submitLabel="${submitLabel}">
		<t:tabelaEntrada>
			<t:property name="tipoTransportador" id="tipoTransportador" style="width:400px" colspan="2" renderAs="doubleline" onchange="verifTipoTransportador();"/>
			<n:panel style="display:none;" id="campos">
				<n:panelGrid columns="4" >
				<c:if test="${param.ACAO!='consultar'}">
					<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
				</c:if>
				<input type="hidden" value="${showLinkBar}" name="showLinkBar" />
				<n:panel id="oculta1"  colspan="2"> 
					<t:property name="razaoSocial" id="razaoSocial" style="width:400px" renderAs="doubleline"/>
					<span class=requiredMark>*</span>
				</n:panel>
				<t:property name="nomeFantasia" id="nomeFantasia" style="width:400px" colspan="4" renderAs="doubleline"/>
				<t:property name="cpfCnpjTransient" class="tootip" title="Somente n�meros" id="cpfCnpjTransient" maxlength="18" onkeypress="javascript:cpfcnpj(this)" colspan="1" renderAs="doubleline"/>
				<n:panel id="oculta2"  colspan="1"> 
					<t:property name="inscricaoMunicipal"  id="inscricaoMunicipal"  renderAs="doubleline"/>
					<span class=requiredMark>*</span>
				</n:panel>
				<n:panel id="oculta3"  colspan="1"> 
					<t:property name="inscricaoEstadual" id="inscricaoEstadual" renderAs="doubleline"/>
				</n:panel>  
				<n:panel colspan="1">
					<t:property name="cep" class="tootip" title="Somente n�meros" id="cep" onkeypress="formatar_mascara(this,  \"#####-###\");" maxlength="9" renderAs="doubleline" onchange="ajaxBuscaCep($('#cep').val())"/>
				</n:panel>
				<t:property name="endereco" id="endereco" colspan="2" style="width:400px" renderAs="doubleline"/>
				<t:property name="numero" id="numero" colspan="1" renderAs="doubleline"/>
				<t:property name="complemento" colspan="1" renderAs="doubleline"/>
				<t:property name="bairro" id="bairro" colspan="1" renderAs="doubleline"/>
				<n:comboReloadGroup useAjax="true">
					<t:property name="uf" id="uf" colspan="1" renderAs="doubleline" />
					<t:property name="municipio" id="municipio" colspan="1" renderAs="doubleline"/>
				</n:comboReloadGroup>
				<t:property name="validadeCadastro" id="validadeCadastro" colspan="1" renderAs="doubleline" class="tootip" title="A data de validade deve ser superior a data atual." />
				<t:property name="telefone1" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline"/>
				<t:property name="telefone2" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline"/>
				<t:property name="celular" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline"/>
				<n:panel id="oculta4"  colspan="1">    
					<t:property name="responsavel" id="responsavel"  renderAs="doubleline"/>
					<span class=requiredMark>*</span>
				</n:panel>
				<t:property name="email" colspan="2" renderAs="doubleline" style="width:400px" id="email"/>
				<c:if test="${empty transportador.id}">
					<t:property name="reEmail" id="reemail" colspan="2" renderAs="doubleline" style="width:400px" onchange="validaIgualdadeCampo($('#email'),this,'e-mail')"/>
				</c:if>
				<t:property name="observacoes" type="TEXT_AREA" colspan="2" renderAs="doubleline" cols="50" rows="5"/>
				<c:if test="${!empty transportador.id}">
					<t:property name="ativo" colspan="4" renderAs="doubleline"/>
				</c:if>
				<c:if test="${empty transportador.id}">
					<n:panel colspan="4"></n:panel>
				</c:if>
				<n:panel id="oculta5"  colspan="1"> 
					<n:group legend="Credenciais PEV" >
						<n:dataGrid itemType="br.com.biharckgroup.scr.bean.PontoEntrega" var="cPEV" itens="${listaPEV}" bodyStyleClasses="," styleClass=",">
							<n:column width="20">
								<c:if test="${param.ACAO == 'consultar'}">
									<n:input name="credenciaisPEV" disabled="disabled" value="${cPEV}" type="checklist" itens="${transportador.credenciaisPEV}"/>
								</c:if>
								<c:if test="${param.ACAO != 'consultar'}">
									<n:input name="credenciaisPEV" value="${cPEV}" type="checklist" itens="${transportador.credenciaisPEV}"/>
								</c:if>
							</n:column>
							<n:column>
								<a href="javascript:void(0)" class="tootip-center" title="${cPEV.endereco},${cPEV.numero} ${cPEV.bairro}" >${cPEV.nome}</a> 
							</n:column>
						</n:dataGrid>
					</n:group>
				</n:panel>
				<c:if test="${empty showLinkBar}">
					<n:group legend="Usu�rios" >
						<n:dataGrid itemType="br.com.biharckgroup.scr.bean.Usuario" itens="${listaUsuario}" bodyStyleClasses="," styleClass=",">
							<n:column width="20">
							<c:if test="${param.ACAO != 'consultar'}">
								<n:input name="usuarios"  value="${row}" type="checklist" itens="${transportador.usuarios}"/>
							</c:if>
							<c:if test="${param.ACAO == 'consultar'}">
								<n:input name="usuarios"  value="${row}" type="checklist" itens="${transportador.usuarios}" disabled="disabled"/>
							</c:if>
							</n:column>
							<n:column>
								<t:property name="nome" mode="output"/>
							</n:column>
						</n:dataGrid>
					</n:group>
				</c:if>
				</n:panelGrid>
			</n:panel>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
$(document).ready(function() {
    $('#tipoTransportador').focus();
    if(!${empty transportador.id}){
    	 verifTipoTransportador();
    }
});

function verifTipoTransportador() {
  tipo = $("#tipoTransportador").val();
  
  acao = '${param.ACAO}';

  if(acao == 'consultar'){
	    ajaxBuscaPorteTransportador(${transportador.tipoTransportador.id});
	 	return ;
  }
  
  if(tipo == "" || tipo == null || tipo == '<null>'){
   $("#campos").fadeOut();
   dialog('Aten��o','Voc� deve selecionar o porte do transportador');
     $("#selecTipoTransportador").html("");  
        return false
  }else
   	ajaxBuscaPorteTransportador(tipo);
}

function validaIgualdade(){
  edicao = ${empty transportador.id};
  if(edicao)
   return validaIgualdadeCampo($('#email'),$('#reemail'),'e-mail');
  else return true;
 }
 
</script>