<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
					<t:property name="transportador" colspan="2" renderAs="doubleline" id="transportador"/>
					<t:property name="credenciaisPEV" colspan="2" renderAs="doubleline"/>
				</n:panelGrid>
			</n:panel>	
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#transportador').focus();
	 });
</script>	 