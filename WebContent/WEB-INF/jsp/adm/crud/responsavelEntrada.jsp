<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
    <t:janelaEntrada submitConfirmationScript="validaForm">
        <t:tabelaEntrada>
            <n:panel>
                <n:panelGrid columns="4" >
                    <t:property name="id" colspan="4" renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
                    <t:property name="nome" id="nome" style="width:400px" colspan="2" renderAs="doubleline"/>
                    <n:panel colspan="2">
                        <t:property name="cep" id="cep" maxlength="9" onkeypress="formatar_mascara(this,  \"#####-###\");" renderAs="doubleline" class="tootip" title="Somente n�meros" onchange="ajaxBuscaCep($('#cep').val())"/>
                    </n:panel>
                    <t:property name="endereco" id="endereco" colspan="2" style="width:400px" renderAs="doubleline"/>
                    <t:property name="numero" id="numero" colspan="1" renderAs="doubleline"/>
                    <t:property name="complemento" colspan="1" renderAs="doubleline"/>
                    <t:property name="bairro" id="bairro" colspan="1" renderAs="doubleline"/>
                    <n:comboReloadGroup useAjax="true">
                        <t:property name="uf" id="uf"colspan="1" renderAs="doubleline"/>
                        <t:property name="municipio" id="municipio" colspan="1" renderAs="doubleline"/>
                    </n:comboReloadGroup>
                    <t:property name="telefone1" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline"/>
                    <t:property name="celular" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline"/>
                    <t:property name="crea"  colspan="1" renderAs="doubleline"/>                                            
                    <t:property name="cpfCnpjTransient" class="tootip" title="Somente n�meros" maxlength="18" onkeypress="javascript:cpfcnpj(this)" colspan="1" renderAs="doubleline"/>
                	<t:property name="grandeGerador" colspan="1" renderAs="doubleline" readonly="readonly" disabled="true"/>
                	<t:property name="email" colspan="2" style="width:400px" renderAs="doubleline" id="email"/>
					<c:if test="${empty responsavel.id}">
						<t:property name="reEmail" colspan="2" style="width:400px" renderAs="doubleline" id="reemail" onchange="validaIgualdadeCampo($('#email'),this,'e-mail')"/>
					</c:if>
                </n:panelGrid>  
            </n:panel>
        </t:tabelaEntrada>  
    </t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
$(document).ready(function() {
    $('#nome').focus();
});

function validaForm(){
    edicao = ${empty usuario.id};
 	if(edicao){
 		return(validaIgualdadeCampo($('#email'),$('#reemail'),'e-mail'));
 	}else return true;
 }
</script>