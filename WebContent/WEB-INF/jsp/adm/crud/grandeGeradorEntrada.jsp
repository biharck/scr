<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>


<t:entrada>
		<t:janelaEntrada submitConfirmationScript="validaIgualdade()">
			<t:tabelaEntrada>
				<n:panel>
					<n:panelGrid columns="4" >
						<c:if test="${param.ACAO != 'consultar'}">
							<t:property name="id" colspan="4" renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
						</c:if>
						
						<t:property name="razaoSocial" id="razaoSocial" style="width:400px" colspan="2" renderAs="doubleline"/>
						<t:property name="nomeFantasia" id="nomeFantasia" style="width:400px" colspan="4" renderAs="doubleline"/>

						<t:property name="cpfCnpjTransient" class="tootip" title="Somente n�meros" maxlength="18" onkeypress="javascript:cpfcnpj(this)" colspan="1" renderAs="doubleline"/>
						<t:property name="inscricaoMunicipal" colspan="1" renderAs="doubleline"/>
						<t:property name="inscricaoEstadual" colspan="1" renderAs="doubleline"/>
						
						<n:panel colspan="1">
							<t:property name="cep" id="cep" maxlength="9" onkeypress="formatar_mascara(this,  \"#####-###\");" renderAs="doubleline" class="tootip" title="Somente n�meros" onchange="ajaxBuscaCep($('#cep').val())"/>
						</n:panel>
						<t:property name="endereco" id="endereco" colspan="2" style="width:400px" renderAs="doubleline"/>
						<t:property name="numero" id="numero" colspan="1" renderAs="doubleline"/>
						
						<t:property name="complemento" colspan="1" renderAs="doubleline"/>
						<t:property name="bairro" id="bairro" colspan="1" renderAs="doubleline"/>
						<n:comboReloadGroup useAjax="true">
							<t:property name="uf" id="uf" colspan="1" renderAs="doubleline"/>
							<t:property name="municipio" id="municipio" colspan="1" renderAs="doubleline"/>
						</n:comboReloadGroup>

						<n:panel colspan="1"></n:panel>						
						<t:property name="telefone1" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline"/>
						<t:property name="telefone2" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline"/>
						<t:property name="celular" class="tootip" title="Somente n�meros" colspan="2" renderAs="doubleline"/>
						<t:property name="responsavel" id="responsavel" colspan="4" renderAs="doubleline" style="width:400px"/>
						
                        <t:property name="email" colspan="2" style="width:400px" renderAs="doubleline" id="email"/>
						<c:if test="${empty grandeGerador.id}">
							<t:property name="reEmail" colspan="2" style="width:400px" renderAs="doubleline" id="reemail" onchange="validaIgualdadeCampo($('#email'),this,'e-mail')"/>
						</c:if>
						
						<t:property name="observacoes" type="TEXT_AREA" colspan="2" renderAs="doubleline" cols="50" rows="5"/>

						<c:if test="${empty grandeGerador.id}">
							<n:panel colspan="2"></n:panel>
						</c:if>
						
                        <n:group legend="Usu�rios">
	                        <n:dataGrid itemType="br.com.biharckgroup.scr.bean.Usuario" itens="${listaUsuario}" bodyStyleClasses="," styleClass=",">
	                            <n:column width="20">
	                                <c:if test="${param.ACAO != 'consultar'}">
	                                    <n:input name="usuarios"  value="${row}" type="checklist" itens="${grandeGerador.usuarios}"/>
	                                </c:if>
	                                <c:if test="${param.ACAO == 'consultar'}">
	                                    <n:input name="usuarios"  value="${row}" type="checklist" itens="${grandeGerador.usuarios}" disabled="disabled"/>
	                                </c:if>
	                            </n:column>
	                            <n:column>
	                                <t:property name="nome" mode="output"/>
	                            </n:column>
	                        </n:dataGrid>
	                    </n:group>
						<c:if test="${!empty grandeGerador.id}">
							<t:property name="ativo" renderAs="doubleline" colspan="2"/>
						</c:if>
					</n:panelGrid>	
				</n:panel>
			</t:tabelaEntrada>	
		</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
$(document).ready(function() {
	$('#razaoSocial').focus();
});

function validaIgualdade(){
 	edicao = ${empty grandeGerador.id};
 	if(edicao)
 		return validaIgualdadeCampo($('#email'),$('#reemail'),'e-mail');
 	else return true;
 }

</script>