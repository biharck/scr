<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<t:property name="nome" style="width:400px;"/>
			<t:property name="ativo"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados showEditarLink="${empty param.hideOptions}" showExcluirLink="${empty param.hideOptions}">
			<t:property name="nome"/>
			<t:property name="grupoTipoResiduo"/>
			<t:property name="ativo" label="Status" trueFalseNullLabels="Ativo,Inativo, "/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>