<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<t:property name="identificacao" style="width:400px"/>
			<t:property name="responsavel" style="width:400px"/>
			<t:property name="email" style="width:400px"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
    <t:janelaResultados>
        <t:tabelaResultados showEditarLink="${empty param.hideOptions}" showExcluirLink="${empty param.hideOptions}">
			<t:property name="identificacao"/>
			<t:property name="responsavel"/>
			<t:property name="endereco"/>
			<t:property name="numero"/>
			<t:property name="bairro"/>
			<t:property name="telefone1"/>
			<t:acao>
				<a href="javascript:dialog();">Abrir Mapa </a>
			</t:acao>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>

<div id="dialog_map" title="Localização">	
	Mapa...
</div>

<script type="text/javascript">
	
	function dialog(){		
		$('#dialog_map').dialog('open');
	}
	
	$(function() {
		$("#dialog_map").dialog({
			bgiframe: true,
			autoOpen: false,
			height: 600,
			width: 800,
			modal: true,
		});
	});
	
	
</script>