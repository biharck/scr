<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4" >
					<c:if test="${param.ACAO != 'consultar'}">
						<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
					</c:if>
					<t:property name="nome" style="width:400px" colspan="2" renderAs="doubleline" id="nome"/>
					<t:property name="descricao" style="width:400px" colspan="2" renderAs="doubleline"/>
					<t:property name="capacidade" colspan="1" renderAs="doubleline"/>
					<t:property name="unidadeMedida" colspan="1" renderAs="doubleline"/>
					<c:if test="${!empty tipoTransporte.id}">
						<t:property name="ativo" renderAs="doubleline" colspan="2"/>
					</c:if>
				</n:panelGrid>
			</n:panel>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#nome').focus();
	 });
</script>	 