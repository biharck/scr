<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<c:if test="${empty showOnlyMsg}">
	<t:listagem>
		<t:janelaFiltro>
			<t:tabelaFiltro>
				<n:panel>
					<n:panelGrid columns="6" >
						<t:property name="razaoSocial" style="width:418px" colspan="6"/>
						<t:property name="nomeFantasia"style="width:418px" colspan="6"/>


						<c:if test="<%=br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaDestinoFinal() %>">
							<t:property name="cnpj" readOnly="readOnly" mode="output"/>
						</c:if>
						<c:if test="<%=!br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaDestinoFinal() %>">
							<t:property name="cnpj" class="tootip" title="Somente n�meros" onkeypress="formatar_mascara(this, \"##.###.###/####-##\");"  maxlength="18" label="CNPJ"/>
						</c:if>


						<t:property name="tipoDestino"/>
					</n:panelGrid>
				</n:panel>
			</t:tabelaFiltro>
		</t:janelaFiltro>
		<t:janelaResultados>
			<t:tabelaResultados>
				<n:column header="CNPJ">
					<t:property name="cnpj" />
				</n:column>
				<t:property name="razaoSocial"/>
				<t:property name="nomeFantasia"/>
				<n:column header="Endere�o">
					<t:property name="endereco"/>
					<t:property name="complemento"/>
				</n:column>
				<t:property name="telefone1"/>
				<t:property name="tipoDestino"/>
			</t:tabelaResultados>
		</t:janelaResultados>
	</t:listagem>
</c:if>
<c:if test="${showOnlyMsg}">
	<div align="center">
		<br />
		<br />
		<n:link onclick="javascript:window.parent.closeIframe();">
			<font style="font-size: 16px;">Fechar </font>
		</n:link>
	</div>
</c:if>