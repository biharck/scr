<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panelGrid columns="1">
				<t:property name="id" colspan="1" renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
				<n:group legend="Resumo das credenciais do PEV" columns="1">
					<t:property name="pontoEntrega" id="pontoEntrega" colspan="4"	renderAs="doubleline" onchange="verifaPEV();" />
					<jsp:include page="/WEB-INF/jsp/include/camposCredenciaisPEV.jsp"/> 
				</n:group>
				<n:group legend="Informa��es sobre os Res�duos" columns="1">
					<n:panelGrid columns="4">
					
						<t:property name="guia" colspan="1" renderAs="doubleline" selectOnePath="/adm/crud/Guia?showMenu=false"/>
						<t:property name="data" colspan="1" renderAs="doubleline" />
						<t:property name="hora" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline" style="width:150px" type="text" maxlength="5"  onkeypress='formatar_mascara(this, "##:##");' />
                        <t:property name="agenteEntradaResiduo" colspan="1" renderAs="doubleline" />
						<t:property name="responsavel" colspan="2" renderAs="doubleline" style="width:400px"/>
						<t:property name="bairroOrigem" colspan="2" style="width:300px;" renderAs="doubleline" />
					</n:panelGrid>
				</n:group>
                <n:group legend="Tipos de Res�duos" columns="1">
                 <n:panel colspan="4"><span class="requiredMark">*</span>
                  <t:detalhe name="grupoTipoResiduosEntradaResiduoPEV" style="width:800px" labelnovalinha="Adicionar Res�duo" >
                    <t:property name="grupoTipoResiduo" onchange="buscaTipoResiduo(this.value,${index})"/>
                    <t:property name="tipoResiduo" />
                   <t:property name="quantidade" type="money" />
                   <t:property name="unidadeMedida" />
                  </t:detalhe>
                 </n:panel>
                </n:group>
			</n:panelGrid>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>	

<script type="text/javascript">
$(document).ready(function() {
    $('#pontoEntrega').focus();
    $("#campos_cred").fadeOut(); 
    
    //consultar --> carrega pelo id, pois combo n�o possui valor
    acao = '${param.ACAO}';
    if(acao == 'consultar'){
        ajaxBuscaCredenciaisPEV(${entradaResiduoPEV.pontoEntrega.id});
    }else if(!${empty entradaResiduoPEV.id}){//edi��o 
        verifaPEV();
    }   

});

function verifaPEV() {
  tipo = $("#pontoEntrega").val();

  if(tipo == "" || tipo == null || tipo == '<null>'){
      $("#campos_cred").fadeOut();  
        dialog('Aten��o','Voc� deve selecionar uma Credencial!');
   return false
  }else
   ajaxBuscaCredenciaisPEV(tipo);
}

</script>			