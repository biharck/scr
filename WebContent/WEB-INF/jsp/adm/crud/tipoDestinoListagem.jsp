<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<t:property name="nome" style="width:418px"/>
			<t:property name="ativo"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados >
			<t:property name="nome"/>
			<t:property name="ativo" label="Status" trueFalseNullLabels="Ativo,Inativo, "/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
