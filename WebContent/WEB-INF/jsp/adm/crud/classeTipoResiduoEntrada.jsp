<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4"> 
					<t:property name="id" colspan="4" renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
					<t:property name="nome" id="nome" style="width:400px;" colspan="4" renderAs="doubleline"/>
					<t:property name="grupoTipoResiduo" colspan="2" renderAs="doubleline" />
					<c:if test="${!empty classeTipoResiduo.id}">
						<t:property name="ativo" colspan="2" renderAs="doubleline"/>
					</c:if>
				</n:panelGrid>
			</n:panel>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#nome').focus();
	 });
</script>