<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:panel>
				<n:panelGrid columns="4"> 
					<c:if test="${param.ACAO != 'consultar'}">
						<t:property name="id" colspan="4" renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
					</c:if>
					<t:property name="nome" id="nome" style="width:400px;" renderAs="doubleline"/>
					<c:if test="${!empty grupoTipoResiduo.id}">
						<t:property name="ativo" renderAs="doubleline"/>
					</c:if>
				</n:panelGrid>
			</n:panel>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#nome').focus();
	 });
</script>