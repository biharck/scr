<%@page import="br.com.biharckgroup.scr.util.SCRUtil"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:listagem showNewLink="<%=br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaSuperUser() %>">
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<n:panel>
				<n:panelGrid columns="4">
					<t:property name="nome" style="width:400px;" colspan="2"/>
					<t:property name="ativo" colspan="2"/>
					
					<c:if test="<%=br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaSuperUser() %>">
						<n:panel valign="top" style="padding-top:4px">Papeis</n:panel>
						<n:dataGrid itemType="br.com.biharckgroup.scr.bean.Papel" itens="${listaPapel}" bodyStyleClasses="," styleClass=",">
							<n:column width="20">
			                    <n:input name="papeis" value="${row}" type="checklist" itens="${usuario.papeis}"/>
			                </n:column>
			                <n:column>
				                <t:property name="nome" mode="output" label=" "/>
			                </n:column>
						</n:dataGrid>
					</c:if>
				</n:panelGrid>
			</n:panel>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados showExcluirLink="false" showEditarLink="<%=br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaSuperUser() %>">
			<t:property name="nome"/>
			<t:property name="login"/>
			<t:property name="ativo" label="Status" trueFalseNullLabels="Ativo,Inativo, "/>
			<n:column header="Papel">
	            <c:forEach items="${usuario.papeisUsuario}" var="bean">
	            	<c:out value="${bean.papel.nome}"></c:out><br/>
	            </c:forEach>
			</n:column>
			<t:property name="superUsuario" />
			<t:property name="dtUltimoLogin" />
			<t:acao>
				&nbsp;
			</t:acao>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>

