<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:listagem>
<t:janelaFiltro>
        <t:tabelaFiltro>
            <t:property name="nome" style="width:400px;"/>
            <t:property name="grandeGerador" readonly="readonly" disabled="true"/>
        </t:tabelaFiltro>
    </t:janelaFiltro>
    <t:janelaResultados>
        <t:tabelaResultados showEditarLink="${empty param.hideOptions}" showExcluirLink="${empty param.hideOptions}">
            <t:property name="grandeGerador"/>
            <n:column header="CPF/CNPJ">
                <t:property name="cpf"/>
                <t:property name="cnpj"/>
            </n:column>
            <t:property name="nome"/>
            <n:column header="Endere�o">
                <t:property name="endereco"/>
                <t:property name="complemento"/>
            </n:column>
            <t:property name="telefone1"/>
        </t:tabelaResultados>
    </t:janelaResultados>
</t:listagem>