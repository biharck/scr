<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<h3>P�gina Principal</h3>
<br>
<div class="ultimaalteracao">
	<h6 class="last-login">
		Bem Vindo: 
		
		<font style="color:#E77272">
			<c:out value="${nomeExibicao}"/>
		</font> 
		
		seu �ltimo login foi dia 
		
		<font style="color:#E77272">
			<c:out value="${dtUltimoLogin}"/>
		</font>
	</h6>
	<br />
	<br />
	
	<c:if test="${cadAguardandoAprov}">
		<center>
			<div style="width:50%; text-align: left;" class="p_dialog p_dialog_error" onclick="location.href='/SCR/adm/process/AguardandoAprovacao'">
				<p>Cadastros</p>
				<p class="p_dialog_sub p_dialog_sub_error">
					Existem cadastros aguardando sua aprova��o
				</p>
			</div>
		</center>
	</c:if>
	<br/>
	<c:if test="<%=br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaGrandeGerador() %>">
		<center>
			<div style="width:50%; text-align: left;" class="p_dialog p_dialog_error" onclick="location.href='/SCR/adm/crud/Guia?guiaStatus=0'">
				<p>Guias </p>
				<p class="p_dialog_sub p_dialog_sub_error">
					<c:if test="${guiasGG > 0}">
						Existe ${guiasGG} guia(s) para o Grande Gerador ${nomeGG} com status Pendente.
					</c:if>
					<c:if test="${guiasGG == 0}">
						N�o h� guias para o Grande Gerador ${nomeGG} com status Pendente.
					</c:if>
				</p>
			</div>
		</center>
	</c:if>
	<br/>
	<c:if test="<%=br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaDestinoFinal() %>">
		<center>
			<div style="width:50%; text-align: left; height: 9.6em; cursor: auto;" class="p_dialog p_dialog_error" >
				<p>Guias</p>
				<p class="p_dialog_sub p_dialog_sub_error" onclick="location.href='/SCR/destinoFinal/process/RecebimentoGuia'" style="cursor:pointer;" >
					Existe ${guiasTotDF} guia(s) encaminhadas para o Destino Final ${nomeDF} com status Pendente.
				</p>
				<p>Projetos</p>
				<p class="p_dialog_sub p_dialog_sub_error" onclick="location.href='/SCR/grandeGerador/crud/Projeto?df=1'" style="cursor:pointer;">
					Existe ${projetosTotDF} projeto(s) de gerenciamento cadastrados para o Destino Final ${nomeDF}.
				</p>
			</div>
			<br/>
			<div style="width:50%; text-align: left; height: 12.6em; cursor: auto;" class="p_dialog p_dialog_error">
				<p>Estimativa de res�duos a serem recebidos no dia ${dataDia}</p>
				<p>Guias</p>
				<p class="p_dialog_sub p_dialog_sub_error" onclick="location.href='/SCR/destinoFinal/crud/GuiaDestinoFinal?date=1'" style="cursor:pointer;" >
					Existe ${guiasDiaDF} guia(s) criadas para o Destino Final ${nomeDF}, totalizando ${somaGuiasDiaDF} m�.
				</p>
				<p>Projetos</p>
				<p class="p_dialog_sub p_dialog_sub_error" onclick="location.href='/SCR/grandeGerador/crud/Projeto?df=1&date=1'" style="cursor:pointer;">
					Existe ${projetosDiaDF} estimativa(s) de projeto(s) de gerenciamento para o Destino Final ${nomeDF}, totalizando ${somaProjetosDiaDF} m�.
				</p>
			</div>
		</center>
	</c:if>
</div>