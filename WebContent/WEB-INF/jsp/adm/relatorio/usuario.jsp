<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:relatorio titulo="Relat�rio <img src='/SCR/img/style/next.png'><span class='titulo-pagina-corrente'> Rela��o de Usu�rios </span>">
 <t:janelaFiltro>
     <t:tabelaFiltro submitAction="filtroUsuario" >
         <t:property name="ativo"/>
         <n:panel valign="top" style="padding-top:4px">Papeis</n:panel>
			<n:dataGrid itemType="br.com.biharckgroup.scr.bean.Papel" itens="${listaPapel}" bodyStyleClasses="," styleClass=",">
				<n:column width="20">
                    <n:input name="papeis" value="${row}" type="checklist" itens="${usuario.papeis}"/>
                </n:column>
                <n:column>
	                <t:property name="nome" mode="output" label=" "/>
                </n:column>
			</n:dataGrid>
     </t:tabelaFiltro>
     <n:submit action="gerar" >Gerar relat�rio</n:submit>
 </t:janelaFiltro>
</t:relatorio>
<n:dataGrid itens="usuarios" itemType="br.com.biharckgroup.scr.bean.Usuario" var="usuario"
	cellspacing="0"
		rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
		rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
		id="tabelaResultados" varIndex="index">
    <t:property name="nome"/>
    <t:property name="login"/>
    <t:property name="email"/>
    <n:column header="Papel">
        <c:forEach items="${usuario.papeisUsuario}" var="bean">
            <c:out value="${bean.papel.nome}"></c:out><br/>
        </c:forEach>
    </n:column>
    <t:property name="ativo" label="Status" trueFalseNullLabels="Ativo,Inativo, "/>
    <t:property name="dtUltimoLogin" />
</n:dataGrid>
