<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:relatorio titulo="Relat�rio <img src='/SCR/img/style/next.png'><span class='titulo-pagina-corrente'> Res�duos Recebidos por Guias </span>">
 <t:janelaFiltro>
     <t:tabelaFiltro submitAction="filtroResiduosRecebidos" >
     	 <n:panelGrid  columns="4">
	        	<t:property name="dataIni"  colspan="2"/>
				<t:property name="dataFim"  colspan="2"/>
				<n:comboReloadGroup useAjax="true">
					<t:property name="uf" id="uf" />
					<t:property name="municipio" id="municipio"/>
				</n:comboReloadGroup>
				<t:property name="destinoFinal" colspan="4"/>
     	 </n:panelGrid>
     </t:tabelaFiltro>
     <n:submit action="gerar" >Gerar relat�rio</n:submit>
 </t:janelaFiltro>
</t:relatorio>
<c:set var="dataAnt" value="null"/>
<n:dataGrid  itens="residuoRecebidoQuantidades" itemType="br.com.biharckgroup.scr.bean.ResiduoRecebidoQuantidade" var="residuoRecebidoQuantidade"
		cellspacing="1"
		rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
		rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
		id="tabelaResultados" varIndex="index">
		
		<t:property name="data"/>
		
		<c:set var="i" value="0"/>
		<c:set var="totData" value="0"/>
		<c:set var="qtd" value="0"/>
		
		<c:forEach items="${tipoResiduos}" var="tipoResiduo">
			<n:column header="${tipoResiduo.descricao }">
				<c:set var="qtd" value="${residuoRecebidoQuantidade.vetorQuantidade[i]}"></c:set>
				<c:out value="${qtd}"/>
				<c:set var="i" value="${i+1}"></c:set>
				<c:set var="totData" value="${totData + qtd}"/>
			</n:column>
		</c:forEach>

		<n:column header="Total" >
<%-- 		<fmt:formatNumber var="${totData}" pattern=".000"/> --%>
			<c:out value="${totData}"></c:out>
		</n:column>

</n:dataGrid>
