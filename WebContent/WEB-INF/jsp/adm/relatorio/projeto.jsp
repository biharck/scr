<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:relatorio titulo="Relat�rio <img src='/SCR/img/style/next.png'><span class='titulo-pagina-corrente'> Rela��o de Projetos </span>">
 <t:janelaFiltro>
     <t:tabelaFiltro submitAction="filtroProjeto" >
     	 <n:panelGrid  columns="4">
	         <t:property name="dataInicio" colspan="2"/>
    	     <t:property name="dataTermino" colspan="2"/>
    	     <t:property name="enumStatusProjeto" colspan="2"/>
    	     <t:property name="transportador" colspan="2"/>
     	 </n:panelGrid>
     </t:tabelaFiltro>
     <n:submit action="gerar" >Gerar relat�rio</n:submit>
 </t:janelaFiltro>
</t:relatorio>
<n:dataGrid itens="projetos" itemType="br.com.biharckgroup.scr.bean.Projeto" var="projeto" 
		cellspacing="0"
		rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
		rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
		id="tabelaResultados" varIndex="index">
    <t:property name="numeroProjeto"/>
    <t:property name="numeroProcesso"/>
    <t:property name="dataInicio"/>
    <t:property name="dataTermino"/>
    <n:column header="Endere�o da Obra">
        <t:property name="enderecoObraCompleto"/>
    </n:column>
        <n:column header="Res�duos">
            <c:forEach items="${projeto.gruposTipoResiduoProjeto}" var="bean">
                <c:out value="${bean.grupoTipoResiduo.nome}"></c:out><br/>
            </c:forEach>
        </n:column>
        <n:column header="Transportadores">
        <c:forEach items="${projeto.transportadoresProjeto}" var="bean">
            <c:out value="${bean.transportador.nomeFantasia}"></c:out><br/>
        </c:forEach>
        </n:column>
        <n:column header="Respons�veis">
            <c:forEach items="${projeto.responsaveisProjeto}" var="bean">
                <c:out value="${bean.responsavel.nome}"></c:out><br/>
            </c:forEach>
        </n:column>
        <n:column header="A��o">
			<n:link url="/adm/relatorio/ProjetoCompleto" action="gerar" parameters="idProjeto=${projeto.id}" class="outterTableHeaderLink">
				<div class="tootip" title="Relat�rio com todas informa��es do Projeto">Imprimir </div>
			</n:link>
		</n:column>	
</n:dataGrid>
