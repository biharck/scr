<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:relatorio titulo="Relat�rio <img src='/SCR/img/style/next.png'><span class='titulo-pagina-corrente'> Entrada de Res�duos no PEV </span>">
 <t:janelaFiltro>
     <t:tabelaFiltro submitAction="filtroEntradaResiduoPEV" >
     	 <n:panelGrid  columns="4">
				<t:property name="pontoEntrega" colspan="4"/>
	        	<t:property name="dataIni"  colspan="2"/>
				<t:property name="dataFim"  colspan="2"/>
     	 </n:panelGrid>
     </t:tabelaFiltro>
     <n:submit action="gerar" >Gerar relat�rio</n:submit>
 </t:janelaFiltro>
</t:relatorio>
<n:dataGrid itens="entradasResiduoPEV" itemType="br.com.biharckgroup.scr.bean.EntradaResiduoPEV" var="entradaResiduoPEV"
	cellspacing="0"
		rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
		rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
		id="tabelaResultados" varIndex="index">
    <t:property name="pontoEntrega"/>
	<t:property name="data"/>
	<t:property name="hora"/>
	<t:property name="pontoEntrega.responsavel" label="Respons�vel pelo Res�duo"/>
	<t:property name="transportador" label="Agente Entrada Res�duo" trueFalseNullLabels="Transportador, Grande Gerador, "/>
	<t:property name="responsavel" />
	<t:property name="bairroOrigem"/>
    <n:column header="Res�duos">
        <c:forEach items="${entradaResiduoPEV.grupoTipoResiduosEntradaResiduoPEV}" var="bean">
            <c:out value="${bean.tipoResiduo.descricao}"/> -
            <c:out value="${bean.quantidade}"/>
            <c:out value="${bean.unidadeMedida.nome}"/><br/>
        </c:forEach>
    </n:column>
</n:dataGrid>
