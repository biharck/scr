<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:relatorio titulo="Relat�rio <img src='/SCR/img/style/next.png'><span class='titulo-pagina-corrente'> Rela��o de Guias </span>">
 <t:janelaFiltro>
     <t:tabelaFiltro submitAction="filtroGuia" >
     	 <n:panelGrid  columns="8">
            <t:property name="numeroGuia" colspan="2"/>
            <t:property name="numeroProjeto" colspan="2"/>
            <t:property name="enderecoObra" colspan="4" style="width:450px"/>
			<t:property name="situacao" colspan="2"/>
			<t:property name="status" colspan="2" />
	        <t:property name="dataIni" colspan="2"/>
            <t:property name="dataFim" colspan="2"/>
			<c:choose>
               <c:when test="<%=br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaDestinoFinal() %>">
            		<t:property name="destinoFinal" style="width:200px" colspan="2" readonly="readonly" disabled="true"/>
               </c:when>
               <c:otherwise>
                  	<t:property name="destinoFinal" style="width:200px" colspan="2"/>
               </c:otherwise>
            </c:choose>
			<c:choose>
               <c:when test="<%=br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaTransportador() %>">
            		<t:property name="transportador" style="width:200px" colspan="2"  readonly="readonly" disabled="true"/>
               </c:when>
               <c:otherwise>
                  	<t:property name="transportador" style="width:200px" colspan="2" />
               </c:otherwise>
            </c:choose>
			<c:choose>
               <c:when test="<%=br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaGrandeGerador() %>">
            		<t:property name="grandeGerador" style="width:200px" colspan="4" readonly="readonly" disabled="true"/>
               </c:when>
               <c:otherwise>
                  	<t:property name="grandeGerador" style="width:200px" colspan="4" />
               </c:otherwise>
            </c:choose>
     	 </n:panelGrid>
     </t:tabelaFiltro>
     <n:submit action="gerar" >Gerar relat�rio</n:submit>
 </t:janelaFiltro>
</t:relatorio>
<n:dataGrid itens="guias" itemType="br.com.biharckgroup.scr.bean.Guia" var="guia"
		cellspacing="0"
		rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
		rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
		id="tabelaResultados" >
    <t:property name="numeroGuia" label="N� Guia"/>
    <t:property name="projeto.numeroProjeto" label="N� Projeto"/>
	<t:property name="grandeGerador"/>
	<t:property name="tipoTransporte"/>
	<t:property name="timeInc" label="Data do Registro"/>
	<t:property name="dataEntrega"/>
	<t:property name="dataRetirada"/>
	<t:property name="placa"/>
	<t:property name="destinoFinal"/>
	<t:property name="enumSituacao"/>
	<t:property name="status" trueFalseNullLabels="Confirmada,Pendente, "/>
	<n:column header="Res�duos">
       <c:forEach items="${guia.residuosGuia}" var="bean">
           <c:out value="${bean.tipoResiduo.descricao}"/> -
           <c:out value="${bean.quantidade}"/>
           <c:out value="${bean.unidadeMedida.nome}"/><br/>
       </c:forEach>
    </n:column>
	<n:column header="A��o">
		<n:link url="/adm/relatorio/GuiaCompleta" action="gerar" parameters="idGuia=${guia.id};rel=GuiaCompleta" class="outterTableHeaderLink">
			<div class="tootip" title="Relat�rio com todas informa��es da Guia">Imprimir </div>
		</n:link>
		<n:link url="/adm/relatorio/GuiaCompleta" action="gerar" parameters="idGuia=${guia.id};rel=CTR" class="outterTableHeaderLink">
			<div class="tootip" title="Documento de Controle de Transporte de Res�duos">Gerar CTR </div>
		</n:link>
	</n:column>	
</n:dataGrid>
