<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<c:if test="${empty showOnlyMsg}">
<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<n:panel>
				<n:panelGrid columns="4" >
					<t:property name="nome" style="width:400px" colspan="4"/>

					<c:if test="<%=br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaPequenoGerador() %>">
						<t:property name="cpf" readOnly="readOnly" mode="output"/>
					</c:if>
					<c:if test="<%=!br.com.biharckgroup.scr.util.SCRUtil.isPessoaLogadaPequenoGerador() %>">
						<t:property name="cpf" label="CPF" class="tootip" title="Somente n�meros" onkeypress="cpfcnpj(this);" maxlength="14" />
					</c:if>
					
				</n:panelGrid>
			</n:panel>	
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<n:column header="CPF">
				<t:property name="cpf"/>
			</n:column>
			<t:property name="nome"/>
			<n:column header="Endere�o">
				<t:property name="endereco"/>
				<t:property name="complemento"/>
			</n:column>
			<t:property name="telefone1"/>
			<t:property name="ativo"/>	
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
</c:if>
<c:if test="${showOnlyMsg}">
	<div align="center">
		<br />
		<br />
		<n:link onclick="javascript:window.parent.closeIframe();">
			<font style="font-size: 16px;">Fechar </font>
		</n:link>
	</div>
</c:if>