<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

 <n:panel style="display:none;" id="campos_cred">
    <n:panelGrid columns="4">
        <hr>
        <c:if test="${param.ACAO == 'consultar'}">
             <t:property name="pontoEntrega.endereco" id="cred_endereco"  colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
             <t:property name="pontoEntrega.numero" id="cred_numero"  maxlength="18" colspan="2" renderAs="doubleline" readonly="readonly" />
             <t:property name="pontoEntrega.uf"  colspan="1" renderAs="doubleline" readonly="readonly" disabled="true" />                  
             <t:property name="pontoEntrega.municipio" colspan="1" renderAs="doubleline" readonly="readonly"  disabled="true"/>
        </c:if>
        <c:if test="${param.ACAO != 'consultar'}">
             <t:property name="" id="cred_endereco"  label="Endere�o" type="TEXT" colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
             <t:property name="" id="cred_numero" label="N�mero" type="TEXT"  maxlength="18" colspan="2" renderAs="doubleline" readonly="readonly" />
             <t:property name="" id="cred_uf" label="UF" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" disabled="true" />                  
             <t:property name="" id="cred_municipio" label="Munic�pio" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly"  disabled="true"/>
        </c:if>  
    </n:panelGrid>
</n:panel>