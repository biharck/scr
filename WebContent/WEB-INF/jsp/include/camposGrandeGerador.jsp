<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

 <n:panel style="display:none;" id="campos_gg">
    <n:panelGrid columns="4">
        <hr>
        <c:if test="${param.ACAO == 'consultar'}">
            <t:property name="grandeGerador.razaoSocial"  id="gg_razaoSocial" style="width:400px" colspan="2" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.nomeFantasia" id="gg_nomeFantasia"   colspan="4" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.cpfCnpjTransient" id="gg_cpfCnpjTransient"   maxlength="18" onkeypress="javascript:cpfcnpj(this)" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.cep" id="gg_cep"  maxlength="9" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.endereco" id="gg_endereco"  colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.numero" id="gg_numero"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.complemento" id="gg_complemento"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.bairro" id="gg_bairro"   colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.uf" id="gg_uf" label="UF" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.municipio" id="gg_municipio" label="Munic�pio" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.telefone1" id="gg_telefone1"  type="TEXT"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.telefone2" id="gg_telefone2" type="TEXT"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.celular" id="gg_celular"   colspan="2" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.email" id="gg_email" colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="grandeGerador.responsavel" id="gg_responsavel" colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
        </c:if>
        <c:if test="${param.ACAO != 'consultar'}">
            <t:property name="" label="Raz�o Social" type="TEXT" id="gg_razaoSocial" style="width:400px" colspan="2" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="gg_nomeFantasia" label="Nome Fantasia" type="TEXT"  colspan="4" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="gg_cpfCnpjTransient" label="CPF/CNPJ" type="TEXT"  maxlength="18" onkeypress="javascript:cpfcnpj(this)" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="gg_cep" label="Cep" type="TEXT"  maxlength="9" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="gg_endereco" label="Endere�o" type="TEXT"  colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="gg_numero" label="N�mero" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="gg_complemento" label="Complemento" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="gg_bairro" label="Bairro" type="TEXT"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="gg_uf"  label="UF" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" editabled="true"/>
            <t:property name="" id="gg_municipio" label="Mun�cipio" type="TEXT"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="gg_telefone1" label="Telefone Principal" type="TEXT"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="gg_telefone2" label="Telefone Alternativo" type="TEXT"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="gg_celular" label="Celular" type="TEXT"  colspan="2" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="gg_email" label="E-mail" type="TEXT" colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="gg_responsavel" label="Respons�vel" type="TEXT" colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
        </c:if>  
    </n:panelGrid>
</n:panel>