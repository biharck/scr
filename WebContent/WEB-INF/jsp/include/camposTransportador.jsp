<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

 <n:panel style="display:none;" id="campos_transp">
    <n:panelGrid columns="4">
        <hr>
        <c:if test="${param.ACAO == 'consultar'}">
            <t:property name="transportador.razaoSocial" id="transp_razaoSocial" style="width:400px" colspan="2" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.nomeFantasia" id="transp_nomeFantasia"  colspan="4" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.cpfCnpjTransient" id="transp_cpfCnpjTransient"   maxlength="18" onkeypress="javascript:cpfcnpj(this)" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.cep" id="transp_cep"  maxlength="9" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.endereco" id="transp_endereco"  colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.numero" id="transp_numero" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.complemento" id="transp_complemento"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.bairro" id="transp_bairro"   colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.uf.nome" id="transp_uf" label="UF" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.municipio" id="transp_municipio" label="Munic�pio" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.telefone1" id="transp_telefone1"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.telefone2" id="transp_telefone2"    colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.celular" id="transp_celular" colspan="2" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.email" id="transp_email"  colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="transportador.responsavel" id="transp_responsavel"   colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
        </c:if>
        <c:if test="${param.ACAO != 'consultar'}">
            <t:property name="" id="transp_razaoSocial" label="Raz�o Social" type="TEXT" style="width:400px" colspan="2" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_nomeFantasia" label="Nome Fantasia" type="TEXT" colspan="4" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_cpfCnpjTransient" label="CPF/CNPJ" type="TEXT" maxlength="18" onkeypress="javascript:cpfcnpj(this)" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_cep" label="Cep" type="TEXT" maxlength="9" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_endereco" label="Endere�o" type="TEXT" colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_numero" label="N�mero" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_complemento" label="Complemento" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_bairro" label="Bairro" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_uf" label="UF" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_municipio" label="Munic�pio" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_telefone1" label="Telefone Principal" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_telefone2" label="Telefone Alternativo" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_celular" label="Celular" type="TEXT" colspan="2" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_email" label="E-mail" type="TEXT"  colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="transp_responsavel" label="Respons�vel" type="TEXT" colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
        </c:if>
    </n:panelGrid>
</n:panel>