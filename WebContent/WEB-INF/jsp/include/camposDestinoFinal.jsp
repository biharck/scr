<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<n:panel style="display:none;" id="campos_dest">
   <n:panelGrid columns="4">
       <hr>
       <c:if test="${param.ACAO == 'consultar'}">
            <t:property name="destinoFinal.razaoSocial" id="dest_razaoSocial"  style="width:400px" colspan="2" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.nomeFantasia" id="dest_nomeFantasia" colspan="4" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.cpfCnpjTransient" id="dest_cpfCnpjTransient"  maxlength="18" onkeypress="javascript:cpfcnpj(this)" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.cep" id="dest_cep"  maxlength="9" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.endereco" id="dest_endereco" colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.numero" id="dest_numero" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.complemento" id="dest_complemento"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.bairro" id="dest_bairro" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.uf" id="dest_uf" label="UF" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.municipio" id="dest_municipio" label="Munic�pio" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.inscricaoMunicipal" id="dest_inscricaoMunicipal" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.inscricaoEstadual" id="dest_inscricaoEstadual"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.telefone1" id="dest_telefone1"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.telefone2" id="dest_telefone2" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.celular" id="dest_celular"  renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.email" id="dest_email" colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="destinoFinal.responsavel" id="dest_responsavel" colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
       </c:if>
       <c:if test="${param.ACAO != 'consultar'}">
            <t:property name="" id="dest_razaoSocial" label="Raz�o Social" type="TEXT" style="width:400px" colspan="2" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_nomeFantasia" label="Nome Fantasia" type="TEXT" colspan="4" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_cpfCnpjTransient" label="CPF/CNPJ" type="TEXT" maxlength="18" onkeypress="javascript:cpfcnpj(this)" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_cep" label="Cep" type="TEXT" maxlength="9" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_endereco" label="Endere�o" type="TEXT" colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_numero" label="N�mero" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_complemento" label="Complemento" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_bairro" label="Bairro" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_uf"  label="UF" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_municipio" label="Mun�cipio" type="TEXT"  colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_inscricaoMunicipal" label="Inscri��o Municipal" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_inscricaoEstadual" label="Inscri��o Estadual" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_telefone1" label="Telefone Principal" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_telefone2" label="Telefone Alternativo" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_celular" label="Celular" type="TEXT" colspan="1" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_email" label="E-mail" type="TEXT" colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
            <t:property name="" id="dest_responsavel" label="Respons�vel" type="TEXT" colspan="2" style="width:400px" renderAs="doubleline" readonly="readonly" />
        </c:if>
   </n:panelGrid>
</n:panel>