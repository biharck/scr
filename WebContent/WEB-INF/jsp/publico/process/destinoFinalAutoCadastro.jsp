<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<t:tela>
	<n:bean name="destinoFinal">
		<n:panel>
			<n:panelGrid columns="4" >
				
				<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
				
				<t:property name="razaoSocial" style="width:400px" colspan="2" renderAs="doubleline" id="df"/>
				<t:property name="nomeFantasia" style="width:400px" colspan="2" renderAs="doubleline"/>
				
				<t:property name="cpfCnpjTransient" class="tootip" title="Somente n�meros"  onkeypress="formatar_mascara(this, \"##.###.###/####-##\");" maxlength="18" renderAs="doubleline"/>
				<t:property name="inscricaoEstadual"  renderAs="doubleline"/>
				<t:property name="inscricaoMunicipal" renderAs="doubleline"/>
				
				<n:panel colspan="1">
					<t:property name="cep" class="tootip" title="Somente n�meros" id="cep" onkeypress="formatar_mascara(this,  \"#####-###\");" maxlength="9" renderAs="doubleline" onchange="ajaxBuscaCep($('#cep').val())"/>
				</n:panel>
				<t:property name="endereco" id="endereco" colspan="2" style="width:400px" renderAs="doubleline"/>
				<t:property name="numero" id="numero" colspan="1" renderAs="doubleline"/>
				
				<t:property name="complemento" renderAs="doubleline"/>
				<t:property name="bairro" id="bairro" renderAs="doubleline"/>
				<n:comboReloadGroup useAjax="true">
					<t:property name="uf" id="uf" renderAs="doubleline"/>
					<t:property name="municipio"  id="municipio" renderAs="doubleline"/>
				</n:comboReloadGroup>
				
				<n:panel colspan="2"></n:panel>
				<t:property name="telefone1" class="tootip" title="Somente n�meros" renderAs="doubleline"/>
				<t:property name="telefone2" class="tootip" title="Somente n�meros" renderAs="doubleline"/>
				<t:property name="celular" class="tootip" title="Somente n�meros" renderAs="doubleline"/>
				<t:property name="responsavel" id="responsavel" renderAs="doubleline"/>
				<t:property name="email" colspan="2" style="width:400px" renderAs="doubleline" id="email"/>
				<c:if test="${empty destinoFinal.id}">
					<t:property name="reEmail" colspan="2" style="width:400px" renderAs="doubleline" id="reemail" onchange="validaIgualdadeCampo($('#email'),this,'e-mail')"/>
				</c:if>
				<t:property name="tipoDestino" renderAs="doubleline" colspan="1"/>
				<t:property name="capacidadeTotal" type="money" colspan="2" renderAs="doubleline" class="tootip" title="Volume total cab�vel na �rea" />
			</n:panelGrid>
			<n:submit action="salvar">Salvar</n:submit>
		</n:panel>
	</n:bean>
</t:tela>
<script type="text/javascript">
	 $(document).ready(function() {
	 	$('#df').focus();
	 });
	 
	 function validaIgualdade(){
	 	edicao = ${empty destinoFinal.id};
	 	if(edicao)
	 		return validaIgualdadeCampo($('#email'),$('#reemail'),'e-mail');
	 	else return true;
	 }
</script>