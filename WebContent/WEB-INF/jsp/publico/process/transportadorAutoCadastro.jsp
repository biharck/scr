<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela>
	<br />
	 <n:panel valign="top" style="padding-top:4px">
		<br />
			<div id="selecTipoTransportador"></div>
		<br />
	</n:panel>
	<n:bean name="transportador">
		<t:property name="tipoTransportador" id="tipoTransportador" style="width:400px" colspan="2" renderAs="doubleline" onchange="verifTipoTransportador();"/>
		<div style="display:none;" id="campos">
			<n:panel >
				<n:panelGrid columns="4" >
					<t:property name="id" colspan="4"  renderAs="doubleline" type="hidden" showLabel="false" label=" "/>
						<n:panel id="oculta1"  colspan="2"> 
							<t:property name="razaoSocial" id="razaoSocial" style="width:400px" renderAs="doubleline"/>
							<span class=requiredMark>*</span>
						</n:panel>
					<t:property name="nomeFantasia" id="nomeFantasia" style="width:400px" colspan="4" renderAs="doubleline"/>
					<t:property name="cpfCnpjTransient" class="tootip" title="Somente n�meros" id="cpfCnpjTransient" maxlength="18" onkeypress="javascript:cpfcnpj(this)" colspan="1" renderAs="doubleline"/>
					<n:panel id="oculta2"  colspan="1"> 
						<t:property name="inscricaoMunicipal" id="inscricaoMunicipal"  renderAs="doubleline"/>
					    <span class=requiredMark>*</span>
                    </n:panel>
					<n:panel id="oculta3"  colspan="1"> 
						<t:property name="inscricaoEstadual" id="inscricaoEstadual" renderAs="doubleline"/>
					</n:panel>  
					<n:panel colspan="1">
						<t:property name="cep" class="tootip" title="Somente n�meros" id="cep" onkeypress="formatar_mascara(this,  \"#####-###\");" maxlength="9" renderAs="doubleline" onchange="ajaxBuscaCep($('#cep').val())"/>
					</n:panel>
					<t:property name="endereco" id="endereco" colspan="2" style="width:400px" renderAs="doubleline"/>
					<t:property name="numero" id="numero" colspan="1" renderAs="doubleline"/>
					<t:property name="complemento" colspan="1" renderAs="doubleline"/>
					<t:property name="bairro" id="bairro" colspan="1" renderAs="doubleline"/>
					<n:comboReloadGroup useAjax="true">
						<t:property name="uf" id="uf" colspan="1" renderAs="doubleline" />
						<t:property name="municipio" id="municipio" colspan="1" renderAs="doubleline"/>
					</n:comboReloadGroup>
					<n:panel colspan="1"></n:panel>
					<t:property name="telefone1" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline"/>
					<t:property name="telefone2" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline"/>
					<t:property name="celular" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline"/>
					<n:panel id="oculta4"  colspan="1">    
						<t:property name="responsavel" id="responsavel"  renderAs="doubleline"/>
						<span class=requiredMark>*</span>
					</n:panel>
					<t:property name="email" colspan="2" renderAs="doubleline" style="width:400px" id="email"/>
					<c:if test="${empty transportador.id}">
						<t:property name="reEmail" id="reemail" colspan="2" renderAs="doubleline" style="width:400px" onchange="validaIgualdadeCampo($('#email'),this,'e-mail')"/>
					</c:if>
				</n:panelGrid>
			</n:panel>
			<n:submit action="salvar">Salvar</n:submit>
		</div>
	</n:bean>
</t:tela>
<script type="text/javascript">
$(document).ready(function() {
    $('#tipoTransportador').focus();
   	 verifTipoTransportador();
});

function verifTipoTransportador() {
  tipo = $("#tipoTransportador").val();
  
  
  if(tipo == "" || tipo == null || tipo == '<null>'){
   $("#campos").fadeOut();
   dialog('Aten��o','Voc� deve selecionar o porte do transportador');
     $("#selecTipoTransportador").html("");  
        return false
  }else
   	ajaxBuscaPorteTransportador(tipo);
}

function validaIgualdade(){
  edicao = ${empty transportador.id};
  if(edicao)
   return validaIgualdadeCampo($('#email'),$('#reemail'),'e-mail');
  else return true;
 }
 
</script>