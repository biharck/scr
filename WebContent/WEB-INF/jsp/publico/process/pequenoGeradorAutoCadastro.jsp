<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela>
	<n:bean name="pequenoGerador">				
		<n:panel>
			<n:panelGrid columns="4" >
				<t:property name="id" colspan="4" renderAs="doubleline" type="hidden" showLabel="false" label=" "/>

				<t:property name="nome" id="nome" style="width:400px" colspan="2" renderAs="doubleline"/>
				<t:property name="cpf" class="tootip" title="Somente n�meros" maxlength="14" onkeypress="javascript:cpfcnpj(this)" colspan="2" renderAs="doubleline"/>

				
				<n:panel colspan="1">
					<t:property name="cep" class="tootip" title="Somente n�meros" id="cep" maxlength="9" onkeypress="formatar_mascara(this,  \"#####-###\");" renderAs="doubleline" onchange="ajaxBuscaCep($('#cep').val())"/>
				</n:panel>
				<t:property name="endereco" id="endereco" colspan="2" style="width:400px" renderAs="doubleline"/>
				<t:property name="numero" id="numero" colspan="1" renderAs="doubleline"/>
				
				<t:property name="complemento" colspan="1" renderAs="doubleline"/>
				<t:property name="bairro" id="bairro" colspan="1" renderAs="doubleline"/>
				<n:comboReloadGroup useAjax="true">
					<t:property name="uf" id="uf" colspan="1" renderAs="doubleline"/>
					<t:property name="municipio"  id="municipio" colspan="1" renderAs="doubleline"/>
				</n:comboReloadGroup>
				
				<t:property name="telefone1" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline"/>
				<t:property name="telefone2" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline"/>
				<t:property name="celular" class="tootip" title="Somente n�meros" colspan="1" renderAs="doubleline"/>
				<n:panel colspan="1"></n:panel>
				
                      <t:property name="email" colspan="2" style="width:400px" renderAs="doubleline" id="email"/>
				<c:if test="${empty pequenoGerador.id}">
					<t:property name="reEmail" colspan="2" style="width:400px" renderAs="doubleline" id="reemail" onchange="validaIgualdadeCampo($('#email'),this,'e-mail')"/>
				</c:if>
			</n:panelGrid>	
			<n:submit action="salvar">Salvar</n:submit>
		</n:panel>
	</n:bean>
</t:tela>
<script type="text/javascript">
$(document).ready(function() {
	$('#nome').focus();
});

function validaIgualdade(){
 	edicao = ${empty pequenoGerador.id};
 	if(edicao)
 		return validaIgualdadeCampo($('#email'),$('#reemail'),'e-mail');
 	else return true;
 }

</script>