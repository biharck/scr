<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<HTML>
	<HEAD>
		<n:head/>
	</HEAD>
	<BODY leftmargin="0" topmargin="0">
		<div class="applicationTitle">
			<a href="${application}">Auto Cadastro ${title}</a>
		</div>
		
		<div class="messageOuterDiv">
		
			<div class="ajax-combo">
				Carregando...
			</div>
		
			<!-- ui-dialog para mensagens -->
			<div id="dialog">
				<p></p>
			</div>
			<n:messages/>&nbsp;
		</div>
		<div class="body">
			<jsp:include page="${bodyPage}" />
		</div>
		<!-- div-auxiliar para armazenar valores temporários -->
		<div id="div-auxiliar" style="display:none;">
		</div>
		<script type="text/javascript">
			/*
				Para iniciar antes de carregar a página
			*/
			$(document).ready(function(){
			   $('.tootip').qtip({
			      content: {
			         text: false // Use each elements title attribute
			      },
               	  position: {
	                  corner: {
	                     tooltip: 'rightBottom', // Use the corner...
	                     target: 'leftTop' // ...and opposite corner
	                  }
               	  },
			      style: {
	                  border: {
	                     width: 5,
	                     radius: 10
	                  },
	                  padding: 10, 
	                  textAlign: 'center',
	                  tip: true, // Give it a speech bubble tip with automatic corner detection
	                  name: 'cream' // Style it according to the preset 'cream' style
	               },
	              adjust: {
		               resize: true,
		               scroll: true
		            }
			   });
			});
			$(document).ready(function(){
			   $('.tootip-center').qtip({
			      content: {
			         text: false // Use each elements title attribute
			      },
               	  position: {
	                  corner: {
	                     tooltip: 'bottomLeft', // Use the corner...
	                     target: 'topRight' // ...and opposite corner
	                  }
               	  },
			      style: {
	                  border: {
	                     width: 5,
	                     radius: 10
	                  },
	                  padding: 10, 
	                  textAlign: 'center',
	                  tip: true, // Give it a speech bubble tip with automatic corner detection
	                  name: 'cream' // Style it according to the preset 'cream' style
	               },
	              adjust: {
		               resize: true,
		               scroll: true
		            }
			   });
			});
		</script>
	</BODY>
</HTML>