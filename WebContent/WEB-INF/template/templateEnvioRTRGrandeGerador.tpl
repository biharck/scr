<span style="font-size:20px; color: #2281CF;">Prezado Grande Gerador,</span>
<hr style="border: 1px #CDCDCD solid;">
<span style="font-size:12px; color:#555;">
	O Relat�rio de Transporte de Res�duos de sua obra situada ao Endere&ccedil;o 	<font style="color:#1F75CC;"> {enderecoObraCompleto}</font>,	sob PGRCC n&ordm;: 	<font style="color:#1F75CC;"> {numeroProjeto}</font>
	foi encaminhado &agrave; Secretaria de Meio Ambiente para an&aacute;lise e valida&ccedil;&atilde;o.
	
	<br/>
	<br/>
	Segue abaixo a an&aacute;lise:<br />
	<br/>
	<font style="color:#E77272">
	{guiasGeradas} 
	</font>
	Guias foram geradas para este PGRCC e/ou Endere&ccedil;o citado acima;
	<br/>
	<font style="color:#E77272">
		{guiasConfirmadas} 
	</font>	
		Guias foram confirmadas nos destinos adequados;
	<br/>
	<font style="color:#E77272">
	{guiasPendentes} 
	</font>
	Guias pendentes;
	<br/>
	<br/>
	Caso queira visualizar o relat&oacute;rio detalhado de guias, clique na op��o desejada:
	<br/><br/>
	Guias Geradas <a href="{dominio}/SCR/adm/crud/Guia?numeroProjeto={numeroProjeto}">Clique aqui</a> 
	<br/>
	Guias Confirmadas <a href="{dominio}/SCR/adm/crud/Guia?guiaStatus=1&numeroProjeto={numeroProjeto}">Clique aqui</a> 
	<br/>
	Guias Pendentes <a href="{dominio}/SCR/adm/crud/Guia?guiaStatus=0&numeroProjeto={numeroProjeto}">Clique aqui</a> 
	<br/>
	<br/>
	Para esclarecimentos entre em contato com o T&eacute;cnico da Secretaria de Meio Ambiente no telefone n&ordm; <font style="color:#1F75CC;">{numTelefoneTecnico}</font>.
	<br /><br />

<i>
Cordialmente,<br/>
<div>
	<font color="#000066" size="1">
		<b><br></b>
	</font>
	<div>
		<div>
			<span style="font-size:x-small">
				<font color="#336666"><img src="{URL_LOGO}" width="96" height="49"><br></font>
			</span>
			<font color="#003333">
				<span style="font-size:x-small">&nbsp;&nbsp;<a href="{URL}" target="_blank">{URL}</a></span>
			</font><br>
		</div>
	</div>
</div>
</i>
<br />
<br />
<font style="font-size:10px;">
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
ESTE E-MAIL FOI GERADO AUTOMATICAMENTE, FAVOR N&Atilde;O RESPONDER A MENSAGEM.<br>
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
</font>

</span>






