<span style="font-size:20px; color: #2281CF;">Prezado Grande Gerador,</span>
<hr style="border: 1px #CDCDCD solid;">
<span style="font-size:12px; color:#555;">
	Segue em anexo o Projeto(PGRCC) de n&aacute;mero
	<font style="color:#1F75CC;"> 
		{numeroProjeto}
	</font>
gerado pelo <font style="color:#E77272">Sistema de Controle de Res&iacute;duos - SCR.</font>
<br/ >
<br/ >
<br/ >
<i>
Cordialmente,<br/>
<div>
	<font color="#000066" size="1">
		<b><br></b>
	</font>
	<div>
		<div>
			<span style="font-size:x-small">
				<font color="#336666"><img src="{URL_LOGO}" width="96" height="49"><br></font>
			</span>
			<font color="#003333">
				<span style="font-size:x-small">&nbsp;&nbsp;<a href="{URL}" target="_blank">{URL}</a></span>
			</font><br>
		</div>
	</div>
</div>
</i>
<br />
<br />
<font style="font-size:10px;">
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
ESTE E-MAIL FOI GERADO AUTOMATICAMENTE, FAVOR N&Atilde;O RESPONDER A MENSAGEM.<br>
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
</font>

</span>


