<span style="font-size:20px; color: #2281CF;">Prezado Sr. {responsavel},</span>
<hr style="border: 1px #CDCDCD solid;">
<span style="font-size:12px; color:#555;">

	Sua empresa 
		<font style="color:#1F75CC;">
			{nomeFantasia}
		</font>, inscrita sob o CNPJ/CPF n&ordm;: 
		<font style="color:#1F75CC;">
			{cpfCnpjTransient}
		</font>, situado no Endere&ccedil;o 
		<font style="color:#1F75CC;">
			{enderecoCompleto}
		</font>, com capacidade m&aacute;xima de 
		<font style="color:#1F75CC;">
			{capacidadeTotal} m&sup3;
		</font>, est&aacute; com volume atual de 
		<font style="color:#1F75CC;">
			{capacidadeAtual} m&sup3;
		</font>, capacidade superior &agrave; 
		<font style="color:#E77272">
			90%
		</font>.<br />
	A Secretaria de Meio Ambiente j&aacute; foi informada.<br /> Favor providenciar a retirada de res&iacute;duos de forma a n&atilde;o comprometer sua opera&ccedil;&atilde;o e posteriormente o bloqueio no sistema.

<br/>
Em caso de d&uacute;vidas, favor entrar em contato com a Secretaria de Meio Ambiente no telefone <font style="color:#1F75CC;">{numTelefoneTecnico}</font>.
<br/>

<br/ >
<br/ >
<i>
Cordialmente,<br/>
<div>
	<font color="#000066" size="1">
		<b><br></b>
	</font>
	<div>
		<div>
			<span style="font-size:x-small">
				<font color="#336666"><img src="{URL_LOGO}" width="96" height="49"><br></font>
			</span>
			<font color="#003333">
				<span style="font-size:x-small">&nbsp;&nbsp;<a href="{URL}" target="_blank">{URL}</a></span>
			</font><br>
		</div>
	</div>
</div>
</i>
<br />
<br />
<font style="font-size:10px;">
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
ESTE E-MAIL FOI GERADO AUTOMATICAMENTE, FAVOR N&Atilde;O RESPONDER A MENSAGEM.<br>
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
</font>

</span>



