<span style="font-size:20px; color: #2281CF;">Prezado Grande Gerador,</span>
<hr style="border: 1px #CDCDCD solid;">
<span style="font-size:12px; color:#555;">
	Segue em anexo o Relat&oacute;rio de Transporte de Res&iacute;duos (RTR) aprovado, referente &agrave;
	obra situada no Endere&ccedil;o 
	<font style="color:#1F75CC;"> 
		{enderecoObraCompleto}
	</font>
	sob PGRCC n&ordm;: 	<font style="color:#1F75CC;"> {numeroProjeto}</font>
<br/ >
<br/ >
<br/ >
<i>
Cordialmente,<br/>
<div>
	<font color="#000066" size="1">
		<b><br></b>
	</font>
	<div>
		<div>
			<span style="font-size:x-small">
				<font color="#336666"><img src="{URL_LOGO}" width="96" height="49"><br></font>
			</span>
			<font color="#003333">
				<span style="font-size:x-small">&nbsp;&nbsp;<a href="{URL}" target="_blank">{URL}</a></span>
			</font><br>
		</div>
	</div>
</div>
</i>
<br />
<br />
<font style="font-size:10px;">
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
ESTE E-MAIL FOI GERADO AUTOMATICAMENTE, FAVOR N&Atilde;O RESPONDER A MENSAGEM.<br>
---------------------------------------------------------------------------------------------------------------------------------------------------------<br>
</font>

</span>



