package br.com.biharckgroup.scr.dao;
import org.nextframework.persistence.DefaultOrderBy;

import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.Municipio;
import br.com.biharckgroup.scr.bean.Projeto;

@DefaultOrderBy("nome")
public class MunicipioDAO extends GenericDAOSCR<Municipio> {
	
	public Municipio loadByNome(String  nome) {
		Municipio bean = query()
				.select("municipio")
				.where("UPPER(municipio.nome) = ?", nome.toUpperCase())			
				.unique();
		
		return bean;
	}
	
	public Municipio findNomeById(Municipio bean){
		return query()
				.select("municipio.nome")
				.where("municipio = ?", bean)
				.unique();
		
	}
	
	
}
