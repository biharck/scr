package br.com.biharckgroup.scr.dao;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.persistence.GenericDAO;

import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.bean.ResponsavelProjeto;
import br.com.biharckgroup.scr.bean.TransportadorProjeto;


public class ResponsavelProjetoDAO extends GenericDAO<ResponsavelProjeto> {
	
	public void deleteByProjeto(Projeto projeto) {
		getHibernateTemplate().bulkUpdate(
				"delete from ResponsavelProjeto where projeto = ?", projeto);
	}
	
	public List<ResponsavelProjeto> findByProjeto(Projeto projeto) {
		if (projeto.getId() == null) {
			return new ArrayList<ResponsavelProjeto>();
		}
		return query().select("responsavelProjeto").from(ResponsavelProjeto.class)
				.where("responsavelProjeto.projeto = ?", projeto).list();
	}
}
