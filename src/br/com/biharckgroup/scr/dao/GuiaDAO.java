package br.com.biharckgroup.scr.dao;

import java.util.Date;
import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.ResultList;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.biharckgroup.scr.adm.filtro.GuiaFiltro;
import br.com.biharckgroup.scr.adm.relatorio.filtro.GuiaReportFiltro;
import br.com.biharckgroup.scr.adm.relatorio.filtro.ResiduoRecebidoGuiaReportFiltro;
import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.EnumCancelada;
import br.com.biharckgroup.scr.bean.EnumIdentificaGerador;
import br.com.biharckgroup.scr.bean.EnumSituacao;
import br.com.biharckgroup.scr.bean.EnumStatus;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.bean.ResiduoGuia;
import br.com.biharckgroup.scr.service.UfService;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRException;


@DefaultOrderBy("guia.id")
public class GuiaDAO extends GenericDAOSCR<Guia> {
	
	private ResiduoGuiaDAO residuoGuiaDAO;
	private UfService ufService;
	
	public void setResiduoGuiaDAO(ResiduoGuiaDAO residuoGuiaDAO) {
		this.residuoGuiaDAO = residuoGuiaDAO;
	}
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	
	@Override
	public void saveOrUpdate(final Guia bean) {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				Guia load = new Guia();
				/* update */
				if(bean.getId() != null){
					load = load(bean);
				}
				//atualiza somente campos do formulario
				
				load.setSituacao(bean.getSituacao());
				load.setStatus(bean.isStatus());
				
				load.setProjeto(bean.getProjeto());
				load.setTransportador(bean.getTransportador());
				load.setGrandeGerador(bean.getGrandeGerador());
				
				load.setCepObra(bean.getCepObra());
				load.setEnderecoObra(bean.getEnderecoObra());
				load.setNumeroObra(bean.getNumeroObra());
				load.setComplementoObra(bean.getComplementoObra());
				load.setBairroObra(bean.getBairroObra());
				load.setMunicipioObra(bean.getMunicipioObra());
				load.setResponsavelObra(bean.getResponsavelObra());
				load.setResiduosGuia(bean.getResiduosGuia());
				load.setTipoTransporte(bean.getTipoTransporte());
				load.setIdentificacaoTransporte(bean.getIdentificacaoTransporte());
				load.setPlaca(bean.getPlaca());
				load.setDataRetirada(bean.getDataRetirada());
				load.setDataEnvio(bean.getDataEnvio());
				load.setDataEntrega(bean.getDataEntrega());
				load.setDestinoFinal(bean.getDestinoFinal());
				load.setCancelada(bean.isCancelada());
				
				//valida uniques
				try{
					GuiaDAO.super.saveOrUpdate(load);
				} catch (DataIntegrityViolationException e) {
					if (DatabaseError.isKeyPresent(e, "unique_identTransp")){ //nota o index na tabela tem q ter o mesmo nome
						throw new SCRException("J� existe um registro de Guia com esta Identifica��o de Transporte cadastrado no sistema.");
					}
				}
				
				//salva numeroGuia
				if(bean.getId() == null){
					load. setNumeroGuia("GG-"+load.getId()); //O n�mero de identifica��o --> GG-id
					updateNumeroGuia(load);
				}
				
				residuoGuiaDAO.deleteByGuia(load);
				if(bean.getResiduosGuia() != null){
					for (ResiduoGuia residuoGuia : bean.getResiduosGuia()) {
						residuoGuia.setGuia(load);
						residuoGuiaDAO.saveOrUpdate(residuoGuia);
					}
				}
				return null;
			}
			
		});
	}
	
	public void updateNumeroGuia(Guia bean) {
		getJdbcTemplate().update("update guia set numeroGuia = ?" +
				" where id = ?", bean.getNumeroGuia(), bean.getId());
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Guia> query,
			FiltroListagem _filtro) {
		GuiaFiltro filtro = (GuiaFiltro) _filtro;
		
		query
			.whereLikeIgnoreAll("identificacaoTransporte", filtro.getIdentificacaoTransporte())
			.whereLikeIgnoreAll("placa", filtro.getPlaca())//;
			.leftOuterJoin("guia.grandeGerador gg")
			.whereLikeIgnoreAll("gg.nomeFantasia", filtro.getNomeFantasia())
			.whereLikeIgnoreAll("guia.numeroGuia", filtro.getNumeroGuia())
			.leftOuterJoin("guia.projeto gp")
			.whereLikeIgnoreAll("gp.numeroProjeto", filtro.getNumeroProjeto());

		if(filtro.getDestinoFinal() != null){
			query
				.leftOuterJoin("guia.destinoFinal gd")	
				.where("gd=?", filtro.getDestinoFinal());
		}
		if(filtro.getTransportador() != null){
			query
				.leftOuterJoin("guia.transportador gt")
				.where("gt=?", filtro.getTransportador());
		}
		if(filtro.getGrandeGerador() != null){
			query
				.where("gg=?", filtro.getGrandeGerador());
		}
		
		if(filtro.getStatus()!=null)			
			query.where("status is "+filtro.getStatus().isOptBool());
		
		if(filtro.getSituacao() != null){
				query.where("situacao = "+filtro.getSituacao().getValor());
		}
		
		if(filtro.getEnumCancelada()!=null)			
			query.where("cancelada is "+filtro.getEnumCancelada().isOptBool());
	}
	
	
	@Override
	public Guia loadForEntrada(Guia form) {
		form = super.loadForEntrada(form);
		form.setResiduosGuia(residuoGuiaDAO.findByGuia(form));
		defineCampoTransient(form);
		return form;
	}
	
	@Override
	public Guia load(Guia form) {
		form = super.load(form);
		form.setResiduosGuia(residuoGuiaDAO.findByGuia(form));
		defineCampoTransient(form);
		return form;
	}
	
	@Override
	public ResultList<Guia> findForListagem(FiltroListagem filtro) {
		// TODO Auto-generated method stub
		
		ResultList<Guia> list = super.findForListagem(filtro);
		for (Guia guia : list.list()) {
			guia.setEnumSituacao(EnumSituacao.getEnumSituacaoByValor(guia.getSituacao()));
		}
		return list;
	}
	
	public List<Guia> findByFiltro(GuiaReportFiltro filtro) {		
		QueryBuilder<Guia> query = query();
		
		query.select(
				"guia.id, "+ 
				" guia.numeroGuia,guia.timeInc, guia.dataEntrega, guia.dataRetirada, guia.placa, guia.status, guia.situacao, "+ 
				"gg.id, gg.id,gg.nomeFantasia, gg.endereco,gg.complemento,gg.numero,gg.bairro, gg.municipio,gg.cpf,gg.cnpj," +
				"gp.id, gp.numeroProjeto, gp.enderecoObra, gp.complementoObra,gp.numeroObra,gp.bairroObra,gp.cepObra, "+
				"gt.id, gt.nome,gt.descricao, dest.nomeFantasia, transp.nomeFantasia, "+
				"rg.quantidade, um.nome, tipoRes.id, tipoRes.descricao")
				.leftOuterJoin("guia.grandeGerador gg")
				.leftOuterJoin("guia.tipoTransporte gt")
				.leftOuterJoin("guia.destinoFinal dest")
				.leftOuterJoin("guia.transportador transp")
				.leftOuterJoin("guia.projeto gp")
				.leftOuterJoin("guia.residuosGuia rg")
				.leftOuterJoin("rg.tipoResiduo tipoRes")
				.leftOuterJoin("rg.unidadeMedida um")
		.whereLikeIgnoreAll("guia.numeroGuia", filtro.getNumeroGuia())
		.whereLikeIgnoreAll("guia.placa", filtro.getPlaca())
		.whereLikeIgnoreAll("gp.numeroProjeto", filtro.getNumeroProjeto())
		.where("cancelada is "+EnumCancelada.NAO.isOptBool());
		
		if(filtro.getDestinoFinal() != null){
			query.where("dest=?", filtro.getDestinoFinal());
		}
		if(filtro.getTransportador() != null){
			query
				.where("transp =?", filtro.getTransportador());
		}
		if(filtro.getGrandeGerador() != null){
			query
				.where("gg=?", filtro.getGrandeGerador());
		}
		if(filtro.getStatus()!=null)			
			query.where("guia.status is "+filtro.getStatus().isOptBool());
		
		if(filtro.getSituacao() != null){
				query.where("guia.situacao = "+filtro.getSituacao().getValor());
		}
		//cast pois cammpo � timestemp
		if(filtro.getDataIni()!=null)
			query.where("date(guia.timeInc) >= ?", filtro.getDataIni());
		if(filtro.getDataFim()!=null)
			query.where("date(guia.timeInc) <= ?", filtro.getDataFim());
				
		//define enum situa��o
		List<Guia> lista = query.list();
		for (Guia guia : lista) {
			guia.setEnumSituacao(EnumSituacao.getEnumSituacaoByValor(guia.getSituacao()));
			if(guia.isStatus())
				guia.setEnumStatus(EnumStatus.CONFIRMADA);
			else
				guia.setEnumStatus(EnumStatus.PENDENTE);
		}
		
		return lista;
	}
	
	public List<Guia> findByProjeto(Projeto projeto) {
		QueryBuilder<Guia> query = query();
		
		query.select("guia.id, guia.numeroGuia,guia.timeInc, guia.dataEntrega, guia.dataRetirada, guia.placa, guia.status, guia.situacao,  " +
				"gg.id, gg.id,gg.nomeFantasia, gg.endereco,gg.complemento,gg.numero,gg.bairro, gg.municipio,gg.cpf,gg.cnpj," +
				" gp.id, gp.numeroProjeto, gp.enderecoObra, gp.complementoObra,gp.numeroObra,gp.bairroObra,gp.cepObra, "+
				"gt.id, gt.nome,gt.descricao, dest.nomeFantasia, transp.nomeFantasia")
				.leftOuterJoin("guia.grandeGerador gg")
				.leftOuterJoin("guia.tipoTransporte gt")
				.leftOuterJoin("guia.destinoFinal dest")
				.leftOuterJoin("guia.transportador transp")
				.leftOuterJoin("guia.projeto gp")
				.where("cancelada is "+EnumCancelada.NAO.isOptBool());
				
				if(projeto !=null && projeto.getId()!=null)
						query.where("gp=?", projeto);
		
		//define enum situa��o
		List<Guia> lista = query.list();
		for (Guia guia : lista) {
			defineCampoTransient(guia);
		}
		
		return lista;
	}
	
	public List<Guia> findByFiltro(ResiduoRecebidoGuiaReportFiltro filtro) {
		QueryBuilder<Guia> query = query();
		
		query.select("guia.id,guia.dataEntrega,"+
		"tipoRes.id, tipoRes.descricao, rg.quantidade, um.nome")
		.leftOuterJoin("guia.projeto gp")
		.leftOuterJoin("guia.destinoFinal dest")
		.leftOuterJoin("guia.municipioObra mun")
		.leftOuterJoin("guia.residuosGuia rg")
		.leftOuterJoin("rg.tipoResiduo tipoRes")
		.leftOuterJoin("rg.unidadeMedida um")
		.where("cancelada is "+EnumCancelada.NAO.isOptBool());
				
		if(filtro.getDestinoFinal() != null){
			query.where("dest=?", filtro.getDestinoFinal());
		}
		if(filtro.getMunicipio() != null){
			query.where("mun=?", filtro.getMunicipio());
		}
		
		//cast pois cammpo � timestemp
		if(filtro.getDataIni()!=null)
			query.where("date(guia.dataRetirada) >= ?", filtro.getDataIni());
		if(filtro.getDataFim()!=null)
			query.where("date(guia.dataRetirada) <= ?", filtro.getDataFim());
				
		List<Guia> lista = query.list();
		
		return lista;
	}
	
	public void defineCampoTransient(Guia form){
		/*Preenche campo cpfCnpjTransient
		 * Quando tiver duas op��es cpf/cnpj verificar qual est� preenchido*/
		String cpfCnpjTransient;
		//transportador
		if(form.getTransportador() != null){
			cpfCnpjTransient = form.getTransportador().getCnpj()!= null && !form.getTransportador().getCnpj().equals("") ? form.getTransportador().getCnpj():  form.getTransportador().getCpf(); 
			form.getTransportador().setCpfCnpjTransient(cpfCnpjTransient);
		}
		//grandeGerador
		if(form.getGrandeGerador() != null){
			cpfCnpjTransient = form.getGrandeGerador().getCnpj()!= null && !form.getGrandeGerador().getCnpj().equals("") ? form.getGrandeGerador().getCnpj():  form.getGrandeGerador().getCpf(); 
			form.getGrandeGerador().setCpfCnpjTransient(cpfCnpjTransient);
		}
		//destinoFinal
		if(form.getDestinoFinal() != null){
			cpfCnpjTransient = form.getDestinoFinal().getCnpj(); 
			form.getDestinoFinal().setCpfCnpjTransient(cpfCnpjTransient);
		}
		
		//define uf
		if(form.getMunicipioObra() != null)
			form.setUfObra(ufService.getUfByMunicipio(form.getMunicipioObra()));
		if(form.getGrandeGerador() != null && form.getGrandeGerador().getMunicipio() !=null)
			form.getGrandeGerador().setUf(ufService.getUfByMunicipio(form.getGrandeGerador().getMunicipio()));
		if(form.getTransportador() != null && form.getTransportador().getMunicipio() !=null)
			form.getTransportador().setUf(ufService.getUfByMunicipio(form.getTransportador().getMunicipio()));
		if(form.getDestinoFinal() != null && form.getDestinoFinal().getMunicipio() !=null)
			form.getDestinoFinal().setUf(ufService.getUfByMunicipio(form.getDestinoFinal().getMunicipio()));
		
		//define enum IdentificaGerador
		if(form.getGrandeGerador() != null)
			form.setEnumIdentificaGerador(EnumIdentificaGerador.SIM);
		else
			form.setEnumIdentificaGerador(EnumIdentificaGerador.NAO);

		//define enum situacao
		form.setEnumSituacao(EnumSituacao.getEnumSituacaoByValor(form.getSituacao()));
		
		if(form.isStatus())
			form.setEnumStatus(EnumStatus.CONFIRMADA);
		else
			form.setEnumStatus(EnumStatus.PENDENTE);
	}
	
	public List<Guia> getGuiasByDestinoFinal(DestinoFinal df){
		return query().select("guia").leftOuterJoin("guia.destinoFinal df").where("df = ?", df).list();
	}
    public List<Guia> getGuiasByDestinoFinal(DestinoFinal df, String numeroGuia){
        return query().select("guia").leftOuterJoin("guia.destinoFinal df").where("df = ?", df).whereLikeIgnoreAll("guia.numeroGuia ", numeroGuia).list();
    }
    public List<Guia> getGuiasByDestinoFinal(DestinoFinal df, boolean status){
		return query().select("guia").leftOuterJoin("guia.destinoFinal df").where("df = ?", df).where("guia.status is "+ status).list();
	}
    public List<Guia> getGuiasDiaByDestinoFinal(DestinoFinal df){
  		return query()
  				.select("guia.id,tipoRes.id, tipoRes.descricao, rg.quantidade, um.nome")
  				.leftOuterJoin("guia.residuosGuia rg")
  				.leftOuterJoin("rg.tipoResiduo tipoRes")
  				.leftOuterJoin("guia.destinoFinal df")
  				.leftOuterJoin("rg.unidadeMedida um")
  				.where("df = ?", df).where("date(guia.timeInc) = ? ", new Date()).list();
  	}

    public List<Guia> getGuiasByGrandeGerador(GrandeGerador gg, boolean status){
        return query().select("guia").leftOuterJoin("guia.grandeGerador gg").where("gg = ?", gg).where("guia.status is "+status ).list();
    }

    public void recebimentoGuia(Guia bean){
        getJdbcTemplate().update("update guia set dataChegada = ?, situacao=?, status=? where id = ?",
        		bean.getDataChegada(), bean.getSituacao(), bean.isStatus(), bean.getId()
        );
    }
   
    @Override
    public void delete(Guia bean) {
    	residuoGuiaDAO.deleteByGuia(bean);
    	super.delete(bean);
    }
}
