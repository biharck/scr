package br.com.biharckgroup.scr.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.biharckgroup.scr.adm.filtro.GrandeGeradorFiltro;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.service.UfService;
import br.com.biharckgroup.scr.service.UsuarioService;

@DefaultOrderBy("razaoSocial")
public class GrandeGeradorDAO extends GenericDAOSCR<GrandeGerador> {

	private UsuarioService usuarioService;
	private UfService ufService;
	
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public void updateListagemQuery(QueryBuilder<GrandeGerador> query, FiltroListagem _filtro) {
		GrandeGeradorFiltro filtro = (GrandeGeradorFiltro) _filtro;

		query
		.whereLikeIgnoreAll("grandeGerador.razaoSocial", filtro.getRazaoSocial())
		.whereLikeIgnoreAll("grandeGerador.nomeFantasia", filtro.getNomeFantasia());

		if(filtro.getCpfCnpjTransient()!=null){
			if (filtro.getCpfCnpjTransient().length()== 14)
				query.whereLikeIgnoreAll("grandeGerador.cpf", filtro.getCpfCnpjTransient());
			else
				query.whereLikeIgnoreAll("grandeGerador.cnpj", filtro.getCpfCnpjTransient());
		}
	}
	
	@Override
	public GrandeGerador load(GrandeGerador bean) {
		bean = super.load(bean);
		/*Preenche campo cpfCnpjTransient
		 * Quando tiver duas op��es cpf/cnpj verificar qual est� preenchido*/
		String cpfCnpjTransient = bean.getCnpj()!= null && !bean.getCnpj().equals("") ? bean.getCnpj():  bean.getCpf(); 
		bean.setCpfCnpjTransient(cpfCnpjTransient);
		bean.setUf(ufService.getUfByMunicipio(bean.getMunicipio()));
		return bean;
	}
	
	@Override
	public GrandeGerador loadForEntrada(GrandeGerador bean) {
		bean = super.loadForEntrada(bean);
		/*Preenche campo cpfCnpjTransient
		 * Quando tiver duas op��es cpf/cnpj verificar qual est� preenchido*/
		String cpfCnpjTransient = bean.getCnpj()!= null && !bean.getCnpj().equals("") ? bean.getCnpj():  bean.getCpf(); 
		bean.setCpfCnpjTransient(cpfCnpjTransient);
		//define usuarios
		bean.setUsuarios(usuarioService.findByGrandeGerador(bean));
		bean.setUf(ufService.getUfByMunicipio(bean.getMunicipio()));
		return bean;
	}
	
	@Override
	public void saveOrUpdate(final GrandeGerador bean) {
		if(bean.getId()==null && bean.isAutoCadastro())
			GrandeGeradorDAO.super.saveOrUpdate(bean);
		else{
			if(bean.getId() == null)
				bean.setAtivo(true);
			GrandeGeradorDAO.super.saveOrUpdate(bean);					
			transactionTemplate.execute(new TransactionCallback<Object>(){
				public Object doInTransaction(TransactionStatus arg0) {
					if(bean.getUsuarios() != null && bean.getUsuarios().get(0).getId()!=null){
						//elimina referencias dos destinos finais na tabela usu�rio
						usuarioService.removeGrandeGerador(bean);
						if(bean.getUsuarios() != null){
							for ( Usuario usuario : bean.getUsuarios()) {
								usuario.setGrandeGerador(bean);
								usuarioService.updateGrandeGerador(usuario, bean);
							}
						}
					}
					return null;
				}
			});
		}
		
		
	}
	
	public List<GrandeGerador> getGrandesGeradoresAutoCadastro(){
		return query()
				.select("grandeGerador")
				.where("grandeGerador.autoCadastro is true")
				.where("grandeGerador.ativo is false")
				.list();
	}
}
