package br.com.biharckgroup.scr.dao;


import java.util.ArrayList;
import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.standard.Next;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.util.CollectionsUtil;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.biharckgroup.scr.adm.filtro.UsuarioFiltro;
import br.com.biharckgroup.scr.adm.relatorio.filtro.UsuarioReportFiltro;
import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.EnumPapel;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.Papel;
import br.com.biharckgroup.scr.bean.PapelUsuario;
import br.com.biharckgroup.scr.bean.PequenoGerador;
import br.com.biharckgroup.scr.bean.Transportador;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.service.UsuarioService;
import br.com.biharckgroup.scr.util.SCRException;


@DefaultOrderBy("usuario.nome")
public class UsuarioDAO extends GenericDAOSCR<Usuario> {
	
	PapelDAO papelDAO;
	PapelUsuarioDAO papelUsuarioDAO;	private static UsuarioDAO instance;
	public static UsuarioDAO getInstance() {
		if(instance == null){
			instance = Next.getObject(UsuarioDAO.class);
		}
		return instance;
	}
	
	public void setPapelUsuarioDAO(PapelUsuarioDAO papelUsuarioDAO) {
		this.papelUsuarioDAO = papelUsuarioDAO;
	}
	
	public void setPapelDAO(PapelDAO papelDAO) {
		this.papelDAO = papelDAO;
	}
	
	public Usuario findByLogin(String login) {
		Usuario bean = query()
			.where("UPPER(usuario.login) = ?", login.toUpperCase())			
			.unique();
		if(bean !=null)
			bean.setPapeis(papelDAO.findByUsuario(bean));
		return bean;
	}
	
	
	@Override
	public Usuario loadForEntrada(Usuario bean) {
		bean = super.loadForEntrada(bean);
		bean.setPapeis(papelDAO.findByUsuario(bean));
		return bean;
	}
	
	
	public PapelDAO getPapelDAO() {
		return papelDAO;
	}
	
	
	@Override
	public void saveOrUpdate(final Usuario bean)  {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				UsuarioService usuarioService = new UsuarioService();
				Usuario load;
				
				/* Insert */
				if(bean.getId() == null){
					load = new Usuario();
					bean.setSenha(usuarioService.encryptPassword(bean));
					load.setSenha(bean.getSenha());
					bean.setAtivo(true);
				}else
					load = load(bean);
				
				//atualiza somente campos do formulario
				load.setNome(bean.getNome());
				load.setLogin(bean.getLogin());
				load.setEmail(bean.getEmail());
				load.setAtivo(bean.isAtivo());
				load.setSenhaProvisoria(bean.isSenhaProvisoria());
				
				//relacionamentos
				load.setDestinoFinal(bean.getDestinoFinal());
				load.setTransportador(bean.getTransportador());
				load.setGrandeGerador(bean.getGrandeGerador());
				load.setPequenoGerador(bean.getPequenoGerador());
				load.setSuperUsuario(bean.isSuperUsuario());
				
				//valida uniques
				UsuarioDAO.super.saveOrUpdate(load);
				
				papelUsuarioDAO.deleteByUsuario(load);
				if(bean.getPapeis() != null){
					for (Papel papel : bean.getPapeis()) {
						PapelUsuario papelUsuario = new PapelUsuario();
						papelUsuario.setPapel(papel);
						papelUsuario.setUsuario(load);
						papelUsuarioDAO.saveOrUpdate(papelUsuario);
					}
				}
				return null;
			}
			
		});
	}
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<Usuario> query,FiltroListagem _filtro) {
		UsuarioFiltro filtro = (UsuarioFiltro) _filtro;

		query.select("usuario.id, usuario.nome, usuario.login,usuario.ativo,usuario.dtUltimoLogin, "+
				"papel.nome, papel.id, usuario.superUsuario")
		.leftOuterJoin("usuario.papeisUsuario pu")
		.leftOuterJoin("pu.papel papel")
		.whereLikeIgnoreAll("usuario.nome", filtro.getNome());
		
		if(filtro.getPapeis()!=null){
			String idsPapel = CollectionsUtil.listAndConcatenate(filtro.getPapeis(), "id", ",");
			query.whereIn("pu.papel", idsPapel);
		}
		
		if(filtro.getAtivo()!=null)			
			query.where("usuario.ativo is "+filtro.getAtivo().isOptBool());
		
		if(filtro.getTipo() != null && filtro.getTipo() instanceof GrandeGerador){
			GrandeGerador gg = (GrandeGerador) filtro.getTipo();
			query.leftOuterJoin("usuario.grandeGerador gg").where("gg=?",gg).list();
		}
		if(filtro.getTipo() != null && filtro.getTipo()  instanceof Transportador){
			Transportador tr = (Transportador) filtro.getTipo();
			query.leftOuterJoin("usuario.transportador tr").where("tr=?",tr).list();
		}
		if(filtro.getTipo() != null && filtro.getTipo()  instanceof DestinoFinal){
			DestinoFinal df = (DestinoFinal) filtro.getTipo();
			query.leftOuterJoin("usuario.destinoFinal df").where("df=?",df).list();
		}
		if(filtro.getTipo() != null && filtro.getTipo()  instanceof PequenoGerador){
			PequenoGerador pg = (PequenoGerador) filtro.getTipo();
			query.leftOuterJoin("usuario.pequenoGerador pg").where("pg=?",pg).list();
		}

		
	}

	public void alteraSenhaAndDataRequisicao(Usuario bean) {
		getJdbcTemplate().update("UPDATE usuario SET senha = ?, senhaAnterior = ?, senhaAnterior2 = ?, senhaProvisoria = ?, dataRequisicao = ? " +
				" where id = ?",bean.getSenha(),bean.getSenhaAnterior(), bean.getSenhaAnterior2(), bean.isSenhaProvisoria(), bean.getDataRequisicao() ,bean.getId());
	}
	
	public void alteraSenha(Usuario bean) {
		getJdbcTemplate().update("UPDATE usuario SET senha = ?, senhaAnterior = ?, senhaAnterior2 = ?, senhaProvisoria = ?" +
				" where id = ?",bean.getSenha(),bean.getSenhaAnterior(), bean.getSenhaAnterior2(), bean.isSenhaProvisoria(), bean.getId());
	}
	
	public void alteraDtUltimoLogin(Usuario bean) {
		getJdbcTemplate().update("UPDATE usuario SET dtUltimoLogin = ?" +
				" where id = ?", bean.getDtUltimoLogin(), bean.getId());
	}
	
	//destino final
	public void removeDestinoFinal(DestinoFinal bean){
		getJdbcTemplate().update("UPDATE usuario SET destinoFinal_id = null WHERE destinoFinal_id = ?", bean.getId());
		
	}
	public void updateDestinoFinal(Usuario usuario,DestinoFinal  bean){
		getJdbcTemplate().update("UPDATE usuario SET destinoFinal_id = ? WHERE id = ?",	bean.getId(),usuario.getId());
	}
	public List<Usuario> findByDestinoFinal(DestinoFinal bean) {
		if (bean.getId() == null) {
			return new ArrayList<Usuario>();
		}
		return query().select("usuario").from(Usuario.class)
				.where("destinoFinal_id = ?", bean).list();
	}
	//transportador
	public void removeTransportador(Transportador bean){
		getJdbcTemplate().update("UPDATE usuario SET transportador_id = null WHERE transportador_id = ?", bean.getId());
		
	}
	public void updateTransportador(Usuario usuario,Transportador  bean){
		getJdbcTemplate().update("UPDATE usuario SET transportador_id = ? WHERE id = ?", bean.getId(),usuario.getId());
	}
	public List<Usuario> findByTransportador(Transportador bean) {
		if (bean.getId() == null) {
			return new ArrayList<Usuario>();
		}
		return query().select("usuario").from(Usuario.class)
				.where("transportador_id = ?", bean).list();
	}
	//grande gerador
	public void removeGrandeGerador(GrandeGerador bean){
		getJdbcTemplate().update("UPDATE usuario SET grandeGerador_id = null WHERE grandeGerador_id = ?", bean.getId());
		
	}
	public void updateGrandeGerador(Usuario usuario,GrandeGerador  bean){
		getJdbcTemplate().update("UPDATE usuario SET grandeGerador_id = ? WHERE id = ?", bean.getId(),usuario.getId());
	}
	public List<Usuario> findByGrandeGerador(GrandeGerador bean) {
		if (bean.getId() == null) {
			return new ArrayList<Usuario>();
		}
		return query().select("usuario").from(Usuario.class)
				.where("grandeGerador_id = ?", bean).list();
	}
	
	//pequeno gerador
	public void removePequenoGerador(PequenoGerador bean){
		getJdbcTemplate().update("UPDATE usuario SET pequenoGerador_id = null WHERE pequenoGerador_id = ?", bean.getId());
		
	}
	public void updatePequenoGerador(Usuario usuario,PequenoGerador  bean){
		getJdbcTemplate().update("UPDATE usuario SET pequenoGerador_id = ? WHERE id = ?", bean.getId(),usuario.getId());
	}
	public List<Usuario> findByPequenoGerador(PequenoGerador bean) {
		if (bean.getId() == null) {
			return new ArrayList<Usuario>();
		}
		return query().select("usuario").from(Usuario.class)
				.where("pequenoGerador_id = ?", bean).list();
	}
	
	//retorna lista de emails dos usu�rios que possui papel de grande gerador que gerou o projeto
	public List<Usuario> findEmailByGrandeGerador(GrandeGerador bean) {
		if (bean.getId() == null) {
			return new ArrayList<Usuario>();
		}
		return query().select("usuario.email").from(Usuario.class)
				.where("grandeGerador_id = ?", bean).list();
	}
	
	//retorna lista de emails dos usu�rios que possui papel administrativo
	public List<Usuario> findEmailPapelAdministrativo() {
		List<Usuario> lista = new ArrayList<Usuario>();
		lista =  query().select("usuario.email")
				.join("usuario.papeisUsuario pu")
				.join("pu.papel papel")
				.where("papel.id = ?", EnumPapel.ADMINISTRATIVO.getValor()).list();
		return lista;
	}
	
	//retorna lista de emails dos usu�rios que possui do destino final informado
		public List<Usuario> findEmailByDestinoFinal(DestinoFinal bean) {
			if (bean.getId() == null) {
				return new ArrayList<Usuario>();
			}
			return query().select("usuario.email").from(Usuario.class)
					.where("destinoFinal_id = ?", bean).list();
		}
	
	public List<Usuario> findByFiltro(UsuarioReportFiltro filtro) {
		QueryBuilder<Usuario> query = query();
		
		query.select("usuario.id, usuario.nome, usuario.login,usuario.ativo,usuario.dtUltimoLogin,usuario.email, "+
					"papel.nome, papel.id")
			.leftOuterJoin("usuario.papeisUsuario pu")
			.leftOuterJoin("pu.papel papel");
		
		if(filtro.getPapeis()!=null){
			String idsPapel = CollectionsUtil.listAndConcatenate(filtro.getPapeis(), "id", ",");
			query.whereIn("pu.papel", idsPapel);
		}
		
		if(filtro.getAtivo()!=null)			
			query.where("usuario.ativo is "+filtro.getAtivo().isOptBool());
		
		return query.list();
	}
	
	/**
	 * <p>Encontra uma usu�rio que possua o respectivo email</p>
	 * @param email {@link String}
	 * @return Um usu�rio com base no email
	 */
	public Usuario findUserEmail(String email) {
		if (email == null || "".equals(email)) {
			throw new SCRException("Email n�o deve ser NULL e/ou vazio");
		} else {
			return query()
					.select("usuario")
					.where("usuario.email = ?",email)
					.unique();
		}
	}
	
	/**
	 * <p>M�todo que busca os usu�rios de um determinado tipo, seja ele
	 * grande gerador, destino final, etc.
	 * @return {@link List} {@link Usuario}
	 */
	public List<Usuario> getUsuariosByAtribuicao(Object tipo){
		if(tipo instanceof GrandeGerador){
			GrandeGerador gg = (GrandeGerador) tipo;
			return query().select("usuario").leftOuterJoin("usuario.grandeGerador gg").where("gg=?",gg).list();
		}
		if(tipo instanceof Transportador){
			Transportador tr = (Transportador) tipo;
			return query().select("usuario").leftOuterJoin("usuario.transportador tr").where("tr=?",tr).list();
		}
		if(tipo instanceof DestinoFinal){
			DestinoFinal df = (DestinoFinal) tipo;
			return query().select("usuario").leftOuterJoin("usuario.destinoFinal df").where("df=?",df).list();
		}
		if(tipo instanceof PequenoGerador){
			PequenoGerador pg = (PequenoGerador) tipo;
			return query().select("usuario").leftOuterJoin("usuario.pequenoGerador pg").where("pg=?",pg).list();
		}
		return findAll();
	}
	
	@Override
	public void delete(Usuario bean) {
		papelUsuarioDAO.deleteByUsuario(bean);
		super.delete(bean);
	}
}
