package br.com.biharckgroup.scr.dao;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.GuiaDestinoFinal;
import br.com.biharckgroup.scr.bean.GuiaTransportador;
import br.com.biharckgroup.scr.bean.ResiduoGuia;


@DefaultOrderBy("residuoguia.guia")
public class ResiduoGuiaDAO extends GenericDAO<ResiduoGuia> {
	
	public void deleteByGuia(Guia guia) {
		getHibernateTemplate().bulkUpdate(
				"delete from ResiduoGuia where guia = ?", guia);
	}
	public void deleteByGuia(GuiaDestinoFinal guia) {
		getHibernateTemplate().bulkUpdate(
				"delete from ResiduoGuia where guia = ?", guia);
	}
	public void deleteByGuia(GuiaTransportador guia) {
		getHibernateTemplate().bulkUpdate(
				"delete from ResiduoGuia where guia = ?", guia);
	}
	public List<ResiduoGuia> findByGuia(Guia guia) {
		if (guia.getId() == null) {
			return new ArrayList<ResiduoGuia>();
		}
		return query().select("residuoGuia").from(ResiduoGuia.class)
				.where("residuoGuia.guia = ?", guia).list();
	}
	public List<ResiduoGuia> findByGuia(GuiaDestinoFinal guia) {
		if (guia.getId() == null) {
			return new ArrayList<ResiduoGuia>();
		}
		return query().select("residuoGuia").from(ResiduoGuia.class)
				.where("residuoGuia.guia = ?", guia).list();
	}
	public List<ResiduoGuia> findByGuia(GuiaTransportador guia) {
		if (guia.getId() == null) {
			return new ArrayList<ResiduoGuia>();
		}
		return query().select("residuoGuia").from(ResiduoGuia.class)
				.where("residuoGuia.guia = ?", guia).list();
	}
	
	
}
