package br.com.biharckgroup.scr.dao;

import java.util.Date;
import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.ResultList;
import org.nextframework.util.CollectionsUtil;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.biharckgroup.scr.adm.filtro.ProjetoFiltro;
import br.com.biharckgroup.scr.adm.relatorio.filtro.ProjetoReportFiltro;
import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.EnumStatusProjeto;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduoProjeto;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.bean.ResponsavelProjeto;
import br.com.biharckgroup.scr.bean.TransportadorProjeto;
import br.com.biharckgroup.scr.service.UfService;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;


@DefaultOrderBy("projeto.id")
public class ProjetoDAO extends GenericDAOSCR<Projeto> {
	
	private ResponsavelProjetoDAO responsavelProjetoDAO;
	private GrupoTipoResiduoProjetoDAO grupoTipoResiduoProjetoDAO;
	private TransportadorProjetoDAO transportadorProjetoDAO;
	private UfService ufService;

	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	public void setResponsavelProjetoDAO(
			ResponsavelProjetoDAO responsavelProjetoDAO) {
		this.responsavelProjetoDAO = responsavelProjetoDAO;
	}
	public void setGrupoTipoResiduoProjetoDAO(
			GrupoTipoResiduoProjetoDAO grupoTipoResiduoProjetoDAO) {
		this.grupoTipoResiduoProjetoDAO = grupoTipoResiduoProjetoDAO;
	}
	public void setTransportadorProjetoDAO(
			TransportadorProjetoDAO transportadorProjetoDAO) {
		this.transportadorProjetoDAO = transportadorProjetoDAO;
	}
	
	@Override
	public void saveOrUpdate(final Projeto bean) {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				try{
					ProjetoDAO.super.saveOrUpdate(bean);
				} catch (DataIntegrityViolationException e) {
					if (DatabaseError.isKeyPresent(e, "unique_numProjeto")){ //nota o index na tabela tem q ter o mesmo nome
						throw new SCRException("J� existe um Projeto com este N�mero de Projeto cadastrado no sistema.");
					}
				}
				
				//salva numeroProjeto
				bean.setNumeroProjeto(SCRUtil.getAno()+"/"+bean.getId()); //O n�mero de identifica��o --> Ano corrente + / + id
				updateNumeroProjeto(bean);
				
				//salva transportadores do projeto
				transportadorProjetoDAO.deleteByProjeto(bean);
				if(bean.getTransportadoresProjeto() != null){
					for (TransportadorProjeto transportadorProjeto : bean.getTransportadoresProjeto()) {
						transportadorProjeto.setProjeto(bean);
						transportadorProjetoDAO.saveOrUpdate(transportadorProjeto);
					}
				}
				//salva responsaveis pelo projeto
				responsavelProjetoDAO.deleteByProjeto(bean);
				if(bean.getResponsaveisProjeto() != null){
					for (ResponsavelProjeto responsavelProjeto : bean.getResponsaveisProjeto()) {
						responsavelProjeto.setProjeto(bean);
						responsavelProjetoDAO.saveOrUpdate(responsavelProjeto);
					}
				}
				//salva grupoTipoResiduoProjeto 
				grupoTipoResiduoProjetoDAO.deleteByProjeto(bean);
				if(bean.getGruposTipoResiduoProjeto() != null){
					for (GrupoTipoResiduoProjeto grupoTipoResiduoProjeto : bean.getGruposTipoResiduoProjeto()) {
						grupoTipoResiduoProjeto.setProjeto(bean);
						grupoTipoResiduoProjetoDAO.saveOrUpdate(grupoTipoResiduoProjeto);
					}
				}
				
				return null;
			}
			
		});
	}
	
	@Override
	public Projeto loadForEntrada(Projeto form) {
		form = super.loadForEntrada(form);
		
		form.setGruposTipoResiduoProjeto(grupoTipoResiduoProjetoDAO.findByProjeto(form));
		form.setTransportadoresProjeto(transportadorProjetoDAO.findByProjeto(form));
		form.setResponsaveisProjeto(responsavelProjetoDAO.findByProjeto(form));
		
		defineCampoTransient(form);
		
		return form;
	}
	
	@Override
	public Projeto load(Projeto form) {
		form = super.load(form);
		
		form.setGruposTipoResiduoProjeto(grupoTipoResiduoProjetoDAO.findByProjeto(form));
		form.setTransportadoresProjeto(transportadorProjetoDAO.findByProjeto(form));
		
		defineCampoTransient(form);
		
		return form;
	}
	
	public Projeto loadByNumeroProjeto(String  numeroProjeto) {
		Projeto bean = query()
				.select("projeto")
				.where("UPPER(projeto.numeroProjeto) = ?", numeroProjeto.toUpperCase())			
				.unique();
		
		bean.setTransportadoresProjeto(transportadorProjetoDAO.findByProjeto(bean));
		defineCampoTransient(bean);
		return bean;
	}
	
	@Override
	public ResultList<Projeto> findForListagem(FiltroListagem filtro) {
		
		ResultList<Projeto> list = super.findForListagem(filtro);
		
		for (Projeto projeto : list.list()) {
			if((new Date()).before(projeto.getDataInicio()))//Criado, quando a data atual < da data inicial
				projeto.setEnumStatusProjeto(EnumStatusProjeto.CRIADO);
			else if((new Date()).after(projeto.getDataInicio()) && (new Date()).before(projeto.getDataTermino())) //quando a data atual > data inicial && data atual < data termino 
				projeto.setEnumStatusProjeto(EnumStatusProjeto.INICIADO);
			else //Finalizado quando a data atual > data final
				projeto.setEnumStatusProjeto(EnumStatusProjeto.FINALIZADO);
		}
		return list;
	}
	
	public void updateNumeroProjeto(Projeto bean) {
		getJdbcTemplate().update("update projeto set numeroProjeto = ?" +
				" where id = ?", bean.getNumeroProjeto(), bean.getId());
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Projeto> query,
			FiltroListagem _filtro) {
		ProjetoFiltro filtro = (ProjetoFiltro) _filtro;

		query
			.whereLikeIgnoreAll("projeto.numeroProjeto", filtro.getNumeroProjeto());
		
		if(filtro.getResponsaveis()!=null){
			String idsResponsaveis = CollectionsUtil.listAndConcatenate(filtro.getResponsaveis(), "id", ",");
			query
			.leftOuterJoin("projeto.responsaveisProjeto rp")
			.whereIn("rp.responsavel", idsResponsaveis);
		}
		
		if(filtro.getGrandeGerador() != null){
			query
				.leftOuterJoin("projeto.grandeGerador pg")
				.where("pg=?", filtro.getGrandeGerador());
		}
		
		if(filtro.getTransportador() != null){
			query
			.leftOuterJoin("projeto.transportadoresProjeto transpProj")
			.leftOuterJoin("transpProj.transportador transportador")
			.where("transportador=?", filtro.getTransportador());
		}
		
		//busca status do projeto
		if(filtro.getEnumStatusProjeto() != null){
			if(filtro.getEnumStatusProjeto() == EnumStatusProjeto.CRIADO)
				query.where("current_date <= projeto.dataInicio ");
			else if(filtro.getEnumStatusProjeto() == EnumStatusProjeto.INICIADO)
				query.where("current_date >= projeto.dataInicio and current_date < projeto.dataTermino ");
			else//finalizado
				query.where("current_date >= projeto.dataTermino ");
		}
		
	   if(filtro.getDestinoFinal()!=null){
			query.leftOuterJoin("projeto.gruposTipoResiduoProjeto pgt")
			.leftOuterJoin("pgt.destinoFinal df")
			.where("df = ?", filtro.getDestinoFinal());
		}
		
		if(filtro.getDataCriacao()!=null)
			query.where("date(projeto.timeInc) = ?",filtro.getDataCriacao());
		
	}
	
	public List<Projeto> findByFiltro(ProjetoReportFiltro filtro) {
		QueryBuilder<Projeto> query = query();
		
		query.select("projeto.id, projeto.numeroProjeto, projeto.numeroProcesso, projeto.dataInicio,projeto.dataTermino," +
				"projeto.enderecoObra,projeto.numeroObra, projeto.bairroObra, projeto.municipioObra,resid.id, resid.nome," +
				" transp.id, transp.nomeFantasia,resp.id, resp.nome")
				.leftOuterJoin("projeto.gruposTipoResiduoProjeto pg")
				.leftOuterJoin("pg.grupoTipoResiduo resid")
				.leftOuterJoin("projeto.transportadoresProjeto pt")
				.leftOuterJoin("pt.transportador transp")
				.leftOuterJoin("projeto.responsaveisProjeto pr")
				.leftOuterJoin("pr.responsavel resp");
		
		if(filtro.getTransportador() != null)
			query.whereIn("transp",filtro.getTransportador().getId().toString());
			
				
		//busca status do projeto
		if(filtro.getEnumStatusProjeto() != null){
			if(filtro.getEnumStatusProjeto() == EnumStatusProjeto.CRIADO)
				query.where("current_date <= projeto.dataInicio ");
			else if(filtro.getEnumStatusProjeto() == EnumStatusProjeto.INICIADO)
				query.where("current_date >= projeto.dataInicio and current_date < projeto.dataTermino ");
			else//finalizado
				query.where("current_date >= projeto.dataTermino ");
		}
		
		if(filtro.getDataInicio()!=null)
			query.where("projeto.dataInicio >= ?", filtro.getDataInicio());
		if(filtro.getDataTermino()!=null)
			query.where("projeto.dataTermino <= ?", filtro.getDataTermino());
				
		
		return query.list();
	}
	
	public void defineCampoTransient(Projeto form){
		/*Preenche campo cpfCnpjTransient
		 * Quando tiver duas op��es cpf/cnpj verificar qual est� preenchido*/
		String cpfCnpjTransient;
		//transportador 
		for (TransportadorProjeto transportadorProjeto : form.getTransportadoresProjeto()) {
			if(transportadorProjeto.getTransportador() != null){
				cpfCnpjTransient = transportadorProjeto.getTransportador().getCnpj()!= null && !transportadorProjeto.getTransportador().getCnpj().equals("") ? transportadorProjeto.getTransportador().getCnpj():  transportadorProjeto.getTransportador().getCpf(); 
				transportadorProjeto.getTransportador().setCpfCnpjTransient(cpfCnpjTransient);
			}
		}
		//define uf
		if(form.getMunicipioObra()!=null)
			form.setUfObra(ufService.getUfByMunicipio(form.getMunicipioObra()));
	}
	
	@Override
	public void delete(Projeto bean) {
		transportadorProjetoDAO.deleteByProjeto(bean);
		responsavelProjetoDAO.deleteByProjeto(bean);
		grupoTipoResiduoProjetoDAO.deleteByProjeto(bean);
		super.delete(bean);
	}
	
	public List<Projeto> getProjetosByDestinoFinal(DestinoFinal df){
		return query().select("projeto")
					  .leftOuterJoin("projeto.gruposTipoResiduoProjeto pg")
					  .leftOuterJoin("pg.destinoFinal df")
					  .where("df = ?", df).list();
	}
	public List<Projeto> getProjetosDiaByDestinoFinal(DestinoFinal df){
		return query().select("projeto.id, pg.quantidade")
				  .leftOuterJoin("projeto.gruposTipoResiduoProjeto pg")
				  .leftOuterJoin("pg.destinoFinal df")
				  .where("df = ?", df).where("date(projeto.timeInc) = ? ", new Date()).list();
	}
	public List<Projeto> getProjetosByGrandeGerador(GrandeGerador gg){
		return query().select("projeto")
					  .leftOuterJoin("projeto.grandeGerador gg")
					  .where("gg = ?", gg).where("projeto.userDel is null").list();
	}
}