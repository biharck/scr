package br.com.biharckgroup.scr.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.biharckgroup.scr.adm.filtro.TipoTransportadorFiltro;
import br.com.biharckgroup.scr.bean.TipoTransportador;


@DefaultOrderBy("nome")
public class TipoTransportadorDAO extends GenericDAOSCR<TipoTransportador> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<TipoTransportador> query,
			FiltroListagem _filtro) {
		
		TipoTransportadorFiltro filtro = (TipoTransportadorFiltro) _filtro;
		
		query.whereLikeIgnoreAll("nome", filtro.getNome());
				
		if(filtro.getAtivo()!=null)			
			query.where("ativo is "+filtro.getAtivo().isOptBool());
		
	}

}
