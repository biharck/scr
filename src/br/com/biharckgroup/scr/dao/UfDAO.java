package br.com.biharckgroup.scr.dao;
import org.nextframework.persistence.DefaultOrderBy;

import br.com.biharckgroup.scr.bean.Municipio;
import br.com.biharckgroup.scr.bean.Uf;


@DefaultOrderBy("nome")
public class UfDAO extends GenericDAOSCR<Uf> {
	
	public Uf getUfByMunicipio(Municipio m){
		return query().join("uf.municipios m").where("m=?",m).unique();
	}
}
