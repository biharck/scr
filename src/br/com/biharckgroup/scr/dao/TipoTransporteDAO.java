package br.com.biharckgroup.scr.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.biharckgroup.scr.adm.filtro.TipoTransporteFiltro;
import br.com.biharckgroup.scr.bean.TipoTransporte;

@DefaultOrderBy("nome")
public class TipoTransporteDAO extends GenericDAOSCR<TipoTransporte> {

	@Override
	public void updateListagemQuery(QueryBuilder<TipoTransporte> query,FiltroListagem _filtro) {
		
		TipoTransporteFiltro filtro = (TipoTransporteFiltro) _filtro;
		query
			.whereLikeIgnoreAll("tipoTransporte.nome", filtro.getNome())
			.whereLikeIgnoreAll("tipoTransporte.descricao", filtro.getDescricao());
		if(filtro.getAtivo()!=null)			
			query.where("tipoTransporte.ativo is "+filtro.getAtivo().isOptBool());
	}

}
