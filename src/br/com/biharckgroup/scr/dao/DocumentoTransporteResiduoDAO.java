package br.com.biharckgroup.scr.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.ResultList;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.biharckgroup.scr.adm.filtro.DocumentoTransporteResiduoFiltro;
import br.com.biharckgroup.scr.bean.DocumentoTransporteResiduo;
import br.com.biharckgroup.scr.bean.EnumSituacaoRTR;


@DefaultOrderBy("documentoTransporteResiduo.id")
public class DocumentoTransporteResiduoDAO extends GenericDAOSCR<DocumentoTransporteResiduo> {
	
	@Override
	public void saveOrUpdate(final DocumentoTransporteResiduo bean) {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				//define situa��o
				bean.setSituacao(bean.getEnumSituacao().getValor());
				DocumentoTransporteResiduoDAO.super.saveOrUpdate(bean);
				return null;
			}
			
		});
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<DocumentoTransporteResiduo> query,FiltroListagem _filtro) {
		DocumentoTransporteResiduoFiltro filtro = (DocumentoTransporteResiduoFiltro) _filtro;
		query
			.leftOuterJoin("documentoTransporteResiduo.projeto p")
			.leftOuterJoin("p.grandeGerador gg")
			.where("gg=?",filtro.getGrandeGerador());
	}
	
	@Override
	public DocumentoTransporteResiduo loadForEntrada(
			DocumentoTransporteResiduo bean) {
		bean =  super.loadForEntrada(bean);
		bean.setEnumSituacao(EnumSituacaoRTR.getEnumSituacaoByValor(bean.getSituacao()));
		
		return bean;
	}
	
	@Override
	public ResultList<DocumentoTransporteResiduo> findForListagem(
			FiltroListagem filtro) {

		ResultList<DocumentoTransporteResiduo> list = super.findForListagem(filtro);
		for (DocumentoTransporteResiduo bean : list.list()) {
			bean.setEnumSituacao(EnumSituacaoRTR.getEnumSituacaoByValor(bean.getSituacao()));
		}
		return list;
	
	}
}
