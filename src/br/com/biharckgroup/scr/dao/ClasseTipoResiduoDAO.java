package br.com.biharckgroup.scr.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.biharckgroup.scr.adm.filtro.ClasseTipoResiduoFiltro;
import br.com.biharckgroup.scr.bean.ClasseTipoResiduo;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRException;

@DefaultOrderBy("nome")
public class ClasseTipoResiduoDAO extends GenericDAOSCR<ClasseTipoResiduo> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<ClasseTipoResiduo> query,	FiltroListagem _filtro) {
		ClasseTipoResiduoFiltro filtro = (ClasseTipoResiduoFiltro) _filtro;
		
		query.whereLikeIgnoreAll("nome", filtro.getNome());
		
		if(filtro.getAtivo()!=null)			
			query.where("ativo is "+filtro.getAtivo().isOptBool());

	}
	@Override
	public void saveOrUpdate(ClasseTipoResiduo bean) {
		try{
			super.saveOrUpdate(bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_nome")){ //nota o index na tabela tem q ter o mesmo nome
				throw new SCRException("J� existe um registro de Classes de Tipos de Res�duos com este nome cadastrado no sistema.");
			}
		}
	}
}
