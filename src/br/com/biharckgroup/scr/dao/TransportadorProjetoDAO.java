package br.com.biharckgroup.scr.dao;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.persistence.GenericDAO;

import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.bean.TransportadorProjeto;


public class TransportadorProjetoDAO extends GenericDAO<TransportadorProjeto> {
	
	public void deleteByProjeto(Projeto projeto) {
		getHibernateTemplate().bulkUpdate(
				"delete from TransportadorProjeto where projeto = ?", projeto);
	}
	
	public List<TransportadorProjeto> findByProjeto(Projeto projeto) {
		if (projeto.getId() == null) {
			return new ArrayList<TransportadorProjeto>();
		}
		return query().select("transportadorProjeto").from(TransportadorProjeto.class)
				.where("transportadorProjeto.projeto = ?", projeto).list();
	}
	
}
