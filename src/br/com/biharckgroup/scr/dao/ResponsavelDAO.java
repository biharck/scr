package br.com.biharckgroup.scr.dao;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.biharckgroup.scr.adm.filtro.ResponsavelFiltro;
import br.com.biharckgroup.scr.bean.EnumPapel;
import br.com.biharckgroup.scr.bean.PapelUsuario;
import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.bean.Responsavel;
import br.com.biharckgroup.scr.bean.ResponsavelProjeto;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRException;

@DefaultOrderBy("responsavel.id")
public class ResponsavelDAO extends GenericDAOSCR<Responsavel> {

	public List<Responsavel> findByProjeto(Projeto projeto) {
		if (projeto.getId() == null) {
			return new ArrayList<Responsavel>();
		}
		return query().select("responsavel").from(ResponsavelProjeto.class)
			.leftOuterJoin("responsavelProjeto.responsavel responsavel")		
			.where("responsavelProjeto.projeto = ?", projeto).list();
	}
	
	@Override
	public Responsavel load(Responsavel bean) {
		bean = super.load(bean);
		/*Preenche campo cpfCnpjTransient
		 * Quando tiver duas op��es cpf/cnpj verificar qual est� preenchido*/
		String cpfCnpjTransient = bean.getCnpj()!= null && !bean.getCnpj().equals("") ? bean.getCnpj():  bean.getCpf(); 
		bean.setCpfCnpjTransient(cpfCnpjTransient);
		return bean;
	}
	
	@Override
	public Responsavel loadForEntrada(Responsavel bean) {
		bean = super.loadForEntrada(bean);
		/*Preenche campo cpfCnpjTransient
		 * Quando tiver duas op��es cpf/cnpj verificar qual est� preenchido*/
		String cpfCnpjTransient = bean.getCnpj()!= null && !bean.getCnpj().equals("") ? bean.getCnpj():  bean.getCpf(); 
		bean.setCpfCnpjTransient(cpfCnpjTransient);
		return bean;
	}
	
	@Override
	public void saveOrUpdate(Responsavel bean) {
		try{
			super.saveOrUpdate(bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_cpf")){ //nota o index na tabela tem q ter o mesmo nome
				throw new SCRException("J� existe um registro de Grande Gerador com este CPF cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "unique_cnpj")){ //nota o index na tabela tem q ter o mesmo nome
				throw new SCRException("J� existe um registro de Grande Gerador com este CNPJ cadastrado no sistema.");
			}
		}
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Responsavel> query,
			FiltroListagem _filtro) {
		ResponsavelFiltro filtro = (ResponsavelFiltro) _filtro;
		
		query
			.whereLikeIgnoreAll("nome", filtro.getNome());
		
		if(filtro.getGrandeGerador() != null){
			query
				.leftOuterJoin("responsavel.grandeGerador rg")
				.where("rg=?", filtro.getGrandeGerador());
		}
		
	}
	
}