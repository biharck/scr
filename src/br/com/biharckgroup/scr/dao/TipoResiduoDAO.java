package br.com.biharckgroup.scr.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.biharckgroup.scr.adm.filtro.TipoResiduoFiltro;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduo;
import br.com.biharckgroup.scr.bean.TipoResiduo;

@DefaultOrderBy("tipoResiduo.grupoTipoResiduo,descricao")
public class TipoResiduoDAO extends GenericDAOSCR<TipoResiduo> {

	@Override
	public void updateListagemQuery(QueryBuilder<TipoResiduo> query,FiltroListagem _filtro) {
		TipoResiduoFiltro filtro = (TipoResiduoFiltro) _filtro;

			query.
				whereLikeIgnoreAll("descricao", filtro.getDescricao())
			.leftOuterJoin("tipoResiduo.grupoTipoResiduo gtr")
			.leftOuterJoin("tipoResiduo.classeTipoResiduo ctr")
			.where("gtr=?",filtro.getGrupoTipoResiduo())
			.where("ctr=?",filtro.getClasseTipoResiduo());
			
		if(filtro.getAtivo()!=null)			
			query.where("tipoResiduo.ativo is "+filtro.getAtivo().isOptBool());
		
		if(filtro.getReciclavel()!=null)			
			query.where("tipoResiduo.reciclavel is "+filtro.getReciclavel().isOptBool());

	}
	
	public List<TipoResiduo> getTiposResiduosByGrupo(GrupoTipoResiduo grupoTipoResiduo){
		return query().select("tipoResiduo").leftOuterJoin("tipoResiduo.grupoTipoResiduo gtr").where("gtr=?",grupoTipoResiduo).list();
	}
}
