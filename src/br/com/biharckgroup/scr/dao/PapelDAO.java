package br.com.biharckgroup.scr.dao;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;

import br.com.biharckgroup.scr.adm.filtro.PapelFiltro;
import br.com.biharckgroup.scr.bean.Papel;
import br.com.biharckgroup.scr.bean.PapelUsuario;
import br.com.biharckgroup.scr.bean.Usuario;

@DefaultOrderBy("nome")
public class PapelDAO extends GenericDAO<Papel> {

	public List<Papel> findByUsuario(Usuario usuario) {
		if (usuario.getId() == null) {
			return new ArrayList<Papel>();
		}
		return query().select("papel").from(PapelUsuario.class)
				.leftOuterJoin("papelUsuario.papel papel")
				.where("papelUsuario.usuario = ?", usuario).list();
	}

	public void deleteByUsuario(Usuario usuario) {
		getHibernateTemplate().bulkUpdate(
				"delete from PapelUsuario where usuario = ?", usuario);
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Papel> query,
			FiltroListagem _filtro) {
		PapelFiltro filtro = (PapelFiltro) _filtro;

		if (filtro.getNome() != null)
			query.whereLikeIgnoreAll("papel.nome", filtro.getNome());
		else
			super.updateListagemQuery(query, _filtro);
	}
	
	public Papel findNomeById(Papel bean){
		return query()
				.select("papel.nome")
				.where("papel = ?", bean)
				.unique();
		
	}
}
