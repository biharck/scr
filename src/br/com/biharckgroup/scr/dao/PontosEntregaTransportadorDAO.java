package br.com.biharckgroup.scr.dao;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import br.com.biharckgroup.scr.bean.PontosEntregaTransportador;
import br.com.biharckgroup.scr.bean.Transportador;

@DefaultOrderBy("pontosEntregaTransportador.transportador")
public class PontosEntregaTransportadorDAO extends GenericDAO<PontosEntregaTransportador> {
	
	public void deleteByTransportador(Transportador transportador) {
		getHibernateTemplate().bulkUpdate(
				"delete from PontosEntregaTransportador where transportador = ?", transportador);
	}
	
}
