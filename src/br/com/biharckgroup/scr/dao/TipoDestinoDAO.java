package br.com.biharckgroup.scr.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.biharckgroup.scr.adm.filtro.TipoDestinoFiltro;
import br.com.biharckgroup.scr.bean.TipoDestino;

@DefaultOrderBy("nome")
public class TipoDestinoDAO extends GenericDAOSCR<TipoDestino> {


	@Override
	public void updateListagemQuery(QueryBuilder<TipoDestino> query,
			FiltroListagem _filtro) {
		TipoDestinoFiltro filtro = (TipoDestinoFiltro) _filtro;

		query.whereLikeIgnoreAll("tipoDestino.nome", filtro.getNome());

		if(filtro.getAtivo()!=null)			
			query.where("tipoDestino.ativo is "+filtro.getAtivo().isOptBool());
	}
	
}