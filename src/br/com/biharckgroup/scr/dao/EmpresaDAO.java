package br.com.biharckgroup.scr.dao;

import org.nextframework.core.standard.Next;

import br.com.biharckgroup.scr.bean.Empresa;

public class EmpresaDAO extends GenericDAOSCR<Empresa>{
	
	/**
	 * <p>Método responsável em retornar os dados da empresa
	 * @return {@link Empresa}
	 * @author biharck
	 */
	private static EmpresaDAO instance;
	public static EmpresaDAO getInstance() {
		if(instance == null){
			instance = Next.getObject(EmpresaDAO.class);
		}
		return instance;
	}
	public Empresa getEmpresa(){
		return
			query().select("empresa").unique();
	}
	
	

}
