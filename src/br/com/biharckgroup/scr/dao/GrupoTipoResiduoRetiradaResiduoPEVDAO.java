package br.com.biharckgroup.scr.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.biharckgroup.scr.bean.GrupoTipoResiduoRetiradaResiduoPEV;
import br.com.biharckgroup.scr.bean.RetiradaResiduoPEV;

public class GrupoTipoResiduoRetiradaResiduoPEVDAO extends GenericDAOSCR<GrupoTipoResiduoRetiradaResiduoPEV> {

	public void deleteByRetiradaResiduoPEV(RetiradaResiduoPEV retiradaResiduoPEV ) {
		getHibernateTemplate().bulkUpdate(
				"delete GrupoTipoResiduoRetiradaResiduoPEV where retiradaResiduoPEV = ?", retiradaResiduoPEV);
	}
	
	public List<GrupoTipoResiduoRetiradaResiduoPEV> findByRetiradaResiduoPEV(RetiradaResiduoPEV bean) {
		if (bean.getId() == null) {
			return new ArrayList<GrupoTipoResiduoRetiradaResiduoPEV>();
		}
		return query().select("grupoTipoResiduoRetiradaResiduoPEV").from(GrupoTipoResiduoRetiradaResiduoPEV.class)
				.where("grupoTipoResiduoRetiradaResiduoPEV.retiradaResiduoPEV = ?", bean).list();
	}
}
