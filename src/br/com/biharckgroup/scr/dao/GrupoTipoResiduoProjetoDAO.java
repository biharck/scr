package br.com.biharckgroup.scr.dao;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import br.com.biharckgroup.scr.bean.GrupoTipoResiduoProjeto;
import br.com.biharckgroup.scr.bean.Projeto;


@DefaultOrderBy("id")
public class GrupoTipoResiduoProjetoDAO extends GenericDAO<GrupoTipoResiduoProjeto> {

	public void deleteByProjeto(Projeto projeto) {
		getHibernateTemplate().bulkUpdate(
				"delete from GrupoTipoResiduoProjeto where projeto = ?", projeto);
	}
	public List<GrupoTipoResiduoProjeto> findByProjeto(Projeto projeto) {
		if (projeto.getId() == null) {
			return new ArrayList<GrupoTipoResiduoProjeto>();
		}
		return query().select("grupoTipoResiduoProjeto").from(GrupoTipoResiduoProjeto.class)
				.where("grupoTipoResiduoProjeto.projeto = ?", projeto).list();
	}
	
}
