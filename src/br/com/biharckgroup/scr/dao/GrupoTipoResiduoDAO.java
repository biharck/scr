package br.com.biharckgroup.scr.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.biharckgroup.scr.adm.filtro.GrupoTipoResiduoFiltro;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduo;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRException;

@DefaultOrderBy("nome")
public class GrupoTipoResiduoDAO extends GenericDAOSCR<GrupoTipoResiduo> {

	@Override
	public void updateListagemQuery(QueryBuilder<GrupoTipoResiduo> query,FiltroListagem _filtro) {
		GrupoTipoResiduoFiltro filtro = (GrupoTipoResiduoFiltro) _filtro;

		query.
			whereLikeIgnoreAll("nome", filtro.getNome());
	
		if(filtro.getAtivo()!=null)			
			query.where("ativo is "+filtro.getAtivo().isOptBool());

	}
	
	@Override
	public void saveOrUpdate(GrupoTipoResiduo bean) {
		try{
			super.saveOrUpdate(bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_nome")){ //nota o index na tabela tem q ter o mesmo nome
				throw new SCRException("J� existe um registro de Grupos de Tipos de Res�duos com este nome cadastrado no sistema.");
			}
		}
	}

}
