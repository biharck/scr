package br.com.biharckgroup.scr.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.biharckgroup.scr.adm.filtro.RetiradaResiduoPEVFiltro;
import br.com.biharckgroup.scr.adm.relatorio.filtro.RetiradaResiduoPEVReportFiltro;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduoRetiradaResiduoPEV;
import br.com.biharckgroup.scr.bean.RetiradaResiduoPEV;

public class RetiradaResiduoPEVDAO extends GenericDAOSCR<RetiradaResiduoPEV> {
	
	private GrupoTipoResiduoRetiradaResiduoPEVDAO grupoTipoResiduoRetiradaResiduoPEVDAO;
	
	public void setGrupoTipoResiduoRetiradaResiduoPEVDAO(GrupoTipoResiduoRetiradaResiduoPEVDAO grupoTipoResiduoRetiradaResiduoPEVDAO) {
		this.grupoTipoResiduoRetiradaResiduoPEVDAO = grupoTipoResiduoRetiradaResiduoPEVDAO;
	}
	
	@Override
	public void saveOrUpdate(final RetiradaResiduoPEV bean) {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				RetiradaResiduoPEV load = new RetiradaResiduoPEV();
				/* update */
				if(bean.getId() != null){
					load = load(bean);
				}
				//atualiza somente campos do formulario
				load.setPontoEntrega(bean.getPontoEntrega());
				load.setData(bean.getData());
				load.setTransportador(bean.getTransportador());
				load.setHora(bean.getHora());
				load.setResponsavel(bean.getResponsavel());
				load.setTransportador(bean.getTransportador());
				load.setGuia(bean.getGuia());

				RetiradaResiduoPEVDAO.super.saveOrUpdate(load);

				grupoTipoResiduoRetiradaResiduoPEVDAO.deleteByRetiradaResiduoPEV(load);
				if(bean.getGrupoTipoResiduosRetiradaResiduoPEV() != null){
					for (GrupoTipoResiduoRetiradaResiduoPEV grupoTipoResiduoRetiradaResiduoPEV : bean.getGrupoTipoResiduosRetiradaResiduoPEV()) {
						grupoTipoResiduoRetiradaResiduoPEV.setRetiradaResiduoPEV(load);
						grupoTipoResiduoRetiradaResiduoPEVDAO.saveOrUpdate(grupoTipoResiduoRetiradaResiduoPEV);
					}
				}
				return null;
			}
			
		});
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<RetiradaResiduoPEV> query, 	FiltroListagem _filtro) {
		RetiradaResiduoPEVFiltro filtro = (RetiradaResiduoPEVFiltro) _filtro;

		query
			.leftOuterJoin("retiradaResiduoPEV.pontoEntrega pe ")
			.leftOuterJoin("retiradaResiduoPEV.transportador tr ")
			.where("pe=?", filtro.getPontoEntrega())
			.where("tr=?",filtro.getTransportador())
			.whereLikeIgnoreAll("retiradaResiduoPEV.responsavel", filtro.getResponsavel());
		
		if(filtro.getDataIni() != null)
			query.where("retiradaResiduoPEV.data >= ?",filtro.getDataIni());
		if(filtro.getDataFim() != null)
			query.where("retiradaResiduoPEV.data <= ?",filtro.getDataFim());
		
	}
	
	public List<RetiradaResiduoPEV> findByFiltro(RetiradaResiduoPEVReportFiltro filtro) {
		QueryBuilder<RetiradaResiduoPEV> query = query();
		
		query.select("transp.id, transp.nomeFantasia, " +
				"pev.id,pev.nome ,  pev.identificacao, pev.identificacao , pev.responsavel," +
				"retiradaResiduoPEV.id, retiradaResiduoPEV.data, retiradaResiduoPEV.hora, retiradaResiduoPEV.responsavel, retiradaResiduoPEV.guia," +
				"grupoTipoRes.nome,grupoTipoRes.id, tipoRes.id, tipoRes.descricao, eg.quantidade, um.nome")
				.leftOuterJoin("retiradaResiduoPEV.grupoTipoResiduosRetiradaResiduoPEV eg")
				.leftOuterJoin("eg.tipoResiduo tipoRes")
				.leftOuterJoin("eg.grupoTipoResiduo grupoTipoRes")
				.leftOuterJoin("eg.unidadeMedida um")
				.leftOuterJoin("retiradaResiduoPEV.pontoEntrega pev")
				.leftOuterJoin("retiradaResiduoPEV.transportador transp")
				.where("pev=?", filtro.getPontoEntrega());
		
		if(filtro.getDataIni() != null)
			query.where("retiradaResiduoPEV.data >= ?",filtro.getDataIni());
		if(filtro.getDataFim() != null)
			query.where("retiradaResiduoPEV.data <= ?",filtro.getDataFim());
		
		return query.list();
	}

	@Override
	public RetiradaResiduoPEV load(RetiradaResiduoPEV bean) {
		bean = super.load(bean);
		bean.setGrupoTipoResiduosRetiradaResiduoPEV(grupoTipoResiduoRetiradaResiduoPEVDAO.findByRetiradaResiduoPEV(bean));
		return bean;
	}
	@Override
	public RetiradaResiduoPEV loadForEntrada(RetiradaResiduoPEV bean) {
		bean = super.loadForEntrada(bean);
		bean.setGrupoTipoResiduosRetiradaResiduoPEV(grupoTipoResiduoRetiradaResiduoPEVDAO.findByRetiradaResiduoPEV(bean));
		return bean;
	}
	
	@Override
	public void apagarFisicamente(RetiradaResiduoPEV bean) {
		// TODO Auto-generated method stub
		super.apagarFisicamente(bean);
	}
	
	
	
	@Override
	public void delete(RetiradaResiduoPEV bean) {
		grupoTipoResiduoRetiradaResiduoPEVDAO.deleteByRetiradaResiduoPEV(bean);
		super.delete(bean);
	}
	
}
