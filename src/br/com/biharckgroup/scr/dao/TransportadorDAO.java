package br.com.biharckgroup.scr.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.biharckgroup.scr.adm.filtro.TransportadorFiltro;
import br.com.biharckgroup.scr.bean.PontoEntrega;
import br.com.biharckgroup.scr.bean.PontosEntregaTransportador;
import br.com.biharckgroup.scr.bean.Transportador;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.service.UfService;
import br.com.biharckgroup.scr.service.UsuarioService;

@DefaultOrderBy("transportador.razaoSocial")
public class TransportadorDAO extends GenericDAOSCR<Transportador> {

	private PontoEntregaDAO  credenciaisPEVDAO;
	private PontosEntregaTransportadorDAO pontosEntregaTransportadorDAO;
	private UsuarioService usuarioService;
	private UfService ufService;
	
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	public void setCredenciaisPEVDAO(PontoEntregaDAO credenciaisPEVDAO) {
		this.credenciaisPEVDAO = credenciaisPEVDAO;
	}
	public void setPontosEntregaTransportadorDAO(PontosEntregaTransportadorDAO pontosEntregaTransportadorDAO) {
		this.pontosEntregaTransportadorDAO = pontosEntregaTransportadorDAO;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}


	@Override
	public void updateListagemQuery(QueryBuilder<Transportador> query,
			FiltroListagem _filtro) {
		TransportadorFiltro filtro = (TransportadorFiltro) _filtro;

		query
		.whereLikeIgnoreAll("transportador.razaoSocial", filtro.getRazaoSocial())
		.whereLikeIgnoreAll("transportador.nomeFantasia", filtro.getNomeFantasia());

		if(filtro.getCpfCnpjTransient()!=null){
			if (filtro.getCpfCnpjTransient().length()== 14)
				query.whereLikeIgnoreAll("transportador.cpf", filtro.getCpfCnpjTransient());
			else
				query.whereLikeIgnoreAll("transportador.cnpj", filtro.getCpfCnpjTransient());
		}
	}
	
	
	@Override
	public Transportador loadForEntrada(Transportador bean) {
		bean = super.loadForEntrada(bean);
		/*Preenche campo cpfCnpjTransient
		 * Quando tiver duas op��es cpf/cnpj verificar qual est� preenchido*/
		String cpfCnpjTransient = bean.getCnpj()!= null && !bean.getCnpj().equals("") ? bean.getCnpj():  bean.getCpf(); 
		bean.setCpfCnpjTransient(cpfCnpjTransient);
		//busca credenciais do transportador
		bean.setCredenciaisPEV(credenciaisPEVDAO.findByTransportador(bean));
		//define usuarios
		bean.setUsuarios(usuarioService.findByTransportador(bean));
		bean.setUf(ufService.getUfByMunicipio(bean.getMunicipio()));
		return bean;
	}
	
	@Override
	public void saveOrUpdate(final Transportador bean) {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				
				if(bean.getId()==null && !bean.isAutoCadastro())
					bean.setAtivo(true);
				TransportadorDAO.super.saveOrUpdate(bean);
				
				if(bean.getUsuarios() != null && bean.getUsuarios().get(0).getId()!=null){
					//elimina referencias dos destinos finais na tabela usu�rio
					usuarioService.removeTransportador(bean);
					if(bean.getUsuarios() != null){
						for ( Usuario usuario : bean.getUsuarios()) {
							usuario.setTransportador(bean);
							usuarioService.updateTransportador(usuario, bean);
						}
					}
				}
				
				pontosEntregaTransportadorDAO.deleteByTransportador(bean);
				if(bean.getCredenciaisPEV() != null){
					for (PontoEntrega pontoEntrega : bean.getCredenciaisPEV()) {
						PontosEntregaTransportador pontoEntregaTransportador = new PontosEntregaTransportador();
						pontoEntregaTransportador.setPontoEntrega(pontoEntrega);
						pontoEntregaTransportador.setTransportador(bean);
						pontosEntregaTransportadorDAO.saveOrUpdate(pontoEntregaTransportador);
					}
				}
				return null;
			}
			
		});
	}
	
	@Override
	public Transportador load(Transportador bean) {
		bean = super.load(bean);
		/*Preenche campo cpfCnpjTransient
		 * Quando tiver duas op��es cpf/cnpj verificar qual est� preenchido*/
		String cpfCnpjTransient = bean.getCnpj()!= null && !bean.getCnpj().equals("") ? bean.getCnpj():  bean.getCpf(); 
		bean.setCpfCnpjTransient(cpfCnpjTransient);
		bean.setUf(ufService.getUfByMunicipio(bean.getMunicipio()));
		return bean;
	}
	
	public Transportador findByUsuario(Usuario usuario) {
		Transportador bean = query()
			.where("transportador.usuario = ?", usuario)			
			.unique();
		return bean;
	}
	
	public List<Transportador> getTransportadoresAutoCadastro(){
		return query()
				.select("transportador")
				.where("transportador.autoCadastro is true")
				.where("transportador.ativo is false")
				.list();
	}

	public Transportador findNomeFantasiaById(Transportador bean){
		return query()
				.select("transportador.nomeFantasia")
				.where("transportador = ?", bean)
				.unique();
		
	}
	
	@Override
	public void delete(Transportador bean) {
		pontosEntregaTransportadorDAO.deleteByTransportador(bean);
		super.delete(bean);
	}
}
