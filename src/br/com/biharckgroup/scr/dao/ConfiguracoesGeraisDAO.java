package br.com.biharckgroup.scr.dao;

import br.com.biharckgroup.scr.bean.ConfiguracoesGerais;

public class ConfiguracoesGeraisDAO extends GenericDAOSCR<ConfiguracoesGerais> {
	
	/**
	 * <p>Método responsável em retornar as Configuracoes Gerais
	 * @return {@link ConfiguracoesGerais}
	 * @author biharck
	 */
	public ConfiguracoesGerais getConfiguracoesGerais(){
		return
			query().select("configuracoesGerais").unique();
	}

}
