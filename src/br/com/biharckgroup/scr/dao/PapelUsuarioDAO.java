package br.com.biharckgroup.scr.dao;

import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import br.com.biharckgroup.scr.bean.PapelUsuario;
import br.com.biharckgroup.scr.bean.Usuario;


@DefaultOrderBy("usuario")
public class PapelUsuarioDAO extends GenericDAO<PapelUsuario> {

	public void deleteByUsuario(Usuario usuario) {
		getHibernateTemplate().bulkUpdate(
				"delete from PapelUsuario where usuario = ?", usuario);
	}

}
