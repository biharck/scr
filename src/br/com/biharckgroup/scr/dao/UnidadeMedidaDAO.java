package br.com.biharckgroup.scr.dao;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.biharckgroup.scr.adm.filtro.UnidadeMedidaFiltro;
import br.com.biharckgroup.scr.bean.UnidadeMedida;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRException;


@DefaultOrderBy("nome")
public class UnidadeMedidaDAO extends GenericDAOSCR<UnidadeMedida> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<UnidadeMedida> query,FiltroListagem _filtro) {
		UnidadeMedidaFiltro filtro = (UnidadeMedidaFiltro) _filtro;
		query.whereLikeIgnoreAll("unidadeMedida.nome", filtro.getNome());
	}
	@Override
	public void saveOrUpdate(UnidadeMedida bean) {
		try{
			super.saveOrUpdate(bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_nome")){ //nota o index na tabela tem q ter o mesmo nome
				throw new SCRException("J� existe um registro de Unidade de Medida com este Nome cadastrado no sistema.");
			}
		}
	}

}
