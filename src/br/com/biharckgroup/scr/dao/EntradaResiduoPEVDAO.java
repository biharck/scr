package br.com.biharckgroup.scr.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.biharckgroup.scr.adm.filtro.EntradaResiduoPEVFiltro;
import br.com.biharckgroup.scr.adm.relatorio.filtro.EntradaResiduoPEVReportFiltro;
import br.com.biharckgroup.scr.adm.relatorio.filtro.ResiduoRecebidoPEVReportFiltro;
import br.com.biharckgroup.scr.bean.EntradaResiduoPEV;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduoEntradaResiduoPEV;


@DefaultOrderBy("entradaResiduoPEV")
public class EntradaResiduoPEVDAO extends GenericDAOSCR<EntradaResiduoPEV> {
	
	private GrupoTipoResiduoEntradaResiduoPEVDAO grupoTipoResiduoEntradaResiduoPEVDAO;
	
	public void setGrupoTipoResiduoEntradaResiduoPEVDAO(
			GrupoTipoResiduoEntradaResiduoPEVDAO grupoTipoResiduoEntradaResiduoPEVDAO) {
		this.grupoTipoResiduoEntradaResiduoPEVDAO = grupoTipoResiduoEntradaResiduoPEVDAO;
	}
	
	@Override
	public void saveOrUpdate(final EntradaResiduoPEV bean) {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				EntradaResiduoPEV load = new EntradaResiduoPEV();
				/* update */
				if(bean.getId() != null){
					load = load(bean);
				}
				//atualiza somente campos do formulario
				load.setAgenteEntradaResiduo(bean.getAgenteEntradaResiduo());
				load.setBairroOrigem(bean.getBairroOrigem());
				load.setPontoEntrega(bean.getPontoEntrega());
				load.setData(bean.getData());
				load.setGuia(bean.getGuia());
				load.setHora(bean.getHora());
				load.setResponsavel(bean.getResponsavel());
				load.setTransportador(bean.getAgenteEntradaResiduo().isOptBool() ? true : false);

				EntradaResiduoPEVDAO.super.saveOrUpdate(load);

				grupoTipoResiduoEntradaResiduoPEVDAO.deleteByEntradaResiduoPEV(load);
				if(bean.getGrupoTipoResiduosEntradaResiduoPEV() != null){
					for (GrupoTipoResiduoEntradaResiduoPEV grupoTipoResiduoEntradaResiduoPEV : bean.getGrupoTipoResiduosEntradaResiduoPEV()) {
						grupoTipoResiduoEntradaResiduoPEV.setEntradaResiduoPEV(load);
						grupoTipoResiduoEntradaResiduoPEVDAO.saveOrUpdate(grupoTipoResiduoEntradaResiduoPEV);
					}
				}
				return null;
			}
			
		});
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<EntradaResiduoPEV> query, 	FiltroListagem _filtro) {
		EntradaResiduoPEVFiltro filtro = (EntradaResiduoPEVFiltro) _filtro;

		query
			.leftOuterJoin("entradaResiduoPEV.pontoEntrega pe ")
			.leftOuterJoin("entradaResiduoPEV.guia eg ")
			.where("pe=?", filtro.getPontoEntrega())
			.whereLikeIgnoreAll("entradaResiduoPEV.responsavel", filtro.getResponsavel());
			
		if(filtro.getIdGuia()!=null)
			query.where("eg=" + filtro.getIdGuia());
		
		if(filtro.getDataIni() != null)
			query.where("entradaResiduoPEV.data >= ?",filtro.getDataIni());
		if(filtro.getDataFim() != null)
			query.where("entradaResiduoPEV.data <= ?",filtro.getDataFim());
			
			
	}
	
	public List<EntradaResiduoPEV> findByFiltro(EntradaResiduoPEVReportFiltro filtro) {
		QueryBuilder<EntradaResiduoPEV> query = query();
		
		query.select(
		"pev.id,pev.nome, pev.identificacao,pev.responsavel, " +
		"entradaResiduoPEV.id, entradaResiduoPEV.data,entradaResiduoPEV.hora, entradaResiduoPEV.responsavel,entradaResiduoPEV.bairroOrigem, entradaResiduoPEV.transportador, " +
		"grupoTipoRes.nome,grupoTipoRes.id, " +
		"tipoRes.id, tipoRes.descricao, " +
		"eg.quantidade, um.nome")		
		.leftOuterJoin("entradaResiduoPEV.grupoTipoResiduosEntradaResiduoPEV eg")
		.leftOuterJoin("eg.tipoResiduo tipoRes")
		.leftOuterJoin("eg.grupoTipoResiduo grupoTipoRes")
		.leftOuterJoin("eg.unidadeMedida um")
		.leftOuterJoin("entradaResiduoPEV.pontoEntrega pev");
	
		if(filtro.getPontoEntrega()!=null)
				query.where("pev=?", filtro.getPontoEntrega());
				
		if(filtro.getDataIni() != null)
			query.where("entradaResiduoPEV.data >= ?",filtro.getDataIni());
		if(filtro.getDataFim() != null)
			query.where("entradaResiduoPEV.data <= ?",filtro.getDataFim());
				
		
		return query.list();
	}
	
	public List<EntradaResiduoPEV> findByFiltro(ResiduoRecebidoPEVReportFiltro filtro) {
		QueryBuilder<EntradaResiduoPEV> query = query();
		
		query.select("entradaResiduoPEV.data,eg.grupoTipoResiduo, eg.tipoResiduo, eg.quantidade, tipoRes.classeTipoResiduo")		
				.leftOuterJoin("entradaResiduoPEV.grupoTipoResiduosEntradaResiduoPEV eg")
				.leftOuterJoin("eg.tipoResiduo tipoRes")
				.leftOuterJoin("eg.grupoTipoResiduo grupoTipoResiduo")
				.leftOuterJoin("entradaResiduoPEV.pontoEntrega pev")
				.where("pev=?", filtro.getPontoEntrega())
//				.leftOuterJoin("entradaResiduoPEV.municipio mun")
//				.where("mun=?", filtro.getMunicipio())
				.leftOuterJoin("tipoRes.classeTipoResiduo classeTipoResiduo")
				.orderBy("entradaResiduoPEV.data");
		
		if(filtro.getDataIni() != null)
			query.where("entradaResiduoPEV.data >= ?",filtro.getDataIni());
		if(filtro.getDataFim() != null)
			query.where("entradaResiduoPEV.data <= ?",filtro.getDataFim());
				
		
		return query.list();
	}
	
	@Override
	public void delete(EntradaResiduoPEV bean) {
		grupoTipoResiduoEntradaResiduoPEVDAO.deleteByEntradaResiduoPEV(bean);
		super.delete(bean);
	}
}