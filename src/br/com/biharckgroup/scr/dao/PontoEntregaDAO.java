package br.com.biharckgroup.scr.dao;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.biharckgroup.scr.adm.filtro.PontoEntregaFiltro;
import br.com.biharckgroup.scr.bean.PontosEntregaTransportador;
import br.com.biharckgroup.scr.bean.PontoEntrega;
import br.com.biharckgroup.scr.bean.Transportador;
import br.com.biharckgroup.scr.service.UfService;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRException;


@DefaultOrderBy("nome")
public class PontoEntregaDAO extends GenericDAOSCR<PontoEntrega> {
	
	private UfService ufService;
	
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<PontoEntrega> query,FiltroListagem _filtro) {
		PontoEntregaFiltro filtro = (PontoEntregaFiltro) _filtro;

			query
				.whereLikeIgnoreAll("identificacao", filtro.getIdentificacao())
				.whereLikeIgnoreAll("responsavel", filtro.getResponsavel())
				.whereLikeIgnoreAll("nome", filtro.getNome())
				.whereLikeIgnoreAll("email", filtro.getEmail());
		
	}
	
	@Override
	public void saveOrUpdate(PontoEntrega bean) {
		try{
			super.saveOrUpdate(bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_identif")){ //nota o index na tabela tem q ter o mesmo nome
				throw new SCRException("J� existe um registro de Ponto Entrega com esta Identifica��o cadastrado no sistema.");
			}
		}
	}
	
	
	public List<PontoEntrega> findByTransportador(Transportador transportador) {
		if (transportador.getId() == null) {
			return new ArrayList<PontoEntrega>();
		}
		return query().select("pontoEntrega").from(PontosEntregaTransportador.class)
				.leftOuterJoin("pontosEntregaTransportador.pontoEntrega pontoEntrega")
				.where("pontosEntregaTransportador.transportador = ?", transportador).list();
	}
	
	@Override
	public PontoEntrega load(PontoEntrega bean) {
		bean = super.load(bean);
		bean.setUf(ufService.getUfByMunicipio(bean.getMunicipio()));
		return bean;
	}
	
	@Override
	public PontoEntrega loadForEntrada(PontoEntrega bean) {
		bean=  super.loadForEntrada(bean);
		bean.setUf(ufService.getUfByMunicipio(bean.getMunicipio()));
		return bean;
		
	}
	
	public PontoEntrega findIdentificadorById(PontoEntrega bean){
		return query()
				.select("pontoEntrega.identificacao")
				.where("pontoEntrega = ?", bean)
				.unique();
		
	}
}
