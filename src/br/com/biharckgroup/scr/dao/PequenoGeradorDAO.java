package br.com.biharckgroup.scr.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.biharckgroup.scr.adm.filtro.PequenoGeradorFiltro;
import br.com.biharckgroup.scr.bean.PequenoGerador;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.service.UsuarioService;

public class PequenoGeradorDAO extends GenericDAOSCR<PequenoGerador>{

	private UsuarioService usuarioService;
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<PequenoGerador> query,FiltroListagem _filtro) {
		PequenoGeradorFiltro filtro = (PequenoGeradorFiltro) _filtro;
		
		query.whereLikeIgnoreAll("pequenoGerador.cpf",filtro.getCpf())
		.whereLikeIgnoreAll("pequenoGerador.nome", filtro.getNome());
	}
	
	public List<PequenoGerador> getPequenosGeradoresAutoCadastro(){
		return query()
				.select("pequenoGerador")
				.where("pequenoGerador.autoCadastro is true")
				.where("pequenoGerador.ativo is false")
				.list();
	}
	
	
	@Override
	public void saveOrUpdate(final PequenoGerador bean) {
		if(bean.getId()==null && bean.isAutoCadastro()){
			PequenoGeradorDAO.super.saveOrUpdate(bean);
		}else{
					
			if(bean.getId()==null)
				bean.setAtivo(true);
					
			PequenoGeradorDAO.super.saveOrUpdate(bean);
			transactionTemplate.execute(new TransactionCallback<Object>(){
				public Object doInTransaction(TransactionStatus arg0) {
					if(bean.getUsuarios() != null && bean.getUsuarios().get(0).getId()!=null){
						//elimina referencias dos destinos finais na tabela usu�rio
						usuarioService.removePequenoGerador(bean);
						if(bean.getUsuarios() != null){
							for ( Usuario usuario : bean.getUsuarios()) {
								usuario.setPequenoGerador(bean);
								usuarioService.updatePequenoGerador(usuario, bean);
							}
						}
					}
					return null;
				}
				
			});
		}
	}
	
	

}
