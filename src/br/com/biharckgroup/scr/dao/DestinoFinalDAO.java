package br.com.biharckgroup.scr.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.biharckgroup.scr.adm.filtro.DestinoFinalFiltro;
import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.service.UfService;
import br.com.biharckgroup.scr.service.UsuarioService;

@DefaultOrderBy("razaoSocial")
public class DestinoFinalDAO extends GenericDAOSCR<DestinoFinal> {
	
	private UfService ufService;
	
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	private UsuarioService usuarioService;
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<DestinoFinal> query, FiltroListagem _filtro) {
		DestinoFinalFiltro filtro = (DestinoFinalFiltro) _filtro;

		query
			.whereLikeIgnoreAll("cnpj", filtro.getCnpj())
			.whereLikeIgnoreAll("razaoSocial", filtro.getRazaoSocial())
			.whereLikeIgnoreAll("nomeFantasia", filtro.getNomeFantasia())
			.leftOuterJoin("destinoFinal.tipoDestino td")
			.where("td=?",filtro.getTipoDestino());
	}

	
	@Override
	public DestinoFinal load(DestinoFinal bean) {
		bean = super.load(bean);
		/*Preenche campo cpfCnpjTransient
		 * Quando tiver duas op��es cpf/cnpj verificar qual est� preenchido
			String cpfCnpjTransient = form.getCnpj()!= null && !form.getCnpj().equals("") ? form.getCnpj():  form.getCpf();  
		 */
		String cpfCnpjTransient = bean.getCnpj(); 
		bean.setCpfCnpjTransient(cpfCnpjTransient);
		bean.setUf(ufService.getUfByMunicipio(bean.getMunicipio()));

		return bean;
	}
	@Override
	public DestinoFinal loadForEntrada(DestinoFinal bean) {
		bean = super.loadForEntrada(bean);
		/*Preenche campo cpfCnpjTransient
		 * Quando tiver duas op��es cpf/cnpj verificar qual est� preenchido
			String cpfCnpjTransient = form.getCnpj()!= null && !form.getCnpj().equals("") ? form.getCnpj():  form.getCpf();  
		 */
		String cpfCnpjTransient = bean.getCnpj(); 
		bean.setCpfCnpjTransient(cpfCnpjTransient);
		bean.setUf(ufService.getUfByMunicipio(bean.getMunicipio()));
		
		//define usuarios
		bean.setUsuarios(usuarioService.findByDestinoFinal(bean));
		return bean;
	}
	
	@Override
	public void saveOrUpdate(DestinoFinal bean) {
		super.saveOrUpdate(bean);
		
		if(bean.getUsuarios() != null && bean.getUsuarios().get(0).getId()!=null){
			//elimina referencias dos destinos finais na tabela usu�rio
			usuarioService.removeDestinoFinal(bean);
			if(bean.getUsuarios() != null){
				for ( Usuario usuario : bean.getUsuarios()) {
					usuario.setDestinoFinal(bean);
					usuarioService.updateDestinoFinal(usuario, bean);
				}
			}
		}
		
	}
	
	public List<DestinoFinal> getDestinosAutoCadastro(){
		return query()
				.select("destinoFinal.id, destinoFinal.nomeFantasia, destinoFinal.timeInc")
				.where("destinoFinal.autoCadastro is true")
				.where("destinoFinal.ativo is false")
				.list();
	}
	
	public void addCapacidadeAtual(double valorEntrada,DestinoFinal destinoFinal){
		getJdbcTemplate().update("UPDATE destinofinal SET capacidadeAtual = capacidadeAtual + ? WHERE id = ?", valorEntrada ,destinoFinal.getId());
	}
	
	public DestinoFinal getCapacidadeAtualAndCapacidadeTotal(DestinoFinal destinoFinal){
		return query().select("destinoFinal.capacidadeAtual, destinoFinal.capacidadeTotal")
				.where("destinoFinal = ?", destinoFinal).unique();
	}
	
	public DestinoFinal findNomeFantasiaById(DestinoFinal bean){
		return query()
				.select("destinoFinal.nomeFantasia")
				.where("destinoFinal = ?", bean)
				.unique();
		
	}
	
}
