package br.com.biharckgroup.scr.dao;
import java.lang.ref.WeakReference;
import java.sql.Timestamp;
import java.util.List;

import org.nextframework.core.standard.Next;
import org.nextframework.core.web.NextWeb;
import org.nextframework.persistence.AliasMap;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.QueryBuilderResultTranslatorImpl;
import org.nextframework.util.Util;

import br.com.biharckgroup.scr.bean.BeanAuditoria;

public abstract class GenericDAOSCR<BEAN> extends GenericDAO<BEAN> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.nextframework.persistence.GenericDAO#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public void saveOrUpdate(BEAN bean) {
		if (((BeanAuditoria) bean).getId() == null) {
			((BeanAuditoria) bean).setTimeInc(new Timestamp(System.currentTimeMillis()));
			((BeanAuditoria) bean).setTimeAlt(new Timestamp(System.currentTimeMillis()));
			if(NextWeb.getUser()==null){
				((BeanAuditoria) bean).setUserInc("Auto Cadastro");
				((BeanAuditoria) bean).setUserAlt("Auto Cadastro");
			}else{
				((BeanAuditoria) bean).setUserInc(NextWeb.getUser().getLogin());
				((BeanAuditoria) bean).setUserAlt(NextWeb.getUser().getLogin());
			}
		} else {
			BeanAuditoria bb = (BeanAuditoria) load(bean);
			((BeanAuditoria) bean).setUserInc(bb.getUserInc());
			((BeanAuditoria) bean).setTimeInc(bb.getTimeInc());
			if(NextWeb.getUser()==null)
				((BeanAuditoria) bean).setUserInc("Auto Cadastro");
			else
				((BeanAuditoria) bean).setUserAlt(Next.getUser().getLogin());
			((BeanAuditoria) bean).setTimeAlt(new Timestamp(System.currentTimeMillis()));
		}
		super.saveOrUpdate(bean);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.nextframework.persistence.GenericDAO#delete(java.lang.Object)
	 */
	@Override
	public void delete(BEAN bean) {
		bean = load(bean);
		((BeanAuditoria) bean).setTimeDel(new Timestamp(System.currentTimeMillis()));
		((BeanAuditoria) bean).setUserDel(Next.getUser().getLogin());
		this.getHibernateTemplate().update(bean);
	}
	
	public void apagarFisicamente(BEAN bean){
		super.delete(bean);
	}
	
	@Override
	protected QueryBuilder<BEAN> query() {
		Class<?> c1 = beanClass;
		Class<BeanAuditoria> ba = BeanAuditoria.class;
		if(ba.isAssignableFrom(c1)){//se herda de beanAuditoria...
			String alias = Util.strings.uncaptalize(this.beanClass.getSimpleName());
			return newQueryBuilder(beanClass)
			 	.from(beanClass)
			 	.where(alias+".userDel is null");
		}		
		return newQueryBuilder(beanClass)
						 .from(beanClass);
	}
	
	@Override
	public List<BEAN> findForCombo(String... extraFields) {
		
		if(extraFields != null && extraFields.length > 0){
			Class<?> c1 = beanClass;
			Class<BeanAuditoria> ba = BeanAuditoria.class;
			if(ba.isAssignableFrom(c1)){//se herda de beanAuditoria...
				String alias = Util.strings.uncaptalize(this.beanClass.getSimpleName());
				return newQueryBuilder(beanClass)
				.select(getComboSelect(extraFields))
				.from(beanClass)
				.where(alias+".userDel is null")
				.orderBy(orderBy)
				.list();
			}	
			else{ 
				return newQueryBuilder(beanClass)
						.select(getComboSelect(extraFields))
						.from(beanClass)
						.orderBy(orderBy)
						.list();
			}
		} else {
			if(queryFindForCombo == null){
				initQueryFindForCombo();
			}
			List listCached = findForComboCache.get();
			if (listCached == null || (System.currentTimeMillis() - lastRead > cacheTime)) {
				listCached = getHibernateTemplate().find(queryFindForCombo);
				listCached = translatorQueryFindForCombo.translate(listCached);
				findForComboCache = new WeakReference<List<?>>(listCached);
				lastRead = System.currentTimeMillis();	
			}
			return listCached;	
		}
	}
	@Override
	protected void initQueryFindForCombo() {
		if (translatorQueryFindForCombo == null) {

			String alias = Util.strings.uncaptalize(beanClass.getSimpleName());
			String[] selectedProperties = getComboSelectedProperties(alias);
			String hbQueryFindForCombo = "select " + getComboSelect() + " " + "from " + beanClass.getName() + " " + alias;
			hbQueryFindForCombo = getQueryFindForCombo(hbQueryFindForCombo);
			
			/*Criado para validar a auditoria e n�o mostrar campos apagados logicamente*/
			Class<?> c1 = beanClass;
			Class<BeanAuditoria> ba = BeanAuditoria.class;
			
			if(ba.isAssignableFrom(c1))
				hbQueryFindForCombo += " where "+alias+".userDel is null";
			/*fim auditoria*/
			
			
			if (orderBy != null && !hbQueryFindForCombo.contains("order by")) {
				hbQueryFindForCombo += "  order by " + orderBy;
			}
			
			
			translatorQueryFindForCombo = new QueryBuilderResultTranslatorImpl();
			translatorQueryFindForCombo.init(selectedProperties, new AliasMap[] { new AliasMap(alias, null, beanClass) });
			queryFindForCombo = hbQueryFindForCombo;
		}
	}
	

}
