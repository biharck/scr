package br.com.biharckgroup.scr.dao;

import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import br.com.biharckgroup.scr.bean.EntradaResiduoPEV;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduoEntradaResiduoPEV;


@DefaultOrderBy("id")
public class GrupoTipoResiduoEntradaResiduoPEVDAO extends GenericDAO<GrupoTipoResiduoEntradaResiduoPEV> {
	
	public void deleteByEntradaResiduoPEV(EntradaResiduoPEV entradaResiduoPEV ) {
		getHibernateTemplate().bulkUpdate(
				"delete GrupoTipoResiduoEntradaResiduoPEV where entradaResiduoPEV = ?", entradaResiduoPEV);
	}
}
