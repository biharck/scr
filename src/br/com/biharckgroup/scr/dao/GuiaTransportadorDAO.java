package br.com.biharckgroup.scr.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.ResultList;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.biharckgroup.scr.adm.filtro.GuiaFiltro;
import br.com.biharckgroup.scr.bean.EnumIdentificaGerador;
import br.com.biharckgroup.scr.bean.EnumSituacao;
import br.com.biharckgroup.scr.bean.EnumStatus;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.GuiaDestinoFinal;
import br.com.biharckgroup.scr.bean.GuiaTransportador;
import br.com.biharckgroup.scr.bean.ResiduoGuia;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.service.UfService;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;


@DefaultOrderBy("guiaTransportador.id")
public class GuiaTransportadorDAO extends GenericDAOSCR<GuiaTransportador> {
	
	private ResiduoGuiaDAO residuoGuiaDAO;
	private UfService ufService;
	
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	public void setResiduoGuiaDAO(ResiduoGuiaDAO residuoGuiaDAO) {
		this.residuoGuiaDAO = residuoGuiaDAO;
	}
	
	@Override
	public void saveOrUpdate(final GuiaTransportador bean) {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				GuiaTransportador load = new GuiaTransportador();
				/* update */
				if(bean.getId() != null){
					load = load(bean);
				}
				
				load.setSituacao(bean.getSituacao());
				load.setStatus(bean.isStatus());
				
				load.setTransportador(bean.getTransportador());
						
				//atualiza somente campos do formulario
				load.setProjeto(bean.getProjeto());
				load.setGrandeGerador(bean.getGrandeGerador());
				
				load.setCepObra(bean.getCepObra());
				load.setEnderecoObra(bean.getEnderecoObra());
				load.setNumeroObra(bean.getNumeroObra());
				load.setComplementoObra(bean.getComplementoObra());
				load.setBairroObra(bean.getBairroObra());
				load.setMunicipioObra(bean.getMunicipioObra());
				load.setResponsavelObra(bean.getResponsavelObra());
				load.setResiduosGuia(bean.getResiduosGuia());
				load.setTipoTransporte(bean.getTipoTransporte());
				load.setIdentificacaoTransporte(bean.getIdentificacaoTransporte());
				load.setPlaca(bean.getPlaca());
				load.setDataRetirada(bean.getDataRetirada());
				load.setDataEnvio(bean.getDataEnvio());
				load.setDataEntrega(bean.getDataEntrega());
				load.setDestinoFinal(bean.getDestinoFinal());
				load.setCancelada(bean.isCancelada());
				
				//valida uniques
				try{
					GuiaTransportadorDAO.super.saveOrUpdate(load);
				} catch (DataIntegrityViolationException e) {
					if (DatabaseError.isKeyPresent(e, "unique_identTransp")){ //nota o index na tabela tem q ter o mesmo nome
						throw new SCRException("J� existe um registro de Guia com esta Identifica��o de Transporte cadastrado no sistema.");
					}
				}
				
				//salva numeroGuia
				if(bean.getId() == null){
					load. setNumeroGuia("TR-"+load.getId()); //O n�mero de identifica��o --> GG-id
					updateNumeroGuia(load);
				}
					
				residuoGuiaDAO.deleteByGuia(load);
				/*Guia e GuiaDestinoFinal representam mesma entidade no banco
				 *converte obj  GuiaDestinoFinal para Guia para salvar dados
				 */
				Guia guiaLoad = new Guia(load.getId());
				if(bean.getResiduosGuia() != null){
					for (ResiduoGuia residuoGuia : bean.getResiduosGuia()) {
						residuoGuia.setGuia(guiaLoad);
						residuoGuiaDAO.saveOrUpdate(residuoGuia);
					}
				}
				return null;
			}
			
		});
	}
	
	public void updateNumeroGuia(GuiaTransportador bean) {
		getJdbcTemplate().update("update guia set numeroGuia = ?" +
				" where id = ?", bean.getNumeroGuia(), bean.getId());
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<GuiaTransportador> query,
			FiltroListagem _filtro) {
		GuiaFiltro filtro = (GuiaFiltro) _filtro;

		Usuario usuario = SCRUtil.getUsuarioLogado();
		query
			.whereLikeIgnoreAll("identificacaoTransporte", filtro.getIdentificacaoTransporte())
			.whereLikeIgnoreAll("placa", filtro.getPlaca())
			.leftOuterJoin("guiaTransportador.grandeGerador gg")
			.whereLikeIgnoreAll("gg.nomeFantasia", filtro.getNomeFantasia())
			.whereLikeIgnoreAll("guiaTransportador.numeroGuia", filtro.getNumeroGuia());
			
		if(filtro.getDestinoFinal() != null){
			query
				.leftOuterJoin("guiaTransportador.destinoFinal gd")	
				.where("gd=?", filtro.getDestinoFinal());
		}
		if(filtro.getTransportador() != null){
			query
				.leftOuterJoin("guiaTransportador.transportador gt")
				.where("gt=?", filtro.getTransportador());
		}
		
		if(filtro.getStatus()!=null)			
			query.where("status is "+filtro.getStatus().isOptBool());
		
		if(filtro.getSituacao() != null){
				query.where("situacao = "+filtro.getSituacao().getValor());
		}
		if(filtro.getEnumCancelada()!=null)			
			query.where("cancelada is "+filtro.getEnumCancelada().isOptBool());
	}
	
	
	@Override
	public GuiaTransportador loadForEntrada(GuiaTransportador form) {
		form = super.loadForEntrada(form);
		form.setResiduosGuia(residuoGuiaDAO.findByGuia(form));
		defineCampoTransient(form);
		return form;
	}
	
	@Override
	public GuiaTransportador load(GuiaTransportador form) {
		form = super.load(form);
		form.setResiduosGuia(residuoGuiaDAO.findByGuia(form));
		defineCampoTransient(form);
		return form;
	}
	
	public void defineCampoTransient(GuiaTransportador form){
		/*Preenche campo cpfCnpjTransient
		 * Quando tiver duas op��es cpf/cnpj verificar qual est� preenchido*/
		String cpfCnpjTransient;
		//transportador
		if(form.getTransportador() != null){
			cpfCnpjTransient = form.getTransportador().getCnpj()!= null && !form.getTransportador().getCnpj().equals("") ? form.getTransportador().getCnpj():  form.getTransportador().getCpf(); 
			form.getTransportador().setCpfCnpjTransient(cpfCnpjTransient);
		}
		//grandeGerador
		if(form.getGrandeGerador() != null){
			cpfCnpjTransient = form.getGrandeGerador().getCnpj()!= null && !form.getGrandeGerador().getCnpj().equals("") ? form.getGrandeGerador().getCnpj():  form.getGrandeGerador().getCpf(); 
			form.getGrandeGerador().setCpfCnpjTransient(cpfCnpjTransient);
		}
		//destinoFinal
		if(form.getDestinoFinal() != null){
			cpfCnpjTransient = form.getDestinoFinal().getCnpj(); 
			form.getDestinoFinal().setCpfCnpjTransient(cpfCnpjTransient);
		}
		
		//define uf
		if(form.getMunicipioObra() != null)
			form.setUfObra(ufService.getUfByMunicipio(form.getMunicipioObra()));
		if(form.getGrandeGerador() != null && form.getGrandeGerador().getMunicipio() !=null)
			form.getGrandeGerador().setUf(ufService.getUfByMunicipio(form.getGrandeGerador().getMunicipio()));
		if(form.getTransportador() != null && form.getTransportador().getMunicipio() !=null)
			form.getTransportador().setUf(ufService.getUfByMunicipio(form.getTransportador().getMunicipio()));
		if(form.getDestinoFinal() != null && form.getDestinoFinal().getMunicipio() !=null)
			form.getDestinoFinal().setUf(ufService.getUfByMunicipio(form.getDestinoFinal().getMunicipio()));
		
		//define enum IdentificaGerador
		if(form.getGrandeGerador() != null)
			form.setEnumIdentificaGerador(EnumIdentificaGerador.SIM);
		else
			form.setEnumIdentificaGerador(EnumIdentificaGerador.NAO);

		//define enum situacao
		form.setEnumSituacao(EnumSituacao.getEnumSituacaoByValor(form.getSituacao()));
		
		if(form.isStatus())
			form.setEnumStatus(EnumStatus.CONFIRMADA);
		else
			form.setEnumStatus(EnumStatus.PENDENTE);
	}
	
	@Override
	public ResultList<GuiaTransportador> findForListagem(FiltroListagem filtro) {
		// TODO Auto-generated method stub
		
		ResultList<GuiaTransportador> list = super.findForListagem(filtro);
		for (GuiaTransportador guia : list.list()) {
			guia.setEnumSituacao(EnumSituacao.getEnumSituacaoByValor(guia.getSituacao()));
		}
		return list;
	}
	
	@Override
	public void delete(GuiaTransportador bean) {
		residuoGuiaDAO.deleteByGuia(bean);
		super.delete(bean);
	}
}
