package br.com.biharckgroup.scr.destinoFinal.controller.process;

import java.util.Date;
import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.util.CollectionsUtil;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.LiberacaoSuperlotacao;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.service.DestinoFinalService;
import br.com.biharckgroup.scr.service.UsuarioService;
import br.com.biharckgroup.scr.util.PathUtil;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/destinoFinal/process/LiberacaoSuperlotacao",authorizationModule=ProcessAuthorizationModule.class)
public class LiberacaoSuperlotacaoProcess extends MultiActionController{

	private DestinoFinalService destinoFinalService;
	private UsuarioService usuarioService;
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setDestinoFinalService(DestinoFinalService destinoFinalService) {
		this.destinoFinalService = destinoFinalService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, LiberacaoSuperlotacao liberacaoSuperlotacao) {
		
		if(liberacaoSuperlotacao==null)
			liberacaoSuperlotacao = new LiberacaoSuperlotacao();
		liberacaoSuperlotacao.setData(new Date());
		
		return new ModelAndView("process/liberacaoSuperlotacao","liberacaoSuperlotacao", liberacaoSuperlotacao);
	}
	
	@Action("salvar")
	public ModelAndView salvar(WebRequestContext request, LiberacaoSuperlotacao bean){

		DestinoFinal destinoFinal = destinoFinalService.load(bean.getDestinoFinal()); //carrega dest final atualizado
		
		if(bean.getCapacidadeLiberada() == (double)0 || bean.getCapacidadeLiberada()>destinoFinal.getCapacidadeAtual()){
			request.addError("O volume � ser liberado deve ser maior que zero e menor que a capacidade atual.");
			return index(request, bean);
		}
			
		
		bean.setData(new Date());
		try {
			//atualiza o volume atual do destino final
			double valorEntrada = - bean.getCapacidadeLiberada();
			destinoFinalService.addCapacidadeAtual(valorEntrada, destinoFinal);
			
			//envia alerta ao t�cnico da SEMEA (usu�rios perfil adm)
			List<Usuario> usuarioEmail = usuarioService.findEmailPapelAdministrativo();
			String destinatariosAdm = CollectionsUtil.listAndConcatenate(usuarioEmail, "email", ";"); 
			destinatariosAdm+=";"+destinoFinal.getEmail();
			SCRUtil.enviaEmailLiberacaoSuperlocatcaoTecnicoSEMEA(destinatariosAdm, destinoFinal, bean);
			//envia alerta ao usuarios do destino final
			usuarioEmail = usuarioService.findEmailByDestinoFinal(destinoFinal);
			String destinatariosDest = CollectionsUtil.listAndConcatenate(usuarioEmail, "email", ";"); 
			destinatariosDest+=";"+destinoFinal.getEmail();
			SCRUtil.enviaEmailLiberacaoSuperlocatcaoDestinoFinal(destinatariosDest, destinoFinal, bean);
			request.addMessage("Libera��o realizada com sucesso!");
			request.addMessage("Foi enviado um email ao T�cnico da SEMEA e aos usu�rios do destino final" +destinoFinal.getRazaoSocial()+ "informando a libera��o.");
			return new ModelAndView("redirect:"+PathUtil.AFTER_LOGIN_GO_TO);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Falha ao salvar o registro, entre em contato com o administrador do sistema");
		}
		return index(request, bean);
	}
	
}
