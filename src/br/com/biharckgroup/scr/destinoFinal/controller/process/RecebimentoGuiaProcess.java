package br.com.biharckgroup.scr.destinoFinal.controller.process;

import java.io.IOException;
import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.util.CollectionsUtil;
import org.springframework.web.servlet.ModelAndView;

import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;

import br.com.biharckgroup.scr.adm.filtro.GuiaFiltro;
import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.EnumSituacao;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.ResiduoGuia;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.dao.ResiduoGuiaDAO;
import br.com.biharckgroup.scr.service.DestinoFinalService;
import br.com.biharckgroup.scr.service.GuiaService;
import br.com.biharckgroup.scr.service.UsuarioService;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/destinoFinal/process/RecebimentoGuia", authorizationModule=ProcessAuthorizationModule.class)
public class RecebimentoGuiaProcess extends MultiActionController {

	private GuiaService guiaService;
	private DestinoFinalService destinoFinalService;
	private UsuarioService usuarioService;
	private ResiduoGuiaDAO residuoGuiaDAO;

	public void setGuiaService(GuiaService guiaService) {
		this.guiaService = guiaService;
	}
	public void setDestinoFinalService(DestinoFinalService destinoFinalService) {
		this.destinoFinalService = destinoFinalService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setResiduoGuiaDAO(ResiduoGuiaDAO residuoGuiaDAO) {
		this.residuoGuiaDAO = residuoGuiaDAO;
	}

	@DefaultAction
	public ModelAndView index(WebRequestContext request, GuiaFiltro filtro) {
		filtro = new GuiaFiltro();
		filtro.setDestinoFinal(SCRUtil.getUsuarioLogado().getDestinoFinal());
		List<Guia> guiasByDestinoFinal = guiaService.getGuiasByDestinoFinal(filtro.getDestinoFinal(),filtro.getNumeroGuia());
		for (Guia guia : guiasByDestinoFinal) {
			guia.setEnumSituacao(EnumSituacao.getEnumSituacaoByValor(guia.getSituacao()));
			guia.setResiduosGuia(residuoGuiaDAO.findByGuia(guia));
		}
		request.setAttribute("guias", guiasByDestinoFinal);
		return new ModelAndView("process/recebimentoGuia", "guiaFiltro", filtro);
	}

	public ModelAndView pesquisar(WebRequestContext request, GuiaFiltro filtro) {
		filtro.setDestinoFinal(SCRUtil.getUsuarioLogado().getDestinoFinal());
		request.setAttribute("guias", guiaService.getGuiasByDestinoFinal(filtro.getDestinoFinal(),filtro.getNumeroGuia()));
		
		return new ModelAndView("process/recebimentoGuia", "guiaFiltro", filtro);
	}

	public ModelAndView receber(WebRequestContext request, Guia guia) {
		String dataRecebimento = getParameter("dataRecebimento");
		if(dataRecebimento == null || dataRecebimento.isEmpty()){
			request.addError("A data de Recebimento deve ser informada!");
			return index(request, new GuiaFiltro());
		}
		
		guia = guiaService.load(guia);
		guia.setDataChegada(SCRUtil.stringToDate(dataRecebimento));
		
		if(guia.getDataRetirada() != null){
			if(guia.getDataChegada().before(guia.getDataRetirada())){
				request.addError("A data de recebimento no Destino Final deve ser superior ou igual a data de retirada do GG.");
				return index(request, new GuiaFiltro());
			}
		}
		
		try { 
			guia.setSituacao("1");
			guia.setStatus(true);
			guiaService.recebimentoGuia(guia);
			//adiciona volume de res�duos da guia recebida ao destino final
			adicionaCapacidadeDetinoFinal(request, guia);
		} catch (Exception e) {
			request.addError("Ups, algo aconteceu ao tentar receber esta Guia! Entre em contato com os administradores do Sistema, eles poder�o lhe ajudar! :)");

		}
		request.addMessage("Guia Recebida com Sucesso! Obrigado.");
		return index(request, new GuiaFiltro());
	}
	
	public ModelAndView receberDivergente(WebRequestContext request, Guia guia) {
		String motivo = getParameter("motivo");
		String dataRecebimento = getParameter("dataRecebimento");
		
		if(motivo == null || motivo.isEmpty()){
			request.addError("O motivo de recebimento com diverg�ncias deve ser informado!");
			return index(request, new GuiaFiltro());
		}
		if(dataRecebimento == null || dataRecebimento.isEmpty()){
			request.addError("A data de Recebimento deve ser informada!");
			return index(request, new GuiaFiltro());
		}
		
		guia = guiaService.load(guia);
		guia.setDataChegada(SCRUtil.stringToDate(dataRecebimento));
		
		if(guia.getDataRetirada() != null){
			if(guia.getDataChegada().before(guia.getDataRetirada())){
				request.addError("A data de recebimento no Destino Final deve ser superior ou igual a data de retirada do GG.");
				return index(request, new GuiaFiltro());
			}
		}

		try {
			guia.setMotivo(motivo);
			guia.setSituacao("2");
			guia.setStatus(true);
			guiaService.recebimentoGuia(guia);
			//adiciona volume de res�duos da guia recebida ao destino final
			adicionaCapacidadeDetinoFinal(request, guia);
		} catch (Exception e) {
			request.addError("Ups, algo aconteceu ao tentar receber esta Guia! Entre em contato com os administradores do Sistema, eles poder�o lhe ajudar! :)");
		}
		request.addMessage("Guia Recebida com Sucesso! Obrigado.");
		return index(request, new GuiaFiltro());
		
	}

	public ModelAndView recusar(WebRequestContext request, Guia guia) {
		String motivo = getParameter("motivo");
		String dataRecebimento = getParameter("dataRecebimento");
		
		if(motivo == null || motivo.isEmpty()){
			request.addError("O motivo de recusa da guia deve ser informado!");
			return index(request, new GuiaFiltro());
		}
		if(dataRecebimento == null || dataRecebimento.isEmpty()){
			request.addError("A data de recusa da guia deve ser informada!");
			return index(request, new GuiaFiltro());
		}
		guia = guiaService.load(guia);
		guia.setDataChegada(SCRUtil.stringToDate(dataRecebimento));
		
		if(guia.getDataRetirada() != null){
			if(guia.getDataChegada().before(guia.getDataRetirada())){
				request.addError("A data de recebimento no Destino Final deve ser superior ou igual a data de retirada do GG.");
				return index(request, new GuiaFiltro());
			}
		}
		
		try {
			guia.setMotivo(motivo);
			guia.setSituacao("0");
			guia.setStatus(true);
			guiaService.recebimentoGuia(guia);
		} catch (Exception e) {
			request.addError("Ups, algo aconteceu ao tentar receber esta Guia! Entre em contato com os administradores do Sistema, eles poder�o lhe ajudar! :)");
			
		}
		request.addMessage("Guia Recebida com Sucesso! Obrigado.");
		return index(request, new GuiaFiltro());
	}
	
	public void adicionaCapacidadeDetinoFinal(WebRequestContext request, Guia guia){
		//ap�s a entrada das guias a quantidades de red�duos devem ser somadas ao volume atual do grande gerador
		double valorEntrada = 0;
		if(guia.getResiduosGuia()!=null)
			for (ResiduoGuia rg : guia.getResiduosGuia()) 
				valorEntrada += rg.getQuantidade();
		
		//atualiza o volume atual do destino final
		destinoFinalService.addCapacidadeAtual(valorEntrada, guia.getDestinoFinal());
		
		//caso destino final atinja capacidade > 90% envia email
		if(destinoFinalService.isCapacidadeMaiorIgualByPorcentagem(90,guia.getDestinoFinal())){
			DestinoFinal destinoFinal = destinoFinalService.load(guia.getDestinoFinal()); //carrega dest final atualizado
			try {
				//envia alerta ao t�cnico da SEMEA (usu�rios perfil adm)
				List<Usuario> usuarioEmail = usuarioService.findEmailPapelAdministrativo();
				String destinatariosAdm = CollectionsUtil.listAndConcatenate(usuarioEmail, "email", ";"); 
				destinatariosAdm+=";"+destinoFinal.getEmail();
				SCRUtil.enviaEmailAlertaCapacidadeTecnicoSEMEA(destinatariosAdm,destinoFinal);
				
				//envia alerta ao usuarios do destino final
				usuarioEmail = usuarioService.findEmailByDestinoFinal(destinoFinal);
				String destinatariosDest = CollectionsUtil.listAndConcatenate(usuarioEmail, "email", ";"); 
				destinatariosDest+=";"+destinoFinal.getEmail();
				SCRUtil.enviaEmailAlertaCapacidadeDestinoFinal(destinatariosDest,destinoFinal);
				request.addMessage("Aten��o! O destino final "+destinoFinal.getRazaoSocial()+" atingiu capacidade superior � 90% com a entrada desta guia. O T�cnico da SEMEA receber� um email informando o ocorrido.");
			
			} catch (MessagingException e) {
				request.addMessage("Aten��o! Ocorreu uma falha ao enviar email para o T�cnico da SEMEA informando que o destino final "+destinoFinal.getRazaoSocial()+" atingiu capacidade superior � 90% com a entrada desta guia.");
			} catch (IOException e) {
				request.addMessage("Aten��o! Ocorreu uma falha ao enviar email para o T�cnico da SEMEA informando que o destino final "+destinoFinal.getRazaoSocial()+" atingiu capacidade superior � 90% com a entrada desta guia.");
			} catch (Exception e) {
				request.addMessage("Aten��o! Ocorreu uma falha ao enviar email para o T�cnico da SEMEA informando que o destino final "+destinoFinal.getRazaoSocial()+" atingiu capacidade superior � 90% com a entrada desta guia.");				e.printStackTrace();
			}
		}
	}

}
