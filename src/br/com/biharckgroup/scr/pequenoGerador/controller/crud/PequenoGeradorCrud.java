package br.com.biharckgroup.scr.pequenoGerador.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.PequenoGeradorFiltro;
import br.com.biharckgroup.scr.bean.PequenoGerador;
import br.com.biharckgroup.scr.service.PequenoGeradorService;
import br.com.biharckgroup.scr.service.UfService;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/pequenoGerador/crud/PequenoGerador",authorizationModule=CrudAuthorizationModule.class)
public class PequenoGeradorCrud extends CrudControllerSCR<PequenoGeradorFiltro, PequenoGerador,PequenoGerador> {

	private UfService ufService;
	private PequenoGeradorService pequenoGeradorService;
	
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	public void setPequenoGeradorService(PequenoGeradorService pequenoGeradorService) {
		this.pequenoGeradorService = pequenoGeradorService;
	}
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		if("salvar".equals(acao)){
			PequenoGerador bean = (PequenoGerador) obj;
			/* Insert */
			if(!SCRUtil.isCPFouCNPJValido(bean.getCpf())){
				errors.reject("","O CPF informado n�o � v�lido.");
			}
		}
		super.validate(obj, errors, acao);
	}
	
	@Override
	protected void entrada(WebRequestContext request, PequenoGerador form)throws Exception {
		if(form.getId()!=null){
			form.setUf(ufService.getUfByMunicipio(form.getMunicipio()));
		}
		super.entrada(request, form);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, PequenoGerador bean)throws CrudException {
		try{
			pequenoGeradorService.saveOrUpdate(bean);
		}catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_cpf")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Pequeno Gerador com este CPF cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "unique_cnpj")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Pequeno Gerador com este CNPJ cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Pequeno Gerador com este e-mail cadastrado no sistema.");
			}
			return doEntrada(request, bean);
		}
		return doListagem(request, new PequenoGeradorFiltro());
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		
		PequenoGeradorFiltro _filtro = (PequenoGeradorFiltro) filtro;
		
		if(!SCRUtil.isPessoaLogadaAdministrador() && !SCRUtil.isPessoaLogadaAdministrativo()){
			if(SCRUtil.isPessoaLogadaPequenoGerador())
				_filtro.setCpf(SCRUtil.getUsuarioLogado().getPequenoGerador().getCpf());
			
		}
		
		return super.doListagem(request, filtro);
	}
	
}
