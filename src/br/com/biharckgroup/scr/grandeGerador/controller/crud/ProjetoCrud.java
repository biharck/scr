package br.com.biharckgroup.scr.grandeGerador.controller.crud;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.util.CollectionsUtil;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.ProjetoFiltro;
import br.com.biharckgroup.scr.adm.relatorio.ProjetoCompletoReport;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduo;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.bean.TipoResiduo;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.service.ProjetoService;
import br.com.biharckgroup.scr.service.ResponsavelService;
import br.com.biharckgroup.scr.service.TipoResiduoService;
import br.com.biharckgroup.scr.service.UsuarioService;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

import com.google.gson.Gson;

@Controller(path = "/grandeGerador/crud/Projeto", authorizationModule = CrudAuthorizationModule.class)
public class ProjetoCrud extends CrudControllerSCR<ProjetoFiltro, Projeto, Projeto> {
	
	private ResponsavelService responsavelService;
	private TipoResiduoService tipoResiduoService;
	private UsuarioService usuarioService;
	private ProjetoService projetoService;

	public void setTipoResiduoService(TipoResiduoService tipoResiduoService) {
		this.tipoResiduoService = tipoResiduoService;
	}
	public void setResponsavelService(ResponsavelService responsavelService) {
		this.responsavelService = responsavelService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Projeto form) throws Exception {
		
		//verifica se usu�rio tem papel de destino final
		if(!SCRUtil.isPessoaLogadaGrandeGerador())
			throw new SCRException("A tela de cadastro de Projetos (PGRS) s� � exibida para usu�rios do tipo Grande Gerador.");

		// define que o grande gerador do projeto � o usu�rio logado
		Usuario usuario = SCRUtil.getUsuarioLogado();
		if(usuario.getGrandeGerador() == null)
			throw new SCRException("N�o existe v�nculo do usu�rio logado com nenhum Grande Gerador.");
		form.setGrandeGerador(usuario.getGrandeGerador());

		super.entrada(request, form);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		
		
		//Aqui deve-se verificar o v�nculo do solicitante com o projeto
		//a primeira op��o � que seja o Grande Gerador do Projeto ou os transportadores
		//que possuem vinculo
		Usuario usuario = SCRUtil.getUsuarioLogado();
//		if(usuario.getGrandeGerador() == null && usuario.getTransportador()==null)
//			throw new SCRException("N�o existe v�nculo do usu�rio logado com Projetos " +
//					"(O usu�rio deve ser um Grande Gerador ou um Transportador relacionado a qualquer projeto).");
		//define filtro padr�o
		ProjetoFiltro _filtro = (ProjetoFiltro) filtro;
		if(_filtro.getGrandeGerador()==null)
			_filtro.setGrandeGerador(usuario.getGrandeGerador());
		if(_filtro.getTransportador()==null)
			_filtro.setTransportador(usuario.getTransportador());
		
		//filtros utilizados na index.jsp
		if(request.getParameter("df")!=null){
			String df = request.getParameter("df");
			if(df.equalsIgnoreCase("1"))		
				_filtro.setDestinoFinal(SCRUtil.getUsuarioLogado().getDestinoFinal());
		}
		if(request.getParameter("date")!=null){
			String df = request.getParameter("date");
			if(df.equalsIgnoreCase("1"))		
				_filtro.setDataCriacao(new Date());
		}
				
		return super.doListagem(request, filtro);
	}
	
	
	@Override
	protected void listagem(WebRequestContext request, FiltroListagem filtro)throws Exception {
		//para filtro por check
		request.setAttribute("listaResponsavel", responsavelService.findAll());
		super.listagem(request, filtro);
	}

	public void buscaTipoResiduo(WebRequestContext context) throws IOException,JSONException {
		try {
			context.getServletResponse().setContentType("application/json");
			context.getServletResponse().setCharacterEncoding("ISO-8859-1");

			if (getParameter("grupo") == null|| getParameter("grupo").equals("<null>")) {
				JSONArray jsonArray = new JSONArray();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("jscript","$('select[name=\"gruposTipoResiduoProjeto["+ getParameter("index")+ "].tipoResiduo\"]').empty();");
				jsonArray.put(jsonObject);
				context.getServletResponse().getWriter().println(jsonArray);
				return;
			}

			// enviando para o jsp
			context.getServletResponse().getWriter().println(formatCombo(tipoResiduoService.getTiposResiduosByGrupo(new GrupoTipoResiduo(SCRUtil.returnOnlyId(getParameter("grupo")))),getParameter("index")));

		} catch (Exception e) {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("erro", true);
			context.getServletResponse().setContentType("application/json");
			context.getServletResponse().setCharacterEncoding("ISO-8859-1");
			context.getServletResponse().getWriter().println(jsonObj);
			e.printStackTrace();
		}
	}

	/**
	 * <p>
	 * Funn��o respons�vel em montar toda estrutura de um combo para povoar o
	 * html
	 * 
	 * @param planos
	 *            lista contendo todos os planos de um determinada operadora
	 * @return {@link JSONArray} formatada para enviar via JSON
	 * @throws JSONException
	 */
	public JSONArray formatCombo(List<TipoResiduo> tiposResiduos, String index)throws JSONException {
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();

		jsonObject.put("jscript","$('select[name=\"gruposTipoResiduoProjeto[" + index+ "].tipoResiduo\"]').empty();");
		jsonArray.put(jsonObject);
		jsonObject = new JSONObject();
		jsonObject.put("jscript","$('select[name=\"gruposTipoResiduoProjeto["+ index+ "].tipoResiduo\"]').append('<option value=\"<null>\"></option>');");
		jsonArray.put(jsonObject);
		for (TipoResiduo tr : tiposResiduos) {
			jsonObject = new JSONObject();
			jsonObject.put("jscript","$('select[name=\"gruposTipoResiduoProjeto["+ index+ "].tipoResiduo\"]').append('<option value=\""+ SCRUtil.getComboFormat(tr, "id", tr.getId())+ "\">" + tr.getDescricao() + "</option>');");
			jsonArray.put(jsonObject);
		}
		return jsonArray;
	}	
	
	@Override
	protected void salvar(WebRequestContext request, Projeto bean) {
		bean.setGrandeGerador(SCRUtil.getUsuarioLogado().getGrandeGerador());//valor campo desabilitado = null
		
		super.salvar(request, bean);
		//envia e-mail aos usu�rios GG, com PDF referente ao projeto (PGRCC) criado
		List<Usuario> usuarioEmail = usuarioService.findEmailByGrandeGerador(bean.getGrandeGerador());
		String destinatariosGG = CollectionsUtil.listAndConcatenate(usuarioEmail, "email", ";"); 
		
		try {
			//cria pfd 
			File anexoProjeto = ProjetoCompletoReport.getInstance().getFileProjetoCompleto(bean);
			//envia email
			SCRUtil.enviaEmailProjeto(bean,destinatariosGG,anexoProjeto);
			request.addMessage("Email contendo projeto gerado foi enviado aos usu�rios do Grande Gerador: " +bean.getGrandeGerador().getNomeFantasia()+" com sucesso!");
		} catch (MessagingException e) {
			request.addError("Ocorreu uma falha ao enviar email contendo projeto gerado aos usu�rios do Grande Gerador: " +bean.getGrandeGerador().getNomeFantasia()+" ! Tente salvar o projeto novamente.");
			e.printStackTrace();
		} catch (IOException e) {
			request.addError("Ocorreu uma falha ao enviar email contendo projeto gerado aos usu�rios do Grande Gerador: " +bean.getGrandeGerador().getNomeFantasia()+" ! Tente salvar o projeto novamente.");
			e.printStackTrace();
		} catch (Exception e) {
			request.addError("Ocorreu uma falha ao enviar email contendo projeto gerado aos usu�rios do Grande Gerador: " +bean.getGrandeGerador().getNomeFantasia()+" ! Tente salvar o projeto novamente.");
			e.printStackTrace();
		}
	}
	
	/**
	 * <p>M�todo Ajax utilizado para buscar dados do projeto para cadastro no {@link Guia}
	 * Quando se informa um projeto no cadastro da guia, deve-se preencher o grande gerador e dados da obra referentes ao projeto
	 * @param request {@link WebRequestContext}
	 * @throws JSONException 
	 * @throws IOException 
	 */
	public void ajaxBuscaProjeto(WebRequestContext request) throws JSONException, IOException{
		JSONObject jsonObj = new JSONObject();
		
		try {
			Gson gson = new Gson();
			Projeto obj = new Projeto();
			String numeroProjeto = request.getParameter("numeroProjeto");
			obj = projetoService.loadByNumeroProjeto(numeroProjeto); //carrga obj
				
			obj.getUfObra().setMunicipios(null);
			obj.setGruposTipoResiduo(null);
			obj.setGruposTipoResiduoProjeto(null);
			obj.setResponsaveisProjeto(null);
			obj.setTransportadoresProjeto(null);
				
			String objJSONString = gson.toJson(obj); //transforma obj para formato Json
			jsonObj.put("obj", objJSONString);
			jsonObj.put("erro", false);
		} catch (Exception e) {
			jsonObj.put("erro", true);
			jsonObj.put("msg", "Ups! aconteceu algo inesperado! tente novamente mais tarde ou entre em contato com o administrador do sistema!");
			e.printStackTrace();
		}
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println(jsonObj);
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Projeto form)
			throws CrudException {
		// TODO Auto-generated method stub
		return super.doEntrada(request, form);
	}
	
	
}
