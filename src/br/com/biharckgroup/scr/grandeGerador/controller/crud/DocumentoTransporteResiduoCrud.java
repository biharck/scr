package br.com.biharckgroup.scr.grandeGerador.controller.crud;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.controller.resource.Resource;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.util.CollectionsUtil;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.DocumentoTransporteResiduoFiltro;
import br.com.biharckgroup.scr.adm.relatorio.DocumentoTransporteResiduoReport;
import br.com.biharckgroup.scr.bean.DocumentoTransporteResiduo;
import br.com.biharckgroup.scr.bean.EnumSituacaoRTR;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.Responsavel;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.dao.ConfiguracoesGeraisDAO;
import br.com.biharckgroup.scr.dao.GuiaDAO;
import br.com.biharckgroup.scr.dao.ProjetoDAO;
import br.com.biharckgroup.scr.dao.ResponsavelDAO;
import br.com.biharckgroup.scr.service.UsuarioService;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path = "/grandeGerador/crud/DocumentoTransporteResiduo", authorizationModule = CrudAuthorizationModule.class)
public class DocumentoTransporteResiduoCrud extends CrudControllerSCR<DocumentoTransporteResiduoFiltro, DocumentoTransporteResiduo, DocumentoTransporteResiduo> {

	private GuiaDAO guiaDAO;
	private UsuarioService usuarioService;
	private ConfiguracoesGeraisDAO configuracoesGeraisDAO;
	protected TransactionTemplate transactionTemplate;
	private ProjetoDAO projetoDAO;
	private ResponsavelDAO responsavelDAO;
	
	public void setGuiaDAO(GuiaDAO guiaDAO) {
		this.guiaDAO = guiaDAO;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setConfiguracoesGeraisDAO(
			ConfiguracoesGeraisDAO configuracoesGeraisDAO) {
		this.configuracoesGeraisDAO = configuracoesGeraisDAO;
	}
	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}
	public void setProjetoDAO(ProjetoDAO projetoDAO) {
		this.projetoDAO = projetoDAO;
	}
	public void setResponsavelDAO(ResponsavelDAO responsavelDAO) {
		this.responsavelDAO = responsavelDAO;
	}
	
	/*
	 * Gera chave de seguran�a,
	 * Envia e-mail para o t�cnico da SEMEA e SPU, solicitanco aprova��o do �RTR�,
	 *  Envia e-mail para o GG informando que RTR seguiu para an�lise
	 */
	@Action("enviarEmail")
	public ModelAndView enviarEmail(WebRequestContext request, DocumentoTransporteResiduo bean) throws CrudException{
		
			String chave = bean.getId()==null ? chave = SCRUtil.randon() : bean.getChave();
			//define sem situa��o, pois aguardar� aprova��o do tecnico
			bean.setEnumSituacao(EnumSituacaoRTR.SEMSITUACAO);
			bean.setChave(chave);
			bean.setEnviado(false);
			//atualiza dados do doc
			super.salvarSemMsg(request, bean);
					
			//busca guias de um projeto
			List<Guia> listaGuia = guiaDAO.findByProjeto(bean.getProjeto());
			int guiasConfirmadas=0, guiasPendentes=0, guiasGeradas = listaGuia.size();
			for (Guia guia1 : listaGuia) {
				if(guia1.isStatus())
					guiasConfirmadas ++;
				else
					guiasPendentes ++;
			}
			
			//define Grande gerador do projeto
			bean.setProjeto(projetoDAO.load(bean.getProjeto()));
			GrandeGerador gg = bean.getProjeto().getGrandeGerador();
			
			try{
				//envia RTR para t�cnico (usu�rios perfil adm)
		        List<Usuario> usuarioEmail = usuarioService.findEmailPapelAdministrativo();
		        String destinatariosAdm = CollectionsUtil.listAndConcatenate(usuarioEmail, "email", ";"); 
		        SCRUtil.enviaEmailRTRTecnicoSEMEA(destinatariosAdm, bean.getProjeto(),gg,guiasGeradas,guiasConfirmadas,guiasPendentes,bean.getId());
		        
		        //inf gg que RTR est� sob an�lise
		        usuarioEmail = usuarioService.findEmailByGrandeGerador(gg);
		        String destinatariosGG = CollectionsUtil.listAndConcatenate(usuarioEmail, "email", ";"); 
		        destinatariosGG+=";"+gg.getEmail();
		        SCRUtil.enviaEmailRTRGrandeGerador(destinatariosGG, bean.getProjeto(),gg,guiasGeradas,guiasConfirmadas,guiasPendentes,bean.getId());
		        
		        request.addMessage("Email contendo RTR Pr�vio foi enviado ao Tecnico da SEMEA com sucesso!");
		        request.addMessage("Email contendo RTR Pr�vio foi enviado aos usu�rios do Grande Gerador: " +gg.getNomeFantasia()+" com sucesso!");
		        
				//atualiza dados do doc
		        bean.setChave(chave);
		        bean.setEnviado(true);
		        super.salvarSemMsg(request, bean);
		        return new ModelAndView("redirect:/grandeGerador/crud/DocumentoTransporteResiduo");	
		        
			} catch (MessagingException e) {
				request.addError("Ocorreu uma falha ao enviar email para o Tecnico da SEMEA e Grande Gerador contendo o RTR Pr�vio!");
			} catch (IOException e) {
				request.addError("Ocorreu uma falha ao enviar email para o Tecnico da SEMEA e Grande Gerador contendo o RTR Pr�vio!");
			} catch (Exception e) {
				request.addError("Ocorreu uma falha ao enviar email para o Tecnico da SEMEA e Grande Gerador contendo o RTR Pr�vio!");
			}
		
			return doPage(request, bean);
	}
	
	 /*  Com o OK do Tecnico da SEMEA, ser� encaminhado 
	  *  anexo: Relat�rio de Transporte de Res�duos(RTR) para o Tecnico da SEMEA, Grande Gerador e para o Tecnico da SPU
	  *  Caso n�o seja aprovado uma email aos respons�veis do PGRCC informando a necessidade de explica��o sobre as guias pendentes.
	  */
	@Action("alterarSituacao")
	public ModelAndView alterarSituacao(WebRequestContext request, DocumentoTransporteResiduo bean) throws CrudException{
			
		if(bean.getEnumSituacao() == EnumSituacaoRTR.SEMSITUACAO){
			request.addError("Voc� deve informar uma situa��o v�lida!");
			return doPage(request, bean);
		}
		
		String chave = bean.getChave();
		//busca guias de um projeto
		List<Guia> listaGuia = guiaDAO.findByProjeto(bean.getProjeto());
		int guiasConfirmadas=0, guiasPendentes=0, guiasGeradas = listaGuia.size();
		for (Guia guia1 : listaGuia) {
			if(guia1.isStatus())
				guiasConfirmadas ++;
			else
				guiasPendentes ++;
		}
		//define Grande gerador do projeto
		bean.setProjeto(projetoDAO.load(bean.getProjeto()));
		GrandeGerador gg = bean.getProjeto().getGrandeGerador();
		String cpfCnpjTransient = gg.getCnpj()!= null && !gg.getCnpj().equals("") ? gg.getCnpj():  gg.getCpf(); 
		gg.setCpfCnpjTransient(cpfCnpjTransient);
		
		if(bean.getEnumSituacao() == EnumSituacaoRTR.APROVADO){
			//cria RTR, documento em anexo no email
			Resource resource = new Resource();
			try {
				resource = DocumentoTransporteResiduoReport.getInstance().getResourceReportByProjeto(bean.getProjeto(), chave,listaGuia,guiasGeradas,guiasConfirmadas,guiasPendentes,gg);
			} catch (IOException e1) {
				request.addError("Ocorreu uma falha ao criar relat�rio !");
			} 
			
			File relatorio = SCRUtil.getFileByDados(resource.getFileName(), resource.getContents());

			try{
				//envia RTR para t�cnico (usu�rios perfil adm)
		        List<Usuario> usuarioEmail = usuarioService.findEmailPapelAdministrativo();
		        String destinatariosAdm = CollectionsUtil.listAndConcatenate(usuarioEmail, "email", ";"); 
		        SCRUtil.enviaEmailRTRAprovadoTecnicoSEMEA(destinatariosAdm, bean.getProjeto(),gg,guiasGeradas,guiasConfirmadas,guiasPendentes,bean.getId(),relatorio);

		      //envia RTR para t�cnico SPU(usu�rios perfil adm)
		        String destinatariosSPU = configuracoesGeraisDAO.getConfiguracoesGerais().getEmailSPU();
		        SCRUtil.enviaEmailRTRAprovadoTecnicoSPU(destinatariosSPU, bean.getProjeto(),gg,guiasGeradas,guiasConfirmadas,guiasPendentes,bean.getId(),relatorio);
		        
		        //envia RTR ao gg
		        usuarioEmail = usuarioService.findEmailByGrandeGerador(gg);
		        String destinatariosGG = CollectionsUtil.listAndConcatenate(usuarioEmail, "email", ";"); 
		        destinatariosGG +=";"+gg.getEmail();
		        SCRUtil.enviaEmailRTRAprovadoGrandeGerador(destinatariosGG, bean.getProjeto(),gg,guiasGeradas,guiasConfirmadas,guiasPendentes,bean.getId(),relatorio);
		        
		        request.addMessage("Email contendo RTR Aprovado foi enviado ao Tecnico da SEMEA com sucesso!");
		        request.addMessage("Email contendo RTR Aprovado foi enviado ao Tecnico da SPU com sucesso !");
		        request.addMessage("Email contendo RTR Aprovado foi enviado aos usu�rios do Grande Gerador: " +gg.getNomeFantasia()+" com sucesso!");
			  
		      //atualiza dados do doc
		       	bean.setSituacao(bean.getEnumSituacao().getValor());
		        super.salvar(request, bean);
		        return new ModelAndView("redirect:/grandeGerador/crud/DocumentoTransporteResiduo");	
		        
			} catch (MessagingException e) {
				request.addError("Ocorreu uma falha ao enviar email para o Tecnico da SEMEA, Tecnico da SPU e Grande Gerador contendo o RTR Aprovado!");
				bean.setEnumSituacao(EnumSituacaoRTR.SEMSITUACAO);
			} catch (IOException e) {
				request.addError("Ocorreu uma falha ao enviar email para o Tecnico da SEMEA, Tecnico da SPU e Grande Gerador contendo o RTR Aprovado!");
				bean.setEnumSituacao(EnumSituacaoRTR.SEMSITUACAO);
			} catch (Exception e) {
				request.addError("Ocorreu uma falha ao enviar email para o Tecnico da SEMEA, Tecnico da SPU e Grande Gerador contendo o RTR Aprovado!");
				bean.setEnumSituacao(EnumSituacaoRTR.SEMSITUACAO);
			}

			//envia relat�rio para tela
//			try {
//				HttpServletResponse response = request.getServletResponse();
//				response.setContentType("application/pdf");
//				response.setContentType(resource.getContentType());
//				response.addHeader("Content-Disposition", "attachment; filename=\"" + resource.getFileName() + "\";");
//				response.getOutputStream().write(resource.getContents());
//			} catch (IOException e) {
//				request.addError("Ocorreu uma falha ao enviar relat�rio para download !");
//			}
		}
		
		if(bean.getEnumSituacao() == EnumSituacaoRTR.REPROVADO){
			//envia responsaveis projeto
			try{
				List<Responsavel> responsaveis = responsavelDAO.findByProjeto(bean.getProjeto());
		        String destinatariosResp = CollectionsUtil.listAndConcatenate(responsaveis, "email", ";"); 
		        SCRUtil.enviaEmailRTRReprovadoResponsaveis(destinatariosResp, bean.getProjeto(),gg,guiasGeradas,guiasConfirmadas,guiasPendentes,bean.getId());
		        request.addMessage("Email solicitando explica��o sobre as guias pendentes foi enviado aos respons�veis do Projeto!");
		        //atualiza dados do doc
		       	bean.setSituacao(bean.getEnumSituacao().getValor());
		        super.salvar(request, bean);
		        return new ModelAndView("redirect:/grandeGerador/crud/DocumentoTransporteResiduo");	
			} catch (MessagingException e) {
				request.addError("Ocorreu uma falha ao enviar email para os Respons�veis do projeto!");
				bean.setEnumSituacao(EnumSituacaoRTR.SEMSITUACAO);
			} catch (IOException e) {
				request.addError("Ocorreu uma falha ao enviar email para os Respons�veis do projeto!");
				bean.setEnumSituacao(EnumSituacaoRTR.SEMSITUACAO);
			} catch (Exception e) {
				request.addError("Ocorreu uma falha ao enviar email para os Respons�veis do projeto!");
				bean.setEnumSituacao(EnumSituacaoRTR.SEMSITUACAO);
			}
		}
			
			
		return doPage(request, bean);
	}
	
	public ModelAndView doPage(WebRequestContext request, DocumentoTransporteResiduo form) throws CrudException {
		//define atributos do metod. doEntrada
		try {
			setInfoForTemplate(request, form);
			setEntradaDefaultInfo(request, form);
		} catch (Exception e) {
			throw new CrudException(ENTRADA, e);
		}
		/*
		 * define se usu�rio logado � um superUser: administrador geral ou tem perfil administrativo(tecnico SEMEA)
		 * com a libera��o para alterar a situa��o do documento: aprovado/reprovado
		 */
		if(SCRUtil.isPessoaLogadaSuperUser())
			request.setAttribute("superUser", true);
		else
			request.setAttribute("superUser", false);
		
		return new ModelAndView("crud/documentoTransporteResiduoEntrada","documentoTransporteResiduo",form);
	}
	
	@Action("filtroGuiaByProjeto")
	public 	ModelAndView filtroGuiaByProjeto(WebRequestContext request,DocumentoTransporteResiduo form)throws Exception {
		//define guias do projeto
		List<Guia> listaGuia = guiaDAO.findByProjeto(form.getProjeto());
		setAttribute("guias", listaGuia);
		return doPage(request, form);
	}
	
	
	@Override
	protected void entrada(WebRequestContext request,
			DocumentoTransporteResiduo form) throws Exception {
		//update
		if(form.getId()==null){
			form.setEnumSituacao(EnumSituacaoRTR.SEMSITUACAO);
			request.setAttribute("guias", null);
		}
		
		/*
		 * define se usu�rio logado � um superUser: administrador geral ou tem perfil administrativo(tecnico SEMEA)
		 * com a libera��o para alterar a situa��o do documento: aprovado/reprovado
		 */
		if(SCRUtil.isPessoaLogadaSuperUser())
			request.setAttribute("superUser", true);
		else
			request.setAttribute("superUser", false);
		
		super.entrada(request, form);
	}
	
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		Usuario usuario = SCRUtil.getUsuarioLogado();
		if(SCRUtil.isPessoaLogadaGrandeGerador()){
			
			DocumentoTransporteResiduoFiltro _filtro = (DocumentoTransporteResiduoFiltro) filtro ;
			if(_filtro.getGrandeGerador()==null){
				_filtro.setGrandeGerador(usuario.getGrandeGerador());
			}
			return super.doListagem(request, _filtro);
		}
		return super.doListagem(request, filtro);
	}
}


		
