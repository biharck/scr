package br.com.biharckgroup.scr.publico.controller.crud;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.bean.EnumPapel;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.Papel;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.dao.UsuarioDAO;
import br.com.biharckgroup.scr.service.GrandeGeradorService;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

import com.google.gson.Gson;

@Controller(path="/publico/process/GrandeGerador")
public class GrandeGeradorProcess extends MultiActionController {

	private GrandeGeradorService grandeGeradorService;
	private UsuarioDAO usuarioDAO;
	private TransactionTemplate transactionTemplate;
	
	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	public void setGrandeGeradorService(GrandeGeradorService grandeGeradorService) {
		this.grandeGeradorService = grandeGeradorService;
	}
	
	@DefaultAction
	public ModelAndView entrada(WebRequestContext request,GrandeGerador bean)throws Exception {
		if(bean == null)
			bean= new GrandeGerador();
		else if(bean.getCpfCnpjTransient()!=null){
			if (bean.getCpfCnpjTransient().length()== 14){
				bean.setCpf(bean.getCpfCnpjTransient());
				bean.setCnpj(null);
			}else{
				bean.setCnpj(bean.getCpfCnpjTransient());
				bean.setCpf(null);
			}
		}
		request.setAttribute("title", " <img src='/SCR/img/style/next.png'><span class='titulo-pagina-corrente'> Grande Gerador </span>");
		return new ModelAndView("process/grandeGeradorAutoCadastro","grandeGerador", bean);
	}
	
	@Action("salvar")
	public ModelAndView salvar(final WebRequestContext request, GrandeGerador gg) throws Exception {
		
		final GrandeGerador bean = gg;
		/* Insert */
		if(bean.getId() == null){
			
			if(!SCRUtil.isCPFouCNPJValido(bean.getCpfCnpjTransient())){
				request.addError("O CPF/CNPJ informado n�o � v�lido.");
				return entrada(request, bean);
			}
			
			if(!bean.getEmail().equals(bean.getReEmail())){
				request.addError("O email e a confirma��o devem ser iguais.");
				return entrada(request, bean);
			}
			//verifica se j� existe usu�rio cadastrado com este email
			Usuario user = usuarioDAO.findUserEmail(gg.getEmail());
			if(user != null){
				request.addError("J� existe um registro de Usu�rio com este Email cadastrado no sistema.");
				return entrada(request, bean);
			}
			//verifica se j� existe usu�rio cadastrado com este login=cnpj
			user = usuarioDAO.findByLogin(SCRUtil.retiraMascara(bean.getCpfCnpjTransient()));
			if(user != null){
				request.addError("J� existe um registro de Usu�rio com este CNPJ cadastrado no sistema.");
				return entrada(request, bean);
			}
			
		}
		
		//passa getCpfCnpjTransient para campo correto
		if (bean.getCpfCnpjTransient().length()== 14){
			bean.setCpf(bean.getCpfCnpjTransient());
			bean.setCnpj(null);
		}else{
			bean.setCnpj(bean.getCpfCnpjTransient());
			bean.setCpf(null);
		}
		bean.setAtivo(true);
		bean.setAutoCadastro(true);
		
		
		/**
		 * O processo para salvar est� dividido em 2 categorias
		 * 1 - salvar o gg
		 * 2 - salvar o usu�rio
		 * em qualquer situa��o, caso ocorra um erro, um rollback � dado no sistema.
		 */
		try {
			transactionTemplate.execute(new TransactionCallback<Object>(){
				public Object doInTransaction(TransactionStatus arg0) {
					
					/**
					 * 1 salvar o GG
					 */
					try{
						grandeGeradorService.saveOrUpdate(bean);
					}catch (DataIntegrityViolationException e) {
						if (DatabaseError.isKeyPresent(e, "unique_cpf")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Grande Gerador com este CPF cadastrado no sistema.");
						}
						if (DatabaseError.isKeyPresent(e, "unique_cnpj")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Grande Gerador com este CNPJ cadastrado no sistema.");
						}
						if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Grande Gerador com este e-mail cadastrado no sistema.");
						}
						return null;
					}
					//FIM SALVAR O GG
					
					/**
					 * 2 - salvar o usu�rio
					 */
					//preparando usu�rio com perfil de cadastro de novos usu�rios
					Usuario user = new Usuario();
					user.setLogin(SCRUtil.retiraMascara(bean.getCpf()==null?bean.getCnpj():bean.getCpf()));
					String senhaTemporaria = SCRUtil.senhaTemporaria(); 
					user.setSenha(senhaTemporaria);
					user.setReSenha(senhaTemporaria);
					user.setAtivo(true);
					user.setSenhaProvisoria(true);
					user.setNome(bean.getNomeFantasia());
					user.setEmail(bean.getEmail());
					user.setReEmail(bean.getEmail());
					user.setSuperUsuario(true);
					
					Papel p = new Papel();
					p.setId(EnumPapel.GRANDEGERADOR.getValor());

					List<Papel> papeis = new ArrayList<Papel>();
					papeis.add(p);
					user.setPapeis(papeis);
					user.setGrandeGerador(bean);
					
					try{
						usuarioDAO.saveOrUpdate(user);
					} catch (DataIntegrityViolationException e) {
						if (DatabaseError.isKeyPresent(e, "unique_login")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Usu�rio com este Login cadastrado no sistema.");
						}
						if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Usu�rio com este Email cadastrado no sistema.");
						}
						return null;
					}	
					
					
					List<Usuario> usuarios = new ArrayList<Usuario>();
					usuarios.add(user);
					bean.setUsuarios(usuarios);
					
					//ATUALIZA A SITUA��O DO GRANDE GERADOR
					try{
						grandeGeradorService.saveOrUpdate(bean);
					} catch (DataIntegrityViolationException e) {
						if (DatabaseError.isKeyPresent(e, "unique_cpf")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Grande Gerador com este CPF cadastrado no sistema.");
						}
						if (DatabaseError.isKeyPresent(e, "unique_cnpj")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Grande Gerador com este CNPJ cadastrado no sistema.");
						}
						if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Grande Gerador com este e-mail cadastrado no sistema.");
						}
						return null;
					}
					
					//Envia email contendo informa��es do usu�rio criado para este destino final
					try {
						SCRUtil.enviaEmailNovoUsuario(user, senhaTemporaria);
					} catch (MessagingException e) {
						e.printStackTrace();
						throw new SCRException("Ups, alguma coisa aconteceu ao tentar enviar o email! Mas tudo bem, entre em contato conosco para podermos ajud�-lo! :)");
					} catch (IOException e) {
						e.printStackTrace();
						throw new SCRException("Ups, alguma coisa aconteceu ao tentar enviar o email! Mas tudo bem, entre em contato conosco para podermos ajud�-lo! :)");
					}
					
					
					//FIM SALVAR O USU�RIO
					request.addMessage("Grande Gerador Cadastrado com sucesso!");
					request.setAttribute("msg", "Seja bem Vindo! <br /> Falta pouco para voc� utilizar o sistema!<br /> " +
							"Para sua seguran�a, um e-mail de confirma��o foi enviado a voc�.<br /> Abra sua caixa de e-mail e clique no link enviado e pronto!<br />" +
							" Voc� j� poder� utilizar nossa solu��o!<br /> Obrigado :)");
					return null;
				}
			});
		} catch (UnexpectedRollbackException e) {
			bean.setId(null);
			return entrada(request, bean);
		}
		return new ModelAndView("process/sucesso");
	}
	
	/**
	 * <p>M�todo Ajax utilizado para buscar dados do grande gerador para cadastro no {@link Guia}
	 * @param request {@link WebRequestContext}
	 * @throws JSONException 
	 * @throws IOException 
	 */
	public void ajaxBuscaGrandeGerador(WebRequestContext request) throws JSONException, IOException{
		JSONObject jsonObj = new JSONObject();
		
		try {
			Gson gson = new Gson();
			String idCompleto = request.getParameter("idGrandeGerador");
			Integer id = SCRUtil.returnOnlyId(idCompleto);
			GrandeGerador obj = new GrandeGerador(id);
			obj = grandeGeradorService.load(obj); //carrga obj
			
			obj.getUf().setMunicipios(null);
			
			String objJSONString = gson.toJson(obj); //transforma obj para formato Json
			jsonObj.put("obj", objJSONString);
			jsonObj.put("erro", false);
		} catch (Exception e) {
			jsonObj.put("erro", true);
			jsonObj.put("msg", "Ups! aconteceu algo inesperado! tente novamente mais tarde ou entre em contato com o administrador do sistema!");
			e.printStackTrace();
		}
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println(jsonObj);
	}
}
