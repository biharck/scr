package br.com.biharckgroup.scr.publico.controller.crud;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.bean.EnumPapel;
import br.com.biharckgroup.scr.bean.Papel;
import br.com.biharckgroup.scr.bean.PequenoGerador;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.dao.PequenoGeradorDAO;
import br.com.biharckgroup.scr.dao.UsuarioDAO;
import br.com.biharckgroup.scr.service.PequenoGeradorService;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/publico/process/PequenoGerador")
public class PequenoGeradorProcess extends MultiActionController{

	private PequenoGeradorService pequenoGeradorService;
	private PequenoGeradorDAO pequenoGeradorDAO;
	private UsuarioDAO usuarioDAO;
	private TransactionTemplate transactionTemplate;
	
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	public void setPequenoGeradorService(PequenoGeradorService pequenoGeradorService) {
		this.pequenoGeradorService = pequenoGeradorService;
	}
	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}
	public void setPequenoGeradorDAO(PequenoGeradorDAO pequenoGeradorDAO) {
		this.pequenoGeradorDAO = pequenoGeradorDAO;
	}
	
	@DefaultAction
	public ModelAndView entrada(WebRequestContext request, PequenoGerador bean)throws Exception {
		getParameter("ACAO");
		if(bean == null)
			bean= new PequenoGerador();
		request.setAttribute("title", " <img src='/SCR/img/style/next.png'><span class='titulo-pagina-corrente'> Pequeno Gerador </span>");
		return new ModelAndView("process/pequenoGeradorAutoCadastro","pequenoGerador", bean);
	}
	
	@Action("salvar")
	public ModelAndView salvar(final WebRequestContext request, PequenoGerador pg) throws Exception {
		
		final PequenoGerador bean = pg;
		/* Insert */
		if(bean.getId() == null){
			
			if(!SCRUtil.isCPFouCNPJValido(bean.getCpf())){
				request.addError("O CPF informado n�o � v�lido.");
				return entrada(request, bean);
			}
			
			if(!bean.getEmail().equals(bean.getReEmail())){
				request.addError("O email e a confirma��o devem ser iguais.");
				return entrada(request, bean);
			}
			//verifica se j� existe usu�rio cadastrado com este email
			Usuario user = usuarioDAO.findUserEmail(pg.getEmail());
			if(user != null){
				request.addError("J� existe um registro de Usu�rio com este Email cadastrado no sistema.");
				return entrada(request, bean);
			}
			//verifica se j� existe usu�rio cadastrado com este login=cnpj
			user = usuarioDAO.findByLogin(SCRUtil.retiraMascara(bean.getCep()));
			if(user != null){
				request.addError("J� existe um registro de Usu�rio com este CPF cadastrado no sistema.");
				return entrada(request, bean);
			}
		}
		
		bean.setAtivo(true);
		bean.setAutoCadastro(true);
		
		/**
		 * O processo para salvar est� dividido em 2 categorias
		 * 1 - salvar o pg
		 * 2 - salvar o usu�rio
		 * em qualquer situa��o, caso ocorra um erro, um rollback � dado no sistema.
		 */
		try {
			transactionTemplate.execute(new TransactionCallback<Object>(){
				public Object doInTransaction(TransactionStatus arg0) {		
					/**
					 * 1 - SALVAR O PG
					 */
					try{
						pequenoGeradorService.saveOrUpdate(bean);						
					}catch (DataIntegrityViolationException e) {
						if (DatabaseError.isKeyPresent(e, "unique_cpf")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Pequeno Gerador com este CPF cadastrado no sistema.");
						}
						if (DatabaseError.isKeyPresent(e, "unique_cnpj")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Pequeno Gerador com este CNPJ cadastrado no sistema.");
						}
						if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Pequeno Gerador com este e-mail cadastrado no sistema.");
						}
						return null;
					}
					//FIM SALVAR PG
					
					/**
					 * 2 - SALVAR O USU�RIO
					 */
					//preparando usu�rio com perfil de cadastro de novos usu�rios
					Usuario user = new Usuario();
					user.setLogin(SCRUtil.retiraMascara(bean.getCpf()));
					String senhaTemporaria = SCRUtil.senhaTemporaria(); 
					user.setSenha(senhaTemporaria);
					user.setReSenha(senhaTemporaria);
					user.setAtivo(true);
					user.setSenhaProvisoria(true);
					user.setNome(bean.getNome());
					user.setEmail(bean.getEmail());
					user.setReEmail(bean.getEmail());
					user.setSuperUsuario(true);
					
					Papel p = new Papel();
					p.setId(EnumPapel.PEQUENOGERADOR.getValor());

					List<Papel> papeis = new ArrayList<Papel>();
					papeis.add(p);
					user.setPapeis(papeis);
					user.setPequenoGerador(bean);
					
					try{
						usuarioDAO.saveOrUpdate(user);
					} catch (DataIntegrityViolationException e) {
						if (DatabaseError.isKeyPresent(e, "unique_login")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Usu�rio com este Login cadastrado no sistema.");
						}
						if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Usu�rio com este Email cadastrado no sistema.");
						}
						return null;
					}	
					
					
					List<Usuario> usuarios = new ArrayList<Usuario>();
					usuarios.add(user);
					bean.setUsuarios(usuarios);
					
					
					try{
						pequenoGeradorService.saveOrUpdate(bean);
					}catch (DataIntegrityViolationException e) {
						if (DatabaseError.isKeyPresent(e, "unique_cpf")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Pequeno Gerador com este CPF cadastrado no sistema.");
						}
						if (DatabaseError.isKeyPresent(e, "unique_cnpj")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Pequeno Gerador com este CNPJ cadastrado no sistema.");
						}
						if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
							request.addError("J� existe um registro de Pequeno Gerador com este e-mail cadastrado no sistema.");
						}
						return null;
					}
					
					
					//Envia email contendo informa��es do usu�rio criado para este destino final
					try {
						SCRUtil.enviaEmailNovoUsuario(user, senhaTemporaria);
					} catch (MessagingException e) {
						e.printStackTrace();
						throw new SCRException("Ups, alguma coisa aconteceu ao tentar enviar o email! Mas tudo bem, entre em contato conosco para podermos ajud�-lo! :)");
					} catch (IOException e) {
						e.printStackTrace();
						throw new SCRException("Ups, alguma coisa aconteceu ao tentar enviar o email! Mas tudo bem, entre em contato conosco para podermos ajud�-lo! :)");
					}
					
					//FIM SALVAR USUARIO
					request.addMessage("Pequeno Gerador Cadastrado com sucesso!");
					request.setAttribute("msg", "Seja bem Vindo! <br /> Falta pouco para voc� utilizar o sistema!<br /> " +
							"Para sua seguran�a, um e-mail de confirma��o foi enviado a voc�.<br /> Abra sua caixa de e-mail e clique no link enviado e pronto!<br />" +
							" Voc� j� poder� utilizar nossa solu��o!<br /> Obrigado :)");
					return null;
				}		
			});
		} catch (UnexpectedRollbackException e) {
			if(bean!=null && bean.getId()!=null)
				pequenoGeradorDAO.apagarFisicamente(bean);
			bean.setId(null);
			return entrada(request, bean);
		}
		return new ModelAndView("process/sucesso");
	}
}
