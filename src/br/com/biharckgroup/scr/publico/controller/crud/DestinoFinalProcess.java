package br.com.biharckgroup.scr.publico.controller.crud;
import java.util.Calendar;

import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.dao.UsuarioDAO;
import br.com.biharckgroup.scr.service.DestinoFinalService;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/publico/process/DestinoFinal")
public class DestinoFinalProcess extends MultiActionController {
	
	private DestinoFinalService destinoFinalService;
	private UsuarioDAO usuarioDAO;
	
	public void setDestinoFinalService(DestinoFinalService destinoFinalService) {
		this.destinoFinalService = destinoFinalService;
	}
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	
	@DefaultAction
	public ModelAndView entrada(WebRequestContext request, DestinoFinal bean)throws Exception {
		if(bean == null)
			bean= new DestinoFinal();
		request.setAttribute("title", " <img src='/SCR/img/style/next.png'><span class='titulo-pagina-corrente'> Destino Final </span>");
		return new ModelAndView("process/destinoFinalAutoCadastro","destinoFinal", bean);
	}
	
	public ModelAndView salvar(WebRequestContext request, DestinoFinal bean) throws Exception {
		//passa getCpfCnpjTransient para campo correto, neste caso s� possui cnpj
		bean.setCnpj(bean.getCpfCnpjTransient());
		/*
		if(!SCRUtil.isCPFouCNPJValido(bean.getCpfCnpjTransient())){
			request.addError("O CPF/CNPJ informado n�o � v�lido.");
			return entrada(request, bean);
		}
		*/
		/* Insert */
		if(bean.getId() == null){
			bean.setValidadeCadastro(SCRUtil.incrementDate(new java.sql.Date(System.currentTimeMillis()), 365, Calendar.DAY_OF_YEAR));
			//verifica se j� existe usu�rio cadastrado com este email
			Usuario user = usuarioDAO.findUserEmail(bean.getEmail());
			if(user != null){
				request.addError("J� existe um registro de Usu�rio com este Email cadastrado no sistema.");
				return entrada(request, bean);
			}
			//verifica se j� existe usu�rio cadastrado com este login=cnpj
			user = usuarioDAO.findByLogin(SCRUtil.retiraMascara(bean.getCnpj()));
			if(user != null){
				request.addError("J� existe um registro de Usu�rio com este CNPJ cadastrado no sistema.");
				return entrada(request, bean);
			}
		}
		
		
		try{
			bean.setAtivo(false);
			bean.setAutoCadastro(true);
			
			//valida uniques
			try{
				destinoFinalService.saveOrUpdate(bean);
			} catch (DataIntegrityViolationException e) {
				if (DatabaseError.isKeyPresent(e, "unique_cnpj")){ //nota o index na tabela tem q ter o mesmo nome
					request.addError("J� existe um registro de Destino Final com este CNPJ cadastrado no sistema.");
					return entrada(request, bean);
				}
				if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
					request.addError("J� existe um registro de Destino Final com este e-mail cadastrado no sistema.");
					return entrada(request, bean);
				}
			}
			request.addMessage("Destino Final Cadastrado com sucesso!");
			request.setAttribute("msg", "Assim que seu cadastro for Aprovado, voc� receber� um e-mail<br /> com " +
					"instru��es de como utilizar o sistema!<br /> Aguarde, logo entraremos em contato com voc�!");
		}catch (Exception e) {
			e.printStackTrace();
			throw new SCRException("Ups! Houve uma falha ao tentar salvar o registro!<br/> Por favor, Entre em contato com o Administrador do sistema!");
		}
		return new ModelAndView("process/sucesso");
	}
}
