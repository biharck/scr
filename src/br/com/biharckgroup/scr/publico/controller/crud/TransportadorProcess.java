package br.com.biharckgroup.scr.publico.controller.crud;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.bean.TipoTransportador;
import br.com.biharckgroup.scr.bean.Transportador;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.dao.UsuarioDAO;
import br.com.biharckgroup.scr.service.TipoTransportadorService;
import br.com.biharckgroup.scr.service.TransportadorService;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/publico/process/Transportador")
public class TransportadorProcess extends MultiActionController {
	
	private TipoTransportadorService tipoTransportadorService;
	private TransportadorService transportadorService;
	private UsuarioDAO usuarioDAO;
	
	public void setPapelService(TipoTransportadorService tipoTransportadorService) {
		this.tipoTransportadorService = tipoTransportadorService;
	}
	public void setTransportadorService(
			TransportadorService transportadorService) {
		this.transportadorService = transportadorService;
	}
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}

	@DefaultAction
	public ModelAndView entrada(WebRequestContext request, Transportador bean)throws Exception {
		if(bean == null){
			bean= new Transportador();
		}
		request.setAttribute("title", " <img src='/SCR/img/style/next.png'><span class='titulo-pagina-corrente'> Transportador </span>");
		return new ModelAndView("process/transportadorAutoCadastro","transportador", bean);
	}
	
	
	/**
	 * <p>M�todo Ajax utilizado para buscar o porte do transportador cadastro no {@link TipoTransportador}
	 * @param request {@link WebRequestContext}
	 * @throws JSONException 
	 * @throws IOException 
	 */
	@Action("ajaxProcuraPorteTransportador")
	public void ajaxProcuraPorteTransportador(WebRequestContext request) throws JSONException, IOException{
		JSONObject jsonObj = new JSONObject();
		
		try {
			String idCompleto = request.getParameter("idTipoTransportador");
			Integer id = SCRUtil.returnOnlyId(idCompleto);
			TipoTransportador tt = new TipoTransportador(id);
			tt = tipoTransportadorService.load(tt);
			jsonObj.put("erro", false);
			jsonObj.put("porte", tt.isGrandeGerador());
		} catch (Exception e) {
			jsonObj.put("erro", true);
			jsonObj.put("msg", "Ups! aconteceu algo inesperado! tente novamente mais tarde ou entre em contato com o administrador do sistema!");
			e.printStackTrace();
		}
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println(jsonObj);
	}
	
	
	@Action("salvar")
	public ModelAndView salvar(WebRequestContext request, Transportador bean) throws Exception{
		
		bean.setValidadeCadastro(SCRUtil.incrementDate(new java.sql.Date(System.currentTimeMillis()), 365, Calendar.DAY_OF_YEAR));
		
		if(bean.getId() == null){
			if(!SCRUtil.isCPFouCNPJValido(bean.getCpfCnpjTransient())){
				request.addError("O CPF/CNPJ informado n�o � v�lido.");
				return entrada(request, bean);
			}
			if(!bean.getEmail().equals(bean.getReEmail())){
				request.addError("O email e a confirma��o devem ser iguais.");
				return entrada(request, bean);
			}
			//verifica se j� existe usu�rio cadastrado com este email
			Usuario user = usuarioDAO.findUserEmail(bean.getEmail());
			if(user != null){
				request.addError("J� existe um registro de Usu�rio com este Email cadastrado no sistema.");
				return entrada(request, bean);
			}
			//verifica se j� existe usu�rio cadastrado com este login=cnpj
			user = usuarioDAO.findByLogin(SCRUtil.retiraMascara(bean.getCpfCnpjTransient()));
			if(user != null){
				request.addError("J� existe um registro de Usu�rio com este CNPJ cadastrado no sistema.");
				return entrada(request, bean);
			}
			if(bean.getValidadeCadastro().before(new Date())){
				request.addError("A data de validade deve ser superior a data atual.");
				return entrada(request, bean);
			}
			
		}
			
		//se for um grande gerador...
		if(bean.getTipoTransportador()!= null){
			TipoTransportador tipoTransportador = tipoTransportadorService.load(bean.getTipoTransportador());
			if(tipoTransportador.isGrandeGerador()){
				if(bean.getRazaoSocial().equals(null) ||  bean.getRazaoSocial().equals("")){
					request.addError("Campo Raz�o Social � obrigat�rio.");
					return entrada(request, bean);
				}
				if(bean.getResponsavel().equals(null) ||  bean.getResponsavel().equals("")){
					request.addError("Campo Respons�vel � obrigat�rio.");
					return entrada(request, bean);
				}
				if(bean.getInscricaoMunicipal().equals(null) ||  bean.getInscricaoMunicipal().equals("")){
					request.addError("Campo Inscri��o Municipal � obrigat�rio.");
					return entrada(request, bean);
				}
			}
					
			
		}
		
		
		//se for pequeno gerador
		if(bean.getTipoTransportador()!= null){
			TipoTransportador tipoTransportador = tipoTransportadorService.load(bean.getTipoTransportador());
			if(!tipoTransportador.isGrandeGerador()){
				bean.setRazaoSocial(null);
				bean.setResponsavel(null);
				bean.setInscricaoEstadual(null);
				bean.setInscricaoMunicipal(null);
			}
		}			
		
		//passa getCpfCnpjTransient para campo correto
		if (bean.getCpfCnpjTransient().length()== 14){
			bean.setCpf(bean.getCpfCnpjTransient());
			bean.setCnpj(null);
		}else{
			bean.setCnpj(bean.getCpfCnpjTransient());
			bean.setCpf(null);
		}
		
		bean.setAtivo(false);
		bean.setAutoCadastro(true);
		try {
			transportadorService.saveOrUpdate(bean);
			
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_cpf")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Transportador com este CPF cadastrado no sistema.");
				return entrada(request, bean);
			}
			if (DatabaseError.isKeyPresent(e, "unique_cnpj")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Transportador com este CNPJ cadastrado no sistema.");
				return entrada(request, bean);
			}
			if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Transportador com este e-mail cadastrado no sistema.");
				return entrada(request, bean);
			}
		}
		
		
		request.addMessage("Transportador Cadastrado com sucesso!");
		request.setAttribute("msg", "Assim que seu cadastro for Aprovado, voc� receber� um e-mail<br /> com " +
				"instru��es de como utilizar o sistema!<br /> Aguarde, logo entraremos em contato com voc�!");
		return new ModelAndView("process/sucesso");
	}
}
