package br.com.biharckgroup.scr.bean;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="residuoguia")
public class ResiduoGuia extends BeanAuditoria  {
    
	private float quantidade; 
	private UnidadeMedida unidadeMedida;
	private TipoResiduo tipoResiduo;
	private Guia guia;
	
	@Required
	public float getQuantidade() {
		return quantidade;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_residuoguia_unidade")
	@DisplayName("Unidade de Medida")
	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_residouguia_tiporesiduo")
	@DisplayName("Tipo de Res�duo")
	public TipoResiduo getTipoResiduo() {
		return tipoResiduo;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_residuoguia_guia")
	public Guia getGuia() {
		return guia;
	}
	
	
	public void setQuantidade(float quantidade) {
		this.quantidade = quantidade;
	}
	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	public void setTipoResiduo(TipoResiduo tipoResiduo) {
		this.tipoResiduo = tipoResiduo;
	}
	public void setGuia(Guia guia) {
		this.guia = guia;
	}
}