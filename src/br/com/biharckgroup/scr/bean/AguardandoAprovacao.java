package br.com.biharckgroup.scr.bean;

import java.sql.Timestamp;

import org.nextframework.bean.annotation.DisplayName;

public class AguardandoAprovacao {

	private Integer id;
	private EnumTipoCadastro enumTipoCadastro;
	private String nome;
	private Timestamp dataCadastro;
	
	public AguardandoAprovacao(){}
	public AguardandoAprovacao(Integer id, EnumTipoCadastro enumTipoCadastro, String nome, Timestamp dataCadastro){
		this.id = id;
		this.enumTipoCadastro = enumTipoCadastro;
		this.nome =nome;
		this.dataCadastro = dataCadastro;
	}
	
	
	public Integer getId() {
		return id;
	}
	@DisplayName("Tipo de Cadastro")
	public EnumTipoCadastro getEnumTipoCadastro() {
		return enumTipoCadastro;
	}
	public String getNome() {
		return nome;
	}
	public Timestamp getDataCadastro() {
		return dataCadastro;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setEnumTipoCadastro(EnumTipoCadastro enumTipoCadastro) {
		this.enumTipoCadastro = enumTipoCadastro;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDataCadastro(Timestamp dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	
	
}
