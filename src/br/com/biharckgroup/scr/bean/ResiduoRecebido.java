package br.com.biharckgroup.scr.bean;


import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

public class ResiduoRecebido  {
	
	private Date data;
	private TipoResiduo tipoResiduo;
	private float quantidade;
	
	
	
	public ResiduoRecebido(Date data,
			TipoResiduo tipoResiduo, float quantidade) {
		super();
		this.data = data;
		this.tipoResiduo = tipoResiduo;
		this.quantidade = quantidade;
	}
	
	public void addQuantidade(float quantidade) {
		this.quantidade += quantidade;
	}
	
	public void setQuantidade(float quantidade) {
		this.quantidade = quantidade;
	}
	public void setTipoResiduo(TipoResiduo tipoResiduo) {
		this.tipoResiduo = tipoResiduo;
	}
	public float getQuantidade() {
		return quantidade;
	}
	public TipoResiduo getTipoResiduo() {
		return tipoResiduo;
	}
	
	
	public Date getData() {
		return data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}

}