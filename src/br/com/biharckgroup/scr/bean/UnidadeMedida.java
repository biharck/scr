package br.com.biharckgroup.scr.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="unidademedida")
@DisplayName("Unidades de Medida")
public class UnidadeMedida extends BeanAuditoria  {
    
	private String nome; 
	
	
	public UnidadeMedida(int i, String s) {
		this.id = i;
		this.nome=s;
	}
	public UnidadeMedida() {
		// TODO Auto-generated constructor stub
	}

	//GET
	@DescriptionProperty
    @Required
    @MaxLength(value=100)
	public String getNome() {
		return nome;
	}
	
	//Set
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}