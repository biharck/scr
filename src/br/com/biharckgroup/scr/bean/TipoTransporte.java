package br.com.biharckgroup.scr.bean;


import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="tipotransporte")
@DisplayName("Tipos de Transporte")
public class TipoTransporte extends BeanAuditoria  {
    
	private boolean ativo;
	private String descricao;
	private float capacidade;
	private UnidadeMedida unidadeMedida;
	private String nome;
	
	public boolean isAtivo() {
		return ativo;
	}
	@DisplayName("Descri��o")
	@MaxLength(value=100)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	@Required
	public float getCapacidade() {
		return capacidade;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_tipotransporte_unidade")
	@DisplayName("Unidade de Medida")
	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}
	@Required
	@MaxLength(value=100)
	public String getNome() {
		return nome;
	}
	
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCapacidade(float capacidade) {
		this.capacidade = capacidade;
	}
	public void setUnidadeMedida(UnidadeMedida unidadeMedida){
		this.unidadeMedida = unidadeMedida;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}