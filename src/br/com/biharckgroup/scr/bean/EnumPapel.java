package br.com.biharckgroup.scr.bean;

public enum EnumPapel {

	ADMINISTRADOR("Administrador Geral", 1),
	GRANDEGERADOR("Grande Gerador", 2),
	PEQUENOGERADOR("Pequeno Gerador", 3),
	DESTINOFINAL("Destino Final",4),
	ADMINISTRATIVO("Administrativo", 5),
	TRANSPORTADOR("Transportador",6);
	
	private EnumPapel(String nome, int valor){
		this.nome = nome;
		this.valor = valor;
	}
	
	private String nome;
	private int valor;
	
	public String getNome() {
		return nome;
	}

	public int getValor() {
		return valor;
	}
}
