package br.com.biharckgroup.scr.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.Email;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="grandegerador")
public class GrandeGerador extends BeanAuditoria  {
    
	private boolean ativo;
	private String razaoSocial;
	private String nomeFantasia;
	private String cnpj;
	private String cpf;
	private String cep;
	private String endereco;
	private String numero;
	private String bairro;
	private String inscricaoEstadual;
	private String inscricaoMunicipal;
	private String email;
	private String reEmail;
	private Telefone telefone1;
	private Telefone telefone2;
	private Telefone celular;
	private String responsavel;
	private String complemento;
	private Uf uf;
	private Municipio municipio;
	private String observacoes;
	private String cpfCnpjTransient;
	private List<Usuario> usuarios;
	private boolean autoCadastro;
	private String enredecoCompleto;
	
	public GrandeGerador(){
	}

	public GrandeGerador(Integer id) {
		this.id=id;
	}
	//transient
	@Transient
	@DisplayName("CPF/CNPJ")
	@Required
	public String getCpfCnpjTransient() {
		return cpfCnpjTransient;
	}

	//get
	public boolean isAtivo() {
		return ativo;
	}
	@MaxLength(value=200)
	@Required
	@DisplayName("Raz�o Social")
	public String getRazaoSocial() {
		return razaoSocial;
	}
	@DescriptionProperty
	@MaxLength(value=200)
	@Required
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getCpf() {
		return cpf;
	}
	public String getCep() {
		return cep;
	}
	@Required
	@MaxLength(value=255)
	@DisplayName("Endere�o")
	public String getEndereco() {
		return endereco;
	}
	@MaxLength(value=10)
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	@Required
	@MaxLength(value=100)
	public String getBairro() {
		return bairro;
	}
	@MaxLength(value=100)
	@DisplayName("Inscri��o Estadual")
	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}
	@MaxLength(value=100)
	@DisplayName("Inscri��o Municipal")
	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}
	@Required
	@Email
	@MaxLength(value = 200)
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	@Required
	@Email
	@DisplayName("Confirma��o E-mail")
	@Transient
	public String getReEmail() {
		return reEmail;
	}
	@Required
	@DisplayName("Telefone Principal")
	public Telefone getTelefone1() {
		return telefone1;
	}
	@DisplayName("Telefone Alternativo")
	public Telefone getTelefone2() {
		return telefone2;
	}
	public Telefone getCelular() {
		return celular;
	}
	@MaxLength(value=50)
	@Required
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		return responsavel;
	}
	@MaxLength(value=100)
	public String getComplemento() {
		return complemento;
	}
	@Transient
	@Required
	@DisplayName("UF")
	public Uf getUf() {
		return uf;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_destfin_mun")
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@MaxLength(value=255)
	@DisplayName("Observa��es")
	public String getObservacoes() {
		return observacoes;
	}
	@Transient
	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	public boolean isAutoCadastro() {
		return autoCadastro;
	}
	
	public void setCpfCnpjTransient(String cpfCnpjTransient) {
		this.cpfCnpjTransient = cpfCnpjTransient;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}
	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setReEmail(String reEmail) {
		this.reEmail = reEmail;
	}
	public void setTelefone1(Telefone telefone1) {
		this.telefone1 = telefone1;
	}
	public void setTelefone2(Telefone telefone2) {
		this.telefone2 = telefone2;
	}
	public void setCelular(Telefone celular) {
		this.celular = celular;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	public void setAutoCadastro(boolean autoCadastro) {
		this.autoCadastro = autoCadastro;
	}
	
	@Transient
	public String getenderecoCompleto() {
		String enderecoCompleto = getEndereco()==null? "":"Rua "+getEndereco();
		enderecoCompleto += getComplemento()==null || getComplemento().equals("")?"":", "+getComplemento();
		enderecoCompleto += getNumero()==null || getNumero().equals("")? "": ", "+getNumero()+" ";
		enderecoCompleto += getBairro()==null|| getBairro().equals("") ?"":", "+getBairro()+" ";
		enderecoCompleto += getMunicipio()==null?"":", "+getMunicipio().getNome()+" ";
		enderecoCompleto += getUf()==null?"":"-"+getUf().getSigla();
		
		return enderecoCompleto;
	}
	
	public void setEnderecoCompleto(String enderecoCompleto) {
		this.enredecoCompleto = enderecoCompleto;
	}
	
}