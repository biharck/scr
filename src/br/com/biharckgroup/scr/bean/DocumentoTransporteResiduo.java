package br.com.biharckgroup.scr.bean;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="documentogerenciamentoresiduo")
@DisplayName("Gera��o Documento de Gerenciamento de Res�duo")
public class DocumentoTransporteResiduo extends BeanAuditoria  {
    
	private String chave;
	private Projeto projeto;
	private String situacao;
	private EnumSituacaoRTR enumSituacao;
	private boolean enviado;
	
	public DocumentoTransporteResiduo() {
	}
	public DocumentoTransporteResiduo(Integer id) {
		this.id = id;
	}
	
	public String getChave() {
		return chave;
	}
	@DisplayName("Situa��o Documento")
	public String getSituacao() {
		return situacao;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_docgerenciamentoresiduo_proj")
	@DisplayName("Projeto (PGRCC)")
	public Projeto getProjeto() {
		return projeto;
	}
	@Transient
	@DisplayName("Situa��o Documento")
	public EnumSituacaoRTR getEnumSituacao() {
		return enumSituacao;
	}
	public boolean isEnviado() {
		return enviado;
	}
	
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public void setChave(String chave) {
		this.chave = chave;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setEnumSituacao(EnumSituacaoRTR enumSituacao) {
		this.enumSituacao = enumSituacao;
	}
	public void setEnviado(boolean enviado) {
		this.enviado = enviado;
	}
	
	
}