package br.com.biharckgroup.scr.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="configuracoesgerais")
public class ConfiguracoesGerais extends BeanAuditoria{

	private String usuarioEmail;
	private String senhaEmail;
	private String smtp;
	private String porta;
	private String emailSPU;
	private Telefone telefoneSPU;
	private Telefone telefoneSEMEA;
	
	@Required
	@DisplayName("E-mail Usu�rio")
	public String getUsuarioEmail() {
		return usuarioEmail;
	}
	@Required
	@DisplayName("Senha Usu�rio")
	public String getSenhaEmail() {
		return senhaEmail;
	}
	@Required
	public String getSmtp() {
		return smtp;
	}
	@Required
	public String getPorta() {
		return porta;
	}
	@Required
	@DisplayName("Email T�cnico da SUP")
	public String getEmailSPU() {
		return emailSPU;
	}
	public Telefone getTelefoneSPU() {
		return telefoneSPU;
	}
	@DisplayName("Telefone SEMEA")
	public Telefone getTelefoneSEMEA() {
		return telefoneSEMEA;
	}
	
	public void setUsuarioEmail(String usuarioEmail) {
		this.usuarioEmail = usuarioEmail;
	}
	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}
	public void setPorta(String porta) {
		this.porta = porta;
	}
	public void setSenhaEmail(String senhaEmail) {
		this.senhaEmail = senhaEmail;
	}
	public void setEmailSPU(String emailSPU) {
		this.emailSPU = emailSPU;
	}
	public void setTelefoneSPU(Telefone telefoneSPU) {
		this.telefoneSPU = telefoneSPU;
	}
	public void setTelefoneSEMEA(Telefone telefoneSEMEA) {
		this.telefoneSEMEA = telefoneSEMEA;
	}
}

