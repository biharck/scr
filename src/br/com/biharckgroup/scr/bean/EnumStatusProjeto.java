package br.com.biharckgroup.scr.bean;

public enum EnumStatusProjeto {

	CRIADO("Criado",'C',""),//quando a data atual < da data inicial
	INICIADO("Iniciado",'I',""), //quando a data atual > data atual data inicial && data atual < data termino 
	FINALIZADO("Finalizado",'F',""),//quando a data atual > data final
	;
	
	private EnumStatusProjeto(String nome, char opcao,String valor){
		this.nome = nome;
		this.opcao = opcao;
		this.valor = valor;
	}
	
	private String nome;
	private char opcao;
	private String valor;
	
	public String getNome() {
		return nome;
	}
	public char getOpcao() {
		return opcao;
	}
	public String getValor() {
		return valor;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
	
}
