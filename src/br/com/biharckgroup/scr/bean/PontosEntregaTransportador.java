package br.com.biharckgroup.scr.bean;


import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="pontosentregatransportador")
@DisplayName("Pontos de Entrega Voluntária do Transportador")
public class PontosEntregaTransportador extends BeanAuditoria  {
    
	private Transportador transportador;
	private PontoEntrega pontoEntrega;
	
	public PontosEntregaTransportador(){}
	
	@ManyToOne
	@ForeignKey(name="fk_pevtransp_pev")
	@Required
	public Transportador getTransportador() {
		return transportador;
	}
	@ManyToOne
	@ForeignKey(name="fk_pevtransp_transp")
	@Required
	public PontoEntrega getPontoEntrega() {
		return pontoEntrega;
	}
	
	public void setTransportador(Transportador transportador) {
		this.transportador = transportador;
	}
	public void setPontoEntrega(PontoEntrega pontoEntrega) {
		this.pontoEntrega = pontoEntrega;
	}
}