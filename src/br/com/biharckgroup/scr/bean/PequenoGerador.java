package br.com.biharckgroup.scr.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.Email;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="pequenogerador")
public class PequenoGerador extends BeanAuditoria  {
    
	private boolean ativo;
	private String nome;
	private String cpf;
	private String cep;
	private String endereco;
	private String numero;
	private String bairro;
	private String email;
	private String reEmail;
	private Telefone telefone1;
	private Telefone telefone2;
	private Telefone celular;
	private String complemento;
	private Uf uf;
	private Municipio municipio;
	private List<Usuario> usuarios;
	private boolean autoCadastro;
	
	public PequenoGerador(){
	}

	public PequenoGerador(Integer id) {
		this.id=id;
	}

	//get
	public boolean isAtivo() {
		return ativo;
	}
	@MaxLength(value=200)
	@Required
	public String getNome() {
		return nome;
	}
	@DisplayName("CPF")
	@Required
	public String getCpf() {
		return cpf;
	}
	public String getCep() {
		return cep;
	}
	@Required
	@MaxLength(value=255)
	@DisplayName("Endere�o")
	public String getEndereco() {
		return endereco;
	}
	@MaxLength(value=10)
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	@Required
	@MaxLength(value=100)
	public String getBairro() {
		return bairro;
	}
	@Required
	@Email
	@MaxLength(value = 200)
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	@Required
	@Email
	@DisplayName("Confirma��o E-mail")
	@Transient
	public String getReEmail() {
		return reEmail;
	}
	@Required
	@DisplayName("Telefone Principal")
	public Telefone getTelefone1() {
		return telefone1;
	}
	@DisplayName("Telefone Alternativo")
	public Telefone getTelefone2() {
		return telefone2;
	}
	public Telefone getCelular() {
		return celular;
	}
	@MaxLength(value=100)
	public String getComplemento() {
		return complemento;
	}
	@Transient
	@Required
	@DisplayName("UF")
	public Uf getUf() {
		return uf;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_destfin_mun")
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@Transient
	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	public boolean isAutoCadastro() {
		return autoCadastro;
	}
	
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setReEmail(String reEmail) {
		this.reEmail = reEmail;
	}
	public void setTelefone1(Telefone telefone1) {
		this.telefone1 = telefone1;
	}
	public void setTelefone2(Telefone telefone2) {
		this.telefone2 = telefone2;
	}
	public void setCelular(Telefone celular) {
		this.celular = celular;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setAutoCadastro(boolean autoCadastro) {
		this.autoCadastro = autoCadastro;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
}