package br.com.biharckgroup.scr.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.authorization.User;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Password;
import org.nextframework.validation.annotation.Email;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

/**
 * Representa um usu�rio no sistema
 */
@Entity
@Table(name="usuario")
@DisplayName("Usu�rios")
public class Usuario extends BeanAuditoria implements User {
    
	private String nome;
	private String login;
	private String senha;
	private String reSenha;
	private String email;
	private String reEmail;
	private boolean ativo;
	private boolean senhaProvisoria;
	private Timestamp dtUltimoLogin;
	private String senhaAnterior;
	private String senhaAnterior2;
	private Timestamp dataRequisicao;
	private List<Papel> papeis;
	private List<PapelUsuario> papeisUsuario;
	private DestinoFinal destinoFinal;
	private GrandeGerador grandeGerador; 
	private Transportador transportador;
	private PequenoGerador pequenoGerador;
	private boolean superUsuario;//este boolean determina se o usu�rio pode cadastrar outro usu�rios caso n�o
	//seja um administrador ou administrativo
	
	
	//TRANSIENT
	@Transient
	public String getPassword() {
		return getSenha();
	}
	@Required
	@Transient
	public List<Papel> getPapeis() {
		return papeis;
	}
	//GET
	@DescriptionProperty
    @Required
    @MaxLength(value=50)
	public String getNome() {
		return nome;
	}
	@Required
	public String getLogin() {
		return login;
	}
	@Password
	@Required
	@MaxLength(value=20)
	public String getSenha() {
		return senha;
	}
	@Password
	@Required
	@Transient
	@DisplayName("Confirma��o da Senha")
	public String getReSenha() {
		return reSenha;
	}
	@Email
	@Required
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	@Required
	@Email
	@Transient
	@DisplayName("Confirma��o do E-mail")
	public String getReEmail() {
		return reEmail;
	}
	public boolean isAtivo() {
		return ativo;
	}
	@DisplayName("Senha Provis�ria")
	public boolean isSenhaProvisoria() {
		return senhaProvisoria;
	}
	@DisplayName("Data �ltimo Login")
	public Timestamp getDtUltimoLogin() {
		return dtUltimoLogin;
	}
	@Password
	public String getSenhaAnterior() {
		return senhaAnterior;
	}
	@Password
	public String getSenhaAnterior2() {
		return senhaAnterior2;
	}
	public Timestamp getDataRequisicao() {
		return dataRequisicao;
	}
	@OneToMany(mappedBy="usuario")
	public List<PapelUsuario> getPapeisUsuario() {
		return papeisUsuario;
	}
	@ManyToOne
	@ForeignKey(name="fk_usuario_destinofinal")
	public DestinoFinal getDestinoFinal() {
		return destinoFinal;
	}
	@ManyToOne
	@ForeignKey(name="fk_usuario_grandegerador")
	public GrandeGerador getGrandeGerador() {
		return grandeGerador;
	}
	@ManyToOne
	@ForeignKey(name="fk_usuario_transportador")
	public Transportador getTransportador() {
		return transportador;
	}
	@ManyToOne
	@ForeignKey(name="fk_usuario_pequenogerador")
	public PequenoGerador getPequenoGerador() {
		return pequenoGerador;
	}
	@DisplayName("Super Usu�rio")
	public boolean isSuperUsuario() {
		return superUsuario;
	}
	
	//Set
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setReEmail(String reEmail) {
		this.reEmail = reEmail;
	}
	public void setReSenha(String reSenha) {
		this.reSenha = reSenha;
	}
	public void setSenhaProvisoria(boolean senhaProvisoria) {
		this.senhaProvisoria = senhaProvisoria;
	}
	public void setDtUltimoLogin(Timestamp dtUltimoLogin) {
		this.dtUltimoLogin = dtUltimoLogin;
	}
	public void setSenhaAnterior(String senhaAnterior) {
		this.senhaAnterior = senhaAnterior;
	}
	public void setSenhaAnterior2(String senhaAnterior2) {
		this.senhaAnterior2 = senhaAnterior2;
	}
	public void setPapeisUsuario(List<PapelUsuario> papeisUsuario) {
		this.papeisUsuario = papeisUsuario;
	}	
	public void setDestinoFinal(DestinoFinal destinoFinal) {
		this.destinoFinal = destinoFinal;
	}
	public void setGrandeGerador(GrandeGerador grandeGerador) {
		this.grandeGerador = grandeGerador;
	}
	public void setTransportador(Transportador transportador) {
		this.transportador = transportador;
	}
	public void addPapeis(Papel papel) {
		if(this.papeis == null)
			this.papeis = new ArrayList<Papel>();
		this.papeis.add(papel);
	}
	public void setDataRequisicao(Timestamp dataRequisicao) {
		this.dataRequisicao = dataRequisicao;
	}
	public void setPequenoGerador(PequenoGerador pequenoGerador) {
		this.pequenoGerador = pequenoGerador;
	}
	public void setSuperUsuario(boolean superUsuario) {
		this.superUsuario = superUsuario;
	}
}