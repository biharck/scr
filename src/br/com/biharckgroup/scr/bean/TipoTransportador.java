package br.com.biharckgroup.scr.bean;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="tipotransportador")
@DisplayName("Tipos de Transportador")
public class TipoTransportador extends BeanAuditoria  {
    
	private String nome;
	private String descricao;
	private boolean ativo;
	private boolean grandeGerador;
	private EnumTipoGerador enumTipoGerador;
	
	
	public TipoTransportador(){
		
	}
	public TipoTransportador(Integer id){
		this.id = id;
	}
	
	@DescriptionProperty
	@Required
	@MaxLength(value = 100)
	public String getNome() {
		return nome;
	}
	@DisplayName("Descri��o")
	@MaxLength(value=255)
	public String getDescricao() {
		return descricao;
	}

	public boolean isGrandeGerador() {
		return grandeGerador;
	}
	@Transient
	@Required
	@DisplayName("Porte do Gerador")
	public EnumTipoGerador getEnumTipoGerador() {
		return enumTipoGerador;
	}
	
	public boolean isAtivo() {
		return ativo;
	}
	
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setEnumTipoGerador(EnumTipoGerador enumTipoGerador) {
		this.enumTipoGerador = enumTipoGerador;
	}
	public void setGrandeGerador(boolean grandeGerador) {
		this.grandeGerador = grandeGerador;
	}
	
	
	
}