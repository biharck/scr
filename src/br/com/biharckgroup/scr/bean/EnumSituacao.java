package br.com.biharckgroup.scr.bean;

public enum EnumSituacao {

	ACEITA("Aceita",'S',"1"),
	NAOACEITA("N�o Aceita",'N',"0"),
	ACEITA_DIVERGENCIA("Aceita com Diverg�ncias",'S',"2"),
	SEMSITUACAO("Sem Situa��o", ' ',"NULL") //null
	;
	
	private EnumSituacao(String nome, char opcao,String valor){
		this.nome = nome;
		this.opcao = opcao;
		this.valor = valor;
	}
	
	private String nome;
	private char opcao;
	private String valor;
	
	public String getNome() {
		return nome;
	}
	public char getOpcao() {
		return opcao;
	}
	public String getValor() {
		if(valor.equals("NULL"))
			return null;
		return valor;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
	
	public static EnumSituacao getEnumSituacaoByValor(String valor){
		if(valor == null)
			return EnumSituacao.SEMSITUACAO;
			
		if(valor.equals(EnumSituacao.ACEITA.valor))
			return EnumSituacao.ACEITA;
		else if(valor.equals(EnumSituacao.ACEITA_DIVERGENCIA.valor))
			return EnumSituacao.ACEITA_DIVERGENCIA;
		else
			return EnumSituacao.NAOACEITA;
	}
}
