package br.com.biharckgroup.scr.bean;


import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="classetiporesiduo")
@DisplayName("Classes de Tipos de Res�duos")
public class ClasseTipoResiduo extends BeanAuditoria  {
    
	
	private String nome;
	private boolean ativo;
	private GrupoTipoResiduo grupoTipoResiduo;
	
	@Required
	@DescriptionProperty
	@MaxLength(value=100)
	public String getNome() {
		return nome;
	}
	public boolean isAtivo() {
		return ativo;
	}
	@ManyToOne
	@ForeignKey(name="fk_classe_grupo")
	@Required
	@DisplayName("Grupos de Tipos de Res�duo")
	public GrupoTipoResiduo getGrupoTipoResiduo() {
		return grupoTipoResiduo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setGrupoTipoResiduo(GrupoTipoResiduo grupoTipoResiduo) {
		this.grupoTipoResiduo = grupoTipoResiduo;
	}
}