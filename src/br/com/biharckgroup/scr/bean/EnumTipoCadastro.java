package br.com.biharckgroup.scr.bean;

public enum EnumTipoCadastro {

	TRANSPORTADOR("Transportador",1),
	DESTINO_FINAL("Destino Final",2),
	PEQUENO_GERADOR("Pequeno Gerador",3),
	GRANDE_GERADOR("Grande Gerador",4)
	;
	
	private EnumTipoCadastro(String nome,int tipo){
		this.nome = nome;
		this.tipo = tipo;
	}
	
	private String nome;
	private int tipo;
	
	public String getNome() {
		return nome;
	}
	public int getTipo() {
		return tipo;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
}
