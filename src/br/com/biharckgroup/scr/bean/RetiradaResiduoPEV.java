package br.com.biharckgroup.scr.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Hora;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="retiradaresiduopev")
@DisplayName("Requisi��o de Retirada de Res�duos do PEV ")
public class RetiradaResiduoPEV extends BeanAuditoria  {
    
	private PontoEntrega pontoEntrega;
	private Transportador transportador;
	private List<GrupoTipoResiduoRetiradaResiduoPEV> grupoTipoResiduosRetiradaResiduoPEV;
	private Date data;
	private Hora hora;
	private String responsavel;
	private List<GrupoTipoResiduo> gruposTipoResiduo;
	private Guia guia;
	
	public RetiradaResiduoPEV() {
	}
	public RetiradaResiduoPEV(Integer id) {
		this.id= id;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_retiradaresiduos_pev")
	@DisplayName("Credenciais PEV")
	public PontoEntrega getPontoEntrega() {
		return pontoEntrega;
	}
	@ManyToOne
	@ForeignKey(name="fk_transportador_credPEV")
	@DisplayName("Transportador")
	public Transportador getTransportador() {
		return transportador;
	}
	@Required
	public Date getData() {
		return data;
	}
	public Hora getHora() {
		return hora;
	}
	@Required
	@DisplayName("Respons�vel pelo Res�duo")
	public String getResponsavel() {
		return responsavel;
	}
	@OneToMany(mappedBy="retiradaResiduoPEV")
	@Required
	public List<GrupoTipoResiduoRetiradaResiduoPEV> getGrupoTipoResiduosRetiradaResiduoPEV() {
		return grupoTipoResiduosRetiradaResiduoPEV;
	}
	@Transient
	public List<GrupoTipoResiduo> getGruposTipoResiduo() {
		return gruposTipoResiduo;
	}
	@ManyToOne
	@ForeignKey(name="fk_retiradaresiduos_guia")
	public Guia getGuia() {
		return guia;
	}
	
	//set
	public void setPontoEntrega(PontoEntrega pontoEntrega) {
		this.pontoEntrega = pontoEntrega;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setHora(Hora hora) {
		this.hora = hora;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public void setTransportador(Transportador transportador) {
		this.transportador = transportador;
	}
	public void setGrupoTipoResiduosRetiradaResiduoPEV(List<GrupoTipoResiduoRetiradaResiduoPEV> grupoTipoResiduosRetiradaResiduoPEV) {
		this.grupoTipoResiduosRetiradaResiduoPEV = grupoTipoResiduosRetiradaResiduoPEV;
	}
	public void setGruposTipoResiduo(List<GrupoTipoResiduo> gruposTipoResiduo) {
		this.gruposTipoResiduo = gruposTipoResiduo;
	}
	public void addGruposTipoResiduo(GrupoTipoResiduo grupoTipoResiduo) {
		if(this.gruposTipoResiduo == null)
			this.gruposTipoResiduo = new ArrayList<GrupoTipoResiduo>();
		this.gruposTipoResiduo.add(grupoTipoResiduo);
	}
	public void setGuia(Guia guia) {
		this.guia = guia;
	}
}