package br.com.biharckgroup.scr.bean;


import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="grupotiporesiduoretiradaresiduopev")
public class GrupoTipoResiduoRetiradaResiduoPEV extends BeanAuditoria  {
    
	private RetiradaResiduoPEV retiradaResiduoPEV;
	private GrupoTipoResiduo grupoTipoResiduo;
	private TipoResiduo tipoResiduo;
	private float quantidade; 
	private UnidadeMedida unidadeMedida;
	
	@Required
	@ManyToOne
	@ForeignKey(name="fk_grupotipoentrada_retiradaresiduo")
	public RetiradaResiduoPEV getRetiradaResiduoPEV() {
		return retiradaResiduoPEV;
	}
	
	@Required
	@ManyToOne
	@ForeignKey(name="fk_grupotiporeirada_grupotiporesiduo")
	@DisplayName("Grupos de Tipos de Res�duo")
	public GrupoTipoResiduo getGrupoTipoResiduo() {
		return grupoTipoResiduo;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_grupotiporetirada_tiporesiduo")
	@DisplayName("Tipos de Res�duo")
	public TipoResiduo getTipoResiduo() {
		return tipoResiduo;
	}
	@Required
	public float getQuantidade() {
		return quantidade;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_grupotiporetirada_unidade")
	@DisplayName("Unidade de Medida")
	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setRetiradaResiduoPEV(RetiradaResiduoPEV retiradaResiduoPEV) {
		this.retiradaResiduoPEV = retiradaResiduoPEV;
	}
	public void setGrupoTipoResiduo(GrupoTipoResiduo grupoTipoResiduo) {
		this.grupoTipoResiduo = grupoTipoResiduo;
	}
	public void setTipoResiduo(TipoResiduo tipoResiduo) {
		this.tipoResiduo = tipoResiduo;
	}
	public void setQuantidade(float quantidade) {
		this.quantidade = quantidade;
	}
	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	
}