package br.com.biharckgroup.scr.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.types.File;

@Entity
@Table(name="arquivo")
public class Arquivo implements File {

    Long id;
    String name;
    String contenttype;
    Long size;
    byte[] content;
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id")
    public Long getCdfile() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getContenttype() {
        return contenttype;
    }
    public Long getSize() {
        return size;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setContenttype(String contenttype) {
        this.contenttype = contenttype;
    }
    public void setSize(Long size) {
        this.size = size;
    }
    public byte[] getContent() {
        return content;
    }
    public void setContent(byte[] content) {
        this.content = content;
    }
    public void setCdfile(Long arg0) {
        this.id = arg0;
    }

}