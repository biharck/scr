package br.com.biharckgroup.scr.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@DisplayName("Projeto - PGRS")
@Table(name="projeto")
public class Projeto extends BeanAuditoria  {
	
	/*Resumo*/
	private String numeroProjeto;
	private Date dataInicio;
	private Date dataTermino;
	private int numeroProcesso;
	/*Endere�o Obra*/
	private String cepObra;
	private String enderecoObra;
	private String numeroObra;
	private String bairroObra;
	private String complementoObra;
	private Uf ufObra;
	private Municipio municipioObra;
	private String caracteristicaObra;
	private String materiaisObra;
	/*Res�duos*/
	private String descricaoDestino;
	private List<GrupoTipoResiduoProjeto> gruposTipoResiduoProjeto;
	/*Iniciativas*/
	private String iniciativaMinimizacao;
	private String iniciativaAbsorcao;
	private String iniciativaAcondicionamento;
	/*Transportador*/
	private List<TransportadorProjeto> transportadoresProjeto;
	/*Respons�veis*/
	private List<ResponsavelProjeto> responsaveisProjeto;
	private List<Transportador> transportadores;
	private List<GrupoTipoResiduo> gruposTipoResiduo;
	private List<Responsavel> responsaveis;
	private EnumStatusProjeto enumStatusProjeto;
	private GrandeGerador grandeGerador;
	private String enderecoObraCompleto;
	
	public Projeto(Integer id) {
		this.id = id;
	}
	public Projeto() {
	}
	@DescriptionProperty
	@DisplayName("N�mero do Projeto")
	public String getNumeroProjeto() {
		return numeroProjeto;
	}
	@Required
	@DisplayName("Data de In�cio")
	public Date getDataInicio() {
		return dataInicio;
	}
	@Required
	@DisplayName("Data de T�rmino")
	public Date getDataTermino() {
		return dataTermino;
	}
	@DisplayName("N�mero do Processo")
	public int getNumeroProcesso() {
		return numeroProcesso;
	}
	@Required
	@MaxLength(value=100)
	@DisplayName("Bairro")
	public String getBairroObra() {
		return bairroObra;
	}
	@Required
	@DisplayName("Cep")
	public String getCepObra() {
		return cepObra;
	}
	@MaxLength(value=100)
	@DisplayName("Complemento")
	public String getComplementoObra() {
		return complementoObra;
	}
	@Required
	@MaxLength(value=200)
	@DisplayName("Endere�o")
	public String getEnderecoObra() {
		return enderecoObra;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_projeto_municipio")
	@DisplayName("Munic�pio")
	public Municipio getMunicipioObra() {
		return municipioObra;
	}
	@MaxLength(value=10)
	@DisplayName("N�mero")
	public String getNumeroObra() {
		return numeroObra;
	}
	@Required
	@Transient
	@DisplayName("UF")
	public Uf getUfObra() {
		return ufObra;
	}
	@MaxLength(value=500)
	@Required
	@DisplayName("Caracter�sticas b�sicas da planta:")
	public String getCaracteristicaObra() {
		return caracteristicaObra;
	}
	@MaxLength(value=500)
	@Required
	@DisplayName("Materiais e compomentes utilizados em cada etapa do processo:")
	public String getMateriaisObra() {
		return materiaisObra;
	}
	@MaxLength(value=500)
	@DisplayName("Descri��o do destino a ser dado a outros tipos de res�duo:")
	public String getDescricaoDestino() {
		return descricaoDestino;
	}
	@OneToMany(mappedBy="projeto")
	@Required
	public List<GrupoTipoResiduoProjeto> getGruposTipoResiduoProjeto() {
		return gruposTipoResiduoProjeto;
	}
	@MaxLength(value=500)
	@Required
	@DisplayName("Iniciativas para minimiza��o dos res�duos:")
	public String getIniciativaMinimizacao() {
		return iniciativaMinimizacao;
	}
	@MaxLength(value=500)
	@Required
	@DisplayName("Iniciativas para absor��o dos res�duos na pr�pria ou em outras plantas:")
	public String getIniciativaAbsorcao() {
		return iniciativaAbsorcao;
	}
	@MaxLength(value=500)
	@Required
	@DisplayName("Iniciativas para acondicionamento diferenciado e transporte adequado:")
	public String getIniciativaAcondicionamento() {
		return iniciativaAcondicionamento;
	}
	@OneToMany(mappedBy="projeto")
	@Required
	public List<TransportadorProjeto> getTransportadoresProjeto() {
		return transportadoresProjeto;
	}
	@OneToMany(mappedBy="projeto")
	@Required
	public List<ResponsavelProjeto> getResponsaveisProjeto() {
		return responsaveisProjeto;
	}
	@Transient
	public List<Transportador> getTransportadores() {
		return transportadores;
	}
	@Transient
	public List<Responsavel> getResponsaveis() {
		return responsaveis;
	}
	@Transient
	public List<GrupoTipoResiduo> getGruposTipoResiduo() {
		return gruposTipoResiduo;
	}
	@Transient
	@DisplayName("Status")
	public EnumStatusProjeto getEnumStatusProjeto() {
		return enumStatusProjeto;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_projeto_gg")
	public GrandeGerador getGrandeGerador() {
		return grandeGerador;
	}
	
	public void setNumeroProjeto(String numeroProjeto) {
		this.numeroProjeto = numeroProjeto;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public void setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
	}
	public void setNumeroProcesso(int numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}
	public void setCepObra(String cepObra) {
		this.cepObra = cepObra;
	}
	public void setEnderecoObra(String enderecoObra) {
		this.enderecoObra = enderecoObra;
	}
	public void setNumeroObra(String numeroObra) {
		this.numeroObra = numeroObra;
	}
	public void setBairroObra(String bairroObra) {
		this.bairroObra = bairroObra;
	}
	public void setComplementoObra(String complementoObra) {
		this.complementoObra = complementoObra;
	}
	public void setUfObra(Uf ufObra) {
		this.ufObra = ufObra;
	}
	public void setMunicipioObra(Municipio municipioObra) {
		this.municipioObra = municipioObra;
	}
	public void setCaracteristicaObra(String caracteristicaObra) {
		this.caracteristicaObra = caracteristicaObra;
	}
	public void setMateriaisObra(String materiaisObra) {
		this.materiaisObra = materiaisObra;
	}
	public void setDescricaoDestino(String descricaoDestino) {
		this.descricaoDestino = descricaoDestino;
	}
	public void setGruposTipoResiduoProjeto(
			List<GrupoTipoResiduoProjeto> gruposTipoResiduoProjeto) {
		this.gruposTipoResiduoProjeto = gruposTipoResiduoProjeto;
	}
	public void setIniciativaAbsorcao(String iniciativaAbsorcao) {
		this.iniciativaAbsorcao = iniciativaAbsorcao;
	}
	public void setIniciativaAcondicionamento(String iniciativaAcondicionamento) {
		this.iniciativaAcondicionamento = iniciativaAcondicionamento;
	}
	public void setIniciativaMinimizacao(String iniciativaMinimizacao) {
		this.iniciativaMinimizacao = iniciativaMinimizacao;
	}
	public void setTransportadoresProjeto(
			List<TransportadorProjeto> transportadoresProjeto) {
		this.transportadoresProjeto = transportadoresProjeto;
	}
	public void setResponsaveisProjeto(
			List<ResponsavelProjeto> responsaveisProjeto) {
		this.responsaveisProjeto = responsaveisProjeto;
	}
	public void setTransportadores(List<Transportador> transportadores) {
		this.transportadores = transportadores;
	}
	public void setGruposTipoResiduo(List<GrupoTipoResiduo> gruposTipoResiduo) {
		this.gruposTipoResiduo = gruposTipoResiduo;
	}
	public void setResponsaveis(List<Responsavel> responsaveis) {
		this.responsaveis = responsaveis;
	}
	public void addTransportadores(Transportador transportador) {
		if(this.transportadores == null)
			this.transportadores = new ArrayList<Transportador>();
		this.transportadores.add(transportador);
	}
	public void addGruposTipoResiduo(GrupoTipoResiduo grupoTipoResiduo) {
		if(this.gruposTipoResiduo == null)
			this.gruposTipoResiduo = new ArrayList<GrupoTipoResiduo>();
		this.gruposTipoResiduo.add(grupoTipoResiduo);
	}
	public void addResponsaveis(Responsavel responsavel) {
		if(this.responsaveis == null)
			this.responsaveis = new ArrayList<Responsavel>();
		this.responsaveis.add(responsavel);
	}
	public void setEnumStatusProjeto(EnumStatusProjeto enumStatusProjeto) {
		this.enumStatusProjeto = enumStatusProjeto;
	}
	public void setGrandeGerador(GrandeGerador grandeGerador) {
		this.grandeGerador = grandeGerador;
	}
	
	@Transient
	public String getEnderecoObraCompleto() {
		String enderecoObraCompleto = getEnderecoObra()==null? "":"Rua "+getEnderecoObra();
		enderecoObraCompleto += getComplementoObra()==null || getComplementoObra().equals("")?"":", "+getComplementoObra();
		enderecoObraCompleto += getNumeroObra()==null || getNumeroObra().equals("")? "": ", "+getNumeroObra()+" ";
		enderecoObraCompleto += getBairroObra()==null|| getBairroObra().equals("") ?"":", "+getBairroObra()+" ";
		enderecoObraCompleto += getMunicipioObra()==null?"":", "+getMunicipioObra().getNome()+" ";
		enderecoObraCompleto += getUfObra()==null?"":"-"+getUfObra().getSigla();
		
		return enderecoObraCompleto;
	}
	
	public void setEnderecoObraCompleto(String enderecoObraCompleto) {
		this.enderecoObraCompleto = enderecoObraCompleto;
	}
	
}