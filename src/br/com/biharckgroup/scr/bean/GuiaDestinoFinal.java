package br.com.biharckgroup.scr.bean;

import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="guia")
public class GuiaDestinoFinal extends BeanAuditoria  {
    
	
	private String numeroGuia;
	/*Endere�o Obra*/
	private String cepObra;
	private String enderecoObra;
	private String numeroObra;
	private String bairroObra;
	private String responsavelObra;
	private String complementoObra;
	private Uf ufObra;
	private Municipio municipioObra;
	/*Fim Endere�o Obra*/
	/*Transporte*/
	private TipoTransporte tipoTransporte;
	private String identificacaoTransporte;
	private String placa;
	private Date dataRetirada;
	private Date dataEnvio;
	private Date dataEntrega;
	private Date dataChegada;
	/*Fim Transporte*/
	private Transportador transportador; 
	private GrandeGerador grandeGerador;
	private List<ResiduoGuia> residuosGuia;
	private DestinoFinal destinoFinal;
	private String situacao; //ACEITA 1 NAOACEITA 0 SEMSITUACAO null
	private boolean status;
	private EnumSituacao enumSituacao;
	private EnumIdentificaGerador enumIdentificaGerador;
	private Projeto projeto;
	private EnumStatus enumStatus;
	private boolean cancelada;
	
	@Transient
	@DisplayName("Situa��o")
	public EnumSituacao getEnumSituacao() {
		return enumSituacao;
	}
	@Required
	@Transient
	@DisplayName("Identificar Gerador?")
	public EnumIdentificaGerador getEnumIdentificaGerador() {
		return enumIdentificaGerador;
	}
	
	
	//GET
	
	@DisplayName("N�mero da Guia")
	@DescriptionProperty
	public String getNumeroGuia() {
		return numeroGuia;
	}
	@MaxLength(value=100)
	@DisplayName("Bairro")
	public String getBairroObra() {
		return bairroObra;
	}
	@DisplayName("Cep")
	public String getCepObra() {
		return cepObra;
	}
	@MaxLength(value=100)
	@DisplayName("Complemento")
	public String getComplementoObra() {
		return complementoObra;
	}
	@MaxLength(value=255)
	@DisplayName("Endere�o")
	public String getEnderecoObra() {
		return enderecoObra;
	}
	@ManyToOne
	@ForeignKey(name="fk_guia_municipio")
	@DisplayName("Munic�pio")
	public Municipio getMunicipioObra() {
		return municipioObra;
	}
	@MaxLength(value=10)
	@DisplayName("N�mero")
	public String getNumeroObra() {
		return numeroObra;
	}
	@DisplayName("Respons�vel")
	@MaxLength(value=50)
	public String getResponsavelObra() {
		return responsavelObra;
	}
	@Transient
	@DisplayName("UF")
	public Uf getUfObra() {
		return ufObra;
	}

	@Required
	@ManyToOne
	@ForeignKey(name="fk_guia_tipotransporte")
	@DisplayName("Tipo de Transporte")
	public TipoTransporte getTipoTransporte() {
		return tipoTransporte;
	}
	@DisplayName("Indentifica��o do Transporte ")
	@Required
	public String getIdentificacaoTransporte() {
		return identificacaoTransporte;
	}
	public String getPlaca() {
		return placa;
	}
	@DisplayName("Data de Sa�da do Coletor ")
	public Date getDataRetirada() {
		return dataRetirada;
	}
	@Required
	@DisplayName("Data de Envio do Coletor")
	public Date getDataEnvio() {
		return dataEnvio;
	}
	@DisplayName("Data de Recebimento do Coletor ")
	public Date getDataEntrega() {
		return dataEntrega;
	}
	@DisplayName("Data de Recebimento no Destino Final")
	public Date getDataChegada() {
		return dataChegada;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_guia_transportador")
	public Transportador getTransportador() {
		return transportador;
	}
	@ManyToOne
	@ForeignKey(name="fk_guia_grandegerador")
	public GrandeGerador getGrandeGerador() {
		return grandeGerador;
	}
	@OneToMany(mappedBy="guia")
	public List<ResiduoGuia> getResiduosGuia() {
		return residuosGuia;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_guia_destinofinal")
	public DestinoFinal getDestinoFinal() {
		return destinoFinal;
	}
	@DisplayName("Situa��o")
	public String getSituacao() {
		return situacao;
	}
	@DisplayName("Status")
	public boolean isStatus() {
		return status;
	}
	@ManyToOne
	@ForeignKey(name="fk_guia_projeto")
	@DisplayName("Projeto (PGRS)")
	public Projeto getProjeto() {
		return projeto;
	}		
	
	@Transient
	@DisplayName("Status")
	public EnumStatus getEnumStatus() {
		return enumStatus;
	}
	@DisplayName("Guia Cancelada")
	public boolean isCancelada() {
		return cancelada;
	}
	
	//SET

	public void setBairroObra(String bairroObra) {
		this.bairroObra = bairroObra;
	}
	public void setCepObra(String cepObra) {
		this.cepObra = cepObra;
	}
	public void setComplementoObra(String complementoObra) {
		this.complementoObra = complementoObra;
	}
	public void setEnderecoObra(String enderecoObra) {
		this.enderecoObra = enderecoObra;
	}
	public void setMunicipioObra(Municipio municipioObra) {
		this.municipioObra = municipioObra;
	}
	public void setNumeroObra(String numeroObra) {
		this.numeroObra = numeroObra;
	}
	public void setResponsavelObra(String responsavelObra) {
		this.responsavelObra = responsavelObra;
	}
	public void setUfObra(Uf ufObra) {
		this.ufObra = ufObra;
	}
	public void setTipoTransporte(TipoTransporte tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}
	public void setIdentificacaoTransporte(String identificacaoTransporte) {
		this.identificacaoTransporte = identificacaoTransporte;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setDataRetirada(Date dataRetirada) {
		this.dataRetirada = dataRetirada;
	}
	public void setDataEnvio(Date dataEnvio) {
		this.dataEnvio = dataEnvio;
	}
	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}
	public void setDataChegada(Date dataChegada) {
		this.dataChegada = dataChegada;
	}
	public void setTransportador(Transportador transportador) {
		this.transportador = transportador;
	}
	public void setGrandeGerador(GrandeGerador grandeGerador) {
		this.grandeGerador = grandeGerador;
	}
	public void setResiduosGuia(List<ResiduoGuia> residuosGuia) {
		this.residuosGuia = residuosGuia;
	}
	public void setDestinoFinal(DestinoFinal destinoFinal) {
		this.destinoFinal = destinoFinal;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public void setEnumSituacao(EnumSituacao enumSituacao) {
		this.enumSituacao = enumSituacao;
	}
	public void setEnumIdentificaGerador(
			EnumIdentificaGerador enumIdentificaGerador) {
		this.enumIdentificaGerador = enumIdentificaGerador;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setNumeroGuia(String numeroGuia) {
		this.numeroGuia = numeroGuia;
	}
	public void setEnumStatus(EnumStatus enumStatus) {
		this.enumStatus = enumStatus;
	}
	public void setCancelada(boolean cancelada) {
		this.cancelada = cancelada;
	}
}