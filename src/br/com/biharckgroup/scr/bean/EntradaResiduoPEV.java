package br.com.biharckgroup.scr.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Hora;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="entradaresiduopev")
@DisplayName("Requisi��o de Entrada de Res�duos no PEV ")
public class EntradaResiduoPEV extends BeanAuditoria  {
    
	private PontoEntrega pontoEntrega;
	private Guia guia;
	private Date data;
	private Hora hora;
	private String responsavel;
	private boolean transportador; // Transportador else Gerador
	private EnumAgenteEntradaResiduo agenteEntradaResiduo;
	private String bairroOrigem;
	private List<GrupoTipoResiduoEntradaResiduoPEV> grupoTipoResiduosEntradaResiduoPEV;
	private List<GrupoTipoResiduo> gruposTipoResiduo;

	@Required
	@ManyToOne
	@ForeignKey(name="fk_entradaresiduos_pev")
	@DisplayName("Credenciais PEV")
	public PontoEntrega getPontoEntrega() {
		return pontoEntrega;
	}
	@ManyToOne
	@ForeignKey(name="fk_entradaresiduos_guia")
	public Guia getGuia() {
		return guia;
	}
	@Required
	public Date getData() {
		return data;
	}
	public Hora getHora() {
		return hora;
	}
	@Required
	@DisplayName("Respons�vel pelo Res�duo")
	public String getResponsavel() {
		return responsavel;
	}
	public boolean isTransportador() {
		return transportador;
	}
	@Transient
	@Required
	public EnumAgenteEntradaResiduo getAgenteEntradaResiduo() {
		return agenteEntradaResiduo;
	}
	public String getBairroOrigem() {
		return bairroOrigem;
	}
	@OneToMany(mappedBy="entradaResiduoPEV")
	@Required
	public List<GrupoTipoResiduoEntradaResiduoPEV> getGrupoTipoResiduosEntradaResiduoPEV() {
		return grupoTipoResiduosEntradaResiduoPEV;
	}
	@Transient
	public List<GrupoTipoResiduo> getGruposTipoResiduo() {
		return gruposTipoResiduo;
	}
	
	//set
	public void setPontoEntrega(PontoEntrega pontoEntrega) {
		this.pontoEntrega = pontoEntrega;
	}
	public void setGuia(Guia guia) {
		this.guia = guia;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setHora(Hora hora) {
		this.hora = hora;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public void setTransportador(boolean transportador) {
		this.transportador = transportador;
	}
	public void setAgenteEntradaResiduo(
			EnumAgenteEntradaResiduo agenteEntradaResiduo) {
		this.agenteEntradaResiduo = agenteEntradaResiduo;
	}
	public void setBairroOrigem(String bairroOrigem) {
		this.bairroOrigem = bairroOrigem;
	}
	public void setGrupoTipoResiduosEntradaResiduoPEV(List<GrupoTipoResiduoEntradaResiduoPEV> grupoTipoResiduosEntradaResiduoPEV) {
		this.grupoTipoResiduosEntradaResiduoPEV = grupoTipoResiduosEntradaResiduoPEV;
	}
	public void setGruposTipoResiduo(List<GrupoTipoResiduo> gruposTipoResiduo) {
		this.gruposTipoResiduo = gruposTipoResiduo;
	}
	public void addGruposTipoResiduo(GrupoTipoResiduo grupoTipoResiduo) {
		if(this.gruposTipoResiduo == null)
			this.gruposTipoResiduo = new ArrayList<GrupoTipoResiduo>();
		this.gruposTipoResiduo.add(grupoTipoResiduo);
	}
}