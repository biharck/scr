package br.com.biharckgroup.scr.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="tipodestino")
@DisplayName("Tipos de Destino")
public class TipoDestino extends BeanAuditoria  {
    
	private boolean ativo;
	private String nome;

	
	public boolean isAtivo() {
		return ativo;
	}
	@DescriptionProperty
	@MaxLength(value=50)
	@Required
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
}