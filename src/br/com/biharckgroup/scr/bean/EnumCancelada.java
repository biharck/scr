package br.com.biharckgroup.scr.bean;

public enum EnumCancelada {

	SIM("Sim",'S',true),
	NAO("N�o", 'N',false)
	;
	
	private EnumCancelada(String nome, char opcao,boolean optBool){
		this.nome = nome;
		this.opcao = opcao;
		this.optBool = optBool;
	}
	
	private String nome;
	private char opcao;
	private boolean optBool;
	
	public String getNome() {
		return nome;
	}
	public char getOpcao() {
		return opcao;
	}
	public boolean isOptBool() {
		return optBool;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
	
}
