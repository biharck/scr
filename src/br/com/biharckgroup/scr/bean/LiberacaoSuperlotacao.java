package br.com.biharckgroup.scr.bean;

import java.util.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;

public class LiberacaoSuperlotacao {

	private DestinoFinal destinoFinal;
	private String responsavel;
	private Date data;
	private double capacidadeLiberada;
	private String motivo;
	
	
	public LiberacaoSuperlotacao(){}
	public LiberacaoSuperlotacao(DestinoFinal destinoFinal, String responsavel, Date data, double capacidadeLiberada, String motivo){
		this.destinoFinal=destinoFinal;
		this.responsavel=responsavel;
		this.data=data;
		this.capacidadeLiberada=capacidadeLiberada;
		this.motivo=motivo;
	}
	@Required
	public DestinoFinal getDestinoFinal() {
		return destinoFinal;
	}
	@Required
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		return responsavel;
	}
	@DisplayName("Data Libera��o")
	public Date getData() {
		return data;
	}
	@Required
	@DisplayName("Capacidade Liberada (m�)")
	public double getCapacidadeLiberada() {
		return capacidadeLiberada;
	}
	@Required
	public String getMotivo() {
		return motivo;
	}
	
	//set
	public void setDestinoFinal(DestinoFinal destinoFinal) {
		this.destinoFinal = destinoFinal;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setCapacidadeLiberada(double capacidadeLiberada) {
		this.capacidadeLiberada = capacidadeLiberada;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
}
