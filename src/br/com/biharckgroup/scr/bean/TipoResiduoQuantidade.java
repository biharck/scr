package br.com.biharckgroup.scr.bean;




public class TipoResiduoQuantidade {
    
	private TipoResiduo tipoResiduo;
	private float quantidade;

	public TipoResiduoQuantidade() {
	}
	
	
	public TipoResiduoQuantidade(TipoResiduo tipoResiduo, float quantidade) {
		super();
		this.tipoResiduo = tipoResiduo;
		this.quantidade = quantidade;
	}

	public void addQuantidade(float quantidade) {
		this.quantidade += quantidade;
	}
	public void setQuantidade(float quantidade) {
		this.quantidade = quantidade;
	}
	public void setTipoResiduo(TipoResiduo tipoResiduo) {
		this.tipoResiduo = tipoResiduo;
	}
	public float getQuantidade() {
		return quantidade;
	}
	public TipoResiduo getTipoResiduo() {
		return tipoResiduo;
	}

}