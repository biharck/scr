package br.com.biharckgroup.scr.bean;


import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="tiporesiduo")
@DisplayName("Tipos de Res�duo")
public class TipoResiduo extends BeanAuditoria  {
    
	private String descricao;
	private boolean ativo;
	private boolean reciclavel;
	private GrupoTipoResiduo grupoTipoResiduo;
	private ClasseTipoResiduo classeTipoResiduo;
	
	
	@MaxLength(value=200)
	@Required
	@DisplayName("Descri��o")
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public boolean isAtivo() {
		return ativo;
	}
	@DisplayName("Recicl�vel")
	public boolean isReciclavel() {
		return reciclavel;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_tipores_grupo")
	@DisplayName("Grupos de Tipos de Res�duo")
	public GrupoTipoResiduo getGrupoTipoResiduo() {
		return grupoTipoResiduo;
	}
	@ManyToOne
	@ForeignKey(name="fk_tipores_classe")
	@DisplayName("Classes de Tipos de Res�duo")
	public ClasseTipoResiduo getClasseTipoResiduo() {
		return classeTipoResiduo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setReciclavel(boolean reciclavel) {
		this.reciclavel = reciclavel;
	}
	public void setGrupoTipoResiduo(GrupoTipoResiduo grupoTipoResiduo) {
		this.grupoTipoResiduo = grupoTipoResiduo;
	}
	public void setClasseTipoResiduo(ClasseTipoResiduo classeTipoResiduo) {
		this.classeTipoResiduo = classeTipoResiduo;
	}
}