package br.com.biharckgroup.scr.bean;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.Email;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Table(name="pontoentregavoluntaria")
@Entity
public class PontoEntrega extends BeanAuditoria {
	
	private String identificacao;
	private boolean ativo;
	private String nome;
	private String cep;
	private String endereco;
	private String numero;
	private String complemento;
	private String bairro;
	private Uf uf;
	private Municipio municipio;
	private String responsavel;
	private String email;
	private String reEmail;
	private String observacoes;
	private Telefone telefone1;
	private Telefone telefone2;
	private Telefone celular;
	private String enredecoCompleto;
	
	public PontoEntrega(){}
	public PontoEntrega(Integer id){this.id = id;}
	
	@Required
	@DescriptionProperty
	@MaxLength(value=100)
	@DisplayName("Identificação")
	public String getIdentificacao() {
		return identificacao;
	}
	public boolean isAtivo() {
		return ativo;
	}
	@Required
	@MaxLength(value=100)
	public String getNome() {
		return nome;
	}
	public String getCep() {
		return cep;
	}
	@Required
	@MaxLength(value=255)
	@DisplayName("Endereço")
	public String getEndereco() {
		return endereco;
	}
	@MaxLength(value=10)
	@DisplayName("Número")
	public String getNumero() {
		return numero;
	}
	@MaxLength(value=50)
	public String getComplemento() {
		return complemento;
	}
	@Required
	@MaxLength(value=50)
	public String getBairro() {
		return bairro;
	}
	@Transient
	@Required
	@DisplayName("UF")
	public Uf getUf() {
		return uf;
	}
	@Required
	@ManyToOne
	@DisplayName("Município")
	@ForeignKey(name="fk_pontoent_mun")
	public Municipio getMunicipio() {
		return municipio;
	}
	@Required
	@MaxLength(value=50)
	@DisplayName("Responsável")
	public String getResponsavel() {
		return responsavel;
	}
	@Required
	@Email
	@DisplayName("E-mail")
	@MaxLength(value = 200)
	public String getEmail() {
		return email;
	}
	@Required
	@Email
	@DisplayName("Confirmação E-mail")
	@Transient
	public String getReEmail() {
		return reEmail;
	}
	@MaxLength(value=255)
	@DisplayName("Observações")
	public String getObservacoes() {
		return observacoes;
	}
	@Required
	@DisplayName("Telefone Principal")
	public Telefone getTelefone1() {
		return telefone1;
	}
	@DisplayName("Telefone Alternativo")
	public Telefone getTelefone2() {
		return telefone2;
	}
	public Telefone getCelular() {
		return celular;
	}
	
	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setReEmail(String reEmail) {
		this.reEmail = reEmail;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	public void setTelefone1(Telefone telefone1) {
		this.telefone1 = telefone1;
	}
	public void setTelefone2(Telefone telefone2) {
		this.telefone2 = telefone2;
	}
	public void setCelular(Telefone celular) {
		this.celular = celular;
	}
	
	@Transient
	public String getenderecoCompleto() {
		String enderecoCompleto = getEndereco()==null? "":"Rua "+getEndereco();
		enderecoCompleto += getComplemento()==null || getComplemento().equals("")?"":", "+getComplemento();
		enderecoCompleto += getNumero()==null || getNumero().equals("")? "": ", "+getNumero()+" ";
		enderecoCompleto += getBairro()==null|| getBairro().equals("") ?"":", "+getBairro()+" ";
		enderecoCompleto += getMunicipio()==null?"":", "+getMunicipio().getNome()+" ";
		enderecoCompleto += getUf()==null?"":"-"+getUf().getSigla();
		
		return enderecoCompleto;
	}
	
	public void setEnderecoCompleto(String enderecoCompleto) {
		this.enredecoCompleto = enderecoCompleto;
	}
}
