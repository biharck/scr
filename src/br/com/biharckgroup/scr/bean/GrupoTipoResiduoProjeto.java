package br.com.biharckgroup.scr.bean;


import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="grupotiporesiduoprojeto")
public class GrupoTipoResiduoProjeto extends BeanAuditoria  {
    
	private Projeto Projeto;
	private GrupoTipoResiduo grupoTipoResiduo;
	private TipoResiduo tipoResiduo;
	private float quantidade; 
	private UnidadeMedida unidadeMedida;
	private DestinoFinal destinoFinal;
	
	@Required
	@ManyToOne
	@ForeignKey(name="fk_grupotipoentrada_projeto")
	@DisplayName("Projeto - PGRCC")
	public Projeto getProjeto() {
		return Projeto;
	}
	
	@Required
	@ManyToOne
	@ForeignKey(name="fk_grupotipoentrada_grupotiporesiduo")
	@DisplayName("Grupos de Tipos de Res�duo")
	public GrupoTipoResiduo getGrupoTipoResiduo() {
		return grupoTipoResiduo;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_grupotipoentrada_tiporesiduo")
	@DisplayName("Tipos de Res�duo")
	public TipoResiduo getTipoResiduo() {
		return tipoResiduo;
	}
	@Required
	public float getQuantidade() {
		return quantidade;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_grupotipoentrada_unidade")
	@DisplayName("Unidade de Medida")
	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_grupotipoentrada_destinoFinal")
	public DestinoFinal getDestinoFinal() {
		return destinoFinal;
	}
	
	public void setProjeto(Projeto projeto) {
		Projeto = projeto;
	}
	public void setGrupoTipoResiduo(GrupoTipoResiduo grupoTipoResiduo) {
		this.grupoTipoResiduo = grupoTipoResiduo;
	}
	public void setTipoResiduo(TipoResiduo tipoResiduo) {
		this.tipoResiduo = tipoResiduo;
	}
	public void setQuantidade(float quantidade) {
		this.quantidade = quantidade;
	}
	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	public void setDestinoFinal(DestinoFinal destinoFinal) {
		this.destinoFinal = destinoFinal;
	}
	
}