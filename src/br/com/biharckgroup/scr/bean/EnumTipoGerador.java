package br.com.biharckgroup.scr.bean;

public enum EnumTipoGerador {
	
	GRANDE_GERADOR("Grande Gerador",true),
	PEQUENO_GERADOR("Pequeno Gerador",false)
	;
	
	private EnumTipoGerador(String nome,boolean optBool){
		this.nome = nome;
		this.optBool = optBool;
	}
	
	private String nome;
	private boolean optBool;
	
	public String getNome() {
		return nome;
	}
	public boolean isOptBool() {
		return optBool;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
}
