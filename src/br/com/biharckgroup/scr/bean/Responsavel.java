package br.com.biharckgroup.scr.bean;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.Email;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="responsavel")
@DisplayName("Respons�vel")
public class Responsavel extends BeanAuditoria  {
    
	private String cnpj;
	private String cpf;
	private String nome;
	private String cep;
	private String endereco;
	private String numero;
	private String bairro;
	private String complemento;
	private Telefone telefone1;
	private Telefone celular;
	private Uf uf;
	private Municipio municipio;
	private String cpfCnpjTransient;
	private String crea;
	private GrandeGerador grandeGerador;
	private String email;
	private String reEmail;
	
	
	public Responsavel(){
	}

	public Responsavel(Integer id) {
		this.id=id;
	}
	//transient
	@Transient
	@DisplayName("CPF/CNPJ")
	@Required
	public String getCpfCnpjTransient() {
		return cpfCnpjTransient;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getCpf() {
		return cpf;
	}
	public String getCep() {
		return cep;
	}
	@Required
	@MaxLength(value=255)
	@DisplayName("Endere�o")
	public String getEndereco() {
		return endereco;
	}
	@MaxLength(value=10)
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	@Required
	@MaxLength(value=100)
	public String getBairro() {
		return bairro;
	}
	@Required
	@DisplayName("Telefone Principal")
	public Telefone getTelefone1() {
		return telefone1;
	}
	public Telefone getCelular() {
		return celular;
	}
	@DescriptionProperty
	@MaxLength(value=50)
	@Required
	public String getNome() {
		return nome;
	}
	@MaxLength(value=100)
	public String getComplemento() {
		return complemento;
	}
	@Transient
	@Required
	@DisplayName("UF")
	public Uf getUf() {
		return uf;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_responsavel_mun")
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@DisplayName("CREA")
	public String getCrea() {
		return crea;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_responsavel_gg")
	public GrandeGerador getGrandeGerador() {
		return grandeGerador;
	}
	@Email
	@Required
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	@Required
	@Email
	@Transient
	@DisplayName("Confirma��o do E-mail")
	public String getReEmail() {
		return reEmail;
	}
	
	public void setCpfCnpjTransient(String cpfCnpjTransient) {
		this.cpfCnpjTransient = cpfCnpjTransient;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setTelefone1(Telefone telefone1) {
		this.telefone1 = telefone1;
	}
	public void setCelular(Telefone celular) {
		this.celular = celular;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setCrea(String crea) {
		this.crea = crea;
	}
	public void setGrandeGerador(GrandeGerador grandeGerador) {
		this.grandeGerador = grandeGerador;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setReEmail(String reEmail) {
		this.reEmail = reEmail;
	}
}