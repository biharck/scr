package br.com.biharckgroup.scr.bean;

import java.util.Date;


public class ResiduoRecebidoQuantidade  {
	
	private Date data;
	private float [] vetorQuantidade;
	
	public ResiduoRecebidoQuantidade(Date data, float[] vetorQuantidade) {
		this.data = data;
		this.vetorQuantidade = vetorQuantidade;
	}

	public float[] getVetorQuantidade() {
		return vetorQuantidade;
	}
	public void setVetorQuantidade(float[] vetorQuantidade) {
		this.vetorQuantidade = vetorQuantidade;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
}