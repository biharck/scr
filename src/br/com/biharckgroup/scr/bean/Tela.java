package br.com.biharckgroup.scr.bean;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="tela")
@DisplayName("Cadastro de Telas do Sistema")
public class Tela extends BeanAuditoria{
	
	private String path;
	private String descricao;
	
	//GET
	@DescriptionProperty
	@Required
	@MaxLength(value=200)
	public String getPath() {
		return path;
	}
	@MaxLength(value=200)
	@Required
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	//SET
	public void setPath(String path) {
		this.path = path;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
