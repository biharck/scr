package br.com.biharckgroup.scr.bean;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="responsavelprojeto")
public class ResponsavelProjeto extends BeanAuditoria  {
    
	private Responsavel responsavel;
	private Projeto projeto;
	
	@Required
	@ManyToOne
	@ForeignKey(name="fk_responsavelProjeto_responsavel")
	@DisplayName("Responsável")
	public Responsavel getResponsavel() {
		return responsavel;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_responsavelProjeto_projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	public void setResponsavel(Responsavel responsavel) {
		this.responsavel = responsavel;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
}