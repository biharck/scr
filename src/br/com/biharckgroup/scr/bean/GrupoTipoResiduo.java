package br.com.biharckgroup.scr.bean;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="grupotiporesiduo")
@DisplayName("Grupos de Tipos de Res�duos")
public class GrupoTipoResiduo extends BeanAuditoria  {
    
	
	private String nome;
	private boolean ativo;
	private List<ClasseTipoResiduo> classeTipoResiduo;
	
	
	public GrupoTipoResiduo(){}
	public GrupoTipoResiduo(Integer id){this.id = id;}
	
	@Required
	@DescriptionProperty
	@MaxLength(value=100)
	public String getNome() {
		return nome;
	}
	public boolean isAtivo() {
		return ativo;
	}
	@OneToMany(mappedBy="grupoTipoResiduo")
	@DisplayName("Classes de Tipos de Res�duo")
	public List<ClasseTipoResiduo> getClasseTipoResiduo() {
		return classeTipoResiduo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setClasseTipoResiduo(List<ClasseTipoResiduo> classeTipoResiduo) {
		this.classeTipoResiduo = classeTipoResiduo;
	}
	
	
	
	
}