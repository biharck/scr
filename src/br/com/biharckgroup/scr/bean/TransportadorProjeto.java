package br.com.biharckgroup.scr.bean;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name="transportadorprojeto")
public class TransportadorProjeto extends BeanAuditoria  {
    
	private Transportador transportador;
	private Projeto projeto;
	
	public TransportadorProjeto(){
	}

	@Required
	@ManyToOne
	@ForeignKey(name="fk_transportadorprojeto_transp")
	public Transportador getTransportador() {
		return transportador;
	}
	@Required
	@ManyToOne
	@ForeignKey(name="fk_transportadorprojeto_projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	public void setTransportador(Transportador transportador) {
		this.transportador = transportador;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
}