package br.com.biharckgroup.scr.bean;

public enum EnumSituacaoRTR {

	//situa��o doc:  relat�rio transporte de res�duo
	APROVADO("Aprovado",'S',"1"),
	REPROVADO("Reprovado",'N',"0"),
	SEMSITUACAO("Sem Situa��o", ' ',"NULL") //null
	;
	
	private EnumSituacaoRTR(String nome, char opcao,String valor){
		this.nome = nome;
		this.opcao = opcao;
		this.valor = valor;
	}
	
	private String nome;
	private char opcao;
	private String valor;
	
	public String getNome() {
		return nome;
	}
	public char getOpcao() {
		return opcao;
	}
	public String getValor() {
		if(valor.equals("NULL"))
			return null;
		return valor;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
	
	public static EnumSituacaoRTR getEnumSituacaoByValor(String valor){
		if(valor == null)
			return EnumSituacaoRTR.SEMSITUACAO;
			
		if(valor.equals(EnumSituacaoRTR.APROVADO.valor))
			return EnumSituacaoRTR.APROVADO;
		else
			return EnumSituacaoRTR.REPROVADO;
	}
}
