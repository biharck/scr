
package br.com.biharckgroup.scr.util;

public class PathUtil {
	public static final String AFTER_LOGIN_GO_TO = "/adm";
	public static final String AFTER_ALTERAR_SENHA_GO_TO = "/adm/process/AlteraSenha";
	
	public static final String DOMINIO = "http://www.orionxhost.com.br";
	public static final String CAMINHO_SISTEMA = DOMINIO+"/SCR";
	
	public static final String EMAILTESTE = "fernandacaroline2008@gmail.com";//;biharck@gmail.com";
	public static final String URL_EMPRESA = "http://www.orionx.com.br";
	public static final String URL_LOGO = "http://www.orionx.com.br/images/logo.png";
	public static final String LOGO_RELATORIO="/img/logoRelatorio.png";

	//Teste
	public static final String DOMINIO_TESTE = "http://localhost:8080";
}
