package br.com.biharckgroup.scr.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class TagFunctionsSCR {
	
	/**
	 * Fun��o respons�vel em representar a tld de truncar um texto
	 * @param texto o texto a ser truncado
	 * @param size tamanho m�ximo do campo
	 * @return texto formatado
	 */
	public static String trunc(String texto, Integer size) {
		if (texto != null && texto.length() >= size)
			texto = texto.substring(0, size) + " ...";
		return texto;
	}

	/**
	 * <p>Fun��o respons�vel em formatar a data no padr�o brasileiro dd/MM/yyyy HH:mm
	 * @param data data a ser formatada
	 * @return data formatada
	 */
	public static String dateFormat(Timestamp data) {
		if (data != null) {
			return new SimpleDateFormat("dd/MM/yyyy").format(data);
		} else {
			return "";
		}
	}

	/**
	 * <p>Fun��o respons�vel em formatar a hora no padr�o brasileiro  HH:mm
	 * @param data data a ser formatada
	 * @return hora formatada
	 */
	public static String hourFormat(Timestamp data) {
		if (data != null) {
			return new SimpleDateFormat("HH:mm:ss").format(data);
		} else {
			return "";
		}
	}
}
