package br.com.biharckgroup.scr.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EmailUtil {
	private String remetente;
	private String login;
	private String senha;
	private String smtp;
	private String porta;
	private String autenticacao;
	private String destinatario;
	private String assunto;
	private String corpo;
	private File anexo;
	protected Multipart multipart;
	protected MimeMessage message;
	

	public EmailUtil(String remetente, String login, String senha, String smtp,
			String porta, String autenticacao, String destinatario,
			String assunto, String corpo) {
		this.remetente = remetente;
		this.login = login;
		this.senha = senha;
		this.smtp = smtp;
		this.porta = porta;
		this.autenticacao = autenticacao;
		this.destinatario = destinatario;
		this.assunto = assunto;
		this.corpo = corpo;		
	}
	
	public EmailUtil(String remetente, String login, String senha, String smtp,
			String porta, String autenticacao, String destinatario,
			String assunto, String corpo,File anexo) {
		this.remetente = remetente;
		this.login = login;
		this.senha = senha;
		this.smtp = smtp;
		this.porta = porta;
		this.autenticacao = autenticacao;
		this.destinatario = destinatario;
		this.assunto = assunto;
		this.corpo = corpo;		
		this.anexo = anexo;
	}
	public EmailUtil(){
	}

	public String getAssunto() {
		return assunto;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public String getCorpo() {
		return corpo;
	}
	public void setCorpo(String corpo) {
		this.corpo = corpo;
	}
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getRemetente() {
		return remetente;
	}
	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getSmtp() {
		return smtp;
	}
	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}
	public String getPorta() {
		return porta;
	}
	public void setPorta(String porta) {
		this.porta = porta;
	}
	public String getAutenticacao() {
		return autenticacao;
	}
	public void setAutenticacao(String autenticacao) {
		this.autenticacao = autenticacao;
	}
	public File getAnexo() {
		return anexo;
	}
	public void setAnexo(File anexo) {
		this.anexo = anexo;
	}
	
	public static void enviar(String remetente, String login, String senha,String smtp, String porta, String autenticacao,String destinatario, String assunto, String corpo)
			throws AddressException, MessagingException {
		new EmailUtil(remetente, login, senha, smtp, porta, autenticacao,
				destinatario, assunto, corpo).enviar();
	}
	
	public void addPart(MimeBodyPart part) throws Exception {
//		multipart = new MimeMultipart();
		multipart.addBodyPart(part);
	}
	
	public EmailUtil addHtmlText(String text) throws Exception {
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart = new MimeBodyPart();
		messageBodyPart.setText(text);
		messageBodyPart.setHeader("Content-Type", "text/html");	
		addPart(messageBodyPart);
		return this;
	}

	public void enviar() throws MessagingException {
		
		ArrayList<String> listEmail = new ArrayList<String>();
		StringTokenizer email = new StringTokenizer(getDestinatario(),";");
		while (email.hasMoreTokens()) {
			listEmail.add(email.nextToken());
		}
		InternetAddress[] addressTo = new InternetAddress[listEmail.size()];
		for (int i = 0; i < listEmail.size(); i++){
			addressTo[i] = new InternetAddress(listEmail.get(i).toString()) ;
		}
		
		Properties p = new Properties();
		p.put("mail.smtp.host", smtp);
		p.put("mail.smtp.port", porta);
		p.put("mail.smtp.auth", autenticacao);
		p.put("mail.smtp.starttls.enable","true");
		Authenticator auth = new UsuarioSenhaEmail(login, senha);
		Session s = Session.getInstance(p, auth);

		Message message = new MimeMessage(s);
		InternetAddress from = new InternetAddress(remetente);
		message.setFrom(from);

		//Estipula para quem ser� enviado  
		message.setRecipients(Message.RecipientType.TO, addressTo);
		message.setSubject(assunto);
		
		MimeMultipart mpRoot = new MimeMultipart("mixed");
		MimeMultipart mpContent = new MimeMultipart("alternative");
		MimeBodyPart contentPartRoot = new MimeBodyPart();
		contentPartRoot.setContent(mpContent);
		mpRoot.addBodyPart(contentPartRoot);

		//enviando html 
		MimeBodyPart mbp2 = new MimeBodyPart();
		mbp2 = new MimeBodyPart();
		mbp2.setText(corpo);
		mbp2.setHeader("Content-Type", "text/html");	
		mpContent.addBodyPart(mbp2);

		//enviando anexo 
		if(anexo !=null){
			String caminhoAnexo = anexo.getAbsolutePath();
			String nomeAnexo = anexo.getName();
			MimeBodyPart mbp3 = new MimeBodyPart();
			DataSource fds = new FileDataSource(caminhoAnexo);
			mbp3.setDisposition(Part.ATTACHMENT);
			mbp3.setDataHandler(new DataHandler(fds));
			mbp3.setFileName(nomeAnexo);
			mpRoot.addBodyPart(mbp3);
		}


		message.setContent(mpRoot);
		message.saveChanges();

		Transport.send(message);
	}
}
