package br.com.biharckgroup.scr.adm.filtro;



import java.util.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.biharckgroup.scr.bean.PontoEntrega;


public class EntradaResiduoPEVFiltro extends FiltroListagem {

	private PontoEntrega pontoEntrega;
	private Integer idGuia; //id(n�mero da guia) 
	private Date dataIni;
	private Date dataFim;
	private String responsavel;
	
	@DisplayName("Credenciais PEV")
	public PontoEntrega getPontoEntrega() {
		return pontoEntrega;
	}
	@DisplayName("N�mero da Guia")
	public Integer getIdGuia() {
		return idGuia;
	}
	@DisplayName("Data Inicio")
	public Date getDataIni() {
		return dataIni;
	}
	@DisplayName("Data Fim")
	public Date getDataFim() {
		return dataFim;
	}
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		return responsavel;
	}
	
	//set
	public void setPontoEntrega(PontoEntrega pontoEntrega) {
		this.pontoEntrega = pontoEntrega;
	}
	public void setIdGuia(Integer idGuia) {
		this.idGuia = idGuia;
	}
	public void setDataIni(Date dataIni) {
		this.dataIni = dataIni;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	
	
}
