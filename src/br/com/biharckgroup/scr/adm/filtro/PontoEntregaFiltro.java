package br.com.biharckgroup.scr.adm.filtro;


import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;


public class PontoEntregaFiltro extends FiltroListagem {

	private String identificacao;
	private String responsavel;
	private String email;
	private String nome;
	
	@DisplayName("Identificação")
	public String getIdentificacao() {
		return identificacao;
	}
	@DisplayName("Responsável")
	public String getResponsavel() {
		return responsavel;
	}
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	public String getNome() {
		return nome;
	}
	
	
	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
