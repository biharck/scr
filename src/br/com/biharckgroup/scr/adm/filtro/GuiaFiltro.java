package br.com.biharckgroup.scr.adm.filtro;


import java.util.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.EnumCancelada;
import br.com.biharckgroup.scr.bean.EnumSituacao;
import br.com.biharckgroup.scr.bean.EnumStatus;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.Transportador;

public class GuiaFiltro extends FiltroListagem {

	private String numeroGuia;
	private String nomeFantasia; //grande gerador
	private String identificacaoTransporte;
	private String placa;
	private EnumStatus status;
	private EnumSituacao situacao;
	private DestinoFinal destinoFinal;
	private Transportador transportador;
	private String numeroProjeto;
	private GrandeGerador grandeGerador;
	private EnumCancelada enumCancelada;
	private Date dataCriacao;
	
	@DisplayName("N�mero da Guia")
	public String getNumeroGuia() {
		return numeroGuia;
	}
	@DisplayName("Nome Grande Gerador")
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	@DisplayName("Indentifica��o do Transporte ")
	public String getIdentificacaoTransporte() {
		return identificacaoTransporte;
	}
	public String getPlaca() {
		return placa;
	}
	public EnumStatus getStatus() {
		return status;
	}
	@DisplayName("Situa��o")
	public EnumSituacao getSituacao() {
		return situacao;
	}
	public DestinoFinal getDestinoFinal() {
		return destinoFinal;
	}
	public Transportador getTransportador() {
		return transportador;
	}
	@DisplayName("N�mero Projeto - PGRS")
	public String getNumeroProjeto() {
		return numeroProjeto;
	}
	public GrandeGerador getGrandeGerador() {
		return grandeGerador;
	}
	@DisplayName("Guia Cancelada")
	public EnumCancelada getEnumCancelada() {
		return enumCancelada;
	}
	@DisplayName("Data Cria��o")
	public Date getDataCriacao() {
		return dataCriacao;
	}
	
	public void setNumeroGuia(String numeroGuia) {
		this.numeroGuia = numeroGuia;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public void setIdentificacaoTransporte(String identificacaoTransporte) {
		this.identificacaoTransporte = identificacaoTransporte;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setStatus(EnumStatus status) {
		this.status = status;
	}
	public void setSituacao(EnumSituacao situacao) {
		this.situacao = situacao;
	}
	public void setDestinoFinal(DestinoFinal destinoFinal) {
		this.destinoFinal = destinoFinal;
	}
	public void setTransportador(Transportador transportador) {
		this.transportador = transportador;
	}
	public void setNumeroProjeto(String numeroProjeto) {
		this.numeroProjeto = numeroProjeto;
	}
	public void setGrandeGerador(GrandeGerador grandeGerador) {
		this.grandeGerador = grandeGerador;
	}
	public void setEnumCancelada(EnumCancelada enumCancelada) {
		this.enumCancelada = enumCancelada;
	}
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
}
