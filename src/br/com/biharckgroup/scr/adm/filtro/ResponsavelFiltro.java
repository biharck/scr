package br.com.biharckgroup.scr.adm.filtro;

import org.nextframework.controller.crud.FiltroListagem;

import br.com.biharckgroup.scr.bean.GrandeGerador;


public class ResponsavelFiltro extends FiltroListagem {

	private String nome;
	private GrandeGerador grandeGerador;
	
	public String getNome() {
		return nome;
	}
	public GrandeGerador getGrandeGerador() {
		return grandeGerador;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setGrandeGerador(GrandeGerador grandeGerador) {
		this.grandeGerador = grandeGerador;
	}
}
