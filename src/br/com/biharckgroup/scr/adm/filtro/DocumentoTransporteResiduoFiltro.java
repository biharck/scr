package br.com.biharckgroup.scr.adm.filtro;


import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.biharckgroup.scr.bean.EnumSituacaoRTR;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.Projeto;

public class DocumentoTransporteResiduoFiltro extends FiltroListagem {

	private String chave;
	private Projeto projeto;
	private EnumSituacaoRTR enumSituacao;
	private GrandeGerador grandeGerador;
	
	public String getChave() {
		return chave;
	}
	@DisplayName("Projeto (PGRCC)")
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Situa��o Documento")
	public EnumSituacaoRTR getEnumSituacao() {
		return enumSituacao;
	}
	public GrandeGerador getGrandeGerador() {
		return grandeGerador;
	}
	
	public void setChave(String chave) {
		this.chave = chave;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setEnumSituacao(EnumSituacaoRTR enumSituacao) {
		this.enumSituacao = enumSituacao;
	}
	public void setGrandeGerador(GrandeGerador grandeGerador) {
		this.grandeGerador = grandeGerador;
	}
	
}
