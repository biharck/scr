package br.com.biharckgroup.scr.adm.filtro;


import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;


public class GrandeGeradorFiltro extends FiltroListagem {

	private String razaoSocial;
	private String nomeFantasia;
	private String cpfCnpjTransient;
	
	@DisplayName("Raz�o Social")
	public String getRazaoSocial() {
		return razaoSocial;
	}
	@DisplayName("Nome Fantasia")
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	@DisplayName("CPF/CNPJ")
	public String getCpfCnpjTransient() {
		return cpfCnpjTransient;
	}
	
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public void setCpfCnpjTransient(String cpfCnpjTransient) {
		this.cpfCnpjTransient = cpfCnpjTransient;
	}
}
