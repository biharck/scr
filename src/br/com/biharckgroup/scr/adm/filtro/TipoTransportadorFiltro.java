package br.com.biharckgroup.scr.adm.filtro;


import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.biharckgroup.scr.bean.EnumAtivo;


public class TipoTransportadorFiltro extends FiltroListagem {

	private String nome;
	private EnumAtivo ativo;
	
	public String getNome() {
		return nome;
	}
	@DisplayName("Status")
	public EnumAtivo getAtivo() {
		return ativo;
	}
	
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivo(EnumAtivo ativo) {
		this.ativo = ativo;
	}
	
	
  
}
