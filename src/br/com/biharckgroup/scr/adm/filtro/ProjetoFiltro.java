package br.com.biharckgroup.scr.adm.filtro;


import java.util.Date;
import java.util.List;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.EnumStatusProjeto;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.bean.Responsavel;
import br.com.biharckgroup.scr.bean.Transportador;

public class ProjetoFiltro extends FiltroListagem {

	private String numeroProjeto;
	private List<Responsavel> responsaveis;
	private EnumStatusProjeto enumStatusProjeto;
	private GrandeGerador grandeGerador;
	private Transportador transportador;
	private DestinoFinal destinoFinal;
	private Date dataCriacao;

	@DisplayName("N�mero do Projeto")
	public String getNumeroProjeto() {
		return numeroProjeto;
	}
	@DisplayName("Respons�veis")
	public List<Responsavel> getResponsaveis() {
		return responsaveis;
	}
	@DisplayName("Status")
	public EnumStatusProjeto getEnumStatusProjeto() {
		return enumStatusProjeto;
	}
	@DisplayName("Data Cria��o")
	public Date getDataCriacao() {
		return dataCriacao;
	}
	public Transportador getTransportador() {
		return transportador;
	}
	public GrandeGerador getGrandeGerador() {
		return grandeGerador;
	}
	public DestinoFinal getDestinoFinal() {
		return destinoFinal;
	}
	public void setNumeroProjeto(String numeroProjeto) {
		this.numeroProjeto = numeroProjeto;
	}
	public void setResponsaveis(List<Responsavel> responsaveis) {
		this.responsaveis = responsaveis;
	}
	public void setEnumStatusProjeto(EnumStatusProjeto enumStatusProjeto) {
		this.enumStatusProjeto = enumStatusProjeto;
	}	
	public void setGrandeGerador(GrandeGerador grandeGerador) {
		this.grandeGerador = grandeGerador;
	}
	public void setTransportador(Transportador transportador) {
		this.transportador = transportador;
	}
	public void setDestinoFinal(DestinoFinal destinoFinal) {
		this.destinoFinal = destinoFinal;
	}
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
}
