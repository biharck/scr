package br.com.biharckgroup.scr.adm.filtro;

import org.nextframework.controller.crud.FiltroListagem;

public class PequenoGeradorFiltro extends FiltroListagem {
	private String nome;
	private String cpf;
	
	public String getNome() {
		return nome;
	}
	public String getCpf() {
		return cpf;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}
