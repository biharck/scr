package br.com.biharckgroup.scr.adm.filtro;


import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.validation.annotation.MaxLength;

import br.com.biharckgroup.scr.bean.TipoDestino;

public class DestinoFinalFiltro extends FiltroListagem {

	private String razaoSocial;
	private String nomeFantasia;
	private TipoDestino tipoDestino;
	private String cnpj;
	
	@DisplayName("Raz�o Social")
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	@DisplayName("Tipo de Destino")
	public TipoDestino getTipoDestino() {
		return tipoDestino;
	}
	@MaxLength(18)
	public String getCnpj() {
		return cnpj;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public void setTipoDestino(TipoDestino tipoDestino) {
		this.tipoDestino = tipoDestino;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
}
