package br.com.biharckgroup.scr.adm.filtro;



import java.util.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.biharckgroup.scr.bean.PontoEntrega;
import br.com.biharckgroup.scr.bean.Transportador;


public class RetiradaResiduoPEVFiltro extends FiltroListagem {

	private PontoEntrega pontoEntrega;
	private Transportador transportador; 
	private Date dataIni;
	private Date dataFim;
	private String responsavel;
	
	@DisplayName("Credenciais PEV")
	public PontoEntrega getPontoEntrega() {
		return pontoEntrega;
	}
	public Transportador getTransportador() {
		return transportador;
	}
	@DisplayName("Data Início")
	public Date getDataIni() {
		return dataIni;
	}
	@DisplayName("Data Fim")
	public Date getDataFim() {
		return dataFim;
	}
	@DisplayName("Responsável")
	public String getResponsavel() {
		return responsavel;
	}
	
	//set
	public void setPontoEntrega(PontoEntrega pontoEntrega) {
		this.pontoEntrega = pontoEntrega;
	}
	public void setTransportador(Transportador transportador) {
		this.transportador = transportador;
	}
	public void setDataIni(Date dataIni) {
		this.dataIni = dataIni;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	
	
}
