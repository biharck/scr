package br.com.biharckgroup.scr.adm.controller;

import java.util.Date;
import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduoProjeto;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.bean.ResiduoGuia;
import br.com.biharckgroup.scr.bean.Transportador;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.service.DestinoFinalService;
import br.com.biharckgroup.scr.service.GrandeGeradorService;
import br.com.biharckgroup.scr.service.GuiaService;
import br.com.biharckgroup.scr.service.PequenoGeradorService;
import br.com.biharckgroup.scr.service.ProjetoService;
import br.com.biharckgroup.scr.service.TransportadorService;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/adm",authorizationModule=ProcessAuthorizationModule.class)
public class IndexController extends MultiActionController {
	
	private DestinoFinalService destinoFinalService;
	private TransportadorService transportadorService;
	private GuiaService guiaService;
	private ProjetoService projetoService;
	
	public void setDestinoFinalService(DestinoFinalService destinoFinalService) {
		this.destinoFinalService = destinoFinalService;
	}
	public void setTransportadorService(TransportadorService transportadorService) {
		this.transportadorService = transportadorService;
	}
	public void setGuiaService(GuiaService guiaService) {
		this.guiaService = guiaService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	
	@DefaultAction
	public ModelAndView action(WebRequestContext request){
		//Identificar se � administrativo, se for obrigat�riamente
		//deve verificar se h� cadastros pendentes aprova��o
		if(SCRUtil.isPessoaLogadaAdministrativo())
			request.setAttribute("cadAguardandoAprov", existemCadastrosAguardandoAprovacao());
		
		Usuario usuario = SCRUtil.getUsuarioLogado();
		
		//caso usu�rio tenha papel de GG, verifica-se quant de guias geradas para  mesmo e n�o recebidas no destino final
		if(SCRUtil.isPessoaLogadaGrandeGerador()){
			if(usuario.getGrandeGerador() == null)
				throw new SCRException("N�o existe v�nculo do usu�rio logado com nenhum Grande Gerador.");
			int guiasGG = guiaService.getGuiasByGrandeGerador(usuario.getGrandeGerador(), false).size();
			request.setAttribute("nomeGG", usuario.getGrandeGerador().getNomeFantasia());
			request.setAttribute("guiasGG", guiasGG);
		}
		
		/*
		 * caso usu�rio tenha papel de Destino Final, verifica-se:
		 *  - total de guias n�o recebidas para o mesmo
		 *  - total de projetos citando o mesmo
		 *  - guias criadas no o dia atual
		 *  - projetos criados no o dia atual
		 */
		
		if(SCRUtil.isPessoaLogadaDestinoFinal()){
			if(usuario.getDestinoFinal() == null)
				throw new SCRException("N�o existe v�nculo do usu�rio logado com nenhum Destino Final.");
			int guiasTotDF = guiaService.getGuiasByDestinoFinal(usuario.getDestinoFinal(),false).size();
			int projetosTotDF = projetoService.getProjetosByDestinoFinal(usuario.getDestinoFinal()).size();
			request.setAttribute("guiasTotDF", guiasTotDF);
			request.setAttribute("projetosTotDF", projetosTotDF);
			
			List<Guia> listaGuiasDiaDF = guiaService.getGuiasDiaByDestinoFinal(usuario.getDestinoFinal());
			int guiasDiaDF = listaGuiasDiaDF.size();
			float somaGuiasDiaDF=(float) 0;
			for (Guia g: listaGuiasDiaDF) {
				for(ResiduoGuia rg: g.getResiduosGuia())
					somaGuiasDiaDF += rg.getQuantidade();
			}
			request.setAttribute("guiasDiaDF", guiasDiaDF);
			request.setAttribute("somaGuiasDiaDF", somaGuiasDiaDF);
			
			
			List<Projeto> listaProjetosDiaDF = projetoService.getProjetosDiaByDestinoFinal(usuario.getDestinoFinal());
			int projetosDiaDF = listaProjetosDiaDF.size();
			float somaProjetosDiaDF=(float) 0;
			for(Projeto p:listaProjetosDiaDF){
				for(GrupoTipoResiduoProjeto gp :p.getGruposTipoResiduoProjeto())
					somaProjetosDiaDF+=gp.getQuantidade();
			}
			request.setAttribute("projetosDiaDF", projetosDiaDF);
			request.setAttribute("somaProjetosDiaDF", somaProjetosDiaDF);
			
			request.setAttribute("nomeDF", usuario.getDestinoFinal().getNomeFantasia());
			request.setAttribute("dataDia", SCRUtil.getDataHora("dd/MM/yyyy", new Date()));	
		}
		
		return new ModelAndView("index");
	}
	
	/**
	 * <p>M�todo respons�vel em verificar se exitem cadastros aguardando aprova��o
	 * o m�todo foi desenvolvido para retornar se o primeiro j� tiver cadastro
	 * para melhor o tempo de resposta</p>
	 * 
	 * @return {@link Boolean} true se existem cadastros aguardando aprova��o
	 * 						   false caso contr�rio.
	 */
	public boolean existemCadastrosAguardandoAprovacao(){
		//somente usu�rios do tipo destino final e transportador precisam de aprova��o
		List<DestinoFinal> destinosFinais      = destinoFinalService.getDestinosAutoCadastro();
		if(destinosFinais!=null && destinosFinais.size()>0)
			return true;
		
//		List<GrandeGerador> grandesGeradores   = grandeGeradorService.getGrandesGeradoresAutoCadastro();
//		if(grandesGeradores!=null && grandesGeradores.size()>0)
//			return true;
//		
//		List<PequenoGerador> pequenosGeradores = pequenoGeradorService.getPequenosGeradoresAutoCadastro();
//		if(pequenosGeradores!=null && pequenosGeradores.size()>0)
//			return true;
		
		List<Transportador> transportadores    = transportadorService.getTransportadoresAutoCadastro();
		if(transportadores!=null && transportadores.size()>0)
			return true;
		
		return false;
	}
	
	
}
