package br.com.biharckgroup.scr.adm.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.bean.AguardandoAprovacao;
import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.EnumTipoCadastro;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.PequenoGerador;
import br.com.biharckgroup.scr.bean.Transportador;
import br.com.biharckgroup.scr.service.DestinoFinalService;
import br.com.biharckgroup.scr.service.GrandeGeradorService;
import br.com.biharckgroup.scr.service.PequenoGeradorService;
import br.com.biharckgroup.scr.service.TransportadorService;

@Controller(path="/adm/process/AguardandoAprovacao",authorizationModule=ProcessAuthorizationModule.class)
public class AguardandoAprovacaoProcess extends MultiActionController{

	private DestinoFinalService destinoFinalService;
	private GrandeGeradorService grandeGeradorService;
	private PequenoGeradorService pequenoGeradorService;
	private TransportadorService transportadorService;
	
	public void setDestinoFinalService(DestinoFinalService destinoFinalService) {
		this.destinoFinalService = destinoFinalService;
	}
	public void setGrandeGeradorService(GrandeGeradorService grandeGeradorService) {
		this.grandeGeradorService = grandeGeradorService;
	}
	public void setPequenoGeradorService(PequenoGeradorService pequenoGeradorService) {
		this.pequenoGeradorService = pequenoGeradorService;
	}
	public void setTransportadorService(TransportadorService transportadorService) {
		this.transportadorService = transportadorService;
	}
	
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request) {
		
		if(getParameter("sucesso")!=null){
			request.getSession().removeAttribute("showLinkBar");
			request.addMessage("Cadastro Aprovado com Sucesso!");
			request.addMessage("Um e-mail contendo as instru��es de acesso ao sistema foi enviado ao usu�rio.");
		}
		
		//somente usu�rios do tipo destino final e transportador precisam de aprova��o
		List<AguardandoAprovacao> listaAguardandoAprovacao = new ArrayList<AguardandoAprovacao>();
		
		List<DestinoFinal> destinosFinais      = destinoFinalService.getDestinosAutoCadastro();
		for (DestinoFinal df : destinosFinais) {
			listaAguardandoAprovacao.add(new AguardandoAprovacao(df.getId(), EnumTipoCadastro.DESTINO_FINAL, df.getNomeFantasia(), df.getTimeInc()));
		}
		
//		List<GrandeGerador> grandesGeradores   = grandeGeradorService.getGrandesGeradoresAutoCadastro();
//		for (GrandeGerador gg : grandesGeradores) {
//			listaAguardandoAprovacao.add(new AguardandoAprovacao(gg.getId(), EnumTipoCadastro.GRANDE_GERADOR, gg.getNomeFantasia(), gg.getTimeInc()));
//		}
//		
//		List<PequenoGerador> pequenosGeradores = pequenoGeradorService.getPequenosGeradoresAutoCadastro();
//		for (PequenoGerador pg : pequenosGeradores) {
//			listaAguardandoAprovacao.add(new AguardandoAprovacao(pg.getId(), EnumTipoCadastro.PEQUENO_GERADOR, pg.getNome(), pg.getTimeInc()));
//		}
		
		List<Transportador> transportadores    = transportadorService.getTransportadoresAutoCadastro();
		for (Transportador tr : transportadores) {
			listaAguardandoAprovacao.add(new AguardandoAprovacao(tr.getId(), EnumTipoCadastro.TRANSPORTADOR, tr.getNomeFantasia(), tr.getTimeInc()));
		}
		
		return new ModelAndView("process/aguardandoAprovacao","cadastros",listaAguardandoAprovacao);
	}
	
}
