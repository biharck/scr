package br.com.biharckgroup.scr.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.GrupoTipoResiduoFiltro;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduo;

@Controller(path="/adm/crud/GrupoTipoResiduo",authorizationModule=CrudAuthorizationModule.class)
public class GrupoTipoResiduoCrud extends CrudControllerSCR<GrupoTipoResiduoFiltro,GrupoTipoResiduo,GrupoTipoResiduo> {
	
	@Override
	protected void salvar(WebRequestContext request, GrupoTipoResiduo bean)	 {
			super.salvar(request, bean);
	}
}
