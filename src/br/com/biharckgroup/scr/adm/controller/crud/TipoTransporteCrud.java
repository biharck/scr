package br.com.biharckgroup.scr.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.TipoTransporteFiltro;
import br.com.biharckgroup.scr.bean.EnumAtivo;
import br.com.biharckgroup.scr.bean.TipoTransporte;

@Controller(path="/adm/crud/TipoTransporte",authorizationModule=CrudAuthorizationModule.class)
public class TipoTransporteCrud extends CrudControllerSCR<TipoTransporteFiltro,TipoTransporte,TipoTransporte> {
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		TipoTransporteFiltro _filtro = (TipoTransporteFiltro) filtro;
		//adiciona filtro padr�o status ativado
		if(_filtro.getAtivo()==null){
			_filtro.setAtivo(EnumAtivo.ATIVO);
		}
		return super.doListagem(request, filtro);
	}
	

}
