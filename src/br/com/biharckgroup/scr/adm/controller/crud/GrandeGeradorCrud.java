package br.com.biharckgroup.scr.adm.controller.crud;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.GrandeGeradorFiltro;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.service.GrandeGeradorService;
import br.com.biharckgroup.scr.service.UsuarioService;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRUtil;

import com.google.gson.Gson;

@Controller(path="/adm/crud/GrandeGerador",authorizationModule=CrudAuthorizationModule.class)
public class GrandeGeradorCrud extends CrudControllerSCR<GrandeGeradorFiltro,GrandeGerador,GrandeGerador> {

	private GrandeGeradorService grandeGeradorService;
	
	public void setGrandeGeradorService(GrandeGeradorService grandeGeradorService) {
		this.grandeGeradorService = grandeGeradorService;
	}
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		if("salvar".equals(acao)){
			GrandeGerador bean = (GrandeGerador) obj;
			/* Insert */
			if(bean.getId() == null){
				if(!bean.getEmail().equals(bean.getReEmail())){
					errors.reject("","O email e a confirma��o devem ser iguais.");	
				}
				if(!SCRUtil.isCPFouCNPJValido(bean.getCpfCnpjTransient())){
					errors.reject("","O CPF/CNPJ informado n�o � v�lido.");
				}
			}
		}
		super.validate(obj, errors, acao);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, GrandeGerador bean)throws CrudException {
		//passa getCpfCnpjTransient para campo correto
		if (bean.getCpfCnpjTransient().length()== 14){
			bean.setCpf(bean.getCpfCnpjTransient());
			bean.setCnpj(null);
		}else{
			bean.setCnpj(bean.getCpfCnpjTransient());
			bean.setCpf(null);
		}
		try{
			grandeGeradorService.saveOrUpdate(bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_cpf")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Grande Gerador com este CPF cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "unique_cnpj")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Grande Gerador com este CNPJ cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Grande Gerador com este e-mail cadastrado no sistema.");
			}
			return doEntrada(request, bean);
		}
		return doListagem(request, new GrandeGeradorFiltro());
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		
		GrandeGeradorFiltro _filtro = (GrandeGeradorFiltro) filtro;
		
		if(!SCRUtil.isPessoaLogadaAdministrador() && !SCRUtil.isPessoaLogadaAdministrativo()){
			if(SCRUtil.isPessoaLogadaGrandeGerador())
				_filtro.setCpfCnpjTransient(
						SCRUtil.getUsuarioLogado().getGrandeGerador().getCpf()==null
						?SCRUtil.getUsuarioLogado().getGrandeGerador().getCnpj()
								:SCRUtil.getUsuarioLogado().getGrandeGerador().getCpf());
			
		}
		
		//guia emitida por destino final ao buscar grande gerador n�o pode exibir bot�o p cadastro de novo gg
		try {
			boolean hideNovo = false;
			if(request.getParameter("hideNovo") !=null && Boolean.parseBoolean(request.getParameter("hideNovo"))){
				hideNovo=true;
				request.setAttribute("hideNovo", hideNovo);
			}
		} catch (Exception e) {
		}
		return super.doListagem(request, filtro);
	}
	
	/**
	 * <p>M�todo Ajax utilizado para buscar dados do grande gerador para cadastro no {@link Guia}
	 * @param request {@link WebRequestContext}
	 * @throws JSONException 
	 * @throws IOException 
	 */
	public void ajaxBuscaGrandeGerador(WebRequestContext request) throws JSONException, IOException{
		JSONObject jsonObj = new JSONObject();
		
		try {
			Gson gson = new Gson();
			GrandeGerador obj = new GrandeGerador();
			String idCompleto = request.getParameter("idGrandeGerador");
			if(idCompleto != null){//campo n�o � obrigat�rio
				Integer id = SCRUtil.returnOnlyId(idCompleto);
				obj = new GrandeGerador(id);
				obj = grandeGeradorService.load(obj); //carrga obj
				
				obj.getUf().setMunicipios(null);
			}
			String objJSONString = gson.toJson(obj); //transforma obj para formato Json
			jsonObj.put("obj", objJSONString);
			jsonObj.put("erro", false);
		} catch (Exception e) {
			jsonObj.put("erro", true);
			jsonObj.put("msg", "Ups! aconteceu algo inesperado! tente novamente mais tarde ou entre em contato com o administrador do sistema!");
			e.printStackTrace();
		}
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println(jsonObj);
	}
	
	@Override
	protected void entrada(WebRequestContext request, GrandeGerador form)throws Exception {

		if(SCRUtil.isPessoaLogadaAdministrador())
			request.setAttribute("listaUsuario", UsuarioService.getInstance().findAll());
		else
			request.setAttribute("listaUsuario", UsuarioService.getInstance().getUsuariosByAtribuicao(SCRUtil.getUsuarioLogado().getGrandeGerador()));
		
		super.entrada(request, form);
	}

}
