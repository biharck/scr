package br.com.biharckgroup.scr.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.TipoTransportadorFiltro;
import br.com.biharckgroup.scr.bean.EnumAtivo;
import br.com.biharckgroup.scr.bean.TipoTransportador;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/adm/crud/TipoTransportador",authorizationModule=CrudAuthorizationModule.class)
public class TipoTransportadorCrud extends CrudControllerSCR<TipoTransportadorFiltro,TipoTransportador,TipoTransportador> {
	

	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		TipoTransportadorFiltro _filtro = (TipoTransportadorFiltro) filtro;
		//adiciona filtro padr�o status ativado
		if(_filtro.getAtivo()==null){
			_filtro.setAtivo(EnumAtivo.ATIVO);
		}
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void entrada(WebRequestContext request, TipoTransportador form)throws Exception {
		if(form.getId()!=null)
			form.setEnumTipoGerador(SCRUtil.getEnumTipoGerador(form.isGrandeGerador()));
	}
}
