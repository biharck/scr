package br.com.biharckgroup.scr.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.TipoDestinoFiltro;
import br.com.biharckgroup.scr.bean.EnumAtivo;
import br.com.biharckgroup.scr.bean.TipoDestino;

@Controller(path="/adm/crud/TipoDestino",authorizationModule=CrudAuthorizationModule.class)
public class TipoDestinoCrud extends CrudControllerSCR<TipoDestinoFiltro,TipoDestino,TipoDestino> {

	@Override
	protected void listagem(WebRequestContext request, FiltroListagem filtro)
			throws Exception {
		// TODO Auto-generated method stub
		super.listagem(request, filtro);
	}

	@Override
	public ModelAndView doListagem(WebRequestContext request,
			FiltroListagem filtro) throws CrudException {
		TipoDestinoFiltro _filtro = (TipoDestinoFiltro) filtro;
		//adiciona filtro padr�o status ativado
		if(_filtro.getAtivo()==null){
			_filtro.setAtivo(EnumAtivo.ATIVO);
		}
		return super.doListagem(request, filtro);
	}
}

