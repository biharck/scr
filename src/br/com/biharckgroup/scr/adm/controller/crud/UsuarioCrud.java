package br.com.biharckgroup.scr.adm.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.UsuarioFiltro;
import br.com.biharckgroup.scr.bean.EnumAtivo;
import br.com.biharckgroup.scr.bean.EnumPapel;
import br.com.biharckgroup.scr.bean.Papel;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.dao.UsuarioDAO;
import br.com.biharckgroup.scr.service.PapelService;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path = "/adm/crud/Usuario", authorizationModule = CrudAuthorizationModule.class)
public class UsuarioCrud extends CrudControllerSCR<UsuarioFiltro, Usuario, Usuario> {

	private PapelService papelService;
	private UsuarioDAO usuarioDAO;

	public void setPapelService(PapelService papelService) {
		this.papelService = papelService;
	}
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		UsuarioFiltro _filtro = (UsuarioFiltro) filtro;
		//adiciona filtro padr�o status ativado
		if(_filtro.getAtivo()==null){
			_filtro.setAtivo(EnumAtivo.ATIVO);
		}
		
		if(!SCRUtil.isPessoaLogadaAdministrador() && !SCRUtil.isPessoaLogadaAdministrativo()){
			if(SCRUtil.isPessoaLogadaDestinoFinal())
				_filtro.setTipo(SCRUtil.getUsuarioLogado().getDestinoFinal());
			
			if(SCRUtil.isPessoaLogadaGrandeGerador())
				_filtro.setTipo(SCRUtil.getUsuarioLogado().getGrandeGerador());
			
			if(SCRUtil.isPessoaLogadaTransportador())
				_filtro.setTipo(SCRUtil.getUsuarioLogado().getTransportador());
	
			if(SCRUtil.isPessoaLogadaPequenoGerador())
				_filtro.setTipo(SCRUtil.getUsuarioLogado().getPequenoGerador());
		}
		
		return super.doListagem(request, filtro);
	}
	
	
	@Override
	protected void listagem(WebRequestContext request, FiltroListagem filtro)throws Exception {
		if(SCRUtil.isPessoaLogadaAdministrador() || SCRUtil.isPessoaLogadaAdministrativo())
			request.setAttribute("listaPapel", papelService.findAll());
		//significa que pode cadastrar novos usu�rios
		else if(SCRUtil.getUsuarioLogado().isSuperUsuario()){
			List<Papel> papeis = new ArrayList<Papel>();
			if(SCRUtil.isPessoaLogadaDestinoFinal())
				papeis.add(new Papel(EnumPapel.DESTINOFINAL.getValor()));
			
			if(SCRUtil.isPessoaLogadaGrandeGerador())
				papeis.add(new Papel(EnumPapel.GRANDEGERADOR.getValor()));
			
			if(SCRUtil.isPessoaLogadaTransportador())
				papeis.add(new Papel(EnumPapel.TRANSPORTADOR.getValor()));
	
			if(SCRUtil.isPessoaLogadaPequenoGerador())
				papeis.add(new Papel(EnumPapel.PEQUENOGERADOR.getValor()));
			
			request.setAttribute("listaPapel", papeis);
		}
	}

	@Override
	protected void entrada(WebRequestContext request, Usuario form) throws Exception {
		if(SCRUtil.isPessoaLogadaAdministrador() || SCRUtil.isPessoaLogadaAdministrativo())
			request.setAttribute("listaPapel", papelService.findAll());
		else
			request.setAttribute("listaPapel", papelService.findByUsuario(SCRUtil.getUsuarioLogado()));
	}
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		if("salvar".equals(acao)){
			Usuario bean = (Usuario) obj;
			if(bean.getId() == null){
				if(!bean.getSenha().equals(bean.getReSenha()))
					errors.reject("","A senha e a confirma��o devem ser iguais.");
				if(!bean.getEmail().equals(bean.getReEmail()))
					errors.reject("","O email e a confirma��o devem ser iguais.");
				if(bean.getPapeis()==null || bean.getPapeis().isEmpty())
					errors.reject("","O usu�rio deve ter pelo menos um papel no sistema.");
			}
		}
		super.validate(obj, errors, acao);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Usuario bean) {
		//se nao for administrador e for super usu�rio...
		if(!SCRUtil.isPessoaLogadaAdministrador() || !SCRUtil.isPessoaLogadaAdministrativo()){
			if(SCRUtil.getUsuarioLogado().isSuperUsuario()){
				if(SCRUtil.isPessoaLogadaDestinoFinal())
					bean.setDestinoFinal(SCRUtil.getUsuarioLogado().getDestinoFinal());
				
				if(SCRUtil.isPessoaLogadaGrandeGerador())
					bean.setGrandeGerador(SCRUtil.getUsuarioLogado().getGrandeGerador());
				
				if(SCRUtil.isPessoaLogadaTransportador())
					bean.setTransportador(SCRUtil.getUsuarioLogado().getTransportador());
		
				if(SCRUtil.isPessoaLogadaPequenoGerador())
					bean.setPequenoGerador(SCRUtil.getUsuarioLogado().getPequenoGerador());
			}
		}	
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Usuario form)throws CrudException {
		try{
			salvar(request, form);
			usuarioDAO.saveOrUpdate(form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_login")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Usu�rio com este Login cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Usu�rio com este Email cadastrado no sistema.");
			}
			return doEntrada(request, form);
		}
		return doListagem(request, new UsuarioFiltro());
	}

}
