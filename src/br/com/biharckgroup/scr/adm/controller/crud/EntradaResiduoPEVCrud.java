package br.com.biharckgroup.scr.adm.controller.crud;

import java.io.IOException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.EntradaResiduoPEVFiltro;
import br.com.biharckgroup.scr.bean.EntradaResiduoPEV;
import br.com.biharckgroup.scr.bean.EnumAgenteEntradaResiduo;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduo;
import br.com.biharckgroup.scr.bean.TipoResiduo;
import br.com.biharckgroup.scr.service.TipoResiduoService;
import br.com.biharckgroup.scr.service.UfService;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path = "/adm/crud/EntradaResiduoPEV", authorizationModule = CrudAuthorizationModule.class)
public class EntradaResiduoPEVCrud extends
		CrudControllerSCR<EntradaResiduoPEVFiltro, EntradaResiduoPEV, EntradaResiduoPEV> {

	private UfService ufService;
	private TipoResiduoService tipoResiduoService;

	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}

	public void setTipoResiduoService(TipoResiduoService tipoResiduoService) {
		this.tipoResiduoService = tipoResiduoService;
	}

	@Override
	protected void entrada(WebRequestContext request, EntradaResiduoPEV form)
			throws Exception {
		if (form.getId() != null) {
			form.getPontoEntrega().setUf(
					ufService.getUfByMunicipio(form.getPontoEntrega().getMunicipio()));
			form.setAgenteEntradaResiduo(form.isTransportador() 
					? EnumAgenteEntradaResiduo.TRANSPORTADOR
					: EnumAgenteEntradaResiduo.GERADOR);
		}
		super.entrada(request, form);
	}

	public void buscaTipoResiduo(WebRequestContext context) throws IOException,JSONException {
		try {
			context.getServletResponse().setContentType("application/json");
			context.getServletResponse().setCharacterEncoding("ISO-8859-1");

			if (getParameter("grupo") == null|| getParameter("grupo").equals("<null>")) {
				JSONArray jsonArray = new JSONArray();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("jscript","$('select[name=\"grupoTipoResiduosEntradaResiduoPEV["+ getParameter("index")+ "].tipoResiduo\"]').empty();");
				jsonArray.put(jsonObject);
				context.getServletResponse().getWriter().println(jsonArray);
				return;
			}

			// enviando para o jsp
			context.getServletResponse().getWriter().println(formatCombo(tipoResiduoService.getTiposResiduosByGrupo(new GrupoTipoResiduo(SCRUtil.returnOnlyId(getParameter("grupo")))),getParameter("index")));

		} catch (Exception e) {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("erro", true);
			context.getServletResponse().setContentType("application/json");
			context.getServletResponse().setCharacterEncoding("ISO-8859-1");
			context.getServletResponse().getWriter().println(jsonObj);
			e.printStackTrace();
		}
	}

	/**
	 * <p>
	 * Funn��o respons�vel em montar toda estrutura de um combo para povoar o
	 * html
	 * 
	 * @param planos
	 *            lista contendo todos os planos de um determinada operadora
	 * @return {@link JSONArray} formatada para enviar via JSON
	 * @throws JSONException
	 */
	public JSONArray formatCombo(List<TipoResiduo> tiposResiduos, String index)throws JSONException {
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();

		jsonObject.put("jscript","$('select[name=\"grupoTipoResiduosEntradaResiduoPEV[" + index+ "].tipoResiduo\"]').empty();");
		jsonArray.put(jsonObject);
		jsonObject = new JSONObject();
		jsonObject.put("jscript","$('select[name=\"grupoTipoResiduosEntradaResiduoPEV["+ index+ "].tipoResiduo\"]').append('<option value=\"<null>\"></option>');");
		jsonArray.put(jsonObject);
		for (TipoResiduo tr : tiposResiduos) {
			jsonObject = new JSONObject();
			jsonObject.put("jscript","$('select[name=\"grupoTipoResiduosEntradaResiduoPEV["+ index+ "].tipoResiduo\"]').append('<option value=\""+ SCRUtil.getComboFormat(tr, "id", tr.getId())+ "\">" + tr.getDescricao() + "</option>');");
			jsonArray.put(jsonObject);
		}
		return jsonArray;
	}
	
	@Override
	protected void salvar(WebRequestContext request, EntradaResiduoPEV bean) {
		if(bean.getGrupoTipoResiduosEntradaResiduoPEV()== null)
			throw new SCRException("Campo Tipos de Res�duos � obrigat�rio.");
		super.salvar(request, bean);
	}
}
