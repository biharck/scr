package br.com.biharckgroup.scr.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.GuiaFiltro;
import br.com.biharckgroup.scr.adm.filtro.ResponsavelFiltro;
import br.com.biharckgroup.scr.bean.Responsavel;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.service.UfService;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/adm/crud/Responsavel",authorizationModule=CrudAuthorizationModule.class)
public class ResponsavelCrud extends CrudControllerSCR<ResponsavelFiltro,Responsavel,Responsavel> {
	
private UfService ufService;

	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}

	@Override
	protected void salvar(WebRequestContext request, Responsavel bean) {
		//passa getCpfCnpjTransient para campo correto
		if (bean.getCpfCnpjTransient().length()== 14){
			bean.setCpf(bean.getCpfCnpjTransient());
			bean.setCnpj(null);
		}else{
			bean.setCnpj(bean.getCpfCnpjTransient());
			bean.setCpf(null);
		}
		// campo desabilitado == null 
		bean.setGrandeGerador( SCRUtil.getUsuarioLogado().getGrandeGerador());
		super.salvar(request, bean);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Responsavel form)
			throws Exception {
		if(form.getId()!=null){
			form.setUf(ufService.getUfByMunicipio(form.getMunicipio()));
		}
		
		//verifica se usu�rio tem papel de grande gerador
		if(!SCRUtil.isPessoaLogadaGrandeGerador())
			throw new SCRException("A tela de cadastro de guias s� � exibida para usu�rios do tipo Grande Gerador.");

		// define que o grande gerador da guia � o usu�rio logado
		Usuario usuario = SCRUtil.getUsuarioLogado();
		if(usuario.getGrandeGerador() == null)
			throw new SCRException("N�o existe v�nculo do usu�rio logado com nenhum Grande Gerador.");
		form.setGrandeGerador(usuario.getGrandeGerador());
		
		super.entrada(request, form);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		
		Usuario usuario = SCRUtil.getUsuarioLogado();
		if(usuario.getGrandeGerador() == null)
			throw new SCRException("N�o existe v�nculo do usu�rio logado com nenhum Grande Gerador.");
		//define filtro pad�o
		ResponsavelFiltro _filtro = (ResponsavelFiltro) filtro;
		_filtro.setGrandeGerador(usuario.getGrandeGerador());
		
		return super.doListagem(request, filtro);
	}
}