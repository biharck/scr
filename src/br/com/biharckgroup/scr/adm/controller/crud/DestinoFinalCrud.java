package br.com.biharckgroup.scr.adm.controller.crud;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.DestinoFinalFiltro;
import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.EnumPapel;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.Papel;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.dao.UsuarioDAO;
import br.com.biharckgroup.scr.service.DestinoFinalService;
import br.com.biharckgroup.scr.service.UsuarioService;
import br.com.biharckgroup.scr.util.DatabaseError;
import br.com.biharckgroup.scr.util.SCRUtil;

import com.google.gson.Gson;

@Controller(path="/adm/crud/DestinoFinal",authorizationModule=CrudAuthorizationModule.class)
public class DestinoFinalCrud extends CrudControllerSCR<DestinoFinalFiltro,DestinoFinal,DestinoFinal> {
	
	private DestinoFinalService destinoFinalService;
	private UsuarioDAO usuarioDAO;
	
	public void setDestinoFinalService(DestinoFinalService destinoFinalService) {
		this.destinoFinalService = destinoFinalService;
	}
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}

	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {

		if("salvar".equals(acao)){
			DestinoFinal bean = (DestinoFinal) obj;
			/* Insert */
			if(bean.getId() == null)
				if(bean.getValidadeCadastro().before(new Date()))
					errors.reject("","A data de validade deve ser superior a data atual.");
				if(!SCRUtil.isCPFouCNPJValido(bean.getCpfCnpjTransient())){
					errors.reject("","O CPF/CNPJ informado n�o � v�lido.");
				}
		}
		super.validate(obj, errors, acao);
	}
	
	@Override
	protected void salvar(WebRequestContext request, DestinoFinal bean) {
		//valida uniques
		try{
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "unique_cnpj")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Destino Final com este CNPJ cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
				request.addError("J� existe um registro de Destino Final com este e-mail cadastrado no sistema.");
			}
		}
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, DestinoFinal bean)throws CrudException {
		//passa getCpfCnpjTransient para campo correto, neste caso s� possui cnpj
		bean.setCnpj(bean.getCpfCnpjTransient());
		
		//para salvar registros que est�o sendo aprovados pelo administrativo
		if(request.getSession().getAttribute("showLinkBar")!=null && ! Boolean.valueOf(request.getSession().getAttribute("showLinkBar").toString())){
			
			//preparando usu�rio com perfil de cadastro de novos usu�rios
			Usuario user = new Usuario();
			user.setLogin(SCRUtil.retiraMascara(bean.getCnpj()));
			String senhaTemporaria = SCRUtil.senhaTemporaria(); 
			user.setSenha(senhaTemporaria);
			user.setReSenha(senhaTemporaria);
			user.setAtivo(true);
			user.setSenhaProvisoria(true);
			user.setNome(bean.getNomeFantasia());
			user.setEmail(bean.getEmail());
			user.setReEmail(bean.getEmail());
			user.setSuperUsuario(true);
			
			Papel p = new Papel();
			p.setId(EnumPapel.DESTINOFINAL.getValor());

			List<Papel> papeis = new ArrayList<Papel>();
			papeis.add(p);
			user.setPapeis(papeis);
			user.setDestinoFinal(bean);
			
			usuarioDAO.saveOrUpdate(user);
			
			
			List<Usuario> usuarios = new ArrayList<Usuario>();
			usuarios.add(user);
			bean.setUsuarios(usuarios);
			
			//Envia email contendo informa��es do usu�rio criado para este destino final
			try {
				SCRUtil.enviaEmailNovoUsuario(user, senhaTemporaria);
			} catch (MessagingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			request.getSession().setAttribute("showOnlyMsg", true);
			try{
				destinoFinalService.saveOrUpdate(bean);
			} catch (DataIntegrityViolationException e) {
				if (DatabaseError.isKeyPresent(e, "unique_cnpj")){ //nota o index na tabela tem q ter o mesmo nome
					request.addError("J� existe um registro de Destino Final com este CNPJ cadastrado no sistema.");
				}
				if (DatabaseError.isKeyPresent(e, "unique_email")){ //nota o index na tabela tem q ter o mesmo nome
					request.addError("J� existe um registro de Destino Final com este e-mail cadastrado no sistema.");
				}
			}
			return redirectClosePage(request);		
		}
		return super.doSalvar(request, bean);
	}
	
	public ModelAndView redirectClosePage(WebRequestContext request){
		return new ModelAndView("crud/pgSaveSuccessProcess");
	}
	
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		
		DestinoFinalFiltro _filtro = (DestinoFinalFiltro) filtro;
		
		if(request.getSession().getAttribute("showOnlyMsg")!=null){
			setAttribute("showOnlyMsg", request.getSession().getAttribute("showOnlyMsg"));
			setAttribute("showMenu", false);
			request.getSession().setAttribute("showLinkBar", false);
			request.getSession().removeAttribute("showOnlyMsg");
		}
		
		if(!SCRUtil.isPessoaLogadaAdministrador() && !SCRUtil.isPessoaLogadaAdministrativo()){
			if(SCRUtil.isPessoaLogadaDestinoFinal())
				_filtro.setCnpj(SCRUtil.getUsuarioLogado().getDestinoFinal().getCnpj());
			
		}
		
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void listagem(WebRequestContext request, FiltroListagem filtro)throws Exception {
		setAttribute("showOnlyMsg", getAttribute("showOnlyMsg"));
		setAttribute("showMenu", getAttribute("showMenu"));
		request.getSession().setAttribute("showLinkBar", request.getSession().getAttribute("showLinkBar"));
		super.listagem(request, filtro);
	}
	
	@Override
	protected void entrada(WebRequestContext request, DestinoFinal form)throws Exception {
		if(form.getId()!=null){
			form.setValidadeCadastro(SCRUtil.incrementDate(new java.sql.Date(System.currentTimeMillis()), 365, Calendar.DAY_OF_YEAR));
		}
		
		if(SCRUtil.isPessoaLogadaAdministrador())
			request.setAttribute("listaUsuario", UsuarioService.getInstance().findAll());
		else
			request.setAttribute("listaUsuario", UsuarioService.getInstance().getUsuariosByAtribuicao(SCRUtil.getUsuarioLogado().getDestinoFinal()));
		
		
		request.setAttribute("submitLabel", "salvar");
		if(request.getParameter("showLinkBar")!=null && !new Boolean(request.getParameter("showLinkBar"))){
			form.setAtivo(true);
			request.getSession().setAttribute("showLinkBar", request.getParameter("showLinkBar"));
			request.setAttribute("submitLabel", "Aprovar Cadastro");
		}else
			request.getSession().removeAttribute("showLinkBar");
		super.entrada(request, form);
	}
	
	/**
	 * <p>M�todo Ajax utilizado para buscar dados do destino final para cadastro no {@link Guia}
	 * @param request {@link WebRequestContext}
	 * @throws JSONException 
	 * @throws IOException 
	 */
	public void ajaxBuscaDestinoFinal(WebRequestContext request) throws JSONException, IOException{
		JSONObject jsonObj = new JSONObject();
		
		try {
			Gson gson = new Gson();
			String idCompleto = request.getParameter("idDestinoFinal");
			Integer id = SCRUtil.returnOnlyId(idCompleto);
			DestinoFinal obj = new DestinoFinal(id);
			obj = destinoFinalService.load(obj); //carrga obj
			
			obj.getUf().setMunicipios(null);
			
			String objJSONString = gson.toJson(obj); //transforma obj para formato Json
			jsonObj.put("obj", objJSONString);
			jsonObj.put("erro", false);
		} catch (Exception e) {
			jsonObj.put("erro", true);
			jsonObj.put("msg", "Ups! aconteceu algo inesperado! tente novamente mais tarde ou entre em contato com o administrador do sistema!");
			e.printStackTrace();
		}
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println(jsonObj);
	}
	

}
