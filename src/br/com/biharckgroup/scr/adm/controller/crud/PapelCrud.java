package br.com.biharckgroup.scr.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.PapelFiltro;
import br.com.biharckgroup.scr.bean.EnumPapel;
import br.com.biharckgroup.scr.bean.Papel;

@Controller(path = "/adm/crud/Papel", authorizationModule = CrudAuthorizationModule.class)
public class PapelCrud extends CrudControllerSCR<PapelFiltro, Papel, Papel> {

	@Override
	protected void entrada(WebRequestContext request, Papel form)throws Exception {
		request.setAttribute("notBeanAuditoria", true);
		super.entrada(request, form);
	}
	
	@Override
	protected void excluir(WebRequestContext request, Papel bean)throws Exception {
		if(EnumPapel.ADMINISTRADOR.getValor() == bean.getId().intValue()
				||EnumPapel.ADMINISTRATIVO.getValor() == bean.getId().intValue()
				||EnumPapel.DESTINOFINAL.getValor() == bean.getId().intValue()
				||EnumPapel.GRANDEGERADOR.getValor() == bean.getId().intValue()
				||EnumPapel.PEQUENOGERADOR.getValor() == bean.getId().intValue()
				||EnumPapel.TRANSPORTADOR.getValor() == bean.getId().intValue()
				){
			request.addError("Voc� n�o pode excluir este papel, pois ele � necess�rio para o funcionamento do sistema!");
			return;
		}else
			super.excluir(request, bean);
	}
}
