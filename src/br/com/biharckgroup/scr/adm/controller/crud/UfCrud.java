package br.com.biharckgroup.scr.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.bean.Uf;

@Controller(path="/adm/crud/Uf",authorizationModule=CrudAuthorizationModule.class)
public class UfCrud extends CrudControllerSCR<FiltroListagem,Uf,Uf> {
	
	@Override
	protected void salvar(WebRequestContext request, Uf bean)  {
		super.salvar(request, bean);
	}

}
