package br.com.biharckgroup.scr.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.bean.Municipio;

@Controller(path="/adm/crud/Municipio",authorizationModule=CrudAuthorizationModule.class)
public class MunicipioCrud extends CrudControllerSCR<FiltroListagem,Municipio,Municipio> {
	

}
