package br.com.biharckgroup.scr.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.ClasseTipoResiduoFiltro;
import br.com.biharckgroup.scr.bean.ClasseTipoResiduo;

@Controller(path="/adm/crud/ClasseTipoResiduo",authorizationModule=CrudAuthorizationModule.class)
public class ClasseTipoResiduoCrud extends CrudControllerSCR<ClasseTipoResiduoFiltro,ClasseTipoResiduo,ClasseTipoResiduo> {
}
