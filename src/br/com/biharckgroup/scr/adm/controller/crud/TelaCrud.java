package br.com.biharckgroup.scr.adm.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.bean.Tela;

@Controller(path = "/adm/crud/Tela", authorizationModule = CrudAuthorizationModule.class)
public class TelaCrud extends CrudControllerSCR<FiltroListagem, Tela, Tela> {


}
