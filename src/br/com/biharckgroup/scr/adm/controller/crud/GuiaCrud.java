package br.com.biharckgroup.scr.adm.controller.crud;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.util.CollectionsUtil;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.GuiaFiltro;
import br.com.biharckgroup.scr.adm.filtro.ProjetoFiltro;
import br.com.biharckgroup.scr.adm.filtro.UsuarioFiltro;
import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.EnumAtivo;
import br.com.biharckgroup.scr.bean.EnumCancelada;
import br.com.biharckgroup.scr.bean.EnumSituacao;
import br.com.biharckgroup.scr.bean.EnumStatus;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.bean.ResiduoGuia;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.service.DestinoFinalService;
import br.com.biharckgroup.scr.service.GuiaService;
import br.com.biharckgroup.scr.service.ProjetoService;
import br.com.biharckgroup.scr.service.UsuarioService;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path = "/adm/crud/Guia", authorizationModule = CrudAuthorizationModule.class)
public class GuiaCrud extends CrudControllerSCR<GuiaFiltro, Guia, Guia> {

	private GuiaService guiaService;
	private DestinoFinalService destinoFinalService;
	private UsuarioService usuarioService;
	
	public void setGuiaService(GuiaService guiaService) {
		this.guiaService = guiaService;
	}
	public void setDestinoFinalService(DestinoFinalService destinoFinalService) {
		this.destinoFinalService = destinoFinalService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public void exemploAjax(WebRequestContext request) throws IOException,JSONException {
		
		JSONObject jsonObj = new JSONObject();
		/*preparando retorno do ajax*/
		if (true) {//caso esteja ok
			jsonObj.put("erro", false);
			jsonObj.put("id", 10);
			jsonObj.put("nome", "um nome qualquer");
			jsonObj.put("endereco", "um endereco qualquer");
		} else {//caso queira lan�ar exception
			jsonObj.put("erro", true);
			jsonObj.put("msg", "uma mensagem de erro");
		}
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println(jsonObj);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Guia bean ){
		Guia load = null;
		if(bean.getId()!=null) //update
			load = guiaService.load(bean);
		
		//define campos desalibitados
		if(bean.getProjeto()!=null){
			//caso proj seja inf a guia ter� gg e end da obra como os do projeto
			Projeto proj = ProjetoService.getInstance().loadByNumeroProjeto(bean.getProjeto().getNumeroProjeto());
			bean.setGrandeGerador(proj.getGrandeGerador());
			bean.setUfObra(proj.getUfObra());
			bean.setMunicipioObra(proj.getMunicipioObra());
		}
		if(bean.getId()!=null){//update - campo n�o � alterado
			bean.setEnumSituacao(EnumSituacao.getEnumSituacaoByValor(load.getSituacao()));
			if(load.isStatus())
				bean.setEnumStatus(EnumStatus.CONFIRMADA);
			else
				bean.setEnumStatus(EnumStatus.PENDENTE);
		}else{//insert
			bean.setEnumSituacao(EnumSituacao.SEMSITUACAO);
			bean.setEnumStatus(EnumStatus.PENDENTE);
		}
		
		//define campo transiente
		bean.setSituacao(bean.getEnumSituacao().getValor());
		bean.setStatus(bean.getEnumStatus().isOptBool());
		
		//salva registro
		super.salvar(request, bean);
		
		/*
		 * ap�s o cadastro da guia calcula-se a nova capacidade no destino final, caso seja maior que 90 % exibe alerta na tela
		 * n�o realiza update no destino final, est� opera��o s� ser� realizada ap�s o recebimento da guia;
		 */
		double valorPossivelEntrada = 0;
		if(bean.getResiduosGuia()!=null)
			for (ResiduoGuia rg : bean.getResiduosGuia()) 
				valorPossivelEntrada += rg.getQuantidade();

		DestinoFinal destinoFinal = destinoFinalService.load(bean.getDestinoFinal());
		if(destinoFinalService.isCapacidadeMaiorIgualByPorcentagem(90,destinoFinal.getCapacidadeAtual() + valorPossivelEntrada, destinoFinal.getCapacidadeTotal()))
				request.addMessage("Aten��o! Caso esta guia seja recebida, o destino final "+destinoFinal.getRazaoSocial()+" atingir� capacidade superior � 90%.");
		
	}
	
	@Override
	protected void entrada(WebRequestContext request, Guia form)
			throws Exception {
		
		if(form.getId()==null){
			form.setEnumSituacao(EnumSituacao.SEMSITUACAO);
			form.setEnumStatus(EnumStatus.PENDENTE);
		}
		// TODO Auto-generated method stub
		super.entrada(request, form);
	}
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		if("salvar".equals(acao)){
            Guia bean = (Guia) obj;
			if(bean.getDataEntrega() != null){//campo n�o obrigat�rio
				if(bean.getDataEntrega().before(bean.getDataEnvio()))
					errors.reject("","A data de recebimento deve ser superior ou igual a data de envio.");
				if(bean.getDataRetirada() != null)
					if(bean.getDataRetirada().before(bean.getDataEntrega()))
						errors.reject("","A data de retirada deve ser superior ou igual a data de recebimento.");
			}
		}
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,
			FiltroListagem filtro) throws CrudException {
		GuiaFiltro _filtro = (GuiaFiltro) filtro;
		
		//adiciona filtro padr�o cancelada false
		if(_filtro.getEnumCancelada()==null){
			_filtro.setEnumCancelada(EnumCancelada.NAO);
		}
		
		//recebe param do link na ms email
		if(request.getParameter("guiaStatus")!=null){
			String guiaStatus = request.getParameter("guiaStatus");
			if(guiaStatus.equalsIgnoreCase("1"))		
				_filtro.setStatus(EnumStatus.CONFIRMADA);
			else
				_filtro.setStatus(EnumStatus.PENDENTE);
		}
		if(request.getParameter("numeroProjeto")!=null){
			String numeroProjeto = request.getParameter("numeroProjeto");
			_filtro.setNumeroProjeto(numeroProjeto);
		}
		
		if(SCRUtil.isPessoaLogadaGrandeGerador()){
			Usuario usuario = SCRUtil.getUsuarioLogado();
			if(usuario.getGrandeGerador() == null)
				throw new SCRException("N�o existe v�nculo do usu�rio logado com nenhum Grande Gerador.");
			//define filtro pad�o
			_filtro.setGrandeGerador(usuario.getGrandeGerador());
		}
		
		return super.doListagem(request, filtro);
	}
	
	
}
