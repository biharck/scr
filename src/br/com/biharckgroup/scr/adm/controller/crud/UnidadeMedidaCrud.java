package br.com.biharckgroup.scr.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.UnidadeMedidaFiltro;
import br.com.biharckgroup.scr.bean.UnidadeMedida;

@Controller(path="/adm/crud/UnidadeMedida",authorizationModule=CrudAuthorizationModule.class)
public class UnidadeMedidaCrud extends CrudControllerSCR<UnidadeMedidaFiltro,UnidadeMedida,UnidadeMedida> {

	@Override
	protected void salvar(WebRequestContext request, UnidadeMedida bean) {
			super.salvar(request, bean);
	}
}
