package br.com.biharckgroup.scr.adm.controller.crud;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.PontoEntregaFiltro;
import br.com.biharckgroup.scr.bean.EntradaResiduoPEV;
import br.com.biharckgroup.scr.bean.PontoEntrega;
import br.com.biharckgroup.scr.service.PontoEntregaService;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

import com.google.gson.Gson;

@Controller(path="/adm/crud/PontoEntrega",authorizationModule=CrudAuthorizationModule.class)
public class PontoEntregaCrud extends CrudControllerSCR<PontoEntregaFiltro,PontoEntrega,PontoEntrega> {
	
	private PontoEntregaService pontoEntregaService;
	
	public void setPontoEntregaService(PontoEntregaService pontoEntregaService) {
		this.pontoEntregaService = pontoEntregaService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, PontoEntrega bean)  {
		/* Insert */
		if(bean.getId() == null){
			if(!bean.getEmail().equals(bean.getReEmail())){
				throw new SCRException("O email e a confirma��o devem ser iguais.");
			}
		}
		super.salvar(request, bean);
		
	}
	
	@Override
	protected void entrada(WebRequestContext request, PontoEntrega form)
			throws Exception {
		super.entrada(request, form);
	}
	
	/**
	 * <p>M�todo Ajax utilizado para buscar dados de credenciaisPEV para cadastro no {@link EntradaResiduoPEV}
	 * @param request {@link WebRequestContext}
	 * @throws JSONException 
	 * @throws IOException 
	 */
	public void ajaxBuscaCredenciaisPEV(WebRequestContext request) throws JSONException, IOException{
		JSONObject jsonObj = new JSONObject();
		
		try {
			Gson gson = new Gson();
			String idCompleto = request.getParameter("idCredencial");
			Integer id = SCRUtil.returnOnlyId(idCompleto);
			PontoEntrega obj = new PontoEntrega(id);
			obj = pontoEntregaService.load(obj); //carrga obj
			
			obj.getUf().setMunicipios(null);
			
			String objJSONString = gson.toJson(obj); //transforma obj para formato Json
			jsonObj.put("obj", objJSONString);
			jsonObj.put("erro", false);
		} catch (Exception e) {
			jsonObj.put("erro", true);
			jsonObj.put("msg", "Ups! aconteceu algo inesperado! tente novamente mais tarde ou entre em contato com o administrador do sistema!");
			e.printStackTrace();
		}
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println(jsonObj);
	}
	
	
}
