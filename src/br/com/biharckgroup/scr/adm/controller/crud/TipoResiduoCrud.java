package br.com.biharckgroup.scr.adm.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.TipoResiduoFiltro;
import br.com.biharckgroup.scr.bean.TipoResiduo;

@Controller(path="/adm/crud/TipoResiduo",authorizationModule=CrudAuthorizationModule.class)
public class TipoResiduoCrud extends CrudControllerSCR<TipoResiduoFiltro,TipoResiduo,TipoResiduo> {
	

}
