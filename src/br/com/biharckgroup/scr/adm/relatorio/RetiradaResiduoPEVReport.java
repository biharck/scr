package br.com.biharckgroup.scr.adm.relatorio;


import java.util.ArrayList;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.controller.resource.ResourceGenerationException;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.relatorio.filtro.RetiradaResiduoPEVReportFiltro;
import br.com.biharckgroup.scr.bean.RetiradaResiduoPEV;
import br.com.biharckgroup.scr.dao.PontoEntregaDAO;
import br.com.biharckgroup.scr.dao.RetiradaResiduoPEVDAO;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/adm/relatorio/RetiradaResiduoPEV",authorizationModule=ReportAuthorizationModule.class)
public class RetiradaResiduoPEVReport extends ReportController<RetiradaResiduoPEVReportFiltro> {
	
	private RetiradaResiduoPEVDAO retiradaResiduoPEVDAO;
	private PontoEntregaDAO pontoEntregaDAO;
	
	public void setRetiradaResiduoPEVDAO(
			RetiradaResiduoPEVDAO retiradaResiduoPEVDAO) {
		this.retiradaResiduoPEVDAO = retiradaResiduoPEVDAO;
	}
	public void setPontoEntregaDAO(PontoEntregaDAO pontoEntregaDAO) {
		this.pontoEntregaDAO = pontoEntregaDAO;
	}
	
	@Override
	public IReport createReport(WebRequestContext request, RetiradaResiduoPEVReportFiltro filtro)throws Exception {

		Report report;
		String msg="";
		if((request.getParameter("rel") != null) && request.getParameter("rel").equals("CTRPEV")){
			report = new Report("CTRPEV");
			
			Integer id = Integer.parseInt(request.getParameter("idRetirada"));
			RetiradaResiduoPEV ret = new RetiradaResiduoPEV(id);
			
			ret = retiradaResiduoPEVDAO.load(ret);
			
			List<RetiradaResiduoPEV> listaRet = new ArrayList<RetiradaResiduoPEV>();
			listaRet.add(ret);
			report.setDataSource(listaRet);
		
		}else{//rela��o de retiradas
			report= new Report("retiradaResiduoPEV");
			
			if(filtro.getDataIni() != null || filtro.getDataFim()!=null){
				String dtIni = filtro.getDataIni()==null?"":SCRUtil.getDataHora("dd/MM/yyyy",filtro.getDataIni());
				String dtFim = filtro.getDataFim()==null?"":SCRUtil.getDataHora("dd/MM/yyyy",filtro.getDataFim());
				msg = "Per�odo: "+  dtIni+ " at� "+dtFim;
			}
			if(filtro.getPontoEntrega()!=null)
				msg += "   PEV: " + pontoEntregaDAO.findIdentificadorById(filtro.getPontoEntrega()).getIdentificacao();
			else
				msg += "   PEV: Todos";
			
			List<RetiradaResiduoPEV> listaRetirada = retiradaResiduoPEVDAO.findByFiltro(filtro);
			report.setDataSource(listaRetirada);
		}
		
		report.addParameter("msg",msg);
		report.addSubReport("SUB_GRUPOTIPORESIDUOENTRADASAIDA", new Report("sub_grupoTipoResiduoEntradaSaida"));
		SCRUtil.addCabecarioReport(report);
		
		return report;
	}
	
	@Action("filtroRetiradaResiduoPEV")
	public ModelAndView doFiltro(WebRequestContext request, RetiradaResiduoPEVReportFiltro filtro)  throws ResourceGenerationException{
		setAttribute("retiradasResiduoPEV", retiradaResiduoPEVDAO.findByFiltro(filtro));
		return super.doFiltro(request, filtro);
	}
	
}
