package br.com.biharckgroup.scr.adm.relatorio.filtro;


import java.sql.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.biharckgroup.scr.bean.EnumStatusProjeto;
import br.com.biharckgroup.scr.bean.Transportador;


public class ProjetoReportFiltro extends FiltroListagem {

	private Date dataInicio;
	private Date dataTermino;
	private EnumStatusProjeto enumStatusProjeto;
	private Transportador transportador;
		
	@DisplayName("Data In�cio")
	public Date getDataInicio() {
		return dataInicio;
	}
	@DisplayName("Data T�rmino")
	public Date getDataTermino() {
		return dataTermino;
	}
	@DisplayName("Status")
	public EnumStatusProjeto getEnumStatusProjeto() {
		return enumStatusProjeto;
	}
	public Transportador getTransportador() {
		return transportador;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public void setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
	}
	public void setEnumStatusProjeto(EnumStatusProjeto enumStatusProjeto) {
		this.enumStatusProjeto = enumStatusProjeto;
	}
	public void setTransportador(Transportador transportador) {
		this.transportador = transportador;
	}
}
