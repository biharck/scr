package br.com.biharckgroup.scr.adm.relatorio.filtro;



import java.util.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.biharckgroup.scr.bean.Municipio;
import br.com.biharckgroup.scr.bean.PontoEntrega;
import br.com.biharckgroup.scr.bean.Uf;


public class ResiduoRecebidoPEVReportFiltro extends FiltroListagem {

	private Date dataIni;
	private Date dataFim;
	private PontoEntrega pontoEntrega;
	
	@DisplayName("Data Inicio")
	public Date getDataIni() {
		return dataIni;
	}
	@DisplayName("Data Fim")
	public Date getDataFim() {
		return dataFim;
	}
	@DisplayName("Credenciais PEV")
	public PontoEntrega getPontoEntrega() {
		return pontoEntrega;
	}
	public void setDataIni(Date dataIni) {
		this.dataIni = dataIni;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public void setPontoEntrega(PontoEntrega pontoEntrega) {
		this.pontoEntrega = pontoEntrega;
	}
}
