package br.com.biharckgroup.scr.adm.relatorio.filtro;


import java.util.List;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.biharckgroup.scr.bean.EnumAtivo;
import br.com.biharckgroup.scr.bean.Papel;

public class UsuarioReportFiltro extends FiltroListagem {

	private EnumAtivo ativo;
	private List<Papel> papeis;

	@DisplayName("Status")
	public EnumAtivo getAtivo() {
		return ativo;
	}
	public List<Papel> getPapeis() {
		return papeis;
	}
	
    public void setAtivo(EnumAtivo ativo) {
		this.ativo = ativo;
	}
    public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}
}
