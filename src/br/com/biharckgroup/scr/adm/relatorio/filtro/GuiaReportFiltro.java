package br.com.biharckgroup.scr.adm.relatorio.filtro;



import java.util.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.validation.annotation.Required;

import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.EnumSituacao;
import br.com.biharckgroup.scr.bean.EnumStatus;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.Transportador;

public class GuiaReportFiltro extends FiltroListagem {

	private EnumStatus status;
	private EnumSituacao situacao;
	private Date dataIni;
	private Date dataFim;
	private DestinoFinal destinoFinal;
	private Transportador transportador;
	private String numeroGuia;
	private String numeroProjeto;
	private String enderecoObra;
	private String placa;
	private GrandeGerador grandeGerador;
	
	public EnumStatus getStatus() {
		return status;
	}
	@DisplayName("Situa��o")
	public EnumSituacao getSituacao() {
		return situacao;
	}
	@DisplayName("Data Inicio")
	public Date getDataIni() {
		return dataIni;
	}
	@DisplayName("Data Fim")
	public Date getDataFim() {
		return dataFim;
	}
	public DestinoFinal getDestinoFinal() {
		return destinoFinal;
	}
	public Transportador getTransportador() {
		return transportador;
	}
	@DisplayName("N�mero da Guia")
	public String getNumeroGuia() {
		return numeroGuia;
	}
	@DisplayName("N�mero do Projeto")
	public String getNumeroProjeto() {
		return numeroProjeto;
	}
	@DisplayName("Endere�o da Obra")
	public String getEnderecoObra() {
		return enderecoObra;
	}
	public String getPlaca() {
		return placa;
	}
	public GrandeGerador getGrandeGerador() {
		return grandeGerador;
	}
	
	public void setStatus(EnumStatus status) {
		this.status = status;
	}
	public void setSituacao(EnumSituacao situacao) {
		this.situacao = situacao;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public void setDataIni(Date dataIni) {
		this.dataIni = dataIni;
	}
	public void setDestinoFinal(DestinoFinal destinoFinal) {
		this.destinoFinal = destinoFinal;
	}
	public void setTransportador(Transportador transportador) {
		this.transportador = transportador;
	}
	public void setNumeroGuia(String numeroGuia) {
		this.numeroGuia = numeroGuia;
	}
	public void setNumeroProjeto(String numeroProjeto) {
		this.numeroProjeto = numeroProjeto;
	}
	public void setEnderecoObra(String enderecoObra) {
		this.enderecoObra = enderecoObra;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setGrandeGerador(GrandeGerador grandeGerador) {
		this.grandeGerador = grandeGerador;
	}
}
