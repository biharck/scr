package br.com.biharckgroup.scr.adm.relatorio.filtro;



import java.util.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.Municipio;
import br.com.biharckgroup.scr.bean.PontoEntrega;
import br.com.biharckgroup.scr.bean.Uf;


public class ResiduoRecebidoGuiaReportFiltro extends FiltroListagem {

	private Date dataIni;
	private Date dataFim;
	private DestinoFinal destinoFinal;
	private Uf uf;
	private Municipio municipio;
	
	
	public DestinoFinal getDestinoFinal() {
		return destinoFinal;
	}
	@DisplayName("UF")
	public Uf getUf() {
		return uf;
	}
	@DisplayName("Município")
	public Municipio getMunicipio() {
		return municipio;
	}
	@DisplayName("Data Inicio Retirada ")
	public Date getDataIni() {
		return dataIni;
	}
	@DisplayName("Data Fim Retirada")
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataIni(Date dataIni) {
		this.dataIni = dataIni;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public void setDestinoFinal(DestinoFinal destinoFinal) {
		this.destinoFinal = destinoFinal;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
}
