package br.com.biharckgroup.scr.adm.relatorio;


import java.awt.Image;
import java.util.Date;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.controller.resource.ResourceGenerationException;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.relatorio.filtro.GuiaReportFiltro;
import br.com.biharckgroup.scr.bean.Arquivo;
import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.Empresa;
import br.com.biharckgroup.scr.bean.EnumSituacao;
import br.com.biharckgroup.scr.bean.EnumStatus;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.dao.ArquivoDAO;
import br.com.biharckgroup.scr.dao.DestinoFinalDAO;
import br.com.biharckgroup.scr.dao.EmpresaDAO;
import br.com.biharckgroup.scr.dao.GuiaDAO;
import br.com.biharckgroup.scr.dao.ProjetoDAO;
import br.com.biharckgroup.scr.dao.TransportadorDAO;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/adm/relatorio/Fiscalizacao",authorizationModule=ReportAuthorizationModule.class)
public class FiscalizacaoReport extends ReportController<GuiaReportFiltro> {
	
	private GuiaDAO guiaDAO;
	private TransportadorDAO transportadorDAO;
	private DestinoFinalDAO destinoFinalDAO;
	
	public void setTransportadorDAO(TransportadorDAO transportadorDAO) {
		this.transportadorDAO = transportadorDAO;
	}
	public void setGuiaDAO(GuiaDAO guiaDAO) {
		this.guiaDAO = guiaDAO;
	}
	public void setDestinoFinalDAO(DestinoFinalDAO destinoFinalDAO) {
		this.destinoFinalDAO = destinoFinalDAO;
	}
	
	@Override
	public IReport createReport(WebRequestContext request, GuiaReportFiltro filtro)throws Exception {
		
		Report report= new Report("guia");

		SCRUtil.addCabecarioReport(report);
		
		String msg="";
		if(filtro.getStatus() != null)
			msg += "Status: " + filtro.getStatus().getNome();
		else
			msg += "Status: Confirmada/Pendente";
		report.addParameter("msg",msg);
		if(filtro.getPlaca()!= null && !filtro.getPlaca().equals(""))
			msg+="  Placa: "+ filtro.getPlaca();
		else
			msg+="  Placa: Todos";
		if(filtro.getNumeroGuia() != null && !filtro.getNumeroGuia().equals(""))
			msg+= " N�mero Guia: "+ filtro.getNumeroGuia();
		else
			msg+="  N�mero Guia: Todos "; 
		
		report.addParameter("msg",msg);
		
		List<Guia> listaGuia = guiaDAO.findByFiltro(filtro);
		report.setDataSource(listaGuia);
		return report;
	}
	@Action("filtroGuia")
	public ModelAndView doFiltro(WebRequestContext request, GuiaReportFiltro filtro) throws ResourceGenerationException {
		List<Guia> listaGuia = guiaDAO.findByFiltro(filtro);
		setAttribute("guias", listaGuia);
		
		/*
		 * Na tela de fiscaliza��o o sistema disponibiliza rela��o de guias
		 * geradas e n�o destinadas ainda
		 */
		filtro.setStatus(EnumStatus.PENDENTE);
		
		return super.doFiltro(request, filtro);
	}
}
