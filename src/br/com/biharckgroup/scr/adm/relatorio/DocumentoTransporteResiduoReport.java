package br.com.biharckgroup.scr.adm.relatorio;


import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.controller.resource.Resource;
import org.nextframework.core.standard.Next;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;
import org.nextframework.util.NextImageResolver;

import br.com.biharckgroup.scr.adm.filtro.DocumentoTransporteResiduoFiltro;
import br.com.biharckgroup.scr.bean.Arquivo;
import br.com.biharckgroup.scr.bean.DocumentoTransporteResiduo;
import br.com.biharckgroup.scr.bean.Empresa;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.dao.ArquivoDAO;
import br.com.biharckgroup.scr.dao.EmpresaDAO;
import br.com.biharckgroup.scr.dao.GuiaDAO;
import br.com.biharckgroup.scr.util.PathUtil;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/adm/relatorio/TransporteResiduo",authorizationModule=ReportAuthorizationModule.class)
public class DocumentoTransporteResiduoReport extends ReportController<DocumentoTransporteResiduoFiltro> {

	private static DocumentoTransporteResiduoReport instance;
	public static DocumentoTransporteResiduoReport getInstance() {
		if(instance == null){
			instance = Next.getObject(DocumentoTransporteResiduoReport.class);
		}
		return instance;
	}
	
	public IReport getReportByProjeto(Projeto projeto,  String chaveDocumentoTransporteResiduo, List<Guia> listaGuia, int guiasGeradas,int guiasConfirmadas,int guiasPendentes, GrandeGerador gg) throws IOException{
		
		Report report = new Report("transporteResiduo");
		
		SCRUtil.addCabecarioReport(report);
		
		String enderecoObraCompleto = projeto.getEnderecoObraCompleto();
		
		StringBuilder texto = new StringBuilder();
		//falta colocar uf e municipio, falta tratar valor null
		texto.append("Prezados,");
		texto.append("\n\nO Grande Gerador, "+ gg.getNomeFantasia() +", CNPJ/CPF " + gg.getCpfCnpjTransient() +" solicitou o Relat�rio de Transporte de Res�duos da obra situada ao endere�o ");
		texto.append(enderecoObraCompleto);
		texto.append(", sob PGRCC "+ projeto.getNumeroProjeto()+".");
		texto.append("\n\nAp�s an�lise dos T�cnicos da Secretaria de Meio Ambiente do Munic�pio de S�o Jos� dos Campos, foi aprovado o Relat�rio de Transporte de Res�duos conforme resumo abaxo.");
		texto.append("\n\n" + guiasGeradas + " Guias foram geradas para este PGRCC e/ou Endere�o citado acima;");
		texto.append("\n\n" + guiasConfirmadas + " Guias foram confirmadas nos destinos adequados;");
		texto.append("\n\n" + guiasPendentes + " Guias pendentes;");
		texto.append("\n\nDesta forma, a Secretaria de Meio Ambiente nada se op�e quanto a destina��o dos res�duos da Obra situada no endere�o ");
		texto.append(enderecoObraCompleto);
		texto.append(", objeto do PGRCC "+ projeto.getNumeroProjeto()+".");	
		
		report.addParameter("texto", texto.toString());
		report.addParameter("localData", "S�o Jos� dos Campos, "+SCRUtil.getDia() + " de  " +SCRUtil.getMes() +" de "+ SCRUtil.getAno());
		
		report.addParameter("chave", chaveDocumentoTransporteResiduo);	
		
		return report;
	}

	public Resource getResourceReportByProjeto(Projeto projeto,  String chaveDocumentoTransporteResiduo, List<Guia> listaGuia,int guiasConfirmadas,int guiasPendentes, int guiasGeradas, GrandeGerador gg) throws IOException{
		IReport report;
		Resource recurso = new Resource();
		report = getReportByProjeto(projeto, chaveDocumentoTransporteResiduo, listaGuia, guiasGeradas, guiasConfirmadas, guiasPendentes, gg);
		String name = getReportName(report);
		byte[] bytes = getReportBytes(report);
		recurso = getPdfResource(name,bytes);
        return recurso;
	}
	
	
	@Override
	public IReport createReport(WebRequestContext request,
			DocumentoTransporteResiduoFiltro filtro) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
