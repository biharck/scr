package br.com.biharckgroup.scr.adm.relatorio;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.standard.Next;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import br.com.biharckgroup.scr.adm.relatorio.filtro.ProjetoReportFiltro;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduoProjeto;
import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.bean.ResponsavelProjeto;
import br.com.biharckgroup.scr.bean.TransportadorProjeto;
import br.com.biharckgroup.scr.service.ProjetoService;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/adm/relatorio/ProjetoCompleto",authorizationModule=ReportAuthorizationModule.class)
public class ProjetoCompletoReport extends ReportController<ProjetoReportFiltro> {
	
	private ProjetoService projetoService;
	
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	
	private static ProjetoCompletoReport instance;
	public static ProjetoCompletoReport getInstance() {
		if(instance == null){
			instance = Next.getObject(ProjetoCompletoReport.class);
		}
		return instance;
	}
	
	@Override
	public IReport createReport(WebRequestContext request, ProjetoReportFiltro filtro)throws Exception {
		Integer id = Integer.parseInt(request.getParameter("idProjeto"));
		Projeto projeto = new Projeto(id);
		return getReportByProjeto(projeto);
	}
	
	public IReport getReportByProjeto(Projeto projeto){
		Report report = new Report("projetoCompleto");

		SCRUtil.addCabecarioReport(report);
		
		report.addParameter("msg","");
		
		projeto = projetoService.loadForEntrada(projeto);
		
		String transportadorStr ="", responsavelStr="";
		for (TransportadorProjeto transportadorProjeto : projeto.getTransportadoresProjeto()) {
			projeto.addTransportadores(transportadorProjeto.getTransportador());
			transportadorStr+=transportadorProjeto.getTransportador().getNomeFantasia()+"\n";
		}
		for (GrupoTipoResiduoProjeto grupoTipoResiduoProjeto : projeto.getGruposTipoResiduoProjeto()) 
			projeto.addGruposTipoResiduo(grupoTipoResiduoProjeto.getGrupoTipoResiduo());
		for (ResponsavelProjeto responsavelProjeto : projeto.getResponsaveisProjeto()) {
			projeto.addResponsaveis(responsavelProjeto.getResponsavel());
			responsavelStr += responsavelProjeto.getResponsavel().getNome()+"\n";
		}

		
		List<Projeto> listaProjeto = new ArrayList<Projeto>();
		listaProjeto.add(projeto);
		report.setDataSource(listaProjeto);
	    report.addSubReport("SUB_TRANSPORTADOR", new Report("sub_transportador"));
        report.addSubReport("SUB_RESPONSAVEL", new Report("sub_responsavel"));
		report.addSubReport("SUB_GRUPOTIPORESIDUOPROJETO", new Report("sub_grupoTipoResiduoProjeto"));
		report.addParameter("transportadorStr", transportadorStr);
		report.addParameter("responsavelStr", responsavelStr);
		return report;
	}
	
	
	/*
	 * M�todo que retorna o relat�rio do projeto em formato pdf
	 */
	public File getFileProjetoCompleto(Projeto projeto)throws Exception {
		IReport report = getReportByProjeto(projeto);
        String name = getReportName(report);
        byte[] bytes = getReportBytes(report);
        return SCRUtil.getFileByDados(name, bytes);
	}
	
	
	
	
}
