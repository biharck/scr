package br.com.biharckgroup.scr.adm.relatorio;


import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.controller.resource.ResourceGenerationException;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.relatorio.filtro.ProjetoReportFiltro;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduoProjeto;
import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.bean.ResponsavelProjeto;
import br.com.biharckgroup.scr.bean.TransportadorProjeto;
import br.com.biharckgroup.scr.dao.ProjetoDAO;
import br.com.biharckgroup.scr.dao.TransportadorDAO;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/adm/relatorio/Projeto",authorizationModule=ReportAuthorizationModule.class)
public class ProjetoReport extends ReportController<ProjetoReportFiltro> {
	
	private ProjetoDAO projetoDAO;
	private TransportadorDAO transportadorDAO;
	
	public void setProjetoDAO(ProjetoDAO projetoDAO) {
		this.projetoDAO = projetoDAO;
	}
	public void setTransportadorDAO(TransportadorDAO transportadorDAO) {
		this.transportadorDAO = transportadorDAO;
	}
	
	@Override
	public IReport createReport(WebRequestContext request, ProjetoReportFiltro filtro)throws Exception {
		
		Report report = new Report("projeto");

		SCRUtil.addCabecarioReport(report);
		
		String msg="";
		if(filtro.getDataInicio() != null || filtro.getDataTermino()!=null){
			String dtIni = filtro.getDataInicio()==null?"":SCRUtil.getDataHora("dd/MM/yyyy",filtro.getDataInicio());
			String dtFim = filtro.getDataTermino()==null?"":SCRUtil.getDataHora("dd/MM/yyyy",filtro.getDataTermino());
			msg = "Per�odo "+  dtIni+ " at� "+dtFim;
		}
		if(filtro.getEnumStatusProjeto() != null)
			msg += "   Status: " + filtro.getEnumStatusProjeto().getNome();
		else
			msg += "   Status: Criado/Iniciado/Finalizado";
		if(filtro.getTransportador() != null)
			msg += "   Transportador: " + transportadorDAO.findNomeFantasiaById(filtro.getTransportador()).getNomeFantasia();
		else
			msg += "   Transportador: Todos";
		
		
		report.addParameter("msg",msg);
		
		List<Projeto> listaProjeto = projetoDAO.findByFiltro(filtro);
		
		for (Projeto projeto : listaProjeto) {
			for (TransportadorProjeto transportadorProjeto : projeto.getTransportadoresProjeto()) 
				projeto.addTransportadores(transportadorProjeto.getTransportador());
			for (GrupoTipoResiduoProjeto grupoTipoResiduoProjeto : projeto.getGruposTipoResiduoProjeto()) 
				projeto.addGruposTipoResiduo(grupoTipoResiduoProjeto.getGrupoTipoResiduo());
			for (ResponsavelProjeto responsavelProjeto : projeto.getResponsaveisProjeto()) 
				projeto.addResponsaveis(responsavelProjeto.getResponsavel());
		}
		
		report.setDataSource(listaProjeto);
        report.addSubReport("SUB_TRANSPORTADOR", new Report("sub_transportador"));
        report.addSubReport("SUB_GRUPOTIPORESIDUO", new Report("sub_grupoTipoResiduo"));
        report.addSubReport("SUB_RESPONSAVEL", new Report("sub_responsavel"));
        
		return report;
	}
	@Action("filtroProjeto")
	public ModelAndView doFiltro(WebRequestContext request, ProjetoReportFiltro filtro) throws ResourceGenerationException {
		setAttribute("projetos", projetoDAO.findByFiltro(filtro));
		return super.doFiltro(request, filtro);
	}
	
	@Action("imprimirProjetoCompleto")
	public IReport imprimirProjetoCompletoReport (WebRequestContext request, ProjetoReportFiltro filtro)throws Exception {
		
		Report report = new Report("projeto");
		SCRUtil.addCabecarioReport(report);
		
		report.addParameter("msg", "Per�odo "+ filtro.getDataInicio() +" a "+filtro.getDataTermino());
		
		List<Projeto> listaProjeto = projetoDAO.findByFiltro(filtro);
		
		for (Projeto projeto : listaProjeto) {
			for (TransportadorProjeto transportadorProjeto : projeto.getTransportadoresProjeto()) 
				projeto.addTransportadores(transportadorProjeto.getTransportador());
			for (GrupoTipoResiduoProjeto grupoTipoResiduoProjeto : projeto.getGruposTipoResiduoProjeto()) 
				projeto.addGruposTipoResiduo(grupoTipoResiduoProjeto.getGrupoTipoResiduo());
			for (ResponsavelProjeto responsavelProjeto : projeto.getResponsaveisProjeto()) 
				projeto.addResponsaveis(responsavelProjeto.getResponsavel());
		}
		
		report.setDataSource(listaProjeto);
        report.addSubReport("SUB_TRANSPORTADOR", new Report("sub_transportador"));
        report.addSubReport("SUB_GRUPOTIPORESIDUO", new Report("sub_grupoTipoResiduo"));
        report.addSubReport("SUB_RESPONSAVEL", new Report("sub_responsavel"));
        
		return report;
	}
	
	
	
}
