package br.com.biharckgroup.scr.adm.relatorio;


import java.util.ArrayList;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import br.com.biharckgroup.scr.adm.relatorio.filtro.GuiaReportFiltro;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.dao.GuiaDAO;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/adm/relatorio/GuiaCompleta",authorizationModule=ReportAuthorizationModule.class)
public class GuiaCompletaReport extends ReportController<GuiaReportFiltro> {
	
	private GuiaDAO guiaDAO;
	
	public void setGuiaDAO(GuiaDAO guiaDAO) {
		this.guiaDAO = guiaDAO;
	}
	
	@Override
	public IReport createReport(WebRequestContext request, GuiaReportFiltro filtro)throws Exception {
		
		Integer id = Integer.parseInt(request.getParameter("idGuia"));
		Guia guia = new Guia(id);
		
		Report report;
		String rel = request.getParameter("rel");
		if(rel!=null && rel.equals("CTR"))
			report = new Report("CTR");
		else
			report = new Report("guiaCompleta");
			
		SCRUtil.addCabecarioReport(report);
		
		String msg ="";
		report.addParameter("msg", msg);
		
		guia = guiaDAO.loadForEntrada(guia);
		
		List<Guia> listaGuia = new ArrayList<Guia>();
		listaGuia.add(guia);
		report.setDataSource(listaGuia);
		report.addSubReport("SUB_RESIDUOGUIA", new Report("sub_residuoGuia"));
		return report;
	}
	
}
