package br.com.biharckgroup.scr.adm.relatorio;


import java.awt.Image;
import java.util.Date;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.controller.resource.ResourceGenerationException;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.relatorio.filtro.EntradaResiduoPEVReportFiltro;
import br.com.biharckgroup.scr.bean.Arquivo;
import br.com.biharckgroup.scr.bean.Empresa;
import br.com.biharckgroup.scr.bean.EntradaResiduoPEV;
import br.com.biharckgroup.scr.bean.PontoEntrega;
import br.com.biharckgroup.scr.dao.ArquivoDAO;
import br.com.biharckgroup.scr.dao.EmpresaDAO;
import br.com.biharckgroup.scr.dao.EntradaResiduoPEVDAO;
import br.com.biharckgroup.scr.dao.PontoEntregaDAO;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/adm/relatorio/EntradaResiduoPEV",authorizationModule=ReportAuthorizationModule.class)
public class EntradaResiduoPEVReport extends ReportController<EntradaResiduoPEVReportFiltro> {
	
	private EntradaResiduoPEVDAO entradaResiduoPEVDAO;
	private PontoEntregaDAO pontoEntregaDAO;
	
	public void setEntradaResiduoPEVDAO(
			EntradaResiduoPEVDAO entradaResiduoPEVDAO) {
		this.entradaResiduoPEVDAO = entradaResiduoPEVDAO;
	}
	public void setPontoEntregaDAO(PontoEntregaDAO pontoEntregaDAO) {
		this.pontoEntregaDAO = pontoEntregaDAO;
	}
	
	@Override
	public IReport createReport(WebRequestContext request, EntradaResiduoPEVReportFiltro filtro)throws Exception {
		
		Report report = new Report("entradaResiduoPEV");
		SCRUtil.addCabecarioReport(report);
		
		String msg="";
		if(filtro.getDataIni() != null || filtro.getDataFim()!=null){
			String dtIni = filtro.getDataIni()==null?"":SCRUtil.getDataHora("dd/MM/yyyy",filtro.getDataIni());
			String dtFim = filtro.getDataFim()==null?"":SCRUtil.getDataHora("dd/MM/yyyy",filtro.getDataFim());
			msg = "Per�odo: "+  dtIni+ " at� "+dtFim;
		}
		if(filtro.getPontoEntrega()!=null)
			msg += "   PEV: " + pontoEntregaDAO.findIdentificadorById(filtro.getPontoEntrega()).getIdentificacao();
		else
			msg += "   PEV: Todos";
		report.addParameter("msg",msg);
		
		List<EntradaResiduoPEV> listaEntrada = entradaResiduoPEVDAO.findByFiltro(filtro);
		report.setDataSource(listaEntrada);
        report.addSubReport("SUB_GRUPOTIPORESIDUOENTRADASAIDA", new Report("sub_grupoTipoResiduoEntradaSaida"));
        
		return report;
	}
	
	@Action("filtroEntradaResiduoPEV")
	public ModelAndView doFiltro(WebRequestContext request, EntradaResiduoPEVReportFiltro filtro) throws ResourceGenerationException {
		setAttribute("entradasResiduoPEV", entradaResiduoPEVDAO.findByFiltro(filtro));
		return super.doFiltro(request, filtro);
	}
	
}
