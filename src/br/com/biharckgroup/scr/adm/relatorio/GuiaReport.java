package br.com.biharckgroup.scr.adm.relatorio;


import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.controller.resource.ResourceGenerationException;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.relatorio.filtro.GuiaReportFiltro;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.dao.DestinoFinalDAO;
import br.com.biharckgroup.scr.dao.GuiaDAO;
import br.com.biharckgroup.scr.dao.TransportadorDAO;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/adm/relatorio/Guia",authorizationModule=ReportAuthorizationModule.class)
public class GuiaReport extends ReportController<GuiaReportFiltro> {
	
	private GuiaDAO guiaDAO;
	private TransportadorDAO transportadorDAO;
	private DestinoFinalDAO destinoFinalDAO;
	
	public void setTransportadorDAO(TransportadorDAO transportadorDAO) {
		this.transportadorDAO = transportadorDAO;
	}
	public void setGuiaDAO(GuiaDAO guiaDAO) {
		this.guiaDAO = guiaDAO;
	}
	public void setDestinoFinalDAO(DestinoFinalDAO destinoFinalDAO) {
		this.destinoFinalDAO = destinoFinalDAO;
	}
	
	@Override
	public IReport createReport(WebRequestContext request, GuiaReportFiltro filtro)throws Exception {
		
		Report report= new Report("guia");

		SCRUtil.addCabecarioReport(report);
		
		String msg="";
		if(filtro.getDataIni() != null || filtro.getDataFim()!=null){
			String dtIni = filtro.getDataIni()==null?"":SCRUtil.getDataHora("dd/MM/yyyy",filtro.getDataIni());
			String dtFim = filtro.getDataFim()==null?"":SCRUtil.getDataHora("dd/MM/yyyy",filtro.getDataFim());
			msg = "Per�odo "+  dtIni+ " at� "+dtFim;
		}
		if(filtro.getSituacao() != null)
			msg += "   Situa��o: " + filtro.getSituacao().getNome();
		else
			msg += "   Status: Aceita/N�o Aceita/Sem Situa��o";
		if(filtro.getStatus() != null)
			msg += "   Status: " + filtro.getStatus().getNome();
		else
			msg += "   Status: Confirmada/Pendente";
		if(filtro.getTransportador() != null)
			msg += "   Transportador: " + transportadorDAO.findNomeFantasiaById(filtro.getTransportador()).getNomeFantasia();
		else
			msg += "   Transportador: Todos";
		if(filtro.getDestinoFinal() != null)
			msg += "   Destino Final: " + destinoFinalDAO.findNomeFantasiaById(filtro.getDestinoFinal()).getNomeFantasia();
		else
			msg += "   Destino Final: Todos";
		
		report.addParameter("msg",msg);
		
		List<Guia> listaGuia = guiaDAO.findByFiltro(filtro);
		report.setDataSource(listaGuia);
		return report;
	}
	@Action("filtroGuia")
	public ModelAndView doFiltro(WebRequestContext request, GuiaReportFiltro filtro) throws ResourceGenerationException {
		
		Usuario usuario = SCRUtil.getUsuarioLogado();
		if(SCRUtil.isPessoaLogadaGrandeGerador()){
			if(usuario.getGrandeGerador() == null)
				throw new SCRException("N�o existe v�nculo do usu�rio logado com nenhum Grande Gerador.");
			filtro.setGrandeGerador(usuario.getGrandeGerador());
		}
		if(SCRUtil.isPessoaLogadaDestinoFinal()){
			if(usuario.getDestinoFinal() == null)
				throw new SCRException("N�o existe v�nculo do usu�rio logado com nenhum Destino Final.");
			filtro.setDestinoFinal(usuario.getDestinoFinal());
		}
		if(SCRUtil.isPessoaLogadaTransportador()){
			if(usuario.getTransportador() == null)
				throw new SCRException("N�o existe v�nculo do usu�rio logado com nenhum Transportador.");
			filtro.setTransportador(usuario.getTransportador());
		}
		
		List<Guia> listaGuia = guiaDAO.findByFiltro(filtro);
		setAttribute("guias", listaGuia);
		return super.doFiltro(request, filtro);
	}
}
