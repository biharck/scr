package br.com.biharckgroup.scr.adm.relatorio;


import java.awt.Image;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.controller.resource.ResourceGenerationException;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.relatorio.filtro.ResiduoRecebidoPEVReportFiltro;
import br.com.biharckgroup.scr.bean.Arquivo;
import br.com.biharckgroup.scr.bean.Empresa;
import br.com.biharckgroup.scr.bean.EntradaResiduoPEV;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduo;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduoEntradaResiduoPEV;
import br.com.biharckgroup.scr.bean.ResiduoRecebido;
import br.com.biharckgroup.scr.bean.ResiduoRecebidoQuantidade;
import br.com.biharckgroup.scr.bean.TipoResiduo;
import br.com.biharckgroup.scr.bean.TipoResiduoQuantidade;
import br.com.biharckgroup.scr.dao.ArquivoDAO;
import br.com.biharckgroup.scr.dao.EmpresaDAO;
import br.com.biharckgroup.scr.dao.EntradaResiduoPEVDAO;
import br.com.biharckgroup.scr.dao.PontoEntregaDAO;
import br.com.biharckgroup.scr.dao.TipoResiduoDAO;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path="/adm/relatorio/ResiduoRecebidoPEV",authorizationModule=ReportAuthorizationModule.class)
public class ResiduoRecebidoPEVReport extends ReportController<ResiduoRecebidoPEVReportFiltro> {

	
	private EntradaResiduoPEVDAO entradaResiduoPEVDAO;
	private PontoEntregaDAO pontoEntregaDAO;
	private TipoResiduoDAO tipoResiduoDAO;
	
	public void setEntradaResiduoPEVDAO(
			EntradaResiduoPEVDAO entradaResiduoPEVDAO) {
		this.entradaResiduoPEVDAO = entradaResiduoPEVDAO;
	}
	public void setPontoEntregaDAO(PontoEntregaDAO pontoEntregaDAO) {
		this.pontoEntregaDAO = pontoEntregaDAO;
	}
	public void setTipoResiduoDAO(TipoResiduoDAO tipoResiduoDAO) {
		this.tipoResiduoDAO = tipoResiduoDAO;
	}
	
	@Override
	public IReport createReport(WebRequestContext request, ResiduoRecebidoPEVReportFiltro filtro)throws Exception {
		
		
		Report report = new Report("residuoRecebidoPEV");
		SCRUtil.addCabecarioReport(report);
		
		String msg="";
		if(filtro.getDataIni() != null || filtro.getDataFim()!=null){
			String dtIni = filtro.getDataIni()==null?"":SCRUtil.getDataHora("dd/MM/yyyy",filtro.getDataIni());
			String dtFim = filtro.getDataFim()==null?"":SCRUtil.getDataHora("dd/MM/yyyy",filtro.getDataFim());
			msg = "Per�odo: "+  dtIni+ " at� "+dtFim;
		}
		if(filtro.getPontoEntrega()!=null)
			msg += "   PEV: " + pontoEntregaDAO.findIdentificadorById(filtro.getPontoEntrega()).getIdentificacao();
		else
			msg += "   PEV: Todos";
		report.addParameter("msg",msg);
		
		List<ResiduoRecebido> lista = buscaDados(request, filtro);

		report.setDataSource(lista);
		return report;
	}
	
	/*
	 * M�tdo respons�vel por buscar entradas realizadas ordenadas pela data
	 */
	@Action("filtroResiduosRecebidos")
	@Override
	public ModelAndView doFiltro(WebRequestContext request, ResiduoRecebidoPEVReportFiltro filtro) throws ResourceGenerationException {
		buscaDados(request, filtro);
		return super.doFiltro(request, filtro);
	}
	
	/*
	 * Para relatorio Ireport envia-se estrutura Lista de residuoRecebidos (data tipoResiduo quant) formato Crosstab
	 * ex: data tipoResiduo quant
	 *      12     x          1
	 *      12     y          2
	 *      13     y          3
	 * Para relatorio tela envia-se estrutura Lista de uma Lista residuoRecebidoQuantidades (data tipoResiduo quant) onde a data � a mesma
	 * ex: data 	quant
	 *      12       1 	2
	 *      13       3
	 */
	
	public List<ResiduoRecebido> buscaDados(WebRequestContext request, ResiduoRecebidoPEVReportFiltro filtro){
		List<EntradaResiduoPEV> entradaResiduoPEVs =  entradaResiduoPEVDAO.findByFiltro(filtro); 
		
		//lista com todos tipos de residuos
		List<ResiduoRecebido> subResiduoRecebidos = new ArrayList<ResiduoRecebido>();

		List<ResiduoRecebido> residuoRecebidos = new ArrayList<ResiduoRecebido>();
		List<TipoResiduo> tipoResiduos = tipoResiduoDAO.findAll();
		
		//enquanto a data da entradaResiduoPEV for a mesma adiciona registros na estrutura TipoResiduoQuantidade
		for (int i = 0; i < entradaResiduoPEVs.size(); i++) {
			EntradaResiduoPEV entradaResiduoPEV = entradaResiduoPEVs.get(i);
			
			//limpa sub grupo de valores -> primeira vez ou trocar de dia
			if(i==0 || entradaResiduoPEVs.get(i).getData() != entradaResiduoPEVs.get(i-1).getData()){ 
				//cria lista com todos tipos de residuos, data entrada e quantidade 0
				subResiduoRecebidos = new ArrayList<ResiduoRecebido>();
				for (TipoResiduo tipoResiduo : tipoResiduos) {
					ResiduoRecebido  r = new ResiduoRecebido(entradaResiduoPEV.getData(), tipoResiduo,0);
					subResiduoRecebidos.add(r);
				}
			}
			
			//entrada do mesmo dia, soma-se quantidades
			for (GrupoTipoResiduoEntradaResiduoPEV grupoTipoResiduoEntradaResiduoPEV : entradaResiduoPEV.getGrupoTipoResiduosEntradaResiduoPEV())
				addQuantidadeListaTipoResiduoQuantidade(subResiduoRecebidos, grupoTipoResiduoEntradaResiduoPEV.getTipoResiduo(),grupoTipoResiduoEntradaResiduoPEV.getQuantidade());
			
			//caso pr�ximo reg tenha data diferente ou seja null adiciona obj na lista
			if(i+1 == entradaResiduoPEVs.size() ||  entradaResiduoPEVs.get(i).getData() != entradaResiduoPEVs.get(i+1).getData())
				residuoRecebidos.addAll(subResiduoRecebidos);
		}
		
		setAttribute("residuoRecebidoQuantidades",defineResiduoRecebidoTela(residuoRecebidos,tipoResiduos.size()));
		setAttribute("tipoResiduos",tipoResiduos);
		setAttribute("residuoRecebidos", residuoRecebidos);
		
		
		return residuoRecebidos;
	}
	
	
	public void addQuantidadeListaTipoResiduoQuantidade(List<ResiduoRecebido> subResiduoRecebidos1, TipoResiduo tipoResiduo, float quantidade){
		for (ResiduoRecebido residuoRecebido : subResiduoRecebidos1) {
			if(residuoRecebido.getTipoResiduo().getId().equals(tipoResiduo.getId())){
				residuoRecebido.addQuantidade(quantidade);
				break;
			}
		}
	}
	
	
	public List<ResiduoRecebidoQuantidade> defineResiduoRecebidoTela(List<ResiduoRecebido> residuoRecebidos, int quantidadeTipoResiduos){
		List<ResiduoRecebidoQuantidade> residuoRecebidoQuantidades = new ArrayList<ResiduoRecebidoQuantidade>();	
		float [] quantidades = new float[quantidadeTipoResiduos];
		int pQuantidades=0;
		for (int i = 0; i < residuoRecebidos.size(); i++) {
			ResiduoRecebido residuoRecebido = residuoRecebidos.get(i);
			quantidades[pQuantidades++] = residuoRecebido.getQuantidade();

			//caso pr�ximo reg tenha data diferente ou seja null adiciona obj na lista
			if(i+1 == residuoRecebidos.size() ||  residuoRecebidos.get(i).getData() != residuoRecebidos.get(i+1).getData()){
				residuoRecebidoQuantidades.add(new ResiduoRecebidoQuantidade(residuoRecebido.getData(), quantidades));
				quantidades = new float[quantidadeTipoResiduos];
				pQuantidades=0;
			}
		}
		return	residuoRecebidoQuantidades;
	}
}
