package br.com.biharckgroup.scr.autenticacao.controller;

import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

@Controller(path="/autenticacao/Logout")
public class LogoutController extends MultiActionController {
	@DefaultAction
	public ModelAndView doLogout(WebRequestContext request){
		request.getSession().invalidate();
		return(new ModelAndView("redirect:/"));
	}
}