package br.com.biharckgroup.scr.service;

import java.util.List;

import org.nextframework.core.standard.Next;
import org.nextframework.service.GenericService;

import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.dao.ProjetoDAO;

public class ProjetoService extends GenericService<Projeto> {
	
	private static ProjetoService instance;
	private ProjetoDAO projetoDAO;
	
	public void setProjetoDAO(ProjetoDAO projetoDAO) {
		this.projetoDAO = projetoDAO;
	}
	
	public static ProjetoService getInstance() {
		if(instance == null){
			instance = Next.getObject(ProjetoService.class);
		}
		return instance;
	}
	
	public Projeto loadByNumeroProjeto(String  numeroProjeto) {
		return projetoDAO.loadByNumeroProjeto(numeroProjeto);
	}
	
	public List<Projeto> getProjetosByDestinoFinal(DestinoFinal df){
		return projetoDAO.getProjetosByDestinoFinal(df);
	}
	public List<Projeto> getProjetosDiaByDestinoFinal(DestinoFinal df){
		return projetoDAO.getProjetosDiaByDestinoFinal(df);
	}
	
	public List<Projeto> getProjetosByGrandeGerador(GrandeGerador gg){
		return projetoDAO.getProjetosByGrandeGerador(gg);
	}
}
