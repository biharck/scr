package br.com.biharckgroup.scr.service;

import java.util.List;

import org.nextframework.service.GenericService;

import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.dao.GrandeGeradorDAO;

public class GrandeGeradorService extends GenericService<GrandeGerador> {
		
	private GrandeGeradorDAO grandeGeradorDAO;
	public void setGrandeGeradorDAO(GrandeGeradorDAO grandeGeradorDAO) {
		this.grandeGeradorDAO = grandeGeradorDAO;
	}
	
	@Override
	public void saveOrUpdate(GrandeGerador bean) {
		if(bean.getId()==null)
			bean.setAtivo(true);
		else{
			GrandeGerador tmp = load(bean);
			bean.setAutoCadastro(tmp.isAutoCadastro());
		}
		super.saveOrUpdate(bean);
	}
	
	public List<GrandeGerador> getGrandesGeradoresAutoCadastro(){
		return grandeGeradorDAO.getGrandesGeradoresAutoCadastro();
	}
}
