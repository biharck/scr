package br.com.biharckgroup.scr.service;


import org.nextframework.service.GenericService;

import br.com.biharckgroup.scr.bean.TipoTransporte;


public class TipoTransporteService extends GenericService<TipoTransporte> {
		
	@Override
	public void saveOrUpdate(TipoTransporte bean) {
		if(bean.getId()==null)
			bean.setAtivo(true);
		super.saveOrUpdate(bean);
	}
	
}
