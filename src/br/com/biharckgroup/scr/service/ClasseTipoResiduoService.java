package br.com.biharckgroup.scr.service;

import org.nextframework.service.GenericService;

import br.com.biharckgroup.scr.bean.ClasseTipoResiduo;


public class ClasseTipoResiduoService extends GenericService<ClasseTipoResiduo> {
		
	@Override
	public void saveOrUpdate(ClasseTipoResiduo bean) {
		if(bean.getId()==null)
			bean.setAtivo(true);
		super.saveOrUpdate(bean);
	}
}
