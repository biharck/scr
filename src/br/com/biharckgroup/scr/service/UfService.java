package br.com.biharckgroup.scr.service;

import org.nextframework.service.GenericService;

import br.com.biharckgroup.scr.bean.Municipio;
import br.com.biharckgroup.scr.bean.Uf;
import br.com.biharckgroup.scr.dao.UfDAO;

public class UfService extends GenericService<Uf> {
	
	private UfDAO ufDAO;
	public void setUfDAO(UfDAO ufDAO) {
		this.ufDAO = ufDAO;
	}
	
	public Uf getUfByMunicipio(Municipio m){
		return ufDAO.getUfByMunicipio(m);
	}	
}
