package br.com.biharckgroup.scr.service;

import java.util.List;

import org.nextframework.service.GenericService;

import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.dao.DestinoFinalDAO;


public class DestinoFinalService extends GenericService<DestinoFinal> {
	
	private DestinoFinalDAO destinoFinalDAO;
	
	public void setDestinoFinalDAO(DestinoFinalDAO destinoFinalDAO) {
		this.destinoFinalDAO = destinoFinalDAO;
	}
	
	@Override
	public void saveOrUpdate(DestinoFinal bean) {
		if(bean.getId()==null && !bean.isAutoCadastro())
			bean.setAtivo(true);
		super.saveOrUpdate(bean);
	}
	
	public List<DestinoFinal> getDestinosAutoCadastro(){
		return destinoFinalDAO.getDestinosAutoCadastro();
	}
	
	public void addCapacidadeAtual(double valorEntrada,DestinoFinal destinoFinal){
		destinoFinalDAO.addCapacidadeAtual(valorEntrada, destinoFinal);
	}
	
	public boolean isCapacidadeMaiorIgualByPorcentagem(int xPorcento ,DestinoFinal destinoFinal){
		DestinoFinal bean = destinoFinalDAO.getCapacidadeAtualAndCapacidadeTotal(destinoFinal);
		if(bean.getCapacidadeAtual() >= (xPorcento/100.0) * bean.getCapacidadeTotal())
			return true;
		else
			return false;
	}
	
	public boolean isCapacidadeMaiorIgualByPorcentagem(int xPorcento ,double capacidadeAtual, double capacidadeTotal){
		if(capacidadeAtual >= (xPorcento/100.0) * capacidadeTotal)
			return true;
		else
			return false;
	}
	
	public DestinoFinal findNomeFantasiaById(DestinoFinal bean){
		return destinoFinalDAO.findNomeFantasiaById(bean);
	}
}
