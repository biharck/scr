package br.com.biharckgroup.scr.service;

import java.util.List;

import org.nextframework.service.GenericService;

import br.com.biharckgroup.scr.bean.GrupoTipoResiduo;
import br.com.biharckgroup.scr.bean.TipoResiduo;
import br.com.biharckgroup.scr.dao.TipoResiduoDAO;


public class TipoResiduoService extends GenericService<TipoResiduo> {
		
	private TipoResiduoDAO tipoResiduoDAO;
	public void setTipoResiduoDAO(TipoResiduoDAO tipoResiduoDAO) {
		this.tipoResiduoDAO = tipoResiduoDAO;
	}
	
	public List<TipoResiduo> getTiposResiduosByGrupo(GrupoTipoResiduo grupoTipoResiduo){
		return tipoResiduoDAO.getTiposResiduosByGrupo(grupoTipoResiduo);
	}
	
	
	@Override
	public void saveOrUpdate(TipoResiduo bean) {
		if(bean.getId()==null)
			bean.setAtivo(true);

		super.saveOrUpdate(bean);
	}
}
