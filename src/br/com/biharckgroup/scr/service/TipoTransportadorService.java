package br.com.biharckgroup.scr.service;

import org.nextframework.service.GenericService;

import br.com.biharckgroup.scr.bean.TipoTransportador;


public class TipoTransportadorService extends GenericService<TipoTransportador> {

	@Override
	public void saveOrUpdate(TipoTransportador bean) {
		bean.setGrandeGerador(bean.getEnumTipoGerador().isOptBool());
		super.saveOrUpdate(bean);
	}
	
}
