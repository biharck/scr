package br.com.biharckgroup.scr.service;

import org.nextframework.service.GenericService;

import br.com.biharckgroup.scr.bean.TipoDestino;


public class TipoDestinoService extends GenericService<TipoDestino> {
		
	@Override
	public void saveOrUpdate(TipoDestino bean) {
		if(bean.getId()==null)
			bean.setAtivo(true);

		super.saveOrUpdate(bean);
	}
	
}
