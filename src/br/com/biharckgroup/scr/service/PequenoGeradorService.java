package br.com.biharckgroup.scr.service;

import java.util.List;

import org.nextframework.service.GenericService;

import br.com.biharckgroup.scr.bean.PequenoGerador;
import br.com.biharckgroup.scr.dao.PequenoGeradorDAO;

public class PequenoGeradorService extends GenericService<PequenoGerador>{

	private PequenoGeradorDAO pequenoGeradorDAO;
	public void setPequenoGeradorDAO(PequenoGeradorDAO pequenoGeradorDAO) {
		this.pequenoGeradorDAO = pequenoGeradorDAO;
	}
	
	@Override
	public void saveOrUpdate(PequenoGerador bean) {
		if(bean.getId()==null){
			bean.setAtivo(true);		
		}else{
			PequenoGerador tmp = load(bean);
			bean.setAutoCadastro(tmp.isAutoCadastro());
		}
		super.saveOrUpdate(bean);
	}
	
	public List<PequenoGerador> getPequenosGeradoresAutoCadastro(){
		return pequenoGeradorDAO.getPequenosGeradoresAutoCadastro();
	}
}
