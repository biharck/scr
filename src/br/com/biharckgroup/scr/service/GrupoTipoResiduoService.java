package br.com.biharckgroup.scr.service;

import org.nextframework.service.GenericService;
import br.com.biharckgroup.scr.bean.GrupoTipoResiduo;


public class GrupoTipoResiduoService extends GenericService<GrupoTipoResiduo> {

	@Override
	public void saveOrUpdate(GrupoTipoResiduo bean) {
		if(bean.getId()==null)
			bean.setAtivo(true);

		super.saveOrUpdate(bean);
	}
	
}
