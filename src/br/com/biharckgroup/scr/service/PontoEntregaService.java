package br.com.biharckgroup.scr.service;

import org.nextframework.service.GenericService;
import br.com.biharckgroup.scr.bean.PontoEntrega;
import br.com.biharckgroup.scr.dao.PontoEntregaDAO;


public class PontoEntregaService extends GenericService<PontoEntrega> {
	
	private PontoEntregaDAO pontoEntregaDAO;
	
	public void setPontoEntregaDAO(PontoEntregaDAO pontoEntregaDAO) {
		this.pontoEntregaDAO = pontoEntregaDAO;
	}
	
	public PontoEntrega findIdentificadorById(PontoEntrega bean){
		return pontoEntregaDAO.findIdentificadorById(bean);
	}
}
