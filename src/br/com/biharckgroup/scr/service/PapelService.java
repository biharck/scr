package br.com.biharckgroup.scr.service;

import java.util.List;

import org.nextframework.service.GenericService;



import br.com.biharckgroup.scr.bean.Papel;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.dao.PapelDAO;

public class PapelService extends GenericService<Papel> {

	private PapelDAO papelDAO;
	public void setPapelDAO(PapelDAO papelDAO) {
		this.papelDAO = papelDAO;
	}
	
	public List<Papel> findByUsuario(Usuario usuario) {
		return papelDAO.findByUsuario(usuario);
	}
	
	public Papel findNomeById(Papel bean){
		return papelDAO.findNomeById(bean);
	}
	
}
