package br.com.biharckgroup.scr.service;

import java.util.List;

import org.nextframework.service.GenericService;

import br.com.biharckgroup.scr.bean.Transportador;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.dao.TransportadorDAO;


public class TransportadorService extends GenericService<Transportador> {
	
	private TransportadorDAO transportadorDAO;
	
	public void setTransportadorDAO(TransportadorDAO transportadorDAO) {
		this.transportadorDAO = transportadorDAO;
	}
	
	public Transportador findByUsuario(Usuario usuario) {
		return transportadorDAO.findByUsuario(usuario);
	}
	
	public List<Transportador> getTransportadoresAutoCadastro(){
		return transportadorDAO.getTransportadoresAutoCadastro();
	}
	
	public Transportador findRazaoSocialById(Transportador bean){
		return transportadorDAO.findNomeFantasiaById(bean);
	}
}
