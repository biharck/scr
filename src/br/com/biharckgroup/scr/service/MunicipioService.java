package br.com.biharckgroup.scr.service;

import org.nextframework.service.GenericService;

import br.com.biharckgroup.scr.bean.Municipio;
import br.com.biharckgroup.scr.dao.MunicipioDAO;



public class MunicipioService extends GenericService<Municipio> {
	
	private MunicipioDAO municipioDAO;
	public void setMunicipioDAO(MunicipioDAO municipioDAO) {
		this.municipioDAO = municipioDAO;
	}
	
	public Municipio loadByNome(String  nome){
		return municipioDAO.loadByNome(nome);
	}
	
}
