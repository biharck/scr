package br.com.biharckgroup.scr.service;

import java.util.List;

import org.nextframework.service.GenericService;

import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.GrandeGerador;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.dao.GuiaDAO;

public class GuiaService extends GenericService<Guia> {

	private GuiaDAO guiaDAO;

	public void setGuiaDAO(GuiaDAO guiaDAO) {
		this.guiaDAO = guiaDAO;
	}

	public List<Guia> getGuiasByDestinoFinal(DestinoFinal df) {
		return guiaDAO.getGuiasByDestinoFinal(df);
	}
	public List<Guia> getGuiasByDestinoFinal(DestinoFinal df, String numeroGuia) {
		return guiaDAO.getGuiasByDestinoFinal(df, numeroGuia);
	}
	public List<Guia> getGuiasByDestinoFinal(DestinoFinal df, boolean status){
		return guiaDAO.getGuiasByDestinoFinal(df, status);
	}
	public List<Guia> getGuiasDiaByDestinoFinal(DestinoFinal df){
		return guiaDAO.getGuiasDiaByDestinoFinal(df);
	}
	
	public List<Guia> getGuiasByGrandeGerador(GrandeGerador gg, boolean status) {
		return guiaDAO.getGuiasByGrandeGerador(gg, status);
	}
	
	public void recebimentoGuia(Guia bean) {
		guiaDAO.recebimentoGuia(bean);
	}

}
