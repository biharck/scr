package br.com.biharckgroup.scr.transportador.controller.crud;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.util.CollectionsUtil;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.biharckgroup.scr.adm.controller.CrudControllerSCR;
import br.com.biharckgroup.scr.adm.filtro.GuiaFiltro;
import br.com.biharckgroup.scr.bean.DestinoFinal;
import br.com.biharckgroup.scr.bean.EnumCancelada;
import br.com.biharckgroup.scr.bean.EnumSituacao;
import br.com.biharckgroup.scr.bean.EnumStatus;
import br.com.biharckgroup.scr.bean.Guia;
import br.com.biharckgroup.scr.bean.GuiaDestinoFinal;
import br.com.biharckgroup.scr.bean.GuiaTransportador;
import br.com.biharckgroup.scr.bean.Projeto;
import br.com.biharckgroup.scr.bean.ResiduoGuia;
import br.com.biharckgroup.scr.bean.Usuario;
import br.com.biharckgroup.scr.service.DestinoFinalService;
import br.com.biharckgroup.scr.service.GuiaDestinoFinalService;
import br.com.biharckgroup.scr.service.GuiaTransportadorService;
import br.com.biharckgroup.scr.service.ProjetoService;
import br.com.biharckgroup.scr.service.TransportadorService;
import br.com.biharckgroup.scr.service.UsuarioService;
import br.com.biharckgroup.scr.util.SCRException;
import br.com.biharckgroup.scr.util.SCRUtil;

@Controller(path = "/transportador/crud/GuiaTransportador", authorizationModule = CrudAuthorizationModule.class)
public class GuiaTransportadorCrud extends CrudControllerSCR<GuiaFiltro, GuiaTransportador, GuiaTransportador> {

	private GuiaTransportadorService guiaTransportadorService;
	private UsuarioService usuarioService;
	private DestinoFinalService destinoFinalService;
	
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setGuiaTransportadorService(
			GuiaTransportadorService guiaTransportadorService) {
		this.guiaTransportadorService = guiaTransportadorService;
	}
	public void setDestinoFinalService(DestinoFinalService destinoFinalService) {
		this.destinoFinalService = destinoFinalService;
	}
	
	
	@Override
	protected void salvar(WebRequestContext request, GuiaTransportador bean ){
		
		bean.setTransportador(SCRUtil.getUsuarioLogado().getTransportador());//********transportador s� gera guias dele
		GuiaTransportador load = null;
		if(bean.getId()!=null) //update
			load = guiaTransportadorService.load(bean);
		
		//define campos desalibitados
		if(bean.getProjeto()!=null){
			//caso proj seja inf a guia ter� gg e end da obra como os do projeto
			Projeto proj = ProjetoService.getInstance().loadByNumeroProjeto(bean.getProjeto().getNumeroProjeto());
			bean.setGrandeGerador(proj.getGrandeGerador());
			bean.setUfObra(proj.getUfObra());
			bean.setMunicipioObra(proj.getMunicipioObra());
		}
		if(bean.getId()!=null){//update - campo n�o � alterado
			bean.setEnumSituacao(EnumSituacao.getEnumSituacaoByValor(load.getSituacao()));
			if(load.isStatus())
				bean.setEnumStatus(EnumStatus.CONFIRMADA);
			else
				bean.setEnumStatus(EnumStatus.PENDENTE);
		}else{//insert
			bean.setEnumSituacao(EnumSituacao.SEMSITUACAO);
			bean.setEnumStatus(EnumStatus.PENDENTE);
		}
		
		//define campo transiente
		bean.setSituacao(bean.getEnumSituacao().getValor());
		bean.setStatus(bean.getEnumStatus().isOptBool());
		
		//salva registro
		super.salvar(request, bean);
		
		
		
		if(bean.getDestinoFinal()!=null){
			/*
			 * ap�s o cadastro da guia calcula-se a nova capacidade no destino final, caso seja maior que 90 % exibe alerta na tela
			 * n�o realiza update no destino final, est� opera��o s� ser� realizada ap�s o recebimento da guia;
			 */
			double valorPossivelEntrada = 0;
			if(bean.getResiduosGuia()!=null)
				for (ResiduoGuia rg : bean.getResiduosGuia()) 
					valorPossivelEntrada += rg.getQuantidade();
	
			DestinoFinal destinoFinal = destinoFinalService.load(bean.getDestinoFinal());
			if(destinoFinalService.isCapacidadeMaiorIgualByPorcentagem(90,destinoFinal.getCapacidadeAtual() + valorPossivelEntrada, destinoFinal.getCapacidadeTotal()))
					request.addMessage("Aten��o! Caso esta guia seja recebida, o destino final "+destinoFinal.getRazaoSocial()+" atingir� capacidade superior � 90%.");
		}

	}
	
	@Override
	protected void entrada(WebRequestContext request, GuiaTransportador form)
			throws Exception {
		
		//verifica se usu�rio tem papel de Transportador
		if(!SCRUtil.isPessoaLogadaTransportador())
			throw new SCRException("A tela de cadastro de guias s� � exibida para usu�rios do tipo Transportador.");

		// define que o Transportador da guia � o usu�rio logado, o receptor
		Usuario usuario = SCRUtil.getUsuarioLogado();
		if(usuario.getTransportador() == null)
			throw new SCRException("N�o existe v�nculo do usu�rio logado com nenhum Transportador.");
		form.setTransportador(usuario.getTransportador());
		
		if(form.getId()==null){
			form.setEnumSituacao(EnumSituacao.SEMSITUACAO);
			form.setEnumStatus(EnumStatus.PENDENTE);
		}
		
		super.entrada(request, form);
	}	
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,
			FiltroListagem filtro) throws CrudException {
		// TODO Auto-generated method stub
		Usuario usuario = SCRUtil.getUsuarioLogado();
		if(usuario.getTransportador() == null)
			throw new SCRException("N�o existe v�nculo do usu�rio logado com nenhum Transportador.");
		//define filtro pad�o
		GuiaFiltro _filtro = (GuiaFiltro) filtro;
		_filtro.setTransportador(usuario.getTransportador());
		
		//adiciona filtro padr�o cancelada false
		if(_filtro.getEnumCancelada()==null){
			_filtro.setEnumCancelada(EnumCancelada.NAO);
		}
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		if("salvar".equals(acao)){
            GuiaTransportador bean = (GuiaTransportador) obj;
			if(bean.getDataEntrega() != null){//campo n�o obrigat�rio
				if(bean.getDataEntrega().before(bean.getDataEnvio()))
					errors.reject("","A data de recebimento deve ser superior ou igual a data de envio.");
				if(bean.getDataRetirada() != null)
					if(bean.getDataRetirada().before(bean.getDataEntrega()))
						errors.reject("","A data de retirada deve ser superior ou igual a data de recebimento.");
			}
		}
	}
	
}
