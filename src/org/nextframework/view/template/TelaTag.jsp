<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<n:form name="${Ttela.formName}" validate="${Ttela.validateForm}" method="${Ttela.formMethod}" action="${Ttela.formAction}" validateFunction="validarFormulario" bypass="${!Ttela.includeForm}">

	<n:validation functionName="validateForm" bypass="${!Ttela.includeForm || !Ttela.validateForm}">
		<script language="javascript">
			// caso seja alterada a fun��o validation ela ser� chamada ap�s a validacao do formulario
			var validation;
			function validarFormulario(){
				<c:if test="${!Ttela.validateForm}">
					return true;
				</c:if>
				var valido = validateForm();
				if(validation){
					valido = validation(valido);
				}
				return valido;
			}
		</script>
		
		<div class="pageTitle">
			${Ttela.titulo}
		</div>

		<div class="menuLeft">
			
		</div>
		<div class="pageBody">
			<n:bean name="${Ttela.useBean}" valueType="${Ttela.beanType}" bypass="${empty Ttela.useBean}">
				<t:propertyConfig mode="${Ttela.propertyMode}" bypass="${empty Ttela.useBean || empty Ttela.propertyMode}">
					<n:doBody />
				</t:propertyConfig>
			</n:bean>
		</div>

	</n:validation>
</n:form>
