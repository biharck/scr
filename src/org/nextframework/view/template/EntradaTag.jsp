<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="tldscr" uri="scr"%>

<c:set var="userAlt"   value="${TEMPLATE_beanName}.userAlt"/>
<c:set var="timeAlt" value="${TEMPLATE_beanName}.timeAlt"/> 

<t:tela titulo="SCR <img src='/SCR/img/style/next.png'> <span class='titulo-pagina-corrente'>${entradaTag.titulo}</span>">
		<c:if test="${consultar}">
			<input type="hidden" name="forcarConsulta" value="true"/>
		</c:if>
		<c:if test="${param.fromInsertOne == 'true'}">
			<input type="hidden" name="fromInsertOne" value="true"/>
		</c:if>

		<c:if test="${entradaTag.showListagemLink || !empty entradaTag.linkArea}">
			<c:if test="${empty showLinkBar}">
				<div class="linkBar">
						${entradaTag.invokeLinkArea}			
						<c:if test="${entradaTag.showListagemLink}">
							<c:if test="${!consultar}">
								<n:link action="listagem" class="outterTableHeaderLink" onclick="return dialogVoltar();" >Listagem</n:link>
							</c:if>
							<c:if test="${consultar}">
								<n:link action="listagem" onclick="dialogAguarde();" class="outterTableHeaderLink">Listagem</n:link>
							</c:if>
						</c:if>				
				</div>
			</c:if>
		</c:if>	

		<div>
			<n:bean name="${TEMPLATE_beanName}">
				<c:if test="${consultar}">
					<c:set var="modeConsultar" value="output" scope="request"/>
					<t:property name="${n:idProperty(n:reevaluate(TEMPLATE_beanName,pageContext))}" mode="input" write="false"/>
				</c:if>
				<t:propertyConfig mode="${n:default('input', modeConsultar)}">
					<n:doBody />
				</t:propertyConfig>
			</n:bean>
		</div>
		<c:if test="${param.ACAO != 'criar'}">
			<c:if test="${empty notBeanAuditoria}">
				<font style="color:#999">
					<n:panel>�ltima altera��o neste registro foi feita por <font style="color:#E77272">${n:reevaluate(userAlt,pageContext)}</font> no dia <font style="color:#E77272">${tldscr:dateformat(n:reevaluate(timeAlt,pageContext))}</font> �s <font style="color:#E77272">${tldscr:hourformat(n:reevaluate(timeAlt,pageContext))}</font> </n:panel>
				</font>
			</c:if>
		</c:if>
</t:tela>


