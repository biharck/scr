<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">

<%
request.setAttribute("app", request.getContextPath());
%>

<script language="JavaScript" src="${app}/resource/js/ajax.js"></script>
<script language="JavaScript" src="${app}/resource/js/input.js"></script>
<script language="JavaScript" src="${app}/resource/js/validate.js"></script>

<c:if test="${tag.includeUtilJs}">
<script language="JavaScript" src="${app}/resource/js/util.js"></script>
</c:if>
<script language="JavaScript" src="${app}/resource/js/dynatable.js"></script>
<script language="JavaScript" src="${app}/resource/js/JSCookMenu.js"></script>

<%-- DEFAULT CSS --%>
<c:if test="${tag.includeDefaultCss}">
<link rel="StyleSheet"        href="${app}/resource/css/default.css" type="text/css">	
</c:if>

<%-- CSS JS DA APLICA��O --%>
<c:if test="${searchCssDir == true}">
	<c:forEach items="${csss}" var="css">
	<link rel="StyleSheet"        href="${app}${css}" type="text/css">	
	</c:forEach>
	<c:forEach items="${csssModule}" var="css">
	<link rel="StyleSheet"        href="${app}${css}" type="text/css">	
	</c:forEach>
</c:if>

 <!--[if IE]>
   <link rel="StyleSheet"        href="${app}/cssIE/default-IE.css" type="text/css">  
 <![endif]-->

<c:if test="${searchJsDir == true}">
	<c:forEach items="${jss}" var="js">
	<script language="JavaScript" src="${app}${js}"></script>	
	</c:forEach>
</c:if>

<%-- INICIALIZA��O DO MENU --%>
<script language="JavaScript">
	//menu
	var cmThemeOfficeBase = '${app}/resource/menu/';
</script>

<%-- MENU --%>
<c:if test="${tag.includeThemeCss}">
<link rel="StyleSheet"        href="${app}/resource/menu/theme.css" type="text/css">
</c:if>
<script language="JavaScript" src="${app}/resource/menu/theme.js"></script>



<%-- INICIALIZA��O DO HTMLAREA --%>
<script language="JavaScript">
	//htmlarea
	try {
		preparaHtmlArea('${app}/resource/htmlarea/');	
	} catch(e){}// se n�o conseguiu achar o javascript n�o dar exce��o
</script>